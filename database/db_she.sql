-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Jun 2019 pada 12.23
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_she`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `accident_reports`
--

CREATE TABLE `accident_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `date_acd` date NOT NULL,
  `time_acd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stat_vict` int(11) NOT NULL,
  `state_vict_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `badge_vict` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_vict` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uk_vict` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uk_vict_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ven_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ven_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_in` date NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `no_jamsos` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_bpjs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shift` int(11) NOT NULL,
  `shift_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_task` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spv_badge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spv_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spv_position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spv_position_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acd_cause` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_badge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_pos_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inj_note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inj_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_aid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `next_aid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `safety_req` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acd_rpt_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unsafe_act` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unsafe_cond` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inv_badge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inv_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_report` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pict_before` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pict_after` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_up` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `accident_reports`
--

INSERT INTO `accident_reports` (`id`, `user_id`, `date_acd`, `time_acd`, `stat_vict`, `state_vict_text`, `badge_vict`, `name_vict`, `uk_vict`, `uk_vict_text`, `company_code`, `company_text`, `ven_code`, `ven_text`, `date_in`, `position`, `position_text`, `age`, `no_jamsos`, `no_bpjs`, `shift`, `shift_text`, `region`, `work_task`, `spv_badge`, `spv_name`, `spv_position`, `spv_position_text`, `location`, `location_text`, `acd_cause`, `s_badge`, `s_name`, `s_position`, `s_pos_text`, `inj_note`, `inj_text`, `first_aid`, `next_aid`, `safety_req`, `acd_rpt_text`, `unsafe_act`, `unsafe_cond`, `note`, `inv_badge`, `inv_name`, `status_report`, `pict_before`, `pict_after`, `follow_up`, `created_at`, `updated_at`) VALUES
(6, 435, '2019-03-28', '13:30:00', 0, 'Non-Karyawan', '2180801', 'Rizki Alfi Ramdhani', '108923', 'Manage Service Development', '5000', 'PT. Semen Indonesia', '123', 'satuduatiga', '2016-01-19', '832932', 'Programmer', 22, '111', '238023923290312', 1, 'Shift 1', 'Jl. Mawar Melati Gg. Semuanya Indah No.1', 'Mengerjakan project SHE Mobile', '00003396', 'INDRA NOFIANDI', '832932', 'Manager', '123', 'Ruang kerja ICT Gedung Semen Indonesia Lt 2 Jl. Veteran', 'Overload', '180008', 'Adhiq Rahmaduha', '832932', 'Programmer juga', 'Pingsan setelah beberapa lama terdiam didepan komputer', 'muncul bisul dan jerawat', 'Nafas buatan', 'Dibawa ke ruang UKS', 'Bekerja tanpa tekanan', 'Bekerja melebihi kapasitas Laptopnya', 'Memaksakan diri', 'Laptop / tools yang tidak memenuhi requirement', 'Optimalkan pembagian porsi kerja', '180008', 'Adhiq Rahmaduha', 'OPEN', 'cefe58d61564d45a3017367cfe6db630.png', NULL, 'sudah siuman', '2019-04-13 00:59:40', '2019-05-15 20:23:01'),
(7, 435, '2019-03-25', '13:30:00', 0, 'Non-Karyawan', '2180801', 'Rizki Alfi Ramdhani', '108923', 'Manage Service Development', '5000', 'PT. Semen Indonesia', '123', 'satuduatiga', '2016-01-19', '832932', 'Programmer', 22, '111', '238023923290312', 1, 'Shift 1', 'Jl. Mawar Melati Gg. Semuanya Indah No.1', 'Mengerjakan project SHE Mobile', '00003396', 'INDRA NOFIANDI', '832932', 'Manager', '123', 'Ruang kerja ICT Gedung Semen Indonesia Lt 2 Jl. Veteran', 'Overload', '180008', 'Adhiq Rahmaduha', '832932', 'Programmer juga', 'Pingsan setelah beberapa lama terdiam didepan komputer', 'muncul bisul dan jerawat', 'Nafas buatan', 'Dibawa ke ruang UKS', 'Bekerja tanpa tekanan', 'Bekerja melebihi kapasitas Laptopnya', 'Memaksakan diri', 'Laptop / tools yang tidak memenuhi requirement', 'Optimalkan pembagian porsi kerja', '180008', 'Adhiq Rahmaduha', 'OPEN', '213189_d1a180d6-43bb-4ac2-a78c-e14af7a885f0.jpg', NULL, 'sudah siuman', '2019-04-13 01:02:27', '2019-04-13 01:02:27'),
(8, 435, '2019-04-21', '13:30:00', 0, 'Non-Karyawan', '2180801', 'Muhammad', '108923', 'Manage Service Development', '5000', 'PT. Semen Indonesia', '123', 'satuduatiga', '2016-01-19', '832932', 'Programmer', 22, '111', '238023923290312', 1, 'Shift 1', 'Jl. Mawar Melati Gg. Semuanya Indah No.1', 'Mengerjakan project SHE Mobile', '00003396', 'INDRA NOFIANDI', '832932', 'Manager', '123', 'Ruang kerja ICT Gedung Semen Indonesia Lt 2 Jl. Veteran', 'Overload', '180008', 'Adhiq Rahmaduha', '832932', 'Programmer juga', 'Pingsan setelah beberapa lama terdiam didepan komputer', 'muncul bisul dan jerawat', 'Nafas buatan', 'Dibawa ke ruang UKS', 'Bekerja tanpa tekanan', 'Bekerja melebihi kapasitas Laptopnya', 'Memaksakan diri', 'Laptop / tools yang tidak memenuhi requirement', 'Optimalkan pembagian porsi kerja', '180008', 'Adhiq Rahmaduha', 'CLOSE', 'Logo_PENS.png', NULL, 'sudah siuman', '2019-04-21 03:06:52', '2019-04-21 03:06:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apd_master_category`
--

CREATE TABLE `apd_master_category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `desccription` varchar(100) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `create_by` varchar(20) DEFAULT NULL,
  `delete_at` datetime DEFAULT NULL,
  `delete_by` varchar(20) DEFAULT NULL,
  `icon_file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `apd_master_category`
--

INSERT INTO `apd_master_category` (`id`, `name`, `desccription`, `create_at`, `create_by`, `delete_at`, `delete_by`, `icon_file`) VALUES
(100, 'PELINDUNG KEPALA / HEAD SAFETY', 'PELINDUNG KEPALA / HEAD SAFETY', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(200, 'PELINDUNG MATA / EYE SAFETY', 'PELINDUNG MATA / EYE SAFETY', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(300, 'PELINDUNG TELINGA / PEREDAM SUARA BISING', 'PELINDUNG TELINGA / PEREDAM SUARA BISING', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(400, 'PELINDUNG HIDUNG', 'PELINDUNG HIDUNG', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(500, 'PELINDUNG TANGAN', 'PELINDUNG TANGAN', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(600, 'PELINDUNG BADAN / BODY SAFETY', 'PELINDUNG BADAN / BODY SAFETY', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(700, 'PELINDUNG KAKI / SAFETY SHOES', 'PELINDUNG KAKI / SAFETY SHOES', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(800, 'FULL BODY HARNESS', 'FULL BODY HARNESS', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(900, 'PAKET OBAT PPPK', 'PAKET OBAT PPPK', '2017-07-20 09:40:23', 'Migrasi Data', NULL, NULL, NULL),
(910, 'LAIN LAIN', 'LAIN LAIN', '2018-11-09 13:56:36', 'Administrator', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `daily_reports`
--

CREATE TABLE `daily_reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `shift` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_dokumen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'OPEN',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '5000',
  `plant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '5002',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `daily_reports`
--

INSERT INTO `daily_reports` (`id`, `shift`, `no_dokumen`, `status`, `user_id`, `tanggal`, `company`, `plant`, `created_at`, `updated_at`) VALUES
(5, '1', 'R/538/001/S1/5', 'OPEN', 435, '2019-05-06', '5000', '5002', '2019-05-05 17:00:00', '2019-05-05 17:00:00'),
(7, '1', 'R/538/001/S1/7', 'OPEN', 435, '2019-05-06', '5000', '5002', '2019-05-05 17:00:00', '2019-05-05 17:00:00'),
(22, '1', 'R/538/001/S1/22', 'CLOSE', 435, '2019-05-06', '5000', '5002', '2019-05-06 08:06:36', '2019-05-06 08:31:43'),
(24, '3', 'R/538/001/S3/24', 'OPEN', 435, '2019-05-06', '5000', '5002', '2019-05-06 08:25:30', '2019-05-06 08:25:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daily_report_k3_details`
--

CREATE TABLE `daily_report_k3_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_unsafe_report` bigint(20) UNSIGNED NOT NULL,
  `unit_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unsafe_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `potential_hazard` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_up` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `is_smig` int(11) NOT NULL DEFAULT '1',
  `o_clock_incident` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  `tindak_lanjut` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recomendation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trouble_maker` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `close_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_list` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comitment_date` date DEFAULT NULL,
  `verification_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `daily_report_k3_details`
--

INSERT INTO `daily_report_k3_details` (`id`, `id_unsafe_report`, `unit_code`, `unit_name`, `activity_description`, `unsafe_type`, `potential_hazard`, `follow_up`, `user_id`, `is_smig`, `o_clock_incident`, `id_category`, `deskripsi`, `kode_en`, `status`, `tindak_lanjut`, `recomendation`, `trouble_maker`, `close_date`, `close_by`, `mail_list`, `comitment_date`, `verification_date`, `created_at`, `updated_at`) VALUES
(11, 2, '2345', 'Unit Tuban 1', 'oli tercecer dimana mana', 'UA', 'terpeleset, terjungkal', 'ditutup daun daun', 435, 1, '13:00:03', 18, 'Sistem peringatan kurang/Indikator rusak', 'UC', 'CLOSE', 'adfsabsf', 'tes recomendation', 'belum tau penyebabnya', NULL, NULL, NULL, NULL, NULL, '2019-05-21 21:14:19', '2019-05-21 21:14:19'),
(13, 2, '2345', 'Unit Tuban 1', 'oli tercecer dimana mana', 'UA', 'terpeleset, terjungkal', 'ditutup daun daun', 435, 1, '13:00:03', 18, 'Sistem peringatan kurang/Indikator rusak', 'UC', 'CLOSE', 'adfsabsf', 'tes recomendation', 'belum tau penyebabnya', NULL, NULL, NULL, NULL, NULL, '2019-05-21 21:32:09', '2019-05-21 21:32:09'),
(14, 2, '2345', 'Unit Tuban 1', 'oli tercecer dimana mana', 'UA', 'terpeleset, terjungkal', 'ditutup daun daun', 435, 1, '13:00:03', 18, 'Sistem peringatan kurang/Indikator rusak', 'UC', 'OPEN', 'adfsabsf', 'tes recomendation', 'belum tau penyebabnya', NULL, NULL, NULL, NULL, NULL, '2019-05-21 21:32:49', '2019-05-21 21:32:49'),
(15, 3, '2345', 'Unit Tuban 1', 'oli tercecer dimana mana', 'UA', 'terpeleset, terjungkal', 'ditutup daun daun', 435, 1, '13:00:03', 18, 'Sistem peringatan kurang/Indikator rusak', 'UC', 'OPEN', 'adfsabsf', 'tes recomendation', 'belum tau penyebabnya', NULL, NULL, NULL, NULL, NULL, '2019-05-21 21:33:30', '2019-05-21 21:33:30'),
(17, 10, '4132', 'PT. SWADAYA GRAHA A.U LAMA', 'Tidak berotoritas', 'Unsafe Action', 'Bisa mencelakakan orang lain', 'Dipanggil ke kantor HRD', 435, 0, '10:34', 1, 'Beroperasi/Bekerja tanpa otoritas', 'UA', 'OPEN', 'Dipanggil ke kantor HRD', 'Selalu diberi pengawasan lebih', 'Masalahnya merekka tidak tau aturan', NULL, NULL, NULL, NULL, NULL, '2019-05-22 22:36:15', '2019-05-22 22:36:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daily_report_k3_s`
--

CREATE TABLE `daily_report_k3_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_date` date NOT NULL,
  `shift` int(11) NOT NULL,
  `area_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_txt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_area_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_area_txt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plant_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plant_txt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inspector_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inspector_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `is_smig` int(11) NOT NULL,
  `accident` int(11) DEFAULT NULL,
  `incident` int(11) DEFAULT NULL,
  `nearmiss` int(11) DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `close_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `daily_report_k3_s`
--

INSERT INTO `daily_report_k3_s` (`id`, `report_date`, `shift`, `area_code`, `area_txt`, `sub_area_code`, `sub_area_txt`, `plant_code`, `plant_txt`, `inspector_code`, `inspector_name`, `user_id`, `is_smig`, `accident`, `incident`, `nearmiss`, `close_date`, `close_by`, `created_at`, `updated_at`) VALUES
(2, '2019-05-21', 2, '1412', 'Area Test', '12523652y6', 'Sub Area dari Area Test', '7302', 'Plant Tuban 1', '124312531562363', 'User Tes', 435, 0, 2, 3, 1, NULL, NULL, '2019-05-21 01:45:17', '2019-05-21 01:45:17'),
(3, '2019-05-22', 3, '1412', 'CCOLER 2', 'Sub Area dari Area Test', 'Sub Area dari Area Test', '7302', '[7302] Plant Tuban I', 'null', 'User Tes', 435, 0, 2, 3, 1, NULL, NULL, '2019-05-21 21:33:12', '2019-05-22 00:29:38'),
(10, '2019-03-22', 2, 'null', 'CRUSHER TUBAN 1', 'Sub sub subs', 'Sub sub subs', 'null', '[7301] Plant Gresik', '00000552', 'MUHAMAD SULKAN, ST.', 435, 1, 0, 4, 1, NULL, NULL, '2019-05-22 00:31:55', '2019-05-22 00:31:55'),
(11, '2019-05-28', 1, NULL, 'CRUSHER TUBAN 2', NULL, 'Sub dari tuban 2', NULL, 'Plant Tuban II', NULL, 'SRI WILUJENG, Ir.', 436, 1, 2, 1, 0, NULL, NULL, '2019-05-27 22:56:02', '2019-05-27 22:56:02'),
(12, '2019-05-29', 2, 'null', 'CRUSHER TUBAN 4', 'Sub areas urban', 'Sub areas urban', 'null', '[7301] Plant Gresik', '00000516', 'DWI SUKESTI, SE.', 435, 1, 2, 1, 1, NULL, NULL, '2019-05-28 19:43:33', '2019-05-28 19:43:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daily_section_activity`
--

CREATE TABLE `daily_section_activity` (
  `id` int(10) NOT NULL,
  `type_action` varchar(30) NOT NULL,
  `date_activity` varchar(10) NOT NULL,
  `timesheet_start` varchar(10) NOT NULL,
  `timesheet_end` varchar(10) NOT NULL,
  `daily_activity` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `shift1` varchar(10) DEFAULT NULL,
  `shift2` varchar(10) DEFAULT NULL,
  `shift3` varchar(10) DEFAULT NULL,
  `pic` varchar(250) NOT NULL,
  `note` varchar(100) NOT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL,
  `status` varchar(20) NOT NULL,
  `list_tools` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daily_section_activity`
--

INSERT INTO `daily_section_activity` (`id`, `type_action`, `date_activity`, `timesheet_start`, `timesheet_end`, `daily_activity`, `image`, `shift1`, `shift2`, `shift3`, `pic`, `note`, `company`, `plant`, `status`, `list_tools`) VALUES
(14, 'SIAGA', '2019-05-27', '08:30:19', '10:50:24', 'Mancing', 'image', NULL, NULL, NULL, 'ZENcode', 'Keributan', 5000, 5001, 'OPEN', '[{\"tools\":\"Jaket\",\"total\":2}]'),
(15, 'SIAGA', '2019-05-28', '10:59:03', '10:59:06', 'fgg', 'dailyreport_', 'V', NULL, NULL, 'ZENcode', 'fgg', 5000, 5001, 'OPEN', '[{\"tools\":\"fdg\",\"total\":2},{\"tools\":\"fgg\",\"total\":2}]');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_order`
--

CREATE TABLE `detail_order` (
  `id` int(11) NOT NULL,
  `kode_apd` varchar(10) NOT NULL,
  `kode_order` varchar(10) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `files` varchar(200) DEFAULT NULL,
  `masa_exp` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(50) NOT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `code_comp` int(11) DEFAULT NULL,
  `code_plant` int(11) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `reject_at` timestamp NULL DEFAULT NULL,
  `reject_by` varchar(50) DEFAULT NULL,
  `note_reject` varchar(100) DEFAULT NULL,
  `release_at` timestamp NULL DEFAULT NULL,
  `nomor_order` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `nama_apd` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `file` varchar(200) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `no_badge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_order`
--

INSERT INTO `detail_order` (`id`, `kode_apd`, `kode_order`, `jumlah`, `files`, `masa_exp`, `created_at`, `updated_at`, `create_at`, `create_by`, `update_at`, `update_by`, `code_comp`, `code_plant`, `keterangan`, `reject_at`, `reject_by`, `note_reject`, `release_at`, `nomor_order`, `nama`, `unit`, `merk`, `nama_apd`, `status`, `file`, `foto`, `no_badge`) VALUES
(32, '100-010001', '86', 6, NULL, 365, '2019-05-15 17:00:00', '2019-05-22 00:45:17', '2019-05-16 09:02:56', 'riza', '2019-05-22 00:45:16', 'riza', 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, NULL, '', '', '', '', '', '', NULL, NULL, 0),
(33, '100-000007', '86', 7, NULL, 365, '2019-05-15 17:00:00', '2019-05-22 00:45:17', '2019-05-16 09:02:56', 'riza', '2019-05-22 00:45:16', 'riza', 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, NULL, '', '', '', '', '', '', NULL, NULL, 0),
(34, '100-010001', '89', 6, NULL, 365, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-16 10:27:04', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(35, '100-000007', '89', 7, NULL, 365, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-16 10:27:04', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(36, '100-010002', '93', 1, 'foto_bef', 365, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-16 10:38:56', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(37, '100-010002', '95', 1, 'foto_bef', 365, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-16 10:42:30', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(38, '100-000007', '95', 1, NULL, 365, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-16 10:42:30', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(39, '100-010002', '96', 1, 'foto_bef', 365, '2019-05-21 17:00:00', '2019-05-21 17:00:00', '2019-05-21 19:58:48', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(40, '100-000007', '96', 1, NULL, 365, '2019-05-21 17:00:00', '2019-05-21 17:00:00', '2019-05-21 19:58:48', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(41, '100-010001', '97', 6, NULL, 365, '2019-05-21 17:00:00', '2019-05-21 17:00:00', '2019-05-21 20:23:36', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(42, '100-000007', '97', 7, NULL, 365, '2019-05-21 17:00:00', '2019-05-21 17:00:00', '2019-05-21 20:23:36', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, '2019-05-22 15:20:42', '', '', '', '', '', '', NULL, NULL, 0),
(43, '100-010002', '100', 1, 'foto_bef', 365, '2019-05-21 17:00:00', '2019-05-22 21:32:02', '2019-05-22 09:31:32', 'riza', '2019-05-22 09:50:03', 'riza', 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 11:08:27', 'RID-100', 'Riza Diniatul Umami', 'Department of Strategic ICT', 'HC 71/MSA', 'Hat Band MSA', 'release', '15.04.818_jurnal_eproc.pdf', 'calendar.2.png', 0),
(44, '100-000007', '100', 1, NULL, 365, '2019-05-21 17:00:00', '2019-05-22 11:08:27', '2019-05-22 09:31:32', 'riza', '2019-05-22 09:50:03', 'riza', 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 11:08:27', 'RID-100', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'release', NULL, NULL, 0),
(45, '100-010002', '101', 1, 'foto_bef', 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:34:29', 'riza', '2019-05-23 05:34:29', NULL, 5000, 5002, 'Permintaan Personal', '2019-05-23 05:34:29', NULL, NULL, '2019-05-23 05:34:29', 'RID-101', 'Riza Diniatul Umami', 'Department of Strategic ICT', 'HC 71/MSA', 'Hat Band MSA', 'troli', NULL, NULL, 2103161043),
(46, '100-000007', '101', 1, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:34:29', 'riza', '2019-05-23 05:34:29', NULL, 5000, 5002, 'Permintaan Personal', '2019-05-23 05:34:29', NULL, NULL, '2019-05-23 05:34:29', 'RID-101', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'troli', NULL, NULL, 2103161043),
(47, '100-010002', '102', 1, 'foto_bef', 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:36:50', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, NULL, 'RID-102', 'Riza Diniatul Umami', 'Department of Strategic ICT', 'HC 71/MSA', 'Hat Band MSA', 'troli', NULL, NULL, 2103161043),
(48, '100-000007', '102', 1, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:36:50', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, NULL, 'RID-102', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'troli', NULL, NULL, 2103161043),
(49, '100-010001', '103', 6, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:40:29', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, NULL, 'RUK-103', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Safety hat Putih Putih', 'troli', NULL, NULL, 2103161043),
(50, '100-000007', '103', 7, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:40:29', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, NULL, 'RUK-103', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'troli', NULL, NULL, 2103161043),
(51, '100-010001', '104', 6, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:46:34', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, NULL, 'RUK-104', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Safety hat Putih Putih', 'troli', NULL, NULL, 2103161043),
(52, '100-000007', '104', 7, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 17:00:00', '2019-05-22 22:46:34', 'riza', NULL, NULL, 5000, 5002, 'Permintaan Unit Kerja', NULL, NULL, NULL, NULL, 'RUK-104', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'troli', NULL, NULL, 2103161043),
(53, '100-010002', '105', 1, 'foto_bef', 365, '2019-05-22 17:00:00', '2019-05-22 22:54:02', '2019-05-22 22:47:58', 'riza', '2019-05-22 22:54:01', 'riza', 5000, 5002, 'Permintaan Personal', '2019-05-22 22:54:01', 'riza', 'APD sudah habis', NULL, 'RID-105', 'Riza Diniatul Umami', 'Department of Strategic ICT', 'HC 71/MSA', 'Hat Band MSA', 'troli', NULL, NULL, 2103161043),
(54, '100-000007', '105', 1, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 22:54:02', '2019-05-22 22:47:58', 'riza', '2019-05-22 22:54:01', 'riza', 5000, 5002, 'Permintaan Personal', '2019-05-22 22:54:01', 'riza', 'APD sudah habis', NULL, 'RID-105', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'troli', NULL, NULL, 2103161043),
(55, '100-010002', '106', 1, 'foto_bef', 365, '2019-05-22 17:00:00', '2019-05-22 22:56:59', '2019-05-22 22:53:56', 'riza', '2019-05-22 22:56:21', 'riza', 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 22:56:59', 'RID-106', 'Riza Diniatul Umami', 'Department of Strategic ICT', 'HC 71/MSA', 'Hat Band MSA', 'release', NULL, NULL, 2103161043),
(56, '100-000007', '106', 1, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 22:56:59', '2019-05-22 22:53:56', 'riza', '2019-05-22 22:56:21', 'riza', 5000, 5002, 'Permintaan Personal', NULL, NULL, NULL, '2019-05-22 22:56:59', 'RID-106', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'release', NULL, NULL, 2103161043),
(57, '100-010001', '107', 6, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 22:58:07', '2019-05-22 22:54:32', 'riza', '2019-05-22 22:58:07', 'riza', 5000, 5002, 'Permintaan Unit Kerja', '2019-05-22 22:58:07', 'riza', 'APD sudah habis', NULL, 'RUK-107', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Safety hat Putih Putih', 'troli', NULL, NULL, 2103161043),
(58, '100-000007', '107', 7, NULL, 365, '2019-05-22 17:00:00', '2019-05-22 22:58:07', '2019-05-22 22:54:32', 'riza', '2019-05-22 22:58:07', 'riza', 5000, 5002, 'Permintaan Unit Kerja', '2019-05-22 22:58:07', 'riza', 'APD sudah habis', NULL, 'RUK-107', 'Riza Diniatul Umami', 'Department of Strategic ICT', '3M', 'Hat Band 3M', 'troli', NULL, NULL, 2103161043);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id` int(11) NOT NULL,
  `kode_apd` varchar(10) NOT NULL,
  `kode_pinjam` varchar(10) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(20) NOT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `update_by` varchar(20) DEFAULT NULL,
  `code_comp` int(11) NOT NULL,
  `code_plant` int(11) NOT NULL,
  `retur_at` timestamp NULL DEFAULT NULL,
  `reject_at` timestamp NULL DEFAULT NULL,
  `reject_by` varchar(20) DEFAULT NULL,
  `note_reject` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `release_at` timestamp NULL DEFAULT NULL,
  `nomor_pinjam` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `nama_apd` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `no_badge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id`, `kode_apd`, `kode_pinjam`, `jumlah`, `create_at`, `create_by`, `update_at`, `update_by`, `code_comp`, `code_plant`, `retur_at`, `reject_at`, `reject_by`, `note_reject`, `created_at`, `updated_at`, `release_at`, `nomor_pinjam`, `nama`, `unit`, `merk`, `nama_apd`, `status`, `no_badge`) VALUES
(1, '100-000006', '1', 1, '2019-03-31 17:00:00', 'INDRA.NOFIANDI', '2019-04-01 17:00:00', 'INDRA.NOFIANDI', 5000, 5002, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(2, '100-000006', '4', 3, '2019-05-07 17:00:00', 'INDRA.NOFIANDI', NULL, NULL, 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-07 10:17:37', '2019-05-07 10:17:37', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(3, '100-010002', '17', 2, '2019-05-16 17:44:43', 'riza', NULL, NULL, 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(4, '100-000007', '17', 3, '2019-05-16 17:44:43', 'riza', NULL, NULL, 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-15 17:00:00', '2019-05-15 17:00:00', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(5, '100-010002', '18', 2, '2019-05-22 02:04:23', 'riza', '2019-05-21 19:04:23', 'riza', 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 19:04:23', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(6, '100-000007', '18', 3, '2019-05-22 02:04:23', 'riza', '2019-05-21 19:04:23', 'riza', 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 19:04:23', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(7, '100-010002', '19', 2, '2019-05-22 02:32:31', 'riza', '2019-05-21 19:32:31', 'riza', 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 19:32:31', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(8, '100-000007', '19', 3, '2019-05-22 02:32:31', 'riza', '2019-05-21 19:32:31', 'riza', 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-21 17:00:00', '2019-05-21 19:32:31', '2019-05-23 05:19:29', '', '', '', '', '', '', 0),
(9, '100-010002', '22', 2, '2019-05-23 06:19:19', 'riza', '2019-05-22 23:19:19', 'riza', 5000, 5002, NULL, '2019-05-22 23:19:19', 'riza', 'APD sudah habis', '2019-05-22 17:00:00', '2019-05-22 23:19:19', '2019-05-23 06:10:45', 'PJM-22', 'Riza Diniatul Umami', '50045222', 'HC 71/MSA', 'Hat Band MSA', 'troli', 2103161043),
(10, '100-000007', '22', 3, '2019-05-23 06:19:19', 'riza', '2019-05-22 23:19:19', 'riza', 5000, 5002, NULL, '2019-05-22 23:19:19', 'riza', 'APD sudah habis', '2019-05-22 17:00:00', '2019-05-22 23:19:19', '2019-05-23 06:10:45', 'PJM-22', 'Riza Diniatul Umami', '50045222', '3M', 'Hat Band 3M', 'troli', 2103161043),
(11, '100-010002', '23', 2, '2019-05-23 07:03:10', 'riza', '2019-05-22 23:15:33', 'admin', 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-22 17:00:00', '2019-05-23 00:03:10', '2019-05-23 00:03:10', 'PJM-23', 'Riza Diniatul Umami', '50045222', 'HC 71/MSA', 'Hat Band MSA', 'release', 2103161043),
(12, '100-000007', '23', 3, '2019-05-23 07:03:10', 'riza', '2019-05-22 23:15:33', 'admin', 5000, 5002, NULL, NULL, NULL, NULL, '2019-05-22 17:00:00', '2019-05-23 00:03:10', '2019-05-23 00:03:10', 'PJM-23', 'Riza Diniatul Umami', '50045222', '3M', 'Hat Band 3M', 'release', 2103161043),
(13, '100-010002', '24', 2, '2019-05-27 04:55:40', 'riza', NULL, NULL, 5000, 5002, '2019-05-26 17:00:00', NULL, NULL, NULL, '2019-05-26 17:00:00', '2019-05-26 17:00:00', NULL, 'PJM-24', 'Riza Diniatul Umami', '50045222', 'HC 71/MSA', 'Hat Band MSA', 'order', 2103161043),
(14, '100-000007', '24', 3, '2019-05-27 04:55:40', 'riza', NULL, NULL, 5000, 5002, '2019-05-26 17:00:00', NULL, NULL, NULL, '2019-05-26 17:00:00', '2019-05-26 17:00:00', NULL, 'PJM-24', 'Riza Diniatul Umami', '50045222', '3M', 'Hat Band 3M', 'order', 2103161043);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fire_report`
--

CREATE TABLE `fire_report` (
  `id` int(10) NOT NULL,
  `fire_date` varchar(100) NOT NULL,
  `fire_time` varchar(100) NOT NULL,
  `job_walking_timestart` varchar(100) NOT NULL,
  `job_walking_timeend` varchar(100) NOT NULL,
  `pic_badge` varchar(10) NOT NULL,
  `pic_name` varchar(100) NOT NULL,
  `incident_place` varchar(100) NOT NULL,
  `note_place` varchar(250) DEFAULT NULL,
  `fire_happened_on` varchar(100) DEFAULT NULL,
  `damage_from_fire` varchar(250) NOT NULL,
  `fire_material_type` varchar(100) NOT NULL,
  `number_victims` int(10) NOT NULL,
  `description_of_fire` varchar(250) NOT NULL,
  `unsafe_condition` varchar(250) NOT NULL,
  `unsafe_action` varchar(250) DEFAULT NULL,
  `list_employees` varchar(100) DEFAULT NULL,
  `work_on_fire` varchar(250) NOT NULL,
  `fire_extinguishers` varchar(100) NOT NULL,
  `shift` int(10) NOT NULL,
  `list_tembusan` varchar(100) DEFAULT NULL,
  `saran` varchar(250) DEFAULT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL,
  `fire_number` varchar(100) NOT NULL,
  `status` varchar(30) NOT NULL,
  `followup_at` varchar(10) DEFAULT NULL,
  `followup_note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fire_report`
--

INSERT INTO `fire_report` (`id`, `fire_date`, `fire_time`, `job_walking_timestart`, `job_walking_timeend`, `pic_badge`, `pic_name`, `incident_place`, `note_place`, `fire_happened_on`, `damage_from_fire`, `fire_material_type`, `number_victims`, `description_of_fire`, `unsafe_condition`, `unsafe_action`, `list_employees`, `work_on_fire`, `fire_extinguishers`, `shift`, `list_tembusan`, `saran`, `company`, `plant`, `fire_number`, `status`, `followup_at`, `followup_note`) VALUES
(5, '2019-04-04', '09:45:10', '09:45:10', '10:45:10', '2103161040', 'Muhamat Zaenal Mahmut', 'Dalam Pabrik', 'Bagian Pengisian CO2', 'testing', 'rusak', 'tabung', 5, 'parah', 'testing', 'testing', '[\"Faiz\",\"Fairuz\",\"Faizul\",\"Faizin\",\"Fauzan\"]', 'testing', '[\"Faiz\",\"Fairuz\",\"Faizul\",\"Faizin\",\"Fauzan\"]', 1, '[\"Faiz\",\"Fairuz\",\"Faizul\",\"Faizin\",\"Fauzan\"]', 'testing', 5000, 5001, '50/03/04', 'OPEN', '2019-04-04', 'testing'),
(7, '2019-05-23', '05:05', '05:05', '05:05', '2103161040', 'pengawas', 'Dalam Pabrik', 'dsfdsf', NULL, 'sdfsf', 'sdfsfsf', 7, 'ff', 'sdfsf', 'sdsf', NULL, 'fdf', '[\"Apar\",\"Hydrant\",\"PMK\"]', 2, NULL, 'sdffs', 5000, 5001, '7/SH/05/23/FR/05/2019', 'CLOSE', NULL, NULL),
(8, '2019-05-28', '09:45:10', '09:45:10', '10:45:10', '2103161040', 'Muhamat Zaenal Mahmut', 'Dalam Pabrik', 'Bagian Pengisian CO2', 'testing', 'rusak', 'tabung', 5, 'parah', 'testing', 'testing', '[\"Faiz\",\"Fairuz\",\"Faizul\",\"Faizin\",\"Fauzan\"]', 'testing', '[\"Apar\",\"Palu\",\"Tangga\"]', 1, '[\"Faiz\"]', 'testing', 5000, 5001, '8/SH/05/28/FR/05/2019', 'OPEN', NULL, NULL),
(10, '2019-05-28', '09:12', '09:12', '09:13', '2103161040', 'SOESETYOKO S., SE.', 'Dalam Pabrik', 'Gh', 'Yy', 'Gu', 'Hh', 67, 'Gh', 'Bb', 'Bh', NULL, 'Yu', '[\"Apar\",\"Hydrant\",\"PMK\"]', 3, NULL, 'Gh', 5000, 5001, '10/SH/05/28/FR/05/2019', 'CLOSE', NULL, NULL),
(11, '2019-06-06', '08:08', '08:08', '09:09', '2103161040', 'fg', 'Dalam Pabrik', 'fgfd', NULL, 'dfgfg', 'fdgfdg', 67, 'fg', 'fdgdg', 'fdg', NULL, 'ggfhg', '[\"Apar\",\"Hydrant\",\"PMK\"]', 2, NULL, 'fdgdg', 5000, 5001, '11/SH/06/06/FR/06/2019', 'OPEN', NULL, NULL),
(12, '2019-06-06', '06:06', '07:06', '08:08', '-', 'Administrator', 'Luar Pabrik', 'fgdg', NULL, 'fgdg', 'fdgd', 56, 'fgg', 'fgdg', 'gfdg', NULL, 'gdg', '[\"Apar\",\"Hydrant\",\"PMK\"]', 3, NULL, 'fgfdg', 5000, 5001, '12/SH/06/06/FR/06/2019', 'OPEN', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fire_report_image`
--

CREATE TABLE `fire_report_image` (
  `id` int(10) NOT NULL,
  `id_fire_report` int(10) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fire_report_image`
--

INSERT INTO `fire_report_image` (`id`, `id_fire_report`, `image`, `description`, `status`) VALUES
(1, 3, '500_F_196493442_3o25PCPWJMNCHolzQDPoM4nGW3mRqLlk.jpg', 'G', 'UPLOAD'),
(2, 3, 'maxresdefault.jpg', 'dsfdsfdf', 'FOLLOWUP'),
(4, 8, '20190524011952_89e5dee27f1728e1d52527b702628e80.jpg', 'gfdgf', 'UPLOAD'),
(5, 3, 'FireReport_3UPLOADKebakaran 2', 'Kebakaran 2', 'UPLOAD'),
(6, 3, 'FireReport_3FOLLOWUPundefined', 'undefined', 'FOLLOWUP'),
(11, 5, 'FireReport_5FOLLOWUPyui', 'yui', 'FOLLOWUP'),
(12, 10, 'FireReport_10UPLOADYui', 'Yui', 'UPLOAD'),
(13, 10, 'FireReport_10FOLLOWUPBhh', 'Bhh', 'FOLLOWUP'),
(14, 12, '20190606041934_CV_Rizki Alfi Ramdhani.pdf', 'fgfdg', 'UPLOAD'),
(15, 12, '20190606042138_Software Testing.png', 'dfdfd', 'UPLOAD');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `kode_apd` varchar(10) NOT NULL,
  `kode_order_apd` varchar(10) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `release_at` date DEFAULT NULL,
  `release_by` varchar(50) DEFAULT NULL,
  `jumlah_release` int(10) DEFAULT NULL,
  `reject_at` date DEFAULT NULL,
  `reject_by` varchar(20) DEFAULT NULL,
  `jumlah_reject` int(10) DEFAULT NULL,
  `note_reject` varchar(200) DEFAULT NULL,
  `exp_days` int(20) NOT NULL,
  `expired_date` date NOT NULL,
  `expired_days_left` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history`
--

INSERT INTO `history` (`id`, `kode_apd`, `kode_order_apd`, `keterangan`, `release_at`, `release_by`, `jumlah_release`, `reject_at`, `reject_by`, `jumlah_reject`, `note_reject`, `exp_days`, `expired_date`, `expired_days_left`) VALUES
(1, '100-010002', 'RID-22269', 'Permintaan Personal / Individu', '2019-04-08', 'ARIES.MUCHLIS', 1, NULL, NULL, NULL, NULL, 1095, '2019-04-27', '19Days Left');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_alarm`
--

CREATE TABLE `inspection_alarm` (
  `id` int(10) NOT NULL,
  `id_alarm` int(10) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `periode` varchar(100) NOT NULL,
  `inspection_date` varchar(100) NOT NULL,
  `week` varchar(100) DEFAULT NULL,
  `type_periode` varchar(100) NOT NULL,
  `detail` varchar(5000) NOT NULL,
  `opt_file` varchar(500) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pic_badge` varchar(100) NOT NULL,
  `pic_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_alarm`
--

INSERT INTO `inspection_alarm` (`id`, `id_alarm`, `location_name`, `periode`, `inspection_date`, `week`, `type_periode`, `detail`, `opt_file`, `date`, `pic_badge`, `pic_name`) VALUES
(2, 1, 'Gedung Utama', '2019-05', '2019-05-10', NULL, 'WEEK', '[{\"id\":1,\"parameter\":\"Membunyikan alarm secara simulasi\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"},{\"id\":2,\"parameter\":\"Memeriksa kerja lonceng\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"},{\"id\":3,\"parameter\":\"Memeriksa tegangan dan keadaan baterai\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"},{\"id\":4,\"parameter\":\"Memeriksa seluruh sistem alarm\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"}]', NULL, '2019-05-15 03:03:17', '2103161040', 'Muhamat Zaenal Mahmut'),
(5, 1, 'Gedung Utama', '2019-05', '2019-05-15', NULL, 'WEEK', '[{\"id\":1,\"parameter\":\"Membunyikan alarm secara simulasi\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"},{\"id\":2,\"parameter\":\"Memeriksa kerja lonceng\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"},{\"id\":3,\"parameter\":\"Memeriksa tegangan dan keadaan baterai\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"},{\"id\":4,\"parameter\":\"Memeriksa seluruh sistem alarm\",\"type\":\"WEEK\",\"status_cond\":\"V\",\"note\":\"Aman\"}]', NULL, '2019-05-15 03:03:29', '2103161040', 'Muhamat Zaenal Mahmut');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_apar`
--

CREATE TABLE `inspection_apar` (
  `id` int(10) NOT NULL,
  `id_apar` int(10) NOT NULL,
  `klem` varchar(100) NOT NULL,
  `press` varchar(100) NOT NULL,
  `hose` varchar(100) NOT NULL,
  `seal` varchar(100) NOT NULL,
  `sapot` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `status_cond` varchar(100) NOT NULL,
  `id_apar_ganti` int(10) DEFAULT NULL,
  `inspection_date` varchar(100) NOT NULL,
  `active_start_date` varchar(100) DEFAULT NULL,
  `active_until_date` varchar(100) DEFAULT NULL,
  `note` varchar(100) NOT NULL,
  `pic_badge` varchar(100) NOT NULL,
  `pic_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_apar`
--

INSERT INTO `inspection_apar` (`id`, `id_apar`, `klem`, `press`, `hose`, `seal`, `sapot`, `weight`, `status_cond`, `id_apar_ganti`, `inspection_date`, `active_start_date`, `active_until_date`, `note`, `pic_badge`, `pic_name`) VALUES
(2, 2, 'U', '4', 'V', 'V', 'V', '4', 'BAIK', NULL, '2019-05-13', NULL, NULL, 'Baik', '2103161040', 'Muhamat Zaenal Mahmut'),
(5, 2, 'U', '1', 'V', 'V', 'X', '1', 'HABIS', NULL, '2019-05-29', NULL, NULL, 'fggdggf', '2103161040', 'Muhamat Zaenal Mahmut'),
(6, 3, 'B', '1', 'V', 'X', 'V', '1', 'GANTI', NULL, '2019-05-29', NULL, NULL, 'DFFDF', '2103161040', 'Muhamat Zaenal Mahmut'),
(8, 4, 'K', '1', 'X', 'X', 'X', '1', 'BAIK', NULL, '2019-06-06', NULL, NULL, 'Buruk', '2103161040', 'Muhamat Zaenal Mahmut'),
(10, 1, 'K', '0', 'X', 'X', 'X', '0', 'BAIK', NULL, '2019-06-06', NULL, NULL, 'sad', '-', 'Administrator'),
(11, 5, 'T', '4', 'V', 'V', 'V', '4', 'BAIK', NULL, '2019-06-06', NULL, NULL, 'Baik Baik', '-', 'Administrator'),
(12, 4, 'K', '0', 'X', 'X', 'X', '0', '-', NULL, '2019-06-06', NULL, NULL, 'ggg', '-', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_area`
--

CREATE TABLE `inspection_area` (
  `id` int(10) NOT NULL,
  `area_name` varchar(50) NOT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_area`
--

INSERT INTO `inspection_area` (`id`, `area_name`, `company`, `plant`) VALUES
(1, 'Crusher Tuban 1', 5000, 5001),
(2, 'Crusher Tuban 2', 5000, 5001),
(3, 'Crusher Tuban 3', 5000, 5001),
(4, 'Crusher Tuban 4', 5000, 5001),
(5, 'OP. Crusher', 5000, 5001),
(6, 'PMCR', 5000, 5001),
(7, 'PLI Crusher', 5000, 5001),
(8, 'Bengkel Listrik', 5000, 5001),
(9, 'Bengkel Mesin', 5000, 5001),
(10, 'CCOLER 2', 5000, 5001),
(11, 'Dormitori', 5000, 5001),
(12, 'Main Office', 5000, 5001),
(13, 'CCR Tuban 1 & 2', 5000, 5001),
(14, 'CCR Tuban 3 & 4', 5000, 5001),
(15, 'KPSG', 5000, 5001),
(16, 'Gedung Adutarium', 5000, 5001),
(17, 'Gedung Diklat', 5000, 5001),
(18, 'House Pump KPSG', 5000, 5001),
(19, 'Gedung Terak', 5000, 5001),
(20, 'Seksi Sekretariat', 5000, 5001),
(21, 'Raw Mill', 5000, 5001),
(22, 'Coal Mill', 5000, 5001),
(23, 'Preheater', 5000, 5001),
(24, 'Kiln Cooler', 5000, 5001),
(25, 'Finish Mill', 5000, 5001),
(26, 'Packer', 5000, 5001),
(27, 'Pelabuhan', 5000, 5001),
(28, 'AAB', 5000, 5001),
(29, 'Mainsub', 5000, 5001),
(30, 'CCT', 5000, 5001),
(31, 'Perkantoran', 5000, 5001),
(32, 'Gedung Utama', 5000, 5001),
(33, 'Lantai 1', 5000, 5001),
(34, 'Lantai 2', 5000, 5001),
(35, 'Lantai 3', 5000, 5001),
(36, 'Lantai 4', 5000, 5001),
(37, 'Lantai 5', 5000, 5001);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_gas`
--

CREATE TABLE `inspection_gas` (
  `id` int(10) NOT NULL,
  `plant` int(10) NOT NULL,
  `pplant` int(10) NOT NULL,
  `pplantdesc` varchar(100) NOT NULL,
  `total_max_cemetron` int(10) NOT NULL,
  `total_residual_cemetron` int(10) NOT NULL,
  `total_usage_cemetron` int(10) NOT NULL,
  `total_max_samator` int(10) NOT NULL,
  `total_residual_samator` int(10) NOT NULL,
  `total_usage_samator` int(10) NOT NULL,
  `inspection_date` varchar(100) NOT NULL,
  `inspection_time` varchar(100) NOT NULL,
  `pic_name` varchar(100) NOT NULL,
  `pic_badge` varchar(100) NOT NULL,
  `shift` varchar(100) NOT NULL,
  `note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_gas`
--

INSERT INTO `inspection_gas` (`id`, `plant`, `pplant`, `pplantdesc`, `total_max_cemetron`, `total_residual_cemetron`, `total_usage_cemetron`, `total_max_samator`, `total_residual_samator`, `total_usage_samator`, `inspection_date`, `inspection_time`, `pic_name`, `pic_badge`, `shift`, `note`) VALUES
(4, 5001, 7301, 'Planning Plant Tuban', 6, 6, 0, 19, 18, 1, '2019-06-04', '12-40-41', 'Zen', '2103161040', '1', NULL),
(5, 5001, 7301, 'Planning Plant Tuban', 6, 5, 1, 19, 15, 4, '2019-05-27', '20-46-58', 'Muhamat Zaenal Mahmut', '2103161040', '1', 'Testing'),
(6, 5001, 7301, 'Planning Plant Tuban', 6, 6, 0, 19, 2, 17, '2019-06-06', '01-49-44', 'Zen', '2103161040', '3', 'Tiga'),
(7, 5001, 7301, 'Planning Plant Tuban', 6, 4, 2, 19, 4, 15, '2019-05-28', '16-54-12', 'LUSIDA AFTIARTI, Dra.', '00000549', '2', 'juhh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_hydrant`
--

CREATE TABLE `inspection_hydrant` (
  `id` int(10) NOT NULL,
  `id_hydrant` int(10) NOT NULL,
  `inspection_date` varchar(100) NOT NULL,
  `pilar_body` varchar(100) NOT NULL,
  `pilar_note` varchar(100) DEFAULT NULL,
  `valve_kiri` varchar(100) NOT NULL,
  `valve_atas` varchar(100) NOT NULL,
  `valve_kanan` varchar(100) NOT NULL,
  `valve_note` varchar(100) DEFAULT NULL,
  `copling_kiri` varchar(100) NOT NULL,
  `copling_kanan` varchar(100) NOT NULL,
  `copling_note` varchar(100) DEFAULT NULL,
  `t_copling_kiri` varchar(100) NOT NULL,
  `t_copling_kanan` varchar(100) NOT NULL,
  `t_copling_note` varchar(100) DEFAULT NULL,
  `box_casing` varchar(100) NOT NULL,
  `box_hose` varchar(100) NOT NULL,
  `box_nozle` varchar(100) NOT NULL,
  `box_kunci` varchar(100) NOT NULL,
  `box_note` varchar(100) DEFAULT NULL,
  `press` varchar(100) NOT NULL,
  `temuan` varchar(100) DEFAULT NULL,
  `foto_temuan` varchar(100) DEFAULT NULL,
  `pic_badge` varchar(100) NOT NULL,
  `pic_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_hydrant`
--

INSERT INTO `inspection_hydrant` (`id`, `id_hydrant`, `inspection_date`, `pilar_body`, `pilar_note`, `valve_kiri`, `valve_atas`, `valve_kanan`, `valve_note`, `copling_kiri`, `copling_kanan`, `copling_note`, `t_copling_kiri`, `t_copling_kanan`, `t_copling_note`, `box_casing`, `box_hose`, `box_nozle`, `box_kunci`, `box_note`, `press`, `temuan`, `foto_temuan`, `pic_badge`, `pic_name`) VALUES
(7, 1, '2019-05-19', 'K', 'KOTOR', 'K', 'K', 'K', 'BAIK', 'K', 'K', 'KOTOR', 'K', 'K', 'KOTOR', 'K', 'K', 'K', 'K', 'KOTOR', '10', 'KOTOR', 'null', '2103161040', 'Muhamat Zaenal Mahmut'),
(10, 4, '2019-05-22', 'V', NULL, 'V', 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', 'V', 'V', NULL, '0', NULL, 'null', '2103161040', 'Muhamat Zaenal Mahmut'),
(11, 6, '2019-05-01', 'V', NULL, 'V', 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', 'V', 'V', NULL, '0', NULL, '20190523050105_1757cfd807b14ec.jpg', '2103161040', 'Muhamat Zaenal Mahmut'),
(13, 2, '2019-05-10', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', 'V', '2', 'V', 'null', '2103161040', 'Muhamat Zaenal Mahmut'),
(14, 1, '2019-05-26', 'V', 'null', 'V', 'V', 'V', 'Baik', 'V', 'V', 'Baik', 'V', 'V', 'Baik', 'K', 'K', 'K', 'K', 'Baik baik', '49', 'null', 'image', '2103161040', 'Muhamat'),
(15, 2, '2019-05-29', 'V', 'dsf', 'V', 'I', 'V', 'df', 'V', 'V', 'df', 'V', 'V', 'dff', 'V', 'V', 'V', 'V', 'dfd', '3', 'dsffd', 'image', '2103161040', 'Muhamat Zaenal Mahmut'),
(16, 6, '2019-06-06', 'V', NULL, 'V', 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', 'V', 'V', NULL, '0', NULL, '20190606142349_Screenshot_2019-06-05-01-23-54-71.png', '2103161040', 'Muhamat Zaenal Mahmut'),
(17, 6, '2019-06-06', 'V', NULL, 'V', 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', 'V', 'V', NULL, '0', NULL, '20190606142848_Capture.PNG', '-', 'Administrator'),
(18, 5, '2019-06-06', 'V', NULL, 'V', 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', NULL, 'V', 'V', 'V', 'V', NULL, '0', NULL, '20190606145245_Selamat.png', '-', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_pickup`
--

CREATE TABLE `inspection_pickup` (
  `id` int(10) NOT NULL,
  `vehicle_id` varchar(100) NOT NULL,
  `vehicle_code` varchar(100) NOT NULL,
  `report_date` varchar(100) NOT NULL,
  `report_time` varchar(100) NOT NULL,
  `shift` varchar(10) NOT NULL,
  `pemanasan` int(10) DEFAULT NULL,
  `spedometer` int(10) DEFAULT NULL,
  `level_bbm` varchar(100) DEFAULT NULL,
  `oli_mesin` varchar(100) DEFAULT NULL,
  `oli_rem` varchar(100) DEFAULT NULL,
  `oli_power` varchar(100) DEFAULT NULL,
  `air_radiator` varchar(100) DEFAULT NULL,
  `air_cad_radiator` varchar(100) DEFAULT NULL,
  `air_wiper` varchar(100) DEFAULT NULL,
  `lampu_cabin` varchar(100) DEFAULT NULL,
  `lampu_kota` varchar(100) DEFAULT NULL,
  `lampu_jauh` varchar(100) DEFAULT NULL,
  `lampu_sign_kiri` varchar(100) DEFAULT NULL,
  `lampu_sign_kanan` varchar(100) DEFAULT NULL,
  `lampu_rem` varchar(100) DEFAULT NULL,
  `lampu_atret` varchar(100) DEFAULT NULL,
  `lampu_sorot` varchar(100) DEFAULT NULL,
  `panel_dashboard` varchar(100) DEFAULT NULL,
  `ban_depan_kiri` varchar(100) DEFAULT NULL,
  `ban_depan_kanan` varchar(100) DEFAULT NULL,
  `ban_belakang_kiri_dalam` varchar(100) DEFAULT NULL,
  `ban_belakang_kiri_luar` varchar(100) DEFAULT NULL,
  `ban_belakang_kanan_dalam` varchar(100) DEFAULT NULL,
  `ban_belakang_kanan_luar` varchar(100) DEFAULT NULL,
  `kaca_spion_kiri` varchar(100) DEFAULT NULL,
  `kaca_spion_kanan` varchar(100) DEFAULT NULL,
  `pic_badge` varchar(100) DEFAULT NULL,
  `pic_name` varchar(100) DEFAULT NULL,
  `sum_negative` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_pickup`
--

INSERT INTO `inspection_pickup` (`id`, `vehicle_id`, `vehicle_code`, `report_date`, `report_time`, `shift`, `pemanasan`, `spedometer`, `level_bbm`, `oli_mesin`, `oli_rem`, `oli_power`, `air_radiator`, `air_cad_radiator`, `air_wiper`, `lampu_cabin`, `lampu_kota`, `lampu_jauh`, `lampu_sign_kiri`, `lampu_sign_kanan`, `lampu_rem`, `lampu_atret`, `lampu_sorot`, `panel_dashboard`, `ban_depan_kiri`, `ban_depan_kanan`, `ban_belakang_kiri_dalam`, `ban_belakang_kiri_luar`, `ban_belakang_kanan_dalam`, `ban_belakang_kanan_luar`, `kaca_spion_kiri`, `kaca_spion_kanan`, `pic_badge`, `pic_name`, `sum_negative`) VALUES
(4, '9', 'PCKP-001', '2019-05-27', '09:08:50', '1', 5, 3, 'R', 'H', 'H', 'H', 'F', 'F', 'F', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', '15', '15', '15', '15', '15', '15', 'BAIK', 'BAIK', '2103161040', 'Muhamat Zaenal Mahmut', 12),
(6, '9', 'PCKP-001', '2019-06-07', '06:06', '1', NULL, NULL, '1', 'L', 'L', 'L', '1', '1', '1', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', NULL, NULL, NULL, NULL, NULL, NULL, 'BAIK', 'BAIK', '-', 'Administrator', 6),
(7, '10', 'PCKP-002', '2019-06-07', '07:07', '1', 20, 20, '1', 'L', 'L', 'L', '1', '1', '1', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', '32', '32', '32', '32', '32', '32', 'BAIK', 'BAIK', '-', 'Administrator', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inspection_pmk`
--

CREATE TABLE `inspection_pmk` (
  `id` int(10) NOT NULL,
  `vehicle_id` varchar(100) NOT NULL,
  `vehicle_code` varchar(100) NOT NULL,
  `report_date` varchar(100) NOT NULL,
  `report_time` varchar(100) NOT NULL,
  `shift` varchar(10) NOT NULL,
  `pemanasan` int(10) DEFAULT NULL,
  `spedometer` int(10) DEFAULT NULL,
  `level_bbm` varchar(100) DEFAULT NULL,
  `working_pressure` int(10) DEFAULT NULL,
  `hour_meter_mesin` int(10) DEFAULT NULL,
  `hour_meter_pompa` int(10) DEFAULT NULL,
  `oli_mesin` varchar(100) DEFAULT NULL,
  `oli_rem` varchar(100) DEFAULT NULL,
  `oli_power` varchar(100) DEFAULT NULL,
  `air_radiator` varchar(100) DEFAULT NULL,
  `air_cad_radiator` varchar(100) DEFAULT NULL,
  `air_wiper` varchar(100) DEFAULT NULL,
  `lampu_cabin` varchar(100) DEFAULT NULL,
  `lampu_kota` varchar(100) DEFAULT NULL,
  `lampu_jauh` varchar(100) DEFAULT NULL,
  `lampu_sign_kiri` varchar(100) DEFAULT NULL,
  `lampu_sign_kanan` varchar(100) DEFAULT NULL,
  `lampu_rem` varchar(100) DEFAULT NULL,
  `lampu_atret` varchar(100) DEFAULT NULL,
  `lampu_sorot` varchar(100) DEFAULT NULL,
  `panel_dashboard` varchar(100) DEFAULT NULL,
  `ban_depan_kiri` varchar(100) DEFAULT NULL,
  `ban_depan_kanan` varchar(100) DEFAULT NULL,
  `ban_belakang_kiri_dalam` varchar(100) DEFAULT NULL,
  `ban_belakang_kiri_luar` varchar(100) DEFAULT NULL,
  `ban_belakang_kanan_dalam` varchar(100) DEFAULT NULL,
  `ban_belakang_kanan_luar` varchar(100) DEFAULT NULL,
  `kaca_spion_kiri` varchar(100) DEFAULT NULL,
  `kaca_spion_kanan` varchar(100) DEFAULT NULL,
  `dongkrak` varchar(100) DEFAULT NULL,
  `stang_kabin` varchar(100) DEFAULT NULL,
  `ganjal_ban` varchar(100) DEFAULT NULL,
  `kunci_roda` varchar(100) DEFAULT NULL,
  `hammer` varchar(100) DEFAULT NULL,
  `kotak_pppk` varchar(100) DEFAULT NULL,
  `hose_25` varchar(100) DEFAULT NULL,
  `hose_15` varchar(100) DEFAULT NULL,
  `nozzle_15_js` varchar(100) DEFAULT NULL,
  `nozzle_15_akron` varchar(100) DEFAULT NULL,
  `nozzle_15_jet` varchar(100) DEFAULT NULL,
  `y_valve` varchar(100) DEFAULT NULL,
  `red_25` varchar(100) DEFAULT NULL,
  `nozzle_foam` varchar(100) DEFAULT NULL,
  `kunci_hydrant` varchar(100) DEFAULT NULL,
  `hose_section` varchar(100) DEFAULT NULL,
  `kunci_hose` varchar(100) DEFAULT NULL,
  `apar` varchar(100) DEFAULT NULL,
  `kapak` varchar(100) DEFAULT NULL,
  `linggis` varchar(100) DEFAULT NULL,
  `tangga_ganda` varchar(100) DEFAULT NULL,
  `senter` varchar(100) DEFAULT NULL,
  `gantol` varchar(100) DEFAULT NULL,
  `timba` varchar(100) DEFAULT NULL,
  `karung` varchar(100) DEFAULT NULL,
  `jacket_pmk` varchar(100) DEFAULT NULL,
  `helm_pmk` varchar(100) DEFAULT NULL,
  `sirine` varchar(100) DEFAULT NULL,
  `acsesoris` varchar(100) DEFAULT NULL,
  `pic_badge` varchar(100) DEFAULT NULL,
  `pic_name` varchar(100) DEFAULT NULL,
  `sum_negative` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inspection_pmk`
--

INSERT INTO `inspection_pmk` (`id`, `vehicle_id`, `vehicle_code`, `report_date`, `report_time`, `shift`, `pemanasan`, `spedometer`, `level_bbm`, `working_pressure`, `hour_meter_mesin`, `hour_meter_pompa`, `oli_mesin`, `oli_rem`, `oli_power`, `air_radiator`, `air_cad_radiator`, `air_wiper`, `lampu_cabin`, `lampu_kota`, `lampu_jauh`, `lampu_sign_kiri`, `lampu_sign_kanan`, `lampu_rem`, `lampu_atret`, `lampu_sorot`, `panel_dashboard`, `ban_depan_kiri`, `ban_depan_kanan`, `ban_belakang_kiri_dalam`, `ban_belakang_kiri_luar`, `ban_belakang_kanan_dalam`, `ban_belakang_kanan_luar`, `kaca_spion_kiri`, `kaca_spion_kanan`, `dongkrak`, `stang_kabin`, `ganjal_ban`, `kunci_roda`, `hammer`, `kotak_pppk`, `hose_25`, `hose_15`, `nozzle_15_js`, `nozzle_15_akron`, `nozzle_15_jet`, `y_valve`, `red_25`, `nozzle_foam`, `kunci_hydrant`, `hose_section`, `kunci_hose`, `apar`, `kapak`, `linggis`, `tangga_ganda`, `senter`, `gantol`, `timba`, `karung`, `jacket_pmk`, `helm_pmk`, `sirine`, `acsesoris`, `pic_badge`, `pic_name`, `sum_negative`) VALUES
(6, '1', 'PMK-001', '2019-05-30', '09:08:50', '1', 5, 3, 'R', 5, 5, 5, 'H', 'H', 'H', 'R', 'R', 'R', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', '15', '15', '15', '15', '15', '15', 'BAIK', 'BAIK', 'TIDAK', 'TIDAK', 'TIDAK', 'TIDAK', 'TIDAK', 'TIDAK', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '2103161040', 'Muhamat Zaenal Mahmut', 22),
(7, '3', 'PMK-002', '2019-06-07', '08:08', '1', NULL, NULL, '1', NULL, NULL, NULL, 'L', 'L', 'L', '1', '1', '1', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', 'BAIK', NULL, NULL, NULL, NULL, NULL, NULL, 'BAIK', 'BAIK', 'ADA', 'ADA', 'ADA', 'ADA', 'ADA', 'ADA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BAIK', 'BAIK', '-', 'Administrator', 30);

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_file_uploads`
--

CREATE TABLE `list_file_uploads` (
  `id_upload_unsafe` bigint(20) UNSIGNED NOT NULL,
  `id_unsafe_report_detail` bigint(20) UNSIGNED NOT NULL,
  `type_file_upload` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploaded_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `inspection_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `list_file_uploads`
--

INSERT INTO `list_file_uploads` (`id_upload_unsafe`, `id_unsafe_report_detail`, `type_file_upload`, `uploaded_path`, `file_description`, `user_id`, `inspection_date`, `created_at`, `updated_at`) VALUES
(9, 11, '0', '27-Zulkarnaen-jetty5.jpg', NULL, 435, NULL, '2019-05-21 21:14:19', '2019-05-21 21:14:19'),
(11, 14, NULL, NULL, NULL, 435, NULL, '2019-05-21 21:32:49', '2019-05-21 21:32:49'),
(12, 15, NULL, NULL, NULL, 435, NULL, '2019-05-21 21:33:30', '2019-05-21 21:33:30'),
(14, 17, NULL, NULL, NULL, 435, NULL, '2019-05-22 22:36:15', '2019-05-22 22:36:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_gas`
--

CREATE TABLE `list_gas` (
  `id` int(10) NOT NULL,
  `cemetron` varchar(100) NOT NULL,
  `samator` varchar(100) NOT NULL,
  `plant` int(10) NOT NULL,
  `pplant` int(10) NOT NULL,
  `pplantdesc` varchar(100) NOT NULL,
  `total_max_cemetron` int(100) NOT NULL,
  `total_max_samator` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `list_gas`
--

INSERT INTO `list_gas` (`id`, `cemetron`, `samator`, `plant`, `pplant`, `pplantdesc`, `total_max_cemetron`, `total_max_samator`) VALUES
(1, 'Cemetron', 'Samator', 5001, 7301, 'Planning Plant Tuban', 6, 19),
(3, 'Cemetron', 'Samator', 5001, 7302, 'Plant Tuban I', 6, 19),
(5, 'Cemetron', 'Samator', 5001, 7303, 'Plant Tuban II', 6, 19),
(7, 'Cemetron', 'Samator', 5001, 7304, 'Plant Tuban III', 6, 19),
(9, 'Cemetron', 'Samator', 5001, 7305, 'Plant Tuban IV', 6, 19),
(13, 'Cemetron', 'Samator', 5001, 7306, 'Plant Tuban V', 6, 19),
(15, 'Cemetron', 'Samator', 5001, 7307, 'Plant Tuban VI', 6, 19),
(17, 'Cemetron', 'Samator', 5002, 7309, 'wakanda', 6, 19),
(18, 'Cemetron', 'Samator', 5002, 7310, 'CCR 34', 6, 19);

-- --------------------------------------------------------

--
-- Struktur dari tabel `list_vehicle`
--

CREATE TABLE `list_vehicle` (
  `id` int(10) NOT NULL,
  `type` varchar(100) NOT NULL,
  `vehicle_code` varchar(100) NOT NULL,
  `vehicle_merk` varchar(100) NOT NULL,
  `tahun` varchar(100) NOT NULL,
  `vehicle_type` varchar(100) NOT NULL,
  `nomor_rangka` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `plant` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `last_inspection` varchar(100) DEFAULT NULL,
  `exp_inspection` varchar(100) DEFAULT NULL,
  `sum_negative` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `list_vehicle`
--

INSERT INTO `list_vehicle` (`id`, `type`, `vehicle_code`, `vehicle_merk`, `tahun`, `vehicle_type`, `nomor_rangka`, `company`, `plant`, `image`, `last_inspection`, `exp_inspection`, `sum_negative`) VALUES
(1, 'PMK', 'PMK-001', 'Merk H', '2019', 'Type G', 'W 9574 SD', '5000', '5001', '5001', '2019-06-07', '2019-06-08', 22),
(3, 'PMK', 'PMK-002', 'Merk H', '2019', 'Type G', 'W 4555 SG', '5000', '5001', '20190524211221_500_F_206500217_Z4JwQt9gCJRIQHadJCJTiHo8QdDKYlXx.jpg', '2019-06-07', '2019-06-08', 30),
(4, 'PMK', 'PMK-003', 'Merk H', '2019', 'Type G', 'W 4564 SG', '5000', '5001', '20190524221436_collection-fluffy-white-clouds_23-2147583452.jpg', NULL, NULL, NULL),
(5, 'PMK', 'PMK-004', 'Merk H', '2019', 'Type G', 'W 9574 SD', '5000', '5001', '20190525005711_89e5dee27f1728e1d52527b702628e80.jpg', NULL, NULL, NULL),
(6, 'PMK', 'PMK-005', 'Merk H', '2019', 'Type G', 'W 9574 SD', '5000', '5001', NULL, NULL, NULL, NULL),
(7, 'PMK', 'PMK-006', 'Merk H', '2019', 'Type G', 'W 9574 SD', '5000', '5001', NULL, NULL, NULL, NULL),
(8, 'PMK', 'PMK-007', 'Merk H', '2019', 'Type G', 'W 9574 SD', '5000', '5001', NULL, NULL, NULL, NULL),
(9, 'PICKUP', 'PCKP-001', 'Merk H', '2019', 'Type G', 'W 9574 SD', '5000', '5001', '5001', '2019-06-07', '2019-06-08', 6),
(10, 'PICKUP', 'PCKP-002', 'Merk H', '2019', 'Type G', 'W 4555 SG', '5000', '5001', 'truckfire.jpg', '2019-06-07', '2019-06-08', 0),
(11, 'PICKUP', 'PCKP-003', 'HG', '2019', 'RTRT', 'W 3454 G', '5000', '5001', '20190607054512_f.PNG', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi_alarm`
--

CREATE TABLE `lokasi_alarm` (
  `id` int(10) NOT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL,
  `unit_kerja` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL,
  `qr_code` varchar(500) NOT NULL,
  `last_inspection` varchar(100) DEFAULT NULL,
  `exp_inspection` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lokasi_alarm`
--

INSERT INTO `lokasi_alarm` (`id`, `company`, `plant`, `unit_kerja`, `area`, `qr_code`, `last_inspection`, `exp_inspection`) VALUES
(1, 5000, 5001, 'Board of Commissioners', 'Gedung Utama', 'Gedung Utama', NULL, NULL),
(2, 5000, 5001, 'Board of Commissioners', 'Lantai 1', 'Lantai 1', NULL, NULL),
(3, 5000, 5001, '', 'Lantai 2', '', NULL, NULL),
(4, 5000, 5001, '', 'Lantai 3', '', NULL, NULL),
(5, 5000, 5001, '', 'Lantai 4', '', NULL, NULL),
(6, 5000, 5001, '', 'Crusher Tuban 1', 'Crusher Tuban 1', NULL, NULL),
(7, 5000, 5001, '', 'House Pump KPSG', 'House Pump KPSG', NULL, NULL),
(8, 5000, 5001, '', 'Mainsub', 'Mainsub', NULL, NULL),
(10, 5000, 5002, '', 'Main Office', 'Main Office', NULL, NULL),
(11, 5000, 5002, '', 'Crusher Tuban 4', 'Crusher Tuban 4', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi_apar`
--

CREATE TABLE `lokasi_apar` (
  `id` int(10) NOT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL,
  `unit_kerja` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL,
  `qr_code` varchar(100) NOT NULL,
  `last_inspection` varchar(100) DEFAULT NULL,
  `exp_inspection` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lokasi_apar`
--

INSERT INTO `lokasi_apar` (`id`, `company`, `plant`, `unit_kerja`, `area`, `qr_code`, `last_inspection`, `exp_inspection`) VALUES
(1, 5000, 5001, 'Bureau of Mining Planning & Monitoring', 'Main Office', 'Main Office', '2019-06-06', '2019-06-01'),
(2, 5000, 5001, 'Group of CCR Kiln & Coal Mill I', 'CCR TUBAN 3 & 4', 'CCR TUBAN 3 & 4', '2019-05-29', '2019-06-04'),
(3, 5000, 5001, 'Group of CCR Kiln & Coal Mill I', 'Perkantoran', 'Perkantoran', '2019-05-29', '2019-06-04'),
(4, 5000, 5001, 'Group of CCR Kiln & Coal Mill I', 'PMCR', 'PMCR', '2019-06-06', '2019-06-12'),
(5, 5000, 5001, 'Group of Intern Utility I', 'OP. Crusher', 'OP. Crusher', '2019-06-06', '2019-06-12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi_hydrant`
--

CREATE TABLE `lokasi_hydrant` (
  `id` int(10) NOT NULL,
  `area` varchar(50) NOT NULL,
  `unit_kerja` varchar(100) NOT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL,
  `qr_code` varchar(100) NOT NULL,
  `last_inspection` varchar(100) DEFAULT NULL,
  `exp_inspection` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lokasi_hydrant`
--

INSERT INTO `lokasi_hydrant` (`id`, `area`, `unit_kerja`, `company`, `plant`, `qr_code`, `last_inspection`, `exp_inspection`) VALUES
(1, 'Crusher Tuban 3', 'Secretariat of Commissioners', 5000, 5001, 'Crusher Tuban 3', '2019-05-26', '2019-06-01'),
(2, 'Main Office', 'Secretariat of Commissioners', 5000, 5001, 'Main Office', '2019-05-29', '2019-06-04'),
(3, 'CCR Tuban 3 & 4', 'Secretariat of Commissioners', 5000, 5001, 'HYDRANT~3', '', ''),
(4, 'Gedung Diklat', 'Secretariat of Commissioners', 5000, 5001, 'HYDRANT~4', '2019-05-22', ''),
(5, 'Perkantoran', 'Secretariat of Commissioners', 5000, 5001, 'Perkantoran', '2019-06-06', '2019-06-12'),
(6, 'PLI Crusher', 'Committees', 5000, 5001, 'PLI Crusher', '2019-06-06', '2019-06-12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lotto`
--

CREATE TABLE `lotto` (
  `id` int(10) NOT NULL,
  `equipment_number` varchar(100) DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `no_er` varchar(100) DEFAULT NULL,
  `activity_description` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `note` varchar(100) DEFAULT NULL,
  `safety_key` varchar(100) DEFAULT NULL,
  `safety_tools` varchar(100) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  `k3_drawin_badge` varchar(100) DEFAULT NULL,
  `k3_drawin_name` varchar(100) DEFAULT NULL,
  `k3_drawin_ket` varchar(100) DEFAULT NULL,
  `k3_drawout_badge` varchar(100) DEFAULT NULL,
  `k3_drawout_name` varchar(100) DEFAULT NULL,
  `k3_drawout_ket` varchar(100) DEFAULT NULL,
  `pmll_drawin_badge` varchar(100) DEFAULT NULL,
  `pmll_drawin_name` varchar(100) DEFAULT NULL,
  `pmll_drawin_ket` varchar(100) DEFAULT NULL,
  `pmll_drawout_badge` varchar(100) DEFAULT NULL,
  `pmll_drawout_name` varchar(100) DEFAULT NULL,
  `pmll_drawout_ket` varchar(100) DEFAULT NULL,
  `operator_drawin_badge` varchar(100) DEFAULT NULL,
  `operator_drawin_name` varchar(100) DEFAULT NULL,
  `operator_drawin_ket` varchar(100) DEFAULT NULL,
  `operator_drawout_badge` varchar(100) DEFAULT NULL,
  `operator_drawout_name` varchar(100) DEFAULT NULL,
  `operator_drawout_ket` varchar(100) DEFAULT NULL,
  `pmlt_drawin_badge` varchar(100) DEFAULT NULL,
  `pmlt_drawin_name` varchar(100) DEFAULT NULL,
  `pmlt_drawin_ket` varchar(100) DEFAULT NULL,
  `pmlt_drawout_badge` varchar(100) DEFAULT NULL,
  `pmlt_drawout_name` varchar(100) DEFAULT NULL,
  `pmlt_drawout_ket` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lotto`
--

INSERT INTO `lotto` (`id`, `equipment_number`, `location`, `no_er`, `activity_description`, `date`, `time`, `note`, `safety_key`, `safety_tools`, `status`, `k3_drawin_badge`, `k3_drawin_name`, `k3_drawin_ket`, `k3_drawout_badge`, `k3_drawout_name`, `k3_drawout_ket`, `pmll_drawin_badge`, `pmll_drawin_name`, `pmll_drawin_ket`, `pmll_drawout_badge`, `pmll_drawout_name`, `pmll_drawout_ket`, `operator_drawin_badge`, `operator_drawin_name`, `operator_drawin_ket`, `operator_drawout_badge`, `operator_drawout_name`, `operator_drawout_ket`, `pmlt_drawin_badge`, `pmlt_drawin_name`, `pmlt_drawin_ket`, `pmlt_drawout_badge`, `pmlt_drawout_name`, `pmlt_drawout_ket`) VALUES
(5, '6788', 'Hhh', '57577', 'Bhj', '2019-05-30', '12:58', 'Hnj', 'Jjjj', '[\'APD\']', 'OPEN', '00000516', 'DWI SUKESTI, SE.', 'Y', '00000514', 'AGUSTINUS FARID DK', 'V', '00000517', 'TITIK SUHARTI', 'X', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null'),
(7, '76776', 'dsfsf', '656656', 'sdfsf', '2019-06-06', '06:06', 'sdfsf', 'sdf', '[\'APD\']', 'OPEN', '00000536', 'SOESETYOKO S., SE.', 'V', '', '', 'X', '', '', 'Y', '', '', 'X', '', '', 'X', '', '', 'X', '', '', 'X', '', '', 'X'),
(8, '5656', 'fgfdg', '5646', 'fgfdg', '2019-06-07', '08:08', 'gdg', 'fgfdg', '[\'APD\']', 'OPEN', '00000548', 'SRI WILUJENG, Ir.', 'V', '', '', 'X', '00000535', 'TEDDY B. SETYADI, SE.', 'Y', '', '', 'X', '', '', 'X', '', '', 'X', '', '', 'X', '', '', 'X');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_apd`
--

CREATE TABLE `master_apd` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `merk` varchar(20) DEFAULT NULL,
  `stok` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(20) DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(20) DEFAULT NULL,
  `code_comp` int(11) NOT NULL,
  `code_plant` int(11) NOT NULL,
  `a_stok` int(11) DEFAULT NULL,
  `size` varchar(5) DEFAULT NULL,
  `individu` varchar(1) DEFAULT NULL,
  `uk` varchar(1) DEFAULT NULL,
  `pinjam` varchar(1) DEFAULT NULL,
  `masa_exp` int(10) DEFAULT NULL,
  `max_order` int(10) DEFAULT NULL,
  `masa_text` varchar(20) DEFAULT NULL,
  `foto_bef` varchar(200) DEFAULT NULL,
  `name_origin` varchar(255) NOT NULL,
  `order_group` int(11) DEFAULT NULL,
  `group_code` int(11) NOT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `order` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_apd`
--

INSERT INTO `master_apd` (`id`, `kode`, `nama`, `merk`, `stok`, `create_at`, `create_by`, `update_at`, `update_by`, `code_comp`, `code_plant`, `a_stok`, `size`, `individu`, `uk`, `pinjam`, `masa_exp`, `max_order`, `masa_text`, `foto_bef`, `name_origin`, `order_group`, `group_code`, `group_name`, `order`, `created_at`, `updated_at`) VALUES
(2876, '600-000002', 'Baju Tahan Panas', 'Firepel', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 730, 1, '2 Tahun', NULL, 'Baju Tahan Panas', 0, 600, 'PELINDUNG BADAN / BODY SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2877, '600-000003', 'Apron Baju Las', 'Kinco', 19, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-04 00:35:41', 'approval by system', 5000, 5002, 18, NULL, '0', '1', '1', 92, 3, '3 Bulan', NULL, 'Apron Baju Las', 0, 600, 'PELINDUNG BADAN / BODY SAFETY', NULL, '2018-05-31 06:45:00', '2019-01-04 00:35:41'),
(2878, '600-000005', 'Rompi Pelampung ', 'set', 25, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-10-06 03:12:00', 'RACHMAT.HARIYOKO', 5000, 5002, 25, NULL, '1', '1', '1', 122, 5, '4 Bulan', NULL, 'Rompi Pelampung ', 0, 600, 'PELINDUNG BADAN / BODY SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2879, '700-020001', 'Safety Boot  No. 6', 'Lynx', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '6', '0', '1', '0', 365, 5, '1, 5 Tahun', NULL, 'Safety Boot', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2880, '700-020002', 'Safety Boot  No. 7', 'Lynx', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '0', '1', '0', 548, 5, '1, 5 Tahun', NULL, 'Safety Boot', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2881, '700-020004', 'Safety Boot  No. 9', 'Lynx', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '0', '1', '0', 548, 5, '1, 5 Tahun', NULL, 'Safety Boot', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2882, '700-070005', 'Saf. Shoes Panjang No. 5', 'Kings \"KWD805CX\"', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '5', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Saf. Shoes Panjang', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2883, '700-070006', 'Saf. Shoes Panjang No. 6', 'Kings \"KWD805CX\"', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '6', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'Saf. Shoes Panjang', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2884, '700-070007', 'Saf. Shoes Panjang Orange No. 7', 'Kings \"KWD805CX\"', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-12-29 10:27:58', 'INDRA.NOFIANDI', 5000, 5002, 0, '7', '1', '0', '1', 360, 0, '12 Bulan', '/files/img/5000/5002/apd/20181229/00003396-02884-FOTO-APD.JPG', 'Saf. Shoes Panjang', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2885, '700-070009', 'Saf. Shoes Panjang No. 9', 'Kings \"KWD805CX\"', 17, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:39:00', 'RACHMAT.HARIYOKO', 5000, 5002, 17, '9', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'Saf. Shoes Panjang', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2886, '700-070010', 'Saf. Shoes Panjang No. 10', 'Kings \"KWD805CX\"', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '10', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'Saf. Shoes Panjang', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2887, '700-080012', 'Saf. Shoes Vantovel No. 6', 'King \"KJ424X\"', 10, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-09-26 03:00:00', 'RACHMAT.HARIYOKO', 5000, 5002, 10, '6', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'Saf. Shoes Vantovel', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2888, '700-080014', 'Saf. Shoes Vantovel No. 8', 'King \"KJ424X\"', 15, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-09-26 03:05:00', 'RACHMAT.HARIYOKO', 5000, 5002, 15, '8', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'Saf. Shoes Vantovel', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2889, '700-080016', 'Saf. Shoes Vantovel No. 10', 'King \"KJ424X\"', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '10', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'Saf. Shoes Vantovel', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2890, '700-080017', 'S. Shoes Darwin No. 5', 'BATA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '5', '1', '0', '1', 548, 0, '1,5 Tahun', NULL, 'S. Shoes Darwin', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2891, '700-080018', 'S. Shoes Darwin No. 6', 'BATA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '6', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'S. Shoes Darwin', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2892, '700-080019', 'S. Shoes Darwin No. 7', 'BATA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'S. Shoes Darwin', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2893, '700-080020', 'S. Shoes Darwin No. 8', 'BATA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'S. Shoes Darwin', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2894, '700-080022', 'S. Shoes Darwin No. 10', 'BATA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '10', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'S. Shoes Darwin', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2895, '700-080023', 'S. Shoes Darwin No. 11', 'BATA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '11', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'S. Shoes Darwin', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2896, '700-080024', 'Shoes Dr.Osha 3151no.5', 'Dr.Osha', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '5', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes Dr.Osha 3151', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2897, '700-080026', 'Shoes Dr.Osha 3132 no.7', 'Dr.Osha', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes Dr.Osha 3132', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2898, '700-080027', 'Shoes Dr.Osha 3132 no.8', 'Dr.Osha', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes Dr.Osha 3132', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2899, '700-080028', 'Shoes Dr.Osha 3132 no.9', 'Dr.Osha', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes Dr.Osha 3132', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2900, '700-080029', 'Shoes Dr.Osha 3225 no.10', 'Dr.Osha', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '10', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes Dr.Osha 3225', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2901, '700-080031', 'Shoes 322 ST. no. 6', 'Unicorn', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '6', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes 322 ST.', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2902, '700-080032', 'Shoes 322 ST. no. 7', 'Unicorn', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes 322 ST.', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2903, '700-080033', 'Shoes 322 ST. no. 8', 'Unicorn', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes 322 ST.', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2904, '700-080034', 'Shoes 322 ST. no. 9', 'Unicorn', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes 322 ST.', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2905, '700-080037', 'Shoes Krusher no. 5', 'Krushers', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '5', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Shoes Krusher', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2906, '800-000001', 'Full Body Harness', 'Protecta', 32, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-10-06 03:16:00', 'RACHMAT.HARIYOKO', 5000, 5002, 32, NULL, '0', '1', '0', 730, 6, '2 Tahun', NULL, 'Full Body Harness', 0, 800, 'FULL BODY HARNESS', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2907, '900-000001', 'Betadine 60 ml', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Betadine 60 ml', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2908, '900-000003', 'Pembalut kasa 10 cm', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Pembalut kasa 10 cm', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2909, '900-000004', 'Pembalut kasa 5 cm', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Pembalut kasa 5 cm', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2910, '900-000006', 'Bioplancenton 15 gram', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Bioplancenton 15 gram', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2911, '900-000007', 'Pembalut Segitiga (MICELA)', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Pembalut Segitiga (MICELA)', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2912, '900-000008', 'Kapas Pembalut 25gr', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Kapas Pembalut 25gr', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2913, '900-000010', 'Hansaplast Roll (Plaster 1.25x45m)', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Hansaplast Roll (Plaster 1.25x45m)', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2914, '900-000011', 'Hansaplast', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Hansaplast', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2915, '900-000012', 'Kotak  P3K (BOX)', NULL, 89, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-13 02:21:00', 'RACHMAT.HARIYOKO', 5000, 5002, 89, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Kotak  P3K (BOX)', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2916, '900-000014', 'Gelas mata', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Gelas mata', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2917, '900-000015', 'Sarung tangan 1 kali pakai', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Sarung tangan 1 kali pakai', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2918, '900-000016', 'Paket Obat P3K', NULL, 38, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-11-16 02:11:00', 'RACHMAT.HARIYOKO', 5000, 5002, 37, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Paket Obat P3K', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2995, '100-020002', 'Safety Hat Kuning  ', 'MSA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-09-24 01:20:00', 'SYAHRIAL.RAMADANI', 5000, 5002, 0, NULL, '1', '0', '1', 1095, 0, '3 Tahun', '/files/img/5000/5001/apd/20180924/00006342-00003-FOTO-APD.JPG', 'Safety Hat Kuning  ', 1, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2996, '100-050005', 'Safety Hat Merah', 'MSA', 8, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:07:00', 'RACHMAT.HARIYOKO', 5000, 5002, 8, NULL, '1', '0', '1', 1095, 0, '3 Tahun', NULL, 'Safety Hat Merah', 1, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2997, '100-000006', 'Hat Band MSA', 'HC 71/MSA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-02 12:54:31', 'approval by system', 5000, 5002, -1, NULL, '1', '0', '1', 365, 0, '1 Tahun', NULL, 'Hat Band', 0, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2998, '200-080003', 'Welding Glasses Hitam S#12', 'JP', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 122, 5, '4 Bulan', NULL, 'Welding Glasses Hitam S#12', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(2999, '200-000003', 'Welding Glasses Clear', 'JP', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 122, 5, '4 Bulan', NULL, 'Welding Glasses Clear', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3000, '200-000004', 'Welding Helmet', 'BLUE EAGLE', 14, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-10-06 03:18:00', 'RACHMAT.HARIYOKO', 5000, 5002, 14, NULL, '0', '1', '0', 365, 4, '1 Tahun', NULL, 'Welding Helmet', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3001, '200-000005', 'Safety Goggle u/ gerinda', 'AO 484 B', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 365, 4, '1 Tahun', NULL, 'Safety Goggle u/ gerinda', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3002, '200-000007', 'Safety Goggle Clear', 'MSA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-01 21:07:29', 'approval by system', 5000, 5002, -1, NULL, '1', '0', '1', 365, 0, '1 Tahun', NULL, 'Safety Goggle Clear', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3003, '200-000009', 'Kaca Face Shield', '3M', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 365, 4, '1 Tahun', NULL, 'Kaca Face Shield', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3004, '200-000011', 'kacamata hitam \"King\'s\"', 'King\'s', 171, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-11-21 08:20:00', 'ARIES.MUCHLIS', 5000, 5002, 168, NULL, '1', '1', '1', 365, 4, '1 Tahun', NULL, 'Kacamata hitam \"King\'s\"', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3005, '300-000001', 'Ear Plug', 'Bilsom 566', 133, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-11-21 08:20:00', 'ARIES.MUCHLIS', 5000, 5002, 132, NULL, '1', '0', '1', 122, 0, '4 Bulan', NULL, 'Ear Plug', 0, 300, 'PELINDUNG TELINGA / PEREDAM SUARA BISING', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3006, '300-000002', 'Ear Muff', 'HL', 21, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:37:00', 'RACHMAT.HARIYOKO', 5000, 5002, 21, NULL, '0', '1', '1', 365, 4, '1 Tahun', NULL, 'Ear Muff', 0, 300, 'PELINDUNG TELINGA / PEREDAM SUARA BISING', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3007, '400-000002', 'Dust Respirator   ', 'Marsk', 67, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-09 17:15:41', 'approval by system', 5000, 5002, 64, NULL, '1', '0', '0', 133, 0, '6 Bulan', NULL, 'Dust Respirator   ', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3008, '400-000004', 'Dust Respirator c/w catrige', 'STS GM 76D', 59, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-04 00:28:06', 'approval by system', 5000, 5002, 58, NULL, '1', '0', '0', 365, 0, '1 Tahun', NULL, 'Dust Respirator c/w catrige', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3009, '400-000006', 'Fre filter Respirator 3m', '3M', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '0', 30, 0, '1 Bulan', NULL, 'Fre filter Respirator 3m', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3010, '400-080009', 'Dust Respirator Unisafe', 'Unisafe', 5, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:24:00', 'RACHMAT.HARIYOKO', 5000, 5002, 5, NULL, '1', '0', '0', 365, 0, '1 Tahun', NULL, 'Dust Respirator Unisafe', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3011, '500-000001', 'Kaos Tangan Kain', 'POLKADOT', 1476, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-10-31 01:20:00', 'RACHMAT.HARIYOKO', 5000, 5002, 1452, NULL, '0', '1', '0', 7, 24, '1 Minggu', NULL, 'Kaos Tangan Kain', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3012, '500-000002', 'Kaos Tangan Kombinasi Kulit', 'Dancel', 442, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-10-06 02:55:00', 'RACHMAT.HARIYOKO', 5000, 5002, 442, NULL, '0', '1', '0', 30, 36, '1 Bulan', NULL, 'Kaos Tangan Kombinasi Kulit', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3013, '500-000004', 'Rubber Mapa u/ Kimia', 'Mapa', 36, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:32:00', 'RACHMAT.HARIYOKO', 5000, 5002, 36, NULL, '0', '1', '0', 365, 24, '1 Tahun', NULL, 'Rubber Mapa u/ Kimia', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3014, '500-000006', 'Kaos Tangan (Phynomic Lite) Size 7', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic Lite) Size 7', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3015, '500-000007', 'Kaos Tangan (Phynomic Lite) Size 8', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic Lite) Size 8', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3016, '500-000008', 'Kaos Tangan (Phynomic Lite) Size 9', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic Lite) Size 9', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3017, '500-000010', 'Kaos Tangan (Unilite 6605) Size 8', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Unilite 6605) Size 8', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3018, '500-000012', 'Kaos Tangan (Phynomic XG) Size 7', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic XG) Size 7', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3019, '500-000013', 'Kaos Tangan (Phynomic XG) Size 8', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic XG) Size 8', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3020, '500-000016', 'Kaos Tangan (Phynomic ESD) Size 8', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '8', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic ESD) Size 8', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3021, '500-000017', 'Kaos Tangan (Phynomic ESD) Size 9', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic ESD) Size 9', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3034, '100-010001', 'Safety Hat Putih', '3M', 36, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-18 15:05:00', 'approval by system', 5000, 5002, 14, NULL, '1', '0', '1', 1095, 0, '3 Tahun', NULL, 'Safety Hat Putih', 1, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3036, '600-000001', 'Jas Hujan Size XL / XXL', 'Pinguin', 47, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-10-16 08:53:00', 'RACHMAT.HARIYOKO', 5000, 5002, 44, 'XL', '0', '1', '0', 365, 3, '1 Tahun', NULL, 'Jas Hujan Size XL / XXL', 0, 600, 'PELINDUNG BADAN / BODY SAFETY', NULL, '2018-05-31 06:45:00', '2019-01-04 00:35:41'),
(3037, '700-080011', 'Saf. Shoes Vantovel No. 5', 'King \"KJ424X\"', 5, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-09-26 02:59:00', 'RACHMAT.HARIYOKO', 5000, 5002, 5, '5', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Saf. Shoes Vantovel', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3038, '700-020036', 'Safety Boot  No. 9', 'Lynx', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '10', '0', '1', '0', 548, 5, '1, 5 Tahun', NULL, 'Safety Boot', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3053, '200-080002', 'Welding Glasses Hitam S#11', 'JP', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 122, 5, '4 Bulan', NULL, 'Welding Glasses Hitam S#11', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3054, '400-000007', 'Filter Holder', '3M', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '0', 92, 0, '3 Bulan', NULL, 'Filter Holder', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3055, '500-000015', 'Kaos Tangan (Phynomic ESD) Size 7', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic ESD) Size 7', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3110, '400-000001', 'Dust Respirator   ', 'Medis', 103, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-03 23:50:17', 'approval by system', 5000, 5002, 100, NULL, '1', '1', '0', 7, 0, '1 Minggu', NULL, 'Dust Respirator   ', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3111, '700-080015', 'Saf. Shoes Vantovel No. 9', 'King \"KJ424X\"', 3, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-09-26 03:05:00', 'RACHMAT.HARIYOKO', 5000, 5002, 3, '9', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 'Saf. Shoes Vantovel', 0, 700, 'PELINDUNG KAKI / SAFETY SHOES', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3172, '910-000001', 'Flashback arestor REGULATOR', 'FBR-1 ( 0656-0004 )', 0, '2018-11-08 17:00:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, NULL, NULL, '0', '1', '0', NULL, NULL, NULL, NULL, 'Flashback arestor REGULATOR', 0, 910, 'LAIN LAIN', NULL, '2018-11-08 17:00:00', '2018-11-08 17:00:00'),
(3173, '910-000002', 'Flashback arestor TORCH', 'FB-1 (0656-00001)', 0, '2018-11-08 17:00:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, NULL, NULL, '0', '1', '0', NULL, NULL, NULL, NULL, 'Flashback arestor TORCH', 0, 910, 'LAIN LAIN', NULL, '2018-11-08 17:00:00', '2018-11-08 17:00:00'),
(3174, '910-000003', 'Fire block', NULL, 0, '2018-11-08 17:00:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, NULL, NULL, '0', '1', '0', NULL, NULL, NULL, NULL, 'Fire block', 0, 910, 'LAIN LAIN', NULL, '2018-11-08 17:00:00', '2018-11-08 17:00:00'),
(3175, '910-000004', 'Safety line', 'Roll', 0, '2018-11-08 17:00:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, NULL, NULL, '0', '1', '0', NULL, NULL, NULL, NULL, 'Safety line', 0, 910, 'LAIN LAIN', NULL, '2018-11-08 17:00:00', '2018-11-08 17:00:00'),
(3182, '500-000018', 'Kaos Tangan (Unidur Foam)', 'UVEX 9', 0, '2018-11-08 17:00:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, NULL, '9', '0', '1', '0', NULL, NULL, NULL, NULL, 'Kaos Tangan (Unidur Foam)', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-11-08 17:00:00', '2018-11-08 17:00:00'),
(3183, '500-000019', 'Kaos Tangan (Unidur Foam)', 'UVEX 8', 0, '2018-11-08 17:00:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, NULL, '8', '0', '1', '0', NULL, NULL, NULL, NULL, 'Kaos Tangan (Unidur Foam)', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-11-08 17:00:00', '2018-11-08 17:00:00'),
(3284, '900-000013', 'Aquadesh (BORWATER)', NULL, 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 0, 0, NULL, NULL, 'Aquadesh (BORWATER)', 0, 900, 'PAKET OBAT PPPK', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3298, '100-030003', 'Safety Hat Hijau', 'MSA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '1', 1095, 0, '3 Tahun', NULL, 'Safety Hat Hijau', 1, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3299, '200-000008', 'Hat Band Face Shield', '3M', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 365, 4, '1 Tahun', NULL, 'Hat Band Face Shield', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3300, '400-000005', 'Dust respirator 3m', '3M', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '0', 365, 0, '1 Tahun', NULL, 'Dust respirator 3m', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3301, '500-000005', 'Kaos Tangan Kombinasi Karet', 'Brick', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 30, 60, '1 Bulan', NULL, 'Kaos Tangan Kombinasi Karet', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3302, '500-000011', 'Kaos Tangan (Unilite 6605) Size 9', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Unilite 6605) Size 9', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3303, '100-000007', 'Hat Band 3M', '3M', 223, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-24 04:15:30', 'approval by system', 5000, 5002, 219, NULL, '1', '0', '1', 365, 0, '1 Tahun', NULL, 'Hat Band', 0, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3323, '100-010002', 'Safety Hat Putih', 'MSA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '1', 1095, 0, '3 Tahun', NULL, 'Safety Hat Putih', 1, 100, 'PELINDUNG KEPALA / HEAD SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3324, '200-000001', 'Safety Goggle u/ blander', 'GWL-445 AO', 11, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:23:00', 'RACHMAT.HARIYOKO', 5000, 5002, 11, NULL, '0', '1', '0', 365, 6, '1 Tahun', NULL, 'Safety Goggle u/ blander', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3325, '200-080006', 'Safety Goggle Hitam', 'MSA', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2019-01-04 16:19:41', 'approval by system', 5000, 5002, -3, NULL, '1', '0', '1', 365, 0, '1 Tahun', NULL, 'Safety Goggle Hitam', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3326, '200-080010', 'Kacamata putih \"kleen guard\"', 'Unicorn', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '1', '1', 365, 4, '1 Tahun', NULL, 'Kacamata putih \"kleen guard\"', 0, 200, 'PELINDUNG MATA / EYE SAFETY', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3327, '400-000003', 'Filter Respirator', 'RC-54/64/U2W', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '0', 92, 0, '3 Bulan', NULL, 'Filter Respirator', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3328, '400-000008', 'Catride 3M 3303K', '3M', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, NULL, '1', '0', '0', 92, 0, '3 Bulan', NULL, 'Catride 3M 3303K', 0, 400, 'PELINDUNG HIDUNG', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3329, '500-000003', 'Kaos Tangan Las', 'Kinco', 96, '2018-05-31 06:45:00', 'ADMINISTRATOR', '2018-08-07 02:34:00', 'RACHMAT.HARIYOKO', 5000, 5002, 96, NULL, '0', '1', '0', 30, 36, '1 Bulan', NULL, 'Kaos Tangan Las', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3330, '500-000009', 'Kaos Tangan (Unilite 6605) Size 7', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '7', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Unilite 6605) Size 7', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00'),
(3331, '500-000014', 'Kaos Tangan (Phynomic XG) Size 9', 'UVEX', 0, '2018-05-31 06:45:00', 'ADMINISTRATOR', NULL, NULL, 5000, 5002, 0, '9', '1', '1', '0', 60, 24, '2 Bulan', NULL, 'Kaos Tangan (Phynomic XG) Size 9', 0, 500, 'PELINDUNG TANGAN', NULL, '2018-05-31 06:45:00', '2018-05-31 06:45:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_30_065629_create_accident_reports_table', 2),
(4, '2019_04_04_080828_create_tools_table', 2),
(5, '2019_04_08_034813_create_sios_table', 2),
(6, '2019_04_10_071445_create_daily_reports_table', 2),
(7, '2019_04_10_071706_create_unsafe_details_table', 2),
(15, '2019_05_08_085540_create_daily_report_k3_s_table', 3),
(19, '2019_05_09_025556_create_daily_report_k3_details_table', 4),
(21, '2019_05_22_025929_create_list_file_uploads_table', 5),
(22, '2019_05_22_054816_create_planning_plants_table', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `news`
--

CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `head` varchar(100) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `writer` varchar(100) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `news`
--

INSERT INTO `news` (`id`, `head`, `content`, `image`, `writer`, `date`, `time`) VALUES
(2, 'dd', 'dfdsf', '20190609162603_store.png', 'Administrator', '2019-06-04', '16:50:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_apd`
--

CREATE TABLE `order_apd` (
  `id` int(11) NOT NULL,
  `kode_order` varchar(10) NOT NULL,
  `no_badge` varchar(10) NOT NULL,
  `kode_uk` varchar(20) NOT NULL,
  `uk_text` varchar(100) NOT NULL,
  `approve1_at` timestamp NULL DEFAULT NULL,
  `approve1_by` varchar(50) DEFAULT NULL,
  `approve2_at` timestamp NULL DEFAULT NULL,
  `approve2_by` varchar(50) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(50) NOT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `code_comp` int(11) NOT NULL,
  `code_plant` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `closed_at` timestamp NULL DEFAULT NULL,
  `closed_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  `reject_at` timestamp NULL DEFAULT NULL,
  `reject_by` varchar(50) DEFAULT NULL,
  `note_reject` varchar(200) DEFAULT NULL,
  `keterangan` varchar(100) NOT NULL,
  `individu` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `release_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_apd`
--

INSERT INTO `order_apd` (`id`, `kode_order`, `no_badge`, `kode_uk`, `uk_text`, `approve1_at`, `approve1_by`, `approve2_at`, `approve2_by`, `create_at`, `create_by`, `update_at`, `update_by`, `code_comp`, `code_plant`, `name`, `closed_at`, `closed_by`, `updated_at`, `created_at`, `status`, `reject_at`, `reject_by`, `note_reject`, `keterangan`, `individu`, `unit`, `release_at`) VALUES
(86, 'RUK-86', '2103161043', '50045222', 'Department of Strategic ICT', '2019-05-22 00:45:05', 'admin', '2019-05-22 00:45:16', 'riza', '2019-05-30 17:01:27', 'riza', '2019-05-22 00:45:16', 'riza', 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 00:45:16', '2019-05-16 16:02:56', 'approve 2', NULL, NULL, NULL, 'Permintaan Unit Kerja', 0, 1, NULL),
(87, 'RID-87', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-16 17:23:55', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-16 10:23:55', '2019-05-16 17:23:55', 'troli', NULL, NULL, NULL, 'Permintaan Personal', 0, 0, '2019-05-22 15:20:10'),
(94, 'RID-94', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-16 17:41:03', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-16 10:41:03', '2019-05-16 17:41:03', 'troli', NULL, NULL, NULL, 'Permintaan Personal', 0, 0, '2019-05-22 15:20:10'),
(95, 'RID-95', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-16 17:42:30', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-16 10:42:30', '2019-05-16 17:42:30', 'troli', NULL, NULL, NULL, 'Permintaan Personal', 0, 0, '2019-05-22 15:20:10'),
(96, 'RID-96', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-22 02:58:48', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-21 19:58:48', '2019-05-22 02:58:48', 'troli', NULL, NULL, NULL, 'Permintaan Personal', 1, 0, '2019-05-22 15:20:10'),
(97, 'RUK-97', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-22 03:23:36', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-21 20:23:36', '2019-05-22 03:23:36', 'troli', NULL, NULL, NULL, 'Permintaan Unit Kerja', 0, 1, '2019-05-22 15:20:10'),
(99, 'RID-99', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-22 16:30:15', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 09:30:15', '2019-05-22 16:30:14', 'troli', NULL, NULL, NULL, 'Permintaan Personal', 1, 0, '2019-05-22 16:30:14'),
(100, 'RID-100', '2103161043', '50045222', 'Department of Strategic ICT', '2019-05-22 09:49:53', 'admin', '2019-05-22 09:50:03', 'riza', '2019-05-22 18:08:27', 'riza', '2019-05-22 09:50:03', 'riza', 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 11:08:27', '2019-05-22 16:31:32', 'release', NULL, NULL, NULL, 'Permintaan Personal', 1, 0, '2019-05-22 11:08:27'),
(101, 'RID-101', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-23 05:34:29', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:34:29', '2019-05-23 05:34:29', 'troli', '2019-05-23 05:34:29', NULL, NULL, 'Permintaan Personal', 1, 0, '2019-05-23 05:34:29'),
(102, 'RID-102', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-23 05:36:50', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:36:50', '2019-05-23 05:36:50', 'troli', '2019-05-23 05:36:50', NULL, NULL, 'Permintaan Personal', 1, 0, '2019-05-23 05:36:50'),
(103, 'RUK-103', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-23 05:47:31', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:40:29', '2019-05-23 05:40:29', 'troli', NULL, NULL, NULL, 'Permintaan Unit Kerja', 0, 1, '2019-05-23 05:40:29'),
(104, 'RUK-104', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-23 05:46:34', 'riza', NULL, NULL, 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:46:34', '2019-05-23 05:46:34', 'troli', NULL, NULL, NULL, 'Permintaan Unit Kerja', 0, 1, '2019-05-23 05:46:34'),
(105, 'RID-105', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-23 05:54:02', 'riza', '2019-05-22 22:54:01', 'riza', 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:54:01', '2019-05-23 05:47:58', 'reject', '2019-05-22 22:54:01', 'riza', 'APD sudah habis', 'Permintaan Personal', 1, 0, '2019-05-23 05:47:58'),
(106, 'RID-106', '2103161043', '50045222', 'Department of Strategic ICT', '2019-05-22 22:55:51', 'admin', '2019-05-22 22:56:21', 'riza', '2019-05-23 05:56:59', 'riza', '2019-05-22 22:56:21', 'riza', 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:56:59', '2019-05-23 05:53:56', 'release', NULL, NULL, NULL, 'Permintaan Personal', 1, 0, '2019-05-22 22:56:59'),
(107, 'RUK-107', '2103161043', '50045222', 'Department of Strategic ICT', NULL, NULL, NULL, NULL, '2019-05-23 05:58:07', 'riza', '2019-05-22 22:58:07', 'riza', 5000, 5002, 'Riza Diniatul Umami', NULL, NULL, '2019-05-22 22:58:07', '2019-05-23 05:54:32', 'reject', '2019-05-22 22:58:07', 'riza', 'APD sudah habis', 'Permintaan Unit Kerja', 0, 1, '2019-05-23 05:54:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `parameter_pengecekan_alarm`
--

CREATE TABLE `parameter_pengecekan_alarm` (
  `id` int(10) NOT NULL,
  `parameter` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `parameter_pengecekan_alarm`
--

INSERT INTO `parameter_pengecekan_alarm` (`id`, `parameter`, `type`) VALUES
(1, 'Membunyikan alarm secara simulasi', 'WEEK'),
(2, 'Memeriksa kerja lonceng', 'WEEK'),
(3, 'Memeriksa tegangan dan keadaan baterai', 'WEEK'),
(4, 'Memeriksa seluruh sistem alarm', 'WEEK'),
(5, 'Menciptakan kebakaran simulasi', 'MONTH'),
(6, 'Lampu-lampu indikator', 'MONTH'),
(7, 'Fasilitas penyediaan sumber tenaga darurat', 'MONTH'),
(8, 'Mencoba dengan kondisi gangguan terhadap sistem', 'MONTH'),
(9, 'Kondisi dan kebersihan panel indikator', 'MONTH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id` int(10) NOT NULL,
  `shift` int(5) NOT NULL,
  `officer` varchar(50) NOT NULL,
  `company` int(4) NOT NULL,
  `plant` int(4) NOT NULL,
  `date_activity` varchar(10) NOT NULL,
  `status_pic` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id`, `shift`, `officer`, `company`, `plant`, `date_activity`, `status_pic`) VALUES
(12, 2, 'Nur Hamid', 5000, 5001, '2019-06-06', 'V'),
(13, 1, 'zen', 5000, 5001, '2019-06-04', 'X'),
(14, 3, 'marwah', 5000, 5001, '2019-06-04', 'X');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pinjam_apd`
--

CREATE TABLE `pinjam_apd` (
  `id` int(11) NOT NULL,
  `kode_pinjam` varchar(10) NOT NULL,
  `nopeg` varchar(10) NOT NULL,
  `unit_kerja` varchar(20) NOT NULL,
  `status_retur` varchar(2) DEFAULT NULL,
  `retur_at` timestamp NULL DEFAULT NULL,
  `approve1_at` timestamp NULL DEFAULT NULL,
  `approve1_by` varchar(50) DEFAULT NULL,
  `approve2_at` timestamp NULL DEFAULT NULL,
  `approve2_by` varchar(50) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` varchar(50) NOT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `update_by` varchar(50) DEFAULT NULL,
  `code_comp` int(11) NOT NULL,
  `code_plant` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `uk_text` varchar(100) NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  `closed_at` timestamp NULL DEFAULT NULL,
  `closed_by` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  `reject_at` timestamp NULL DEFAULT NULL,
  `reject_by` varchar(50) DEFAULT NULL,
  `note_reject` varchar(200) DEFAULT NULL,
  `keterangan` varchar(100) NOT NULL,
  `release_at` timestamp NULL DEFAULT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pinjam_apd`
--

INSERT INTO `pinjam_apd` (`id`, `kode_pinjam`, `nopeg`, `unit_kerja`, `status_retur`, `retur_at`, `approve1_at`, `approve1_by`, `approve2_at`, `approve2_by`, `create_at`, `create_by`, `update_at`, `update_by`, `code_comp`, `code_plant`, `tanggal_pinjam`, `uk_text`, `note`, `closed_at`, `closed_by`, `created_at`, `updated_at`, `status`, `reject_at`, `reject_by`, `note_reject`, `keterangan`, `release_at`, `name`) VALUES
(23, 'PJM-23', '2103161043', '50045222', NULL, NULL, '2019-05-22 23:15:19', 'riza', '2019-05-22 23:15:33', 'admin', '2019-05-23 07:03:10', 'riza', '2019-05-22 23:15:33', 'admin', 5000, 5002, '2019-05-23', 'Department of Strategic ICT', 'Pinjam', NULL, NULL, '2019-05-23 06:13:07', '2019-05-23 00:03:10', 'release', NULL, NULL, NULL, 'Permintaan Pinjam', '2019-05-23 00:03:10', 'Riza Diniatul Umami'),
(24, 'PJM-24', '2103161043', '50045222', NULL, '2019-05-26 17:00:00', NULL, NULL, NULL, NULL, '2019-05-27 04:55:40', 'riza', NULL, NULL, 5000, 5002, '2019-05-27', 'Department of Strategic ICT', 'Pinjam', NULL, NULL, '2019-05-27 04:55:40', '2019-05-26 21:55:40', 'order', NULL, NULL, NULL, 'Permintaan Pinjam', NULL, 'Riza Diniatul Umami');

-- --------------------------------------------------------

--
-- Struktur dari tabel `planning_plants`
--

CREATE TABLE `planning_plants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pplant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pplantdesc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rnum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `planning_plants`
--

INSERT INTO `planning_plants` (`id`, `plant`, `pplant`, `pplantdesc`, `rnum`, `created_at`, `updated_at`) VALUES
(1, '5001', '7902', 'Planning Plant Tuban', '1', NULL, NULL),
(2, '5001', '7303', 'Plant Tuban II', '2', NULL, NULL),
(3, '5001', '7304', 'Plant Tuban III', '3', NULL, NULL),
(4, '5001', '7302', 'Plant Tuban I', '4', NULL, NULL),
(5, '5001', '7305', 'Plant Tuban IV', '5', NULL, NULL),
(6, '5002', '7301', 'Plant Gresik', '6', NULL, NULL),
(7, '5003', '5901', 'Planning Plant Rembang', '7', NULL, NULL),
(8, '5004', '7979', 'Plant Cigading I (Satu)', '8', '2019-05-28 21:13:25', '2019-05-28 21:13:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `plant`
--

CREATE TABLE `plant` (
  `id` int(10) NOT NULL,
  `plant` int(4) NOT NULL,
  `plant_text` varchar(30) NOT NULL,
  `company` int(4) NOT NULL,
  `company_text` varchar(30) NOT NULL,
  `nik_kasi` varchar(10) NOT NULL,
  `rnum` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `plant`
--

INSERT INTO `plant` (`id`, `plant`, `plant_text`, `company`, `company_text`, `nik_kasi`, `rnum`) VALUES
(1, 5001, 'Tuban', 5000, 'PT. Semen Indonesia', '00001072', 1),
(2, 5002, 'Gresik', 5000, 'PT. Semen Indonesia', '00003396', 2),
(3, 5003, 'Rembang', 5000, 'PT. Semen Indonesia', 'null', 3),
(4, 5004, 'Cigading', 5000, 'PT. Semen Indonesia', 'null', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pplant`
--

CREATE TABLE `pplant` (
  `id` int(10) NOT NULL,
  `plant` int(4) NOT NULL,
  `pplant` int(4) NOT NULL,
  `pplantdesc` varchar(30) NOT NULL,
  `rnum` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pplant`
--

INSERT INTO `pplant` (`id`, `plant`, `pplant`, `pplantdesc`, `rnum`) VALUES
(1, 5001, 7301, 'Planning Plant Tuban', 1),
(2, 5001, 7302, 'Plant Tuban I', 2),
(3, 5001, 7303, 'Plant Tuban II', 3),
(4, 5001, 7304, 'Plant Tuban III', 4),
(5, 5001, 7305, 'Plant Tuban IV', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `release`
--

CREATE TABLE `release` (
  `id` int(10) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(50) NOT NULL,
  `update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(50) DEFAULT NULL,
  `receiver_badge` varchar(50) NOT NULL,
  `receiver_name` varchar(200) NOT NULL,
  `count` int(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `file` varchar(200) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `id_detail` int(11) NOT NULL,
  `kode_apd` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `release`
--

INSERT INTO `release` (`id`, `kode`, `tipe`, `create_at`, `create_by`, `update_at`, `update_by`, `receiver_badge`, `receiver_name`, `count`, `status`, `file`, `foto`, `id_detail`, `kode_apd`, `created_at`, `updated_at`) VALUES
(1, '100', 'PELINDUNG KEPALA / HEAD SAFETY', '2019-05-22 01:06:32', 'riza', NULL, NULL, '2103161043', 'riza', 6, 'release', NULL, NULL, 32, '100-010001', '2019-05-22 01:06:32', '2019-05-22 01:06:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_apd_master_apd`
--

CREATE TABLE `she_apd_master_apd` (
  `ID` double NOT NULL,
  `KODE` varchar(10) NOT NULL,
  `NAMA` varchar(100) NOT NULL,
  `MERK` varchar(20) DEFAULT NULL,
  `STOK` double DEFAULT '0',
  `CREATE_AT` datetime DEFAULT NULL,
  `CREATE_BY` varchar(20) DEFAULT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(20) DEFAULT NULL,
  `DELETE_AT` datetime DEFAULT NULL,
  `DELETE_BY` varchar(20) DEFAULT NULL,
  `CODE_COMP` double DEFAULT NULL,
  `CODE_PLANT` double DEFAULT NULL,
  `A_STOK` double DEFAULT NULL,
  `SIZE` varchar(5) DEFAULT NULL,
  `INDIVIDU` varchar(1) DEFAULT NULL,
  `UK` varchar(1) DEFAULT NULL,
  `PINJAM` varchar(1) DEFAULT NULL,
  `MASA_EXP` bigint(20) DEFAULT NULL,
  `MAX_ORDER` bigint(20) DEFAULT NULL,
  `MASA_TEXT` varchar(20) DEFAULT NULL,
  `FOTO_BEF` varchar(200) DEFAULT NULL,
  `IS_K3` double NOT NULL DEFAULT '0',
  `ESELON` varchar(100) DEFAULT NULL,
  `NAME_ORIGIN` varchar(255) NOT NULL DEFAULT '0',
  `ORDER_GROUP` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_apd_master_apd`
--

INSERT INTO `she_apd_master_apd` (`ID`, `KODE`, `NAMA`, `MERK`, `STOK`, `CREATE_AT`, `CREATE_BY`, `UPDATE_AT`, `UPDATE_BY`, `DELETE_AT`, `DELETE_BY`, `CODE_COMP`, `CODE_PLANT`, `A_STOK`, `SIZE`, `INDIVIDU`, `UK`, `PINJAM`, `MASA_EXP`, `MAX_ORDER`, `MASA_TEXT`, `FOTO_BEF`, `IS_K3`, `ESELON`, `NAME_ORIGIN`, `ORDER_GROUP`) VALUES
(2876, '600-000002', 'Baju Tahan Panas', 'Firepel', 0, '2018-05-31 13:45:00', 'ADMINISTRATOR', NULL, NULL, NULL, NULL, 5000, 5002, 0, NULL, '0', '1', '0', 730, 1, '2 Tahun', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Baju Tahan Panas', 0),
(2877, '600-000003', 'Apron Baju Las', 'Kinco', 19, '2018-05-31 13:45:00', 'ADMINISTRATOR', '2019-01-04 07:35:41', 'approval by system', NULL, NULL, 5000, 5002, 18, NULL, '0', '1', '1', 92, 3, '3 Bulan', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Apron Baju Las', 0),
(2878, '600-000005', 'Rompi Pelampung ', 'set', 25, '2018-05-31 13:45:00', 'ADMINISTRATOR', '2018-10-06 10:12:00', 'RACHMAT.HARIYOKO', NULL, NULL, 5000, 5002, 25, NULL, '1', '1', '1', 122, 5, '4 Bulan', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Rompi Pelampung ', 0),
(2879, '700-020001', 'Safety Boot  No. 6', 'Lynx', 0, '2018-05-31 13:45:00', 'ADMINISTRATOR', NULL, NULL, NULL, NULL, 5000, 5002, 0, '6', '0', '1', '0', 365, 5, '1, 5 Tahun', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Safety Boot', 0),
(2880, '700-020002', 'Safety Boot  No. 7', 'Lynx', 0, '2018-05-31 13:45:00', 'ADMINISTRATOR', NULL, NULL, NULL, NULL, 5000, 5002, 0, '7', '0', '1', '0', 548, 5, '1, 5 Tahun', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Safety Boot', 0),
(2881, '700-020004', 'Safety Boot  No. 9', 'Lynx', 0, '2018-05-31 13:45:00', 'ADMINISTRATOR', NULL, NULL, NULL, NULL, 5000, 5002, 0, '9', '0', '1', '0', 548, 5, '1, 5 Tahun', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Safety Boot', 0),
(2882, '700-070005', 'Saf. Shoes Panjang No. 5', 'Kings \"KWD805CX\"', 0, '2018-05-31 13:45:00', 'ADMINISTRATOR', NULL, NULL, NULL, NULL, 5000, 5002, 0, '5', '1', '0', '1', 548, 0, '1, 5 Tahun', NULL, 0, '[\"1\",\"2\",\"3\",\"4\",\"5\"]', 'Saf. Shoes Panjang', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_master_funcloc`
--

CREATE TABLE `she_master_funcloc` (
  `id` int(11) NOT NULL,
  `idequpment` varchar(250) NOT NULL,
  `equptname` varchar(250) NOT NULL,
  `equptimage` varchar(250) DEFAULT NULL,
  `sapequipnum` varchar(250) NOT NULL,
  `equpmentcode` varchar(250) NOT NULL,
  `pplant` varchar(250) NOT NULL,
  `status` varchar(100) NOT NULL,
  `funcloc` varchar(250) NOT NULL,
  `mplant` varchar(250) NOT NULL,
  `rnum` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_master_funcloc`
--

INSERT INTO `she_master_funcloc` (`id`, `idequpment`, `equptname`, `equptimage`, `sapequipnum`, `equpmentcode`, `pplant`, `status`, `funcloc`, `mplant`, `rnum`) VALUES
(1, '10003513', '719BA199', NULL, '10003513', 'ROOM, OUTDOOR AC ROOM 1, TUBAN FACTORY O', '7902', 'INST', 'SG-2307-GN-719-BA155', '2307', '1'),
(2, '10003514', '719BA200', NULL, '10003514', 'BUILDING,  TUBAN FACTORY OPERATIONS OFFI', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '2'),
(3, '10003515', '719BA201', NULL, '10003515', 'ROOM CORPORATE SECRETARY, TUBAN FACTORY', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '3'),
(4, '10003516', '719BA202', NULL, '10003516', 'ROOM, GM OF CORPORATE SECRETARY, TUBAN F', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '4'),
(5, '10003517', '719BA203', NULL, '10003517', 'ROOM, DOCUMENTS STORAGE, CORPORATE SECRE', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '5'),
(6, '10003518', '719BA204', NULL, '10003518', 'ROOM, BUREAU OF CORPORATE SECRETARY 1, T', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '6'),
(7, '10003519', '719BA205', NULL, '10003519', 'ROOM, BUREAU OF CORPORATE SECRETARY 2, T', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '7'),
(8, '10003520', '719BA206', NULL, '10003520', 'ROOM, BUREAU OF CORPORATE SECRETARY 3, T', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '8'),
(9, '10003521', '719BA207', NULL, '10003521', 'ROOM, MGR OF CORPORATE SECRETARY 1, TUBA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '9'),
(10, '10003522', '719BA208', NULL, '10003522', 'ROOM, MGR OF CORPORATE SECRETARY 2, TUBA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '10'),
(11, '10003523', '719BA209', NULL, '10003523', 'ROOM, MGR OF CORPORATE SECRETARY 3, TUBA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '11'),
(12, '10003524', '719BA210', NULL, '10003524', 'ROOM, MEETING ROOM DEPARTMENT OF CORPORA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '12'),
(13, '10003525', '719BA211', NULL, '10003525', 'ROOM, DEPARTMENT OF LEGAL & RISK MANAGEM', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '13'),
(14, '10003526', '719BA212', NULL, '10003526', 'ROOM, GM OF LEGAL & RISK MANAGEMENT, TUB', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '14'),
(15, '10003527', '719BA213', NULL, '10003527', 'ROOM, DOCUMENTS STORAGE, DISTRIBUTION &', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '15'),
(16, '10003528', '719BA214', NULL, '10003528', 'ROOM, BUREAU OF LEGAL & RISK MANAGEMENT', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '16'),
(17, '10003529', '719BA215', NULL, '10003529', 'ROOM, BUREAU OF LEGAL & RISK MANAGEMENT', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '17'),
(18, '10003530', '719BA216', NULL, '10003530', 'ROOM, BUREAU OF LEGAL & RISK MANAGEMENT', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '18'),
(19, '10003531', '719BA217', NULL, '10003531', 'ROOM, MGR OF LEGAL & RISK MANAGEMENT 1,', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '19'),
(20, '10003532', '719BA218', NULL, '10003532', 'ROOM, MGR OF LEGAL & RISK MANAGEMENT 2,', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '20'),
(21, '10003533', '719BA219', NULL, '10003533', 'ROOM, MEETING ROOM DEPARTMENT OF OF LEGA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '21'),
(22, '10003534', '719BA220', NULL, '10003534', 'ROOM INTERNAL AUDIT, TUBAN FACTORY OPERA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '22'),
(23, '10003535', '719BA221', NULL, '10003535', 'ROOM, DEPARTMENT OF INTERNAL AUDITS, TUB', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '23'),
(24, '10003536', '719BA222', NULL, '10003536', 'ROOM, GM OF INTERNAL AUDITS, TUBAN FACTO', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '24'),
(25, '10003537', '719BA223', NULL, '10003537', 'ROOM, DOCUMENTS STORAGE, INTERNAL AUDITS', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '25'),
(26, '10003538', '719BA224', NULL, '10003538', 'ROOM, BUREAU OF INTERNAL AUDITS 1, TUBAN', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '26'),
(27, '10003539', '719BA225', NULL, '10003539', 'ROOM, BUREAU OF INTERNAL AUDITS 2, TUBAN', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '27'),
(28, '10003540', '719BA226', NULL, '10003540', 'ROOM, BUREAU OF INTERNAL AUDITS 3, TUBAN', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '28'),
(29, '10003541', '719BA227', NULL, '10003541', 'ROOM, MGR OF INTERNAL AUDITS 1, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '29'),
(30, '10003542', '719BA228', NULL, '10003542', 'ROOM, MGR OF INTERNAL AUDITS 2, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '30'),
(31, '10003544', '719BA230', NULL, '10003544', 'ROOM, MGR OF INTERNAL AUDITS 4, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '31'),
(32, '10003545', '719BA231', NULL, '10003545', 'ROOM, MGR OF INTERNAL AUDITS 5, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '32'),
(33, '10003546', '719BA232', NULL, '10003546', 'ROOM, MGR OF INTERNAL AUDITS 6, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '33'),
(34, '10003548', '719BA234', NULL, '10003548', 'ROOM, MGR OF INTERNAL AUDITS 8, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '34'),
(35, '10003549', '719BA235', NULL, '10003549', 'ROOM, MGR OF INTERNAL AUDITS 9, TUBAN FA', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '35'),
(36, '10003550', '719BA236', NULL, '10003550', 'ROOM, MGR OF INTERNAL AUDITS 10, TUBAN F', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '36'),
(37, '10003552', '719BA238', NULL, '10003552', 'ROOM, DIRECTOR LOBBY LIFT, TUBAN FACTORY', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '37'),
(38, '10003553', '719BA239', NULL, '10003553', 'ROOM, ELECTRICAL PANEL ROOM, TUBAN FACTO', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '38'),
(39, '10003555', '719BA241', NULL, '10003555', 'ROOM, FEMALE MUSHOLLA, TUBAN FACTORY OPE', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '39'),
(40, '10003556', '719BA242', NULL, '10003556', 'ROOM, PANTRY, TUBAN FACTORY OPERATIONS O', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '40'),
(41, '10003557', '719BA243', NULL, '10003557', 'ROOM, MALE TOILET ROOM, TUBAN FACTORY OP', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '41'),
(42, '10003559', '719BA245', NULL, '10003559', 'ROOM, STORAGE, TUBAN FACTORY OPERATIONS', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '42'),
(43, '10003560', '719BA246', NULL, '10003560', 'ROOM, INDOOR AC ROOM, TUBAN FACTORY OPER', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '43'),
(44, '10003561', '719BA247', NULL, '10003561', 'ROOM, OUTDOOR AC ROOM 1, TUBAN FACTORY O', '7902', 'INST', 'SG-2307-GN-719-BA200', '2307', '44'),
(45, '10003563', '719BA249', NULL, '10003563', 'DIRECTOR TRANSIT ROOM, TUBAN FACTORY OPE', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '45'),
(46, '10003564', '719BA250', NULL, '10003564', 'DIRECTOR MEETING ROOM, TUBAN FACTORY OPE', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '46'),
(47, '10003565', '719BA251', NULL, '10003565', 'COMMISIONER MEETING ROOM, TUBAN FACTORY', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '47'),
(48, '10003567', '719BA253', NULL, '10003567', 'WAITING ROOM, TUBAN FACTORY OPERATIONS O', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '48'),
(49, '10003568', '719BA254', NULL, '10003568', 'LARGE MEETING ROOM, TUBAN FACTORY OPERAT', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '49'),
(50, '10003570', '719BA256', NULL, '10003570', 'SMALL MEETING ROOM 1, TUBAN FACTORY OPER', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '50'),
(51, '10003571', '719BA257', NULL, '10003571', 'SMALL MEETING ROOM 2, TUBAN FACTORY OPER', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '51'),
(52, '10003572', '719BA258', NULL, '10003572', 'DRY PANTRY ROOM, TUBAN FACTORY OPERATION', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '52'),
(53, '10003574', '719BA260', NULL, '10003574', 'MALE TOILET ROOM, TUBAN FACTORY OPERATIO', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '53'),
(54, '10003575', '719BA261', NULL, '10003575', 'FEMALE TOILET ROOM, TUBAN FACTORY OPERAT', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '54'),
(55, '10003576', '719BA262', NULL, '10003576', 'MALE MUSHOLLA ROOM, TUBAN FACTORY OPERAT', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '55'),
(56, '10003578', '719BA264', NULL, '10003578', 'ELECTRICAL PANEL ROOM, TUBAN FACTORY OPE', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '56'),
(57, '10003579', '719BA265', NULL, '10003579', 'OPERATOR ROOM, TUBAN FACTORY OPERATIONS', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '57'),
(58, '10003581', '719BA267', NULL, '10003581', 'ROOM, STORAGE, TUBAN FACTORY OPERATIONS', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '58'),
(59, '10003582', '719BA268', NULL, '10003582', 'PREFUNCTION AREA, TUBAN FACTORY OPERATIO', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '59'),
(60, '10003583', '719BA269', NULL, '10003583', 'LOBBY LIFT, TUBAN FACTORY OPERATIONS OFF', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '60'),
(61, '10003585', '719BA271', NULL, '10003585', 'ROOM, OUTDOOR AC ROOM 1, TUBAN FACTORY O', '7902', 'INST', 'SG-2307-GN-719-BA248', '2307', '61'),
(62, '10003586', '720BA01', NULL, '10003586', 'BUILDING,  TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '62'),
(63, '10003588', '720BA03', NULL, '10003588', 'MEETING ROOM B, TUBAN FACTORY OPERATIONA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '63'),
(64, '10003589', '720BA04', NULL, '10003589', 'HALL ROOM A, TUBAN FACTORY OPERATIONAL S', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '64'),
(65, '10003591', '720BA06', NULL, '10003591', 'ROOM, DEPARTMENT OF GENERAL FACILITIES O', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '65'),
(66, '10003592', '720BA07', NULL, '10003592', 'ROOM, DOCUMENT STORAGE,  TUBAN FACTORY O', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '66'),
(67, '20013224', '443DA8M01', NULL, '20013224', 'MOTOR, TERTIARY AIR DAMPER 443DA8 FR/443', '7902', 'ASEQ', 'SG-2304-KL-443-DA08', '2304', '67'),
(68, '20013225', '443DA8M02', NULL, '20013225', 'MOTOR, COOLING FAN DAMPER- 443DA8 FR/443', '7902', 'ASEQ', 'SG-2304-KL-443-DA08', '2304', '68'),
(69, '20013226', '443EH1', NULL, '20013226', 'HOIST, ELECTRIC- #1 5-TON F/CL.COOLER @', '7902', 'INST', 'SG-2304-KL-443-EH01', '2304', '69'),
(70, '20013227', '443EH1M01', NULL, '20013227', 'MOTOR, ELECTRIC HOIST', '7902', 'ASEQ', 'SG-2304-KL-443-EH01', '2304', '70'),
(71, '20013228', '443EH2', NULL, '20013228', 'HOIST, ELECTRIC- #2 20-TON F/KILN EQ. @', '7902', 'INST', 'SG-2304-KL-443-EH02', '2304', '71'),
(72, '20013229', '443EH2M01', NULL, '20013229', 'MOTOR, ELECTRIC HOIST', '7902', 'ASEQ', 'SG-2304-KL-443-EH02', '2304', '72'),
(73, '20013230', '443EL1M01', NULL, '20013230', 'MOTOR, PASENGER ELEVATOR~443EL1 @ PYROPR', '7902', 'ASEQ', 'SG-2304-KL-443-EL01', '2304', '73'),
(74, '20013231', '443EP1', NULL, '20013231', 'PRECIPITATOR, ELECTROSTATIC- @ PYROPROCE', '7902', 'INST', 'SG-2304-KL-443-EP01', '2304', '74'),
(75, '20013232', '443FN1M01', NULL, '20013232', 'MOTOR, #1 ID-FAN 443FN1 F/ILCPREH. 442PH', '7902', 'ASEQ', 'SG-2304-KL-443-FN01', '2304', '75'),
(76, '20013233', '443FN1M02', NULL, '20013233', 'MOTOR, #1 ID-FAN INCHING DRIVE 443FN1 @', '7902', 'ASEQ', 'SG-2304-KL-443-FN01', '2304', '76'),
(77, '20013235', '443FN1PC1', NULL, '20013235', 'PANEL CONVERTER', '7902', 'ASEQ', 'SG-2304-KL-443-FN01', '2304', '77'),
(78, '20013236', '443FN1T01', NULL, '20013236', 'TRANSFORMATOR, ID FAN 443FN1', '7902', 'ASEQ', 'SG-2304-KL-443-FN01', '2304', '78'),
(79, '20013237', '443FN2M01', NULL, '20013237', 'MOTOR, #1 ID-FAN 443FN2 F/ILCPREH. 443PH', '7902', 'ASEQ', 'SG-2304-KL-443-FN02', '2304', '79'),
(80, '20013238', '443FN2M03', NULL, '20013238', 'MOTOR, #1 ID-FAN INCHING DRIVE 443FN2 @', '7902', 'ASEQ', 'SG-2304-KL-443-FN02', '2304', '80'),
(81, '20013239', '443FN2PC1', NULL, '20013239', 'PANEL CONVERTER', '7902', 'ASEQ', 'SG-2304-KL-443-FN02', '2304', '81'),
(82, '20013240', '443FN4M01', NULL, '20013240', 'MOTOR, #1 TIRE COOLING FAN F/KILN 443KL1', '7902', 'ASEQ', 'SG-2304-KL-443-FN04', '2304', '82'),
(83, '20013241', '443FN4M02', NULL, '20013241', 'MOTOR, #2 TIRE COOLING FAN F/KILN 443KL1', '7902', 'ASEQ', 'SG-2304-KL-443-FN04', '2304', '83'),
(84, '20013242', '443FN4M03', NULL, '20013242', 'MOTOR, #3 TIRE COOLING FAN F/KILN 443KL1', '7902', 'ASEQ', 'SG-2304-KL-443-FN04', '2304', '84'),
(85, '20013243', '443FN4M04', NULL, '20013243', 'MOTOR, #4 TIRE COOLING FAN F/KILN 443KL1', '7902', 'ASEQ', 'SG-2304-KL-443-FN04', '2304', '85'),
(86, '20013244', '443FN5M01', NULL, '20013244', 'MOTOR, #01 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '86'),
(87, '20013245', '443FN5M02', NULL, '20013245', 'MOTOR, #02 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '87'),
(88, '20013247', '443FN5M04', NULL, '20013247', 'MOTOR, #04 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '88'),
(89, '20013248', '443FN5M05', NULL, '20013248', 'MOTOR, #05 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '89'),
(90, '20013249', '443FN5M06', NULL, '20013249', 'MOTOR, #06 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '90'),
(91, '20013250', '443FN5M07', NULL, '20013250', 'MOTOR, #07 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '91'),
(92, '20013251', '443FN5M08', NULL, '20013251', 'MOTOR, #08 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '92'),
(93, '20013252', '443FN5M09', NULL, '20013252', 'MOTOR, #09 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '93'),
(94, '20013253', '443FN5M10', NULL, '20013253', 'MOTOR, #10 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '94'),
(95, '20013254', '443FN5M11', NULL, '20013254', 'MOTOR, #11 SHELL COOLING FAN F/KILN 443K', '7902', 'ASEQ', 'SG-2304-KL-443-FN05', '2304', '95'),
(96, '20013256', '443FN6M01', NULL, '20013256', 'MOTOR, NOSE RING FAN 443FN6 F/KILN 443KL', '7902', 'ASEQ', 'SG-2304-KL-443-FN06', '2304', '96'),
(97, '20013257', '443FN7M01', NULL, '20013257', 'MOTOR, COOLING FAN #1 443FN7 F/LEFT COOL', '7902', 'ASEQ', 'SG-2304-KL-443-FN07', '2304', '97'),
(98, '20013258', '443FN8M01', NULL, '20013258', 'MOTOR, COOLING FAN #1 443FN8 F/CENTER CO', '7902', 'ASEQ', 'SG-2304-KL-443-FN08', '2304', '98'),
(99, '20013260', '443FNAM01', NULL, '20013260', 'MOTOR, COOLING FAN #2 443FNA F/LEFT COOL', '7902', 'ASEQ', 'SG-2304-KL-443-FN0A', '2304', '99'),
(100, '20013261', '443FNBM01', NULL, '20013261', 'MOTOR, COOLING FAN #2 443FNB F/RIGHT COO', '7902', 'ASEQ', 'SG-2304-KL-443-FN0B', '2304', '100'),
(101, '20013262', '443FNCM01', NULL, '20013262', 'MOTOR, COOLING FAN #3 443FNC F/LEFT COOL', '7902', 'ASEQ', 'SG-2304-KL-443-FN0C', '2304', '101'),
(102, '20013265', '443FNFM01', NULL, '20013265', 'MOTOR, COOLING FAN #P2 443FNFF/COOLER 44', '7902', 'ASEQ', 'SG-2304-KL-443-FN0F', '2304', '102'),
(103, '20013266', '443FNGM01', NULL, '20013266', 'MOTOR, COOLING FAN #5 443FNG F/COOLER 44', '7902', 'ASEQ', 'SG-2304-KL-443-FN0G', '2304', '103'),
(104, '20013268', '443FNIM01', NULL, '20013268', 'MOTOR, COOLING FAN #7 443FNI F/COOLER 44', '7902', 'ASEQ', 'SG-2304-KL-443-FN0I', '2304', '104'),
(105, '20013269', '443FNJM01', NULL, '20013269', 'MOTOR, COOLING FAN #8 443FNJ F/COOLER 44', '7902', 'ASEQ', 'SG-2304-KL-443-FN0J', '2304', '105'),
(106, '20013270', '443FNKM01', NULL, '20013270', 'MOTOR, COOLING FAN #9 443FNK F/COOLER 44', '7902', 'ASEQ', 'SG-2304-KL-443-FN0K', '2304', '106'),
(107, '20013272', '443FNRM01', NULL, '20013272', 'MOTOR, VENT FAN 443FNR (500-KVA) F/EP 44', '7902', 'ASEQ', 'SG-2304-KL-443-FN0R', '2304', '107'),
(108, '20013273', '443FNRT01', NULL, '20013273', 'TRANSFORMATOR FOR MOTOR 443FNRPOS ER #19', '7902', 'ASEQ', 'SG-2304-KL-443-FN0R', '2304', '108'),
(109, '20013274', '443FNSM01', NULL, '20013274', 'MOTOR, VENT FAN 443FNS F/EP 443EP1 @ PYR', '7902', 'ASEQ', 'SG-2304-KL-443-FN0S', '2304', '109'),
(110, '20013276', '443KL1M01', NULL, '20013276', 'MOTOR, MASTER KILN MAIN DRIVE TUBAN 3', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '110'),
(111, '20013277', '443KL1M0101', NULL, '20013277', 'MOTOR, MASTER KILN MAIN DRIVE', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '111'),
(112, '20013278', '443KL1M0102', NULL, '20013278', 'MOTOR, AUX MASTER PRIMARY FAN KILN MAIN', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '112'),
(113, '20013280', '443KL1M02', NULL, '20013280', 'MOTOR, SLAVE KILN MAIN DRIVE TUBAN 3', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '113'),
(114, '20013281', '443KL1M0201', NULL, '20013281', 'MOTOR, SLAVE KILN MAIN DRIVE', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '114'),
(115, '20013282', '443KL1M0202', NULL, '20013282', 'MOTOR, AUX SLAVE PRIMARY FAN KILN MAIN D', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '115'),
(116, '20013285', '443KL1T01', NULL, '20013285', 'TRANSFORMATOR FOR MOTORPOS ER #18 TUBAN-', '7902', 'ASEQ', 'SG-2304-KL-443-KL01', '2304', '116'),
(117, '20013286', '443PH1M01', NULL, '20013286', 'MOTOR, #1 CHANGE GATE PREHEATER VESSEL 4', '7902', 'ASEQ', 'SG-2304-KL-443-PH01', '2304', '117'),
(118, '20013287', '443PH1M02', NULL, '20013287', 'MOTOR, #2 CHANGE GATE PREHEATER VESSEL 4', '7902', 'ASEQ', 'SG-2304-KL-443-PH01', '2304', '118'),
(119, '20013289', '443SC2M01', NULL, '20013289', 'MOTOR, SCREW CONVEYOR 443SC2 F/443CN1 &', '7902', 'ASEQ', 'SG-2304-KL-443-SC02', '2304', '119'),
(120, '20013290', '443SG1M01', NULL, '20013290', 'MOTOR, SLIDE GATE- 443SG1 FR/COOLER 443C', '7902', 'ASEQ', 'SG-2304-KL-443-SG01', '2304', '120'),
(121, '20013291', '443SG2M01', NULL, '20013291', 'MOTOR, SLIDE GATE- 443SG2 FR/COOLER 443C', '7902', 'ASEQ', 'SG-2304-KL-443-SG02', '2304', '121'),
(122, '20013293', '443TS2M01', NULL, '20013293', 'MOTOR, BLOWER TELEVISION SYSTEM 443TS2 F', '7902', 'ASEQ', 'SG-2304-KL-443-TS02', '2304', '122'),
(123, '20013294', '443WP1M01', NULL, '20013294', 'MOTOR, WATERMOTOR, WATERMOTOR, WATER', '7902', 'ASEQ', 'SG-2304-KL-443-WP01', '2304', '123'),
(124, '20013295', '443WP2M01', NULL, '20013295', 'MOTOR, WATERMOTOR, WATERMOTOR, WATER', '7902', 'ASEQ', 'SG-2304-KL-443-WP02', '2304', '124'),
(125, '20013297', '443WP4M01', NULL, '20013297', 'MOTOR, WATERPUMP FAN DRIVE 443FN1', '7902', 'ASEQ', 'SG-2304-KL-443-WP04', '2304', '125'),
(126, '20013298', '443WP5M01', NULL, '20013298', 'MOTOR, PUMP BOOSTER HIDRANT', '7902', 'ASEQ', 'SG-2304-KL-443-WP05', '2304', '126'),
(127, '20013300', '443WS1MO2', NULL, '20013300', 'MOTOR, # 2 WATER SPRAY SYSTEMF/ 443WS1', '7902', 'ASEQ', 'SG-2304-KL-443-WS01', '2304', '127'),
(128, '20014461', '625FC1M02', NULL, '20014461', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '128'),
(129, '20014462', '625FC1M03', NULL, '20014462', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '129'),
(130, '20014463', '625FC1M04', NULL, '20014463', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '130'),
(131, '20014464', '625FC1M05', NULL, '20014464', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '131'),
(132, '20014465', '625FC1M06', NULL, '20014465', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '132'),
(133, '20014466', '625FC1M07', NULL, '20014466', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '133'),
(134, '20014467', '625FC1M08', NULL, '20014467', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC01', '2304', '134'),
(135, '20014468', '625FC2M01', NULL, '20014468', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '135'),
(136, '20014469', '625FC2M02', NULL, '20014469', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '136'),
(137, '20014470', '625FC2M03', NULL, '20014470', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '137'),
(138, '20014471', '625FC2M04', NULL, '20014471', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '138'),
(139, '20014473', '625FC2M06', NULL, '20014473', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '139'),
(140, '20014474', '625FC2M07', NULL, '20014474', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '140'),
(141, '20014475', '625FC2M08', NULL, '20014475', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-625-FC02', '2304', '141'),
(142, '20014476', '625FC5M01', NULL, '20014476', 'MOTOR, FLOW CONTR.GATE- #1 F/AIRSL.625AE', '7902', 'ASEQ', 'SG-2304-PC-625-FC05', '2304', '142'),
(143, '20014477', '625FC5M02', NULL, '20014477', 'MOTOR, FLOW CONTR.GATE- #2 F/AIRSL.625AE', '7902', 'ASEQ', 'SG-2304-PC-625-FC05', '2304', '143'),
(144, '20014478', '625FC5M03', NULL, '20014478', 'MOTOR, FLOW CONTR.GATE- #3 F/AIRSL.625AE', '7902', 'ASEQ', 'SG-2304-PC-625-FC05', '2304', '144'),
(145, '20014479', '625FC5M04', NULL, '20014479', 'MOTOR, FLOW CONTR.GATE- #4 F/AIRSL.625AE', '7902', 'ASEQ', 'SG-2304-PC-625-FC05', '2304', '145'),
(146, '20014481', '625FN11M01', NULL, '20014481', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS07', '2304', '146'),
(147, '20014482', '625FN1M01', NULL, '20014482', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS01', '2304', '147'),
(148, '20014484', '625FN3M01', NULL, '20014484', 'MOTOR, AXIAL MOTOR, AXIAL MOTOR, AXIAL', '7902', 'ASEQ', 'SG-2304-PC-625-BI01', '2304', '148'),
(149, '20014485', '625FN4M01', NULL, '20014485', 'MOTOR, AXIAL MOTOR, AXIAL MOTOR, AXIAL', '7902', 'ASEQ', 'SG-2304-PC-625-BI02', '2304', '149'),
(150, '20014486', '625FN5M01', NULL, '20014486', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS03', '2304', '150'),
(151, '20014487', '625FN6M01', NULL, '20014487', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS04', '2304', '151'),
(152, '20014488', '625FN7M01', NULL, '20014488', 'MOTOR, ID FAN- 625FN7 F/625BF1 @ CEM.FEE', '7902', 'ASEQ', 'SG-2304-PC-625-AS04', '2304', '152'),
(153, '20014489', '625FN8M01', NULL, '20014489', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS05', '2304', '153'),
(154, '20014491', '625FNBM01', NULL, '20014491', 'MOTOR, ID MOTOR, ID MOTOR, ID', '7902', 'ASEQ', 'SG-2304-PC-627-BF01', '2304', '154'),
(155, '20014492', '625FNDM01', NULL, '20014492', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS07', '2304', '155'),
(156, '20014493', '625FNEM01', NULL, '20014493', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-625-AS08', '2304', '156'),
(157, '20014495', '625RF2M01', NULL, '20014495', 'MOTOR, ROTARY FEEDER- 625RF2 F/DC 625BF2', '7902', 'ASEQ', 'SG-2303-PC-625-BF02', '2303', '157'),
(158, '20014497', '626BE1M01', 'media/dtupload/equipment/maarif_20014497_2019011010071049957.jpg', '20014497', 'MOTOR, BELT BUCKET ELEV. 626BE1 @ CEM.FE', '7902', 'ASEQ', 'SG-2303-PC-626-BE01', '2303', '158'),
(159, '20014499', '626FC1M01', NULL, '20014499', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC01', '2304', '159'),
(160, '20014500', '626FC1M02', NULL, '20014500', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC01', '2304', '160'),
(161, '20014501', '626FC1M03', NULL, '20014501', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC01', '2304', '161'),
(162, '20014503', '626FC1M05', NULL, '20014503', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC01', '2304', '162'),
(163, '20014504', '626FC1M06', NULL, '20014504', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC01', '2304', '163'),
(164, '20014505', '626FC1M07', NULL, '20014505', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC01', '2304', '164'),
(165, '20014507', '626FC2M01', NULL, '20014507', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC02', '2304', '165'),
(166, '20014509', '626FC2M03', NULL, '20014509', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC02', '2304', '166'),
(167, '20014511', '626FC2M05', NULL, '20014511', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC02', '2304', '167'),
(168, '20014512', '626FC2M06', NULL, '20014512', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC02', '2304', '168'),
(169, '20014513', '626FC2M07', NULL, '20014513', 'MOTOR, FLOW MOTOR, FLOW MOTOR, FLOW', '7902', 'ASEQ', 'SG-2304-PC-626-FC02', '2304', '169'),
(170, '20014515', '626FN10M01', NULL, '20014515', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-626-AS05', '2304', '170'),
(171, '20014516', '626FN11M01', NULL, '20014516', 'MOTOR, HIGH MOTOR, HIGH MOTOR, HIGH', '7902', 'ASEQ', 'SG-2304-PC-626-AS05', '2304', '171'),
(172, '20014518', '626FN2M01', NULL, '20014518', 'MOTOR, HIGH PRESS.BLOWER 626FN2 @  CEM.F', '7902', 'ASEQ', 'SG-2304-PC-626-AS02', '2304', '172'),
(173, '20014519', '626RF1M01', NULL, '20014519', 'MOTOR, ROTARY FEEDER- 626RF1 F/626BF1 @', '7902', 'ASEQ', 'SG-2304-PC-626-BF01', '2304', '173'),
(174, '20014520', '627BE1M01', NULL, '20014520', 'MOTOR, BELT BUCKET ELEV. 627BE1 @ CEM.FE', '7902', 'ASEQ', 'SG-2304-PC-627-BE01', '2304', '174'),
(175, '20014522', '627EL1M01', NULL, '20014522', 'MOTOR, PERSONAL ELEVATORPACKING AREATUBA', '7902', 'ASEQ', 'SG-2304-PC-627-EL01', '2304', '175'),
(176, '20014523', '628BE1M01', NULL, '20014523', 'MOTOR, BELT BUCKET ELEV. 628BE1 @ CEM.FE', '7902', 'ASEQ', 'SG-2304-PC-628-BE01', '2304', '176'),
(177, '20014524', '628BE1M02', NULL, '20014524', 'MOTOR, INCHING DRIVE BELT BUCKET ELEV. 6', '7902', 'ASEQ', 'SG-2304-PC-628-BE01', '2304', '177'),
(178, '20014526', '631TO1M02', NULL, '20014526', 'MOTOR, GEAR- F/RUNNING GEAR LORRY STERN', '7902', 'ASEQ', 'SG-2302-PC-631-TO01', '2302', '178'),
(179, '20014527', '631TO2M01', NULL, '20014527', 'MOTOR, MOBILE FLAT BELT CONVEYOR- 6314E2', '7902', 'ASEQ', 'SG-2302-PC-631-TO02', '2302', '179'),
(180, '20014528', '631TO2M02', NULL, '20014528', 'MOTOR, GEAR- F/RUNNING GEAR LORRY STERN', '7902', 'ASEQ', 'SG-2302-PC-631-TO02', '2302', '180'),
(181, '20014529', '631BC1M01', NULL, '20014529', 'MOTOR, BAG DISCHARGE BELT CONVEYOR- F/PA', '7902', 'ASEQ', 'SG-2302-PC-631-BC01', '2302', '181'),
(182, '20014531', '631BC3M01', NULL, '20014531', 'MOTOR, BELT CONVEYOR- DRIVEN ROLLER WAY', '7902', 'ASEQ', 'SG-2302-PC-631-BC03', '2302', '182'),
(183, '20014532', '631BC4M01', NULL, '20014532', 'MOTOR, FLAT BELT CONVEYOR- F/PACKING MAC', '7902', 'ASEQ', 'SG-2302-PC-631-BC04', '2302', '183'),
(184, '20014535', '631BP1M01', NULL, '20014535', 'MOTOR, BAG APPLICATOR/PLACER RADIMAT - P', '7902', 'ASEQ', 'SG-2302-PC-631-BP01', '2302', '184'),
(185, '20014536', '631BR1M01', NULL, '20014536', 'MOTOR, BAG REJECTING DEVICE- ROLLER WAY', '7902', 'ASEQ', 'SG-2302-PC-631-BR01', '2302', '185'),
(186, '20014537', '631BR2M01', NULL, '20014537', 'MOTOR, BAG HOISTING REJECTING DEVICE- F/', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '186'),
(187, '10001964', 'FF-9', NULL, '10001964', 'RUMAH DINAS BLOK FF-9 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '187'),
(188, '10001965', 'FF-10', NULL, '10001965', 'RUMAH DINAS BLOK FF-10 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '188'),
(189, '10001966', 'FF-11', NULL, '10001966', 'RUMAH DINAS BLOK FF-11 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '189'),
(190, '10001967', 'FF-12', NULL, '10001967', 'RUMAH DINAS BLOK FF-12 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '190'),
(191, '10001968', 'FF-14', NULL, '10001968', 'RUMAH DINAS BLOK FF-14 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '191'),
(192, '10001969', 'FF-15', NULL, '10001969', 'RUMAH DINAS BLOK FF-15 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '192'),
(193, '10001970', 'FF-16', NULL, '10001970', 'RUMAH DINAS BLOK FF-16 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '193'),
(194, '10001971', 'FF-17', NULL, '10001971', 'RUMAH DINAS BLOK FF-17 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '194'),
(195, '10001972', 'FF-18', NULL, '10001972', 'RUMAH DINAS BLOK FF-18 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '195'),
(196, '10001973', 'FF-19', NULL, '10001973', 'RUMAH DINAS BLOK FF-19 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '196'),
(197, '10001974', 'FF-20', NULL, '10001974', 'RUMAH DINAS BLOK FF-20 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '197'),
(198, '10001975', 'FF-21', NULL, '10001975', 'RUMAH DINAS BLOK FF-21 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-FF', '2307', '198'),
(199, '10001976', 'CC-2', NULL, '10001976', 'RUMAH DINAS BLOK CC-2 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '199'),
(200, '10001977', 'CC-4', NULL, '10001977', 'RUMAH DINAS BLOK CC-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '200'),
(201, '10001978', 'CC-6', NULL, '10001978', 'RUMAH DINAS BLOK CC-6 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '201'),
(202, '10001979', 'CC-8', NULL, '10001979', 'RUMAH DINAS BLOK CC-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '202'),
(203, '10001980', 'CC-10', NULL, '10001980', 'RUMAH DINAS BLOK CC-10 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '203'),
(204, '10001981', 'CC-12', NULL, '10001981', 'RUMAH DINAS BLOK CC-12 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '204'),
(205, '10001982', 'CC-14', NULL, '10001982', 'RUMAH DINAS BLOK CC-14 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '205'),
(206, '10001983', 'CC-16', NULL, '10001983', 'RUMAH DINAS BLOK CC-16 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '206'),
(207, '10001984', 'CC-18', NULL, '10001984', 'RUMAH DINAS BLOK CC-18 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '207'),
(208, '10001985', 'CC-20', NULL, '10001985', 'RUMAH DINAS BLOK CC-20 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-CC', '2307', '208'),
(209, '10001986', 'DD-2', NULL, '10001986', 'RUMAH DINAS BLOK DD-2 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '209'),
(210, '10001987', 'DD-4', NULL, '10001987', 'RUMAH DINAS BLOK DD-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '210'),
(211, '10001988', 'DD-6', NULL, '10001988', 'RUMAH DINAS BLOK DD-6 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '211'),
(212, '10001989', 'DD-8', NULL, '10001989', 'RUMAH DINAS BLOK DD-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '212'),
(213, '10001990', 'DD-10', NULL, '10001990', 'RUMAH DINAS BLOK DD-10 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '213'),
(214, '10001991', 'DD-12', NULL, '10001991', 'RUMAH DINAS BLOK DD-12 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '214'),
(215, '10001992', 'DD-14', NULL, '10001992', 'RUMAH DINAS BLOK DD-14 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '215'),
(216, '10001993', 'DD-16', NULL, '10001993', 'RUMAH DINAS BLOK DD-16 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '216'),
(217, '10001994', 'DD-18', NULL, '10001994', 'RUMAH DINAS BLOK DD-18 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-DD', '2307', '217'),
(218, '10001995', 'GG-1', NULL, '10001995', 'RUMAH DINAS BLOK GG-1 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '218'),
(219, '10001996', 'GG-2', NULL, '10001996', 'RUMAH DINAS BLOK GG-2 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '219'),
(220, '10001998', 'GG-4', NULL, '10001998', 'RUMAH DINAS BLOK GG-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '220'),
(221, '10001999', 'GG-5', NULL, '10001999', 'RUMAH DINAS BLOK GG-5 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '221'),
(222, '10002000', 'GG-6', NULL, '10002000', 'RUMAH DINAS BLOK GG-6 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '222'),
(223, '10002002', 'GG-8', NULL, '10002002', 'RUMAH DINAS BLOK GG-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '223'),
(224, '10002003', 'GG-9', NULL, '10002003', 'RUMAH DINAS BLOK GG-9 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '224'),
(225, '10002005', 'GG-11', NULL, '10002005', 'RUMAH DINAS BLOK GG-11 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '225'),
(226, '10002006', 'GG-12', NULL, '10002006', 'RUMAH DINAS BLOK GG-12 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '226'),
(227, '10002007', 'GG-14', NULL, '10002007', 'RUMAH DINAS BLOK GG-14 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '227'),
(228, '10002009', 'GG-16', NULL, '10002009', 'RUMAH DINAS BLOK GG-16 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '228'),
(229, '10002010', 'GG-17', NULL, '10002010', 'RUMAH DINAS BLOK GG-17 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '229'),
(230, '10002012', 'GG-19', NULL, '10002012', 'RUMAH DINAS BLOK GG-19 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '230'),
(231, '10002013', 'GG-20', NULL, '10002013', 'RUMAH DINAS BLOK GG-20 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '231'),
(232, '10002014', 'GG-21', NULL, '10002014', 'RUMAH DINAS BLOK GG-21 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-GG', '2307', '232'),
(233, '10002016', 'JJ-2', NULL, '10002016', 'RUMAH DINAS BLOK JJ-2 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-JJ', '2307', '233'),
(234, '10002017', 'JJ-3', NULL, '10002017', 'RUMAH DINAS BLOK JJ-3 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-JJ', '2307', '234'),
(235, '10002019', 'JJ-5', NULL, '10002019', 'RUMAH DINAS BLOK JJ-5 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-JJ', '2307', '235'),
(236, '10002020', 'JJ-6', NULL, '10002020', 'RUMAH DINAS BLOK JJ-6 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-JJ', '2307', '236'),
(237, '10002021', 'JJ-7', NULL, '10002021', 'RUMAH DINAS BLOK JJ-7 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-JJ', '2307', '237'),
(238, '10002023', 'KK-1', NULL, '10002023', 'RUMAH DINAS BLOK KK-1 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-KK', '2307', '238'),
(239, '10002024', 'KK-2', NULL, '10002024', 'RUMAH DINAS BLOK KK-2 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-KK', '2307', '239'),
(240, '10002026', 'KK-4', NULL, '10002026', 'RUMAH DINAS BLOK KK-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-KK', '2307', '240'),
(241, '10002027', 'KK-5', NULL, '10002027', 'RUMAH DINAS BLOK KK-5 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-KK', '2307', '241'),
(242, '10002028', 'KK-6', NULL, '10002028', 'RUMAH DINAS BLOK KK-6 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-KK', '2307', '242'),
(243, '10002030', 'KK-8', NULL, '10002030', 'RUMAH DINAS BLOK KK-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-KK', '2307', '243'),
(244, '10002031', 'LL-1', NULL, '10002031', 'RUMAH DINAS BLOK LL-1 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '244'),
(245, '10002033', 'LL-3', NULL, '10002033', 'RUMAH DINAS BLOK LL-3 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '245'),
(246, '10002034', 'LL-4', NULL, '10002034', 'RUMAH DINAS BLOK LL-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '246'),
(247, '10002035', 'LL-5', NULL, '10002035', 'RUMAH DINAS BLOK LL-5 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '247'),
(248, '10002037', 'LL-7', NULL, '10002037', 'RUMAH DINAS BLOK LL-7 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '248'),
(249, '10002038', 'LL-8', NULL, '10002038', 'RUMAH DINAS BLOK LL-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '249'),
(250, '10002040', 'LL-10', NULL, '10002040', 'RUMAH DINAS BLOK LL-10 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-LL', '2307', '250'),
(251, '10002041', 'MM-1', NULL, '10002041', 'RUMAH DINAS BLOK MM-1 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '251'),
(252, '10002043', 'MM-3', NULL, '10002043', 'RUMAH DINAS BLOK MM-3 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '252'),
(253, '10002044', 'MM-4', NULL, '10002044', 'RUMAH DINAS BLOK MM-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '253'),
(254, '10002045', 'MM-5', NULL, '10002045', 'RUMAH DINAS BLOK MM-5 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '254'),
(255, '10002047', 'MM-7', NULL, '10002047', 'RUMAH DINAS BLOK MM-7 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '255'),
(256, '10002048', 'MM-8', NULL, '10002048', 'RUMAH DINAS BLOK MM-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '256'),
(257, '10002050', 'MM-10', NULL, '10002050', 'RUMAH DINAS BLOK MM-10 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-MM', '2307', '257'),
(258, '10002051', 'NN-1', NULL, '10002051', 'RUMAH DINAS BLOK NN-1 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '258'),
(259, '10002052', 'NN-2', NULL, '10002052', 'RUMAH DINAS BLOK NN-2 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '259'),
(260, '10002054', 'NN-4', NULL, '10002054', 'RUMAH DINAS BLOK NN-4 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '260'),
(261, '10002055', 'NN-5', NULL, '10002055', 'RUMAH DINAS BLOK NN-5 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '261'),
(262, '10002057', 'NN-7', NULL, '10002057', 'RUMAH DINAS BLOK NN-7 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '262'),
(263, '10002058', 'NN-8', NULL, '10002058', 'RUMAH DINAS BLOK NN-8 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '263'),
(264, '10003593', '720BA08', NULL, '10003593', 'ROOM, GM OF COMMUNITY DEVELOPMENT OFFICE', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '264'),
(265, '10003594', '720BA09', NULL, '10003594', 'MEETING ROOM DEPARTMENT OF GENERAL FACIL', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '265'),
(266, '10003595', '720BA10', NULL, '10003595', 'ROOM, DEPARTMENT OF COMMUNITY DEVELOPMEN', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '266'),
(267, '10003596', '720BA11', NULL, '10003596', 'ROOM, MEETING OF COMMUNITY DEVELOPMENT,', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '267'),
(268, '10003597', '720BA12', NULL, '10003597', 'ROOM, DOCUMENT STORAGE OF COMMUNITY DEVE', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '268'),
(269, '10003598', '720BA13', NULL, '10003598', 'ROOM, GM OF GENERAL FACILITIES OF COMMUN', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '269'),
(270, '10003599', '720BA14', NULL, '10003599', 'TERRACE, DEPARTEMENT OF COMMUNITY DEVELO', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '270'),
(271, '10003600', '720BA15', NULL, '10003600', 'TOILET, KANTOR DEPT PENGELOLAAN SOSIAL &', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '271'),
(272, '10003601', '720BA16', NULL, '10003601', 'FEMALE TOILET ROOM, TUBAN FACTORY OPERAT', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '272'),
(273, '10003602', '720BA17', NULL, '10003602', 'MALE TOILET ROOM, TUBAN FACTORY OPERATIO', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '273'),
(274, '10003603', '720BA18', NULL, '10003603', 'ROOM, PANTRY, TUBAN FACTORY OPERATIONAL', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA01', '2307', '274'),
(275, '10003604', '720BA19', NULL, '10003604', 'BUILDING,  TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '275'),
(276, '10003605', '720BA20', NULL, '10003605', 'STAGE, TUBAN FACTORY OPERATIONAL SUPPORT', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '276'),
(277, '10003606', '720BA21', NULL, '10003606', 'ROOM, FITTING ROOMS 1, TUBAN FACTORY OPE', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '277'),
(278, '10003607', '720BA22', NULL, '10003607', 'ROOM, FITTING ROOMS 2, TUBAN FACTORY OPE', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '278'),
(279, '10003608', '720BA23', NULL, '10003608', 'ROOM, PREPARATION ROOM, TUBAN FACTORY OP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '279'),
(280, '10003609', '720BA24', NULL, '10003609', 'FUNCTION HALL, TUBAN FACTORY OPERATIONAL', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '280'),
(281, '10003610', '720BA25', NULL, '10003610', 'ROOM, STORAGE 1, TUBAN FACTORY OPERATION', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '281'),
(282, '10003611', '720BA26', NULL, '10003611', 'ROOM, STORAGE 2, TUBAN FACTORY OPERATION', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '282'),
(283, '10003612', '720BA27', NULL, '10003612', 'ROOM, PANTRY,TUBAN FACTORY OPERATIONAL S', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '283'),
(284, '10003613', '720BA28', NULL, '10003613', 'SELASAR 1, TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '284'),
(285, '10003614', '720BA29', NULL, '10003614', 'SELASAR 2, TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '285'),
(286, '10003616', '720BA31', NULL, '10003616', 'PREFUNCTION HALL B, TUBAN FACTORY OPERAT', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '286'),
(287, '10003617', '720BA32', NULL, '10003617', 'ROOM, FEMALE TOILET, TUBAN FACTORY OPERA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '287'),
(288, '10003619', '720BA34', NULL, '10003619', 'SKY BRIDGE, TUBAN FACTORY OPERATIONAL SU', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA19', '2307', '288'),
(289, '10003620', '720BA35', NULL, '10003620', 'BUILDING,   2ND FLOOR TUBAN FACTORY OPER', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '289'),
(290, '10003622', '720BA37', NULL, '10003622', 'CANTEEN, TUBAN FACTORY OPERATIONAL SUPPO', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '290'),
(291, '10003623', '720BA38', NULL, '10003623', 'MAIN KITCHEN, TUBAN FACTORY OPERATIONAL', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '291'),
(292, '10003625', '720BA40', NULL, '10003625', 'LOADING DOCK, TUBAN FACTORY OPERATIONAL', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '292'),
(293, '10003626', '720BA41', NULL, '10003626', 'DRIVER WAITING ROOM, TUBAN FACTORY OPERA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '293'),
(294, '10003627', '720BA42', NULL, '10003627', 'CLEANING SERVICE ROOM , TUBAN FACTORY OP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '294'),
(295, '10003629', '720BA44', NULL, '10003629', 'STORAGE CLEANING SERVICE, TUBAN FACTORY', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '295'),
(296, '10003630', '720BA45', NULL, '10003630', 'STORAGE HOUSEHOLD, TUBAN FACTORY OPERATI', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '296'),
(297, '10003632', '720BA47', NULL, '10003632', 'STORAGE 1, TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '297'),
(298, '10003633', '720BA48', NULL, '10003633', 'STORAGE 2, TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '298'),
(299, '10003635', '720BA50', NULL, '10003635', 'ROOM, FEMALE TOILET, TUBAN FACTORY OPERA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '299'),
(300, '10003636', '720BA51', NULL, '10003636', 'ROOM, MALE TOILET, TUBAN FACTORY OPERATI', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '300'),
(301, '10003638', '720BA53', NULL, '10003638', 'FEMALE MUSHOLLA ROOM, TUBAN FACTORY OPER', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '301'),
(302, '10003639', '720BA54', NULL, '10003639', 'CAR PARK , TUBAN FACTORY OPERATIONAL SUP', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA35', '2307', '302'),
(303, '10003641', '720BA56', NULL, '10003641', 'ROOM, TRAINING, TUBAN FACTORY OPERATIONA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '303'),
(304, '10003642', '720BA57', NULL, '10003642', 'ROOM, INSTRUCTUR ROOM, TUBAN FACTORY OPE', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '304'),
(305, '10003644', '720BA59', NULL, '10003644', 'ROOM, MALE TOILET, TUBAN FACTORY OPERATI', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '305'),
(306, '10003645', '720BA60', NULL, '10003645', 'ROOM, FEMALE TOILET, TUBAN FACTORY OPERA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '306'),
(307, '10003647', '720BA62', NULL, '10003647', 'ROOM, CLASS ROOM A, TUBAN FACTORY OPERAT', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '307'),
(308, '10003648', '720BA63', NULL, '10003648', 'ROOM, CLASS ROOM B, TUBAN FACTORY OPERAT', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '308'),
(309, '10003650', '720BA65', NULL, '10003650', 'ROOM, CLASS ROOM D, TUBAN FACTORY OPERAT', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '309'),
(310, '10003651', '720BA66', NULL, '10003651', 'ROOM, BUREAU OF TRAINING, TUBAN FACTORY', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '310'),
(311, '10003653', '720BA68', NULL, '10003653', 'ROOM, MGR OF TRAINING 2, TUBAN FACTORY O', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '311'),
(312, '10003654', '720BA69', NULL, '10003654', 'ROOM, MEETING ROOM OF TRAINING, TUBAN FA', '7902', 'DLFL INAC INST', 'SG-2307-GN-720-BA55', '2307', '312'),
(313, '10003717', '821007ABA1', NULL, '10003717', 'BUILDING, ELECTRICAL ROOM 7 A', '7902', 'INST', 'SG-2302-EL-821-ER07A', '2302', '313'),
(314, '10003718', '604GY01', NULL, '10003718', 'GALERY AND SUPPORT BELT CONVEYOR', '2902', 'INST', 'SG-2404-CT-604-GY01', '2404', '314'),
(315, '10003719', '604BC02ME1', NULL, '10003719', 'BELT CONVEYOR MECHANICAL EQUIPMENT BELT', '2902', 'ASEQ', 'SG-2404-CT-604-BC02', '2404', '315'),
(316, '10003861', '512BC10SS1', NULL, '10003861', 'STRUCTURES, STEEL  F/ BELT CONVEYOR, TRO', '2902', 'ASEQ', 'SG-2303-FM-512-BC10', '2303', '316'),
(317, '10003862', '512BC11SS1', NULL, '10003862', 'STRUCTURES, STEEL  F/ BELT CONVEYOR, TRO', '2902', 'ASEQ', 'SG-2303-FM-512-BC11', '2303', '317'),
(318, '10003883', '717BA25', NULL, '10003883', 'KANTOR BIRO PENGEND GANGGUAN OPERASIONAL', '7902', 'INST', 'SG-2307-GN-717-BA25', '2307', '318'),
(319, '10003886', '717BA30', NULL, '10003886', 'BUILDING, PLANT WAREHOUSE TUBAN', '7902', 'INST', 'SG-2307-GN-717-BA30', '2307', '319'),
(320, '10003887', '251BG1', NULL, '10003887', 'BUILDING, CLAY CRUSHER TUBAN 1', '7902', 'INST', 'SG-2302-CR-251-BG01', '2302', '320'),
(321, '10003888', '231BG1', NULL, '10003888', 'BUILDING, LIMESTONE CRUSHER TUBAN 1', '7902', 'INST', 'SG-2302-CR-231-BG01', '2302', '321'),
(322, '10003890', '241BG2', NULL, '10003890', 'BUILDING, TRANSFER LIMESTONE TRANSPORT', '7902', 'INST', 'SG-2302-CR-241-BG02', '2302', '322'),
(323, '10003891', '717BA15', NULL, '10003891', 'GEDUNG ARSIP PABRIK TUBAN', '7902', 'INST', 'SG-2307-GN-717-BA15', '2307', '323'),
(324, '10003893', '717BA17', NULL, '10003893', 'RUANG ABSENSI TUBAN', '7902', 'INST', 'SG-2307-GN-717-BA17', '2307', '324'),
(325, '10003894', '717BA18', NULL, '10003894', 'CENTRAL PARKIR SPD MOTOR TUBAN', '7902', 'INST', 'SG-2307-GN-717-BA18', '2307', '325'),
(326, '10003895', '717BA19', NULL, '10003895', 'GAPURA PINTU MASUK PABRIK TUBAN', '7902', 'INST', 'SG-2307-GN-717-BA19', '2307', '326'),
(327, '20013301', '443WS1MO3', NULL, '20013301', 'MOTOR, # 3 WATER SPRAY SYSTEMF/ 443WS1', '7902', 'ASEQ', 'SG-2304-KL-443-WS01', '2304', '327'),
(328, '20013302', '451CV5M01', NULL, '20013302', 'MOTOR,CHAIN CONVEYOR- #5 FR/E.PREC.441EP', '7902', 'ASEQ', 'SG-2302-KL-451-CV05', '2302', '328'),
(329, '20013303', '451CV6M01', NULL, '20013303', 'MOTOR,CHAIN CONVEYOR- #6 FR/E.PREC.441EP', '7902', 'ASEQ', 'SG-2302-KL-451-CV06', '2302', '329'),
(330, '20013304', '451CV7M01', NULL, '20013304', 'MOTOR,CHAIN CONVEYOR- #7 FR/E.PREC.441EP', '7902', 'ASEQ', 'SG-2302-KL-451-CV07', '2302', '330'),
(331, '20013305', '451DB1M01', NULL, '20013305', 'MOTOR, PAN CONVEYOR CLINKER-#01 FR/CLINK', '7902', 'ASEQ', 'SG-2302-KL-451-DB01', '2302', '331'),
(332, '20013307', '451DB3M01', NULL, '20013307', 'MOTOR, PAN CONVEYOR CLINKER -#03 T/CLINK', '7902', 'ASEQ', 'SG-2302-KL-451-DB03', '2302', '332'),
(333, '20013308', '451EH1', NULL, '20013308', 'HOIST, ELECTRIC #01 F/CLINKER#01 F/CLINK', '7902', 'INST', 'SG-2302-KL-451-EH01', '2302', '333'),
(334, '20013309', '451EH1M01', NULL, '20013309', 'MOTOR, ELECTRIC HOIST#01 F/CLINKER#01 F/', '7902', 'ASEQ', 'SG-2302-KL-451-EH01', '2302', '334'),
(335, '20013310', '451EH1M02', NULL, '20013310', 'MOTOR, TROLEY ELECTRIC HOIST#01 F/CLINKE', '7902', 'ASEQ', 'SG-2302-KL-451-EH01', '2302', '335'),
(336, '20013311', '451EH2', NULL, '20013311', 'HOIST, ELECTRIC #02 F/CLINKER#02 F/CLINK', '7902', 'INST', 'SG-2302-KL-451-EH02', '2302', '336'),
(337, '20013312', '451EH2M01', NULL, '20013312', 'MOTOR, ELECTRIC HOIST#02 F/CLINKER#01 F/', '7902', 'ASEQ', 'SG-2302-KL-451-EH02', '2302', '337'),
(338, '20013313', '451EH2M02', NULL, '20013313', 'MOTOR, TROLEY ELECTRIC HOIST#02 F/CLINKE', '7902', 'ASEQ', 'SG-2302-KL-451-EH02', '2302', '338'),
(339, '20013314', '451EL1M01', NULL, '20013314', 'MOTOR,LIFT BARANG U/ DOME STORAGE', '7902', 'ASEQ', 'SG-2302-KL-451-EL01', '2302', '339'),
(340, '20013315', '451FN1M01', NULL, '20013315', 'MOTOR, ID-FAN DUST COLL.CLINKER- F/COOLE', '7902', 'ASEQ', 'SG-2302-KL-451-BF01', '2302', '340'),
(341, '20013316', '451FN3M01', NULL, '20013316', 'MOTOR, ID-FAN DUST COLLECTOR CLINKER - F', '7902', 'ASEQ', 'SG-2302-KL-451-BF03', '2302', '341'),
(342, '20013317', '451FN4M01', NULL, '20013317', 'MOTOR, ID-FAN DUST COLLECTOR CLINKER - F', '7902', 'ASEQ', 'SG-2302-KL-451-BF04', '2302', '342'),
(343, '20013318', '451FN5M01', NULL, '20013318', 'MOTOR, ID-FAN DUST COLLECTOR CLINKER - F', '7902', 'ASEQ', 'SG-2302-KL-451-BF05', '2302', '343'),
(344, '20013319', '451LM1', NULL, '20013319', 'MONITOR, LEVEL SYSTEM @CLINKERSYSTEM @ C', '7902', 'INST', 'SG-2302-KL-451-LM01', '2302', '344'),
(345, '20013320', '451RF1M01', NULL, '20013320', 'MOTOR, ROTARY FEEDER @ CLINKER SCREW CON', '7902', 'ASEQ', 'SG-2302-KL-451-RF01', '2302', '345'),
(346, '20013321', '451RF2M01', NULL, '20013321', 'MOTOR, ROTARY FEEDER @ CLINKER SCREW CON', '7902', 'ASEQ', 'SG-2302-KL-451-RF02', '2302', '346'),
(347, '20013322', '451RF3M01', NULL, '20013322', 'MOTOR, ROTARY FEEDER @ CLINKER SCREW CON', '7902', 'ASEQ', 'SG-2302-KL-451-RF03', '2302', '347'),
(348, '20013323', '451RF4M01', NULL, '20013323', 'MOTOR, ROTARY FEEDER @ CLINKER SCREW CON', '7902', 'ASEQ', 'SG-2302-KL-451-RF04', '2302', '348'),
(349, '20013324', '451SC5M01', NULL, '20013324', 'MOTOR, SCREW CONVEYOR CLINKER- #05 FR/EL', '7902', 'ASEQ', 'SG-2302-KL-451-SC05', '2302', '349'),
(350, '20013325', '451SC6M01', NULL, '20013325', 'MOTOR, SCREW CONVEYOR CLINKER- #06 FR/EL', '7902', 'ASEQ', 'SG-2302-KL-451-SC06', '2302', '350'),
(351, '20013326', '451SC7M01', NULL, '20013326', 'MOTOR, SCREW CONVEYOR CLINKER-#07 T/CLIN', '7902', 'ASEQ', 'SG-2302-KL-451-SC07', '2302', '351');
INSERT INTO `she_master_funcloc` (`id`, `idequpment`, `equptname`, `equptimage`, `sapequipnum`, `equpmentcode`, `pplant`, `status`, `funcloc`, `mplant`, `rnum`) VALUES
(352, '20013327', '451SG1MO1', NULL, '20013327', 'MOTOR, SLIDE GATE #01 F/PAN CONV.DISCHAR', '7902', 'ASEQ', 'SG-2302-KL-451-SG01', '2302', '352'),
(353, '20013328', '451SG1MO2', NULL, '20013328', 'MOTOR, SLIDE GATE #02 F/PAN CONV.DISCHAR', '7902', 'ASEQ', 'SG-2302-KL-451-SG01', '2302', '353'),
(354, '20013329', '451SG3M01', NULL, '20013329', 'MOTOR, SLIDE GATE #03 F/PAN CONV.DISCHAR', '7902', 'ASEQ', 'SG-2302-KL-451-SG03', '2302', '354'),
(355, '20013331', '451SJ1', NULL, '20013331', 'PUMP, SUMP- F/CLINKER COOLER BUILDING', '7902', 'INST', 'SG-2302-KL-451-SJ01', '2302', '355'),
(356, '20013333', '451SM1M01', NULL, '20013333', 'MOTOR, SAMPLER #01 F/CLINKER COOLER', '7902', 'ASEQ', 'SG-2302-KL-451-SM01', '2302', '356'),
(357, '20013335', '452CV1M01', NULL, '20013335', 'MOTOR, CHAIN CONVEYOR #1 @ CLINKER CONVE', '7902', 'ASEQ', 'SG-2303-KL-452-CV01', '2303', '357'),
(358, '20013336', '452CV2M01', NULL, '20013336', 'MOTOR, CHAIN CONVEYOR #2 @ CLINKER CONVE', '7902', 'ASEQ', 'SG-2303-KL-452-CV02', '2303', '358'),
(359, '20013337', '452CV3M01', NULL, '20013337', 'MOTOR, CHAIN CONVEYOR #3 @ CLINKER CONVE', '7902', 'ASEQ', 'SG-2303-KL-452-CV03', '2303', '359'),
(360, '20013339', '452CV5M01', NULL, '20013339', 'MOTOR, CHAIN CONVEYOR #5 @ CLINKER CONVE', '7902', 'ASEQ', 'SG-2303-KL-452-CV05', '2303', '360'),
(361, '20013340', '452DB1M01', NULL, '20013340', 'MOTOR, PAN CONVEYOR #1 FR/COOLER 452CC1', '7902', 'ASEQ', 'SG-2303-KL-452-DB01', '2303', '361'),
(362, '20013341', '452DB2M01', NULL, '20013341', 'MOTOR, PAN CONVEYOR #2 FR/COOLER 452CC1', '7902', 'ASEQ', 'SG-2303-KL-452-DB02', '2303', '362'),
(363, '20013343', '452DB4M01', NULL, '20013343', 'MOTOR, PAN CONVEYOR #4 FR/P.CONV. 452DB3', '7902', 'ASEQ', 'SG-2303-KL-452-DB04', '2303', '363'),
(364, '20013344', '452DB5M01', NULL, '20013344', 'MOTOR, PAN CONVEYOR #5 FR/P.CONV. 452DB4', '7902', 'ASEQ', 'SG-2303-KL-452-DB05', '2303', '364'),
(365, '20013345', '452DB6M01', NULL, '20013345', 'MOTOR, PAN CONVEYOR #6 FR/P.CONV. 452DB3', '7902', 'ASEQ', 'SG-2303-KL-452-DB06', '2303', '365'),
(366, '20013347', '452EH1M01', NULL, '20013347', 'MOTOR ELECTRIC HOIST', '7902', 'ASEQ', 'SG-2303-KL-452-EH01', '2303', '366'),
(367, '20013348', '452EH1M02', NULL, '20013348', 'MOTOR TROLEY', '7902', 'ASEQ', 'SG-2303-KL-452-EH01', '2303', '367'),
(368, '20013350', '452EH2M01', NULL, '20013350', 'MOTOR ELECTRIC HOIST', '7902', 'ASEQ', 'SG-2303-KL-452-EH02', '2303', '368'),
(369, '20013352', '452FN1M01', NULL, '20013352', 'MOTOR, ID FAN #1 F/DUST COLLECTOR 452BF1', '7902', 'ASEQ', 'SG-2303-KL-452-BF01', '2303', '369'),
(370, '20013354', '452FN3M01', NULL, '20013354', 'MOTOR, ID FAN- #3 F/DUST COLLECTOR 452BF', '7902', 'ASEQ', 'SG-2303-KL-452-BF03', '2303', '370'),
(371, '20013355', '452FN4M01', NULL, '20013355', 'MOTOR, ID FAN- #4 F/DUST COLLECTOR 452BF', '7902', 'ASEQ', 'SG-2303-KL-452-BF04', '2303', '371'),
(372, '20013356', '452RF1M01', NULL, '20013356', 'MOTOR, ROTARY FEEDER #1 @ DISCH.END CHAI', '7902', 'ASEQ', 'SG-2303-KL-452-RF01', '2303', '372'),
(373, '20013358', '452SJ1M01', NULL, '20013358', 'MOTOR, SUMP PUMP- F/CLINKER CONVEYING TO', '7902', 'ASEQ', 'SG-2303-KL-452-SJ01', '2303', '373'),
(374, '20013359', '452SL1M01', NULL, '20013359', 'MOTOR, LOADING SPOUT HOIST #1@ CLINKER B', '7902', 'ASEQ', 'SG-2303-KL-452-SJ01', '2303', '374'),
(375, '20013360', '452SM1CS1', NULL, '20013360', 'PANEL, CONTROL SYSTEM CLINKER SAMPLER', '7902', 'ASEQ', 'SG-2303-KL-452-SM01', '2303', '375'),
(376, '20013362', '452SM2M01', NULL, '20013362', 'MOTOR, CLINKERSAMPLER #2 @CLINKER CON', '7902', 'ASEQ', 'SG-2303-KL-452-SM02', '2303', '376'),
(377, '20013363', '453CV1M01', NULL, '20013363', 'MOTOR, CHAIN CONVEYOR- 453CV1FR/443EP1 @', '7902', 'ASEQ', 'SG-2304-KL-453-CV01', '2304', '377'),
(378, '20013364', '453CV2M01', NULL, '20013364', 'MOTOR, CHAIN CONVEYOR- 453CV2FR/443EP1 @', '7902', 'ASEQ', 'SG-2304-KL-453-CV02', '2304', '378'),
(379, '20013366', '453CV4M01', NULL, '20013366', 'MOTOR, CHAIN CONVEYOR- 453CV4FR/443CV3 @', '7902', 'ASEQ', 'SG-2304-KL-453-CV04', '2304', '379'),
(380, '20013367', '453CV5M01', NULL, '20013367', 'MOTOR, CHAIN CONVEYOR- 453CV5FR/443CV4 @', '7902', 'ASEQ', 'SG-2304-KL-453-CV05', '2304', '380'),
(381, '20013370', '453DB3M01', NULL, '20013370', 'MOTOR, PAN CONVEYOR- 453DB3 FR/453SM1 @', '7902', 'ASEQ', 'SG-2304-KL-453-DB03', '2304', '381'),
(382, '20013371', '453EH1', NULL, '20013371', 'HOIST, ELECTRIC- #1 3-TON @ CLINKER CONV', '7902', 'INST', 'SG-2304-KL-453-EH01', '2304', '382'),
(383, '20013372', '453EH1M01', NULL, '20013372', 'MOTOR,ELECTRIC HOIST', '7902', 'ASEQ', 'SG-2304-KL-453-EH01', '2304', '383'),
(384, '20013374', '453EH2M01', NULL, '20013374', 'MOTOR,ELECTRIC HOIST', '7902', 'ASEQ', 'SG-2304-KL-453-EH02', '2304', '384'),
(385, '20013375', '453FN1M01', NULL, '20013375', 'MOTOR, ID-FAN 453FN1 F/453BF1@ CLINKER C', '7902', 'ASEQ', 'SG-2304-KL-453-BF01', '2304', '385'),
(386, '20013376', '453FN2M01', NULL, '20013376', 'MOTOR, ID-FAN 453FN2 F/453BF2@ CLINKER C', '7902', 'ASEQ', 'SG-2304-KL-453-BF02', '2304', '386'),
(387, '20013378', '453LS1M01', NULL, '20013378', 'MOTOR, LOADING SPOUT BIN 453BI1 @ CLINKE', '7902', 'ASEQ', 'SG-2304-KL-453-LS01', '2304', '387'),
(388, '20014539', '631EH1', NULL, '20014539', 'ELECTRIC HOIST #1 TUBAN 1', '7902', 'INST', 'SG-2302-PC-631-EH01', '2302', '388'),
(389, '20014540', '631FN1M01', NULL, '20014540', 'MOTOR, ID FAN- DUST COLLECTOR#06 F/PACK.', '7902', 'ASEQ', 'SG-2302-PC-631-BF01', '2302', '389'),
(390, '20014541', '631FN2M01', NULL, '20014541', 'MOTOR, BAG DESTROYER BIN FAN-F/PACK.MACH', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '390'),
(391, '20014543', '631EH2', NULL, '20014543', 'CRANE, OVERHEAD- F/REPAIR #02 F/PACKING', '7902', 'INST', 'SG-2302-PC-631-EH02', '2302', '391'),
(392, '20014545', '631PM1M02', NULL, '20014545', 'MOTOR, CEMENT BAG PACKER 631PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '392'),
(393, '20014546', '631PM1M03', NULL, '20014546', 'MOTOR, CEMENT BAG PACKER 631PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '393'),
(394, '20014547', '631PM1M04', NULL, '20014547', 'MOTOR, CEMENT BAG PACKER 631PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '394'),
(395, '20014548', '631PM1M05', NULL, '20014548', 'MOTOR, CEMENT BAG PACKER 631PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '395'),
(396, '20014549', '631PM1M06', NULL, '20014549', 'MOTOR, CEMENT BAG PACKER 631PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '396'),
(397, '20014550', '631PM1M07', NULL, '20014550', 'MOTOR, CEMENT BAG PACKER- HAVER ROTO #01', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '397'),
(398, '20014551', '631RF1M01', NULL, '20014551', 'MOTOR, DRIVE- DOUBLE ROTARY FEEDER @ STO', '7902', 'ASEQ', 'SG-2302-PC-631-RF01', '2302', '398'),
(399, '20014552', '631RF2M01', NULL, '20014552', 'MOTOR, ROTARY FEEDER- DUST COLLECTOR #06', '7902', 'ASEQ', 'SG-2302-PC-631-RF02', '2302', '399'),
(400, '20014553', '631SC1M01', NULL, '20014553', 'MOTOR, SCREW CONVEYOR- SPILLAGE RETURN F', '7902', 'ASEQ', 'SG-2302-PC-631-SC01', '2302', '400'),
(401, '20014554', '631SX1M01', NULL, '20014554', 'MOTOR, BAG DESTROYER- ROTARY CUTTER-I F/', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '401'),
(402, '20014555', '631SX2M01', NULL, '20014555', 'MOTOR, BAG DESTROYER- ROTARY CUTTER-II F', '7902', 'ASEQ', 'SG-2302-PC-631-PM01', '2302', '402'),
(403, '20014557', '632TO1M01', NULL, '20014557', 'MOTOR, MOBILE FLAT BELT CONVEYOR- 6324E1', '7902', 'ASEQ', 'SG-2302-PC-632-TO01', '2302', '403'),
(404, '20014558', '632TO1M02', NULL, '20014558', 'MOTOR, GEAR- F/RUNNING GEAR LORRY STERN', '7902', 'ASEQ', 'SG-2302-PC-632-TO01', '2302', '404'),
(405, '20014560', '632TO2M02', NULL, '20014560', 'MOTOR, GEAR- F/RUNNING GEAR LORRY STERN', '7902', 'ASEQ', 'SG-2302-PC-632-TO02', '2302', '405'),
(406, '20014561', '632BC1M01', NULL, '20014561', 'MOTOR, BAG DISCHARGE BELT CONVEYOR- F/PA', '7902', 'ASEQ', 'SG-2302-PC-632-BC01', '2302', '406'),
(407, '20014563', '632BC3M01', NULL, '20014563', 'MOTOR, BELT CONVEYOR- DRIVEN ROLLER WAY', '7902', 'ASEQ', 'SG-2302-PC-632-BC03', '2302', '407'),
(408, '20014564', '632BC4M01', NULL, '20014564', 'MOTOR, FLAT BELT CONVEYOR- F/PACKING MAC', '7902', 'ASEQ', 'SG-2302-PC-632-BC04', '2302', '408'),
(409, '20014565', '632BC5M01', NULL, '20014565', 'MOTOR, FLAT BELT CONVEYOR- HORZ.REVERS F', '7902', 'ASEQ', 'SG-2302-PC-632-BC05', '2302', '409'),
(410, '20014566', '632BN1M01', NULL, '20014566', 'MOTOR, BAG CLEANER- SUPPORTING ROLLER F/', '7902', 'ASEQ', 'SG-2302-PC-632-BN01', '2302', '410'),
(411, '20014567', '632BP1M01', NULL, '20014567', 'MOTOR, BAG APPLICATOR/PLACER RADIMAT - P', '7902', 'ASEQ', 'SG-2302-PC-632-BP01', '2302', '411'),
(412, '20014568', '632BR1M01', NULL, '20014568', 'MOTOR, BAG REJECTING DEVICE- ROLLER WAY', '7902', 'ASEQ', 'SG-2302-PC-632-BR01', '2302', '412'),
(413, '20014570', '632BW1M01', NULL, '20014570', 'MOTOR, WEIGHT COMPENSATION WEIGHER- F/PA', '7902', 'ASEQ', 'SG-2302-PC-632-BW01', '2302', '413'),
(414, '20014571', '632FN1M01', NULL, '20014571', 'MOTOR, ID FAN- DUST COLLECTOR#07 F/PACK.', '7902', 'ASEQ', 'SG-2302-PC-632-BF01', '2302', '414'),
(415, '20014572', '632FN2M01', NULL, '20014572', 'MOTOR, BAG DESTROYER BIN FAN-F/PACK.MACH', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '415'),
(416, '20014574', '632PM1M02', NULL, '20014574', 'MOTOR, CEMENT BAG PACKER 632PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '416'),
(417, '20014575', '632PM1M03', NULL, '20014575', 'MOTOR, CEMENT BAG PACKER 632PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '417'),
(418, '20014576', '632PM1M04', NULL, '20014576', 'MOTOR, CEMENT BAG PACKER 632PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '418'),
(419, '20014578', '632PM1M06', NULL, '20014578', 'MOTOR, CEMENT BAG PACKER 632PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '419'),
(420, '20014579', '632PM1M07', NULL, '20014579', 'MOTOR, CEMENT BAG PACKER- HAVER ROTO #01', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '420'),
(421, '20014580', '632RF1M01', NULL, '20014580', 'MOTOR, DRIVE- DOUBLE ROTARY FEEDER @ STO', '7902', 'ASEQ', 'SG-2302-PC-632-RF01', '2302', '421'),
(422, '20014582', '632SC1M01', NULL, '20014582', 'MOTOR, SCREW CONVEYOR- SPILLAGE RETURN F', '7902', 'ASEQ', 'SG-2302-PC-632-SC01', '2302', '422'),
(423, '20014583', '632SX1M01', NULL, '20014583', 'MOTOR, BAG DESTROYER- ROTARY CUTTER-I F/', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '423'),
(424, '20014584', '632SX2M01', NULL, '20014584', 'MOTOR, BAG DESTROYER- ROTARY CUTTER-II F', '7902', 'ASEQ', 'SG-2302-PC-632-PM01', '2302', '424'),
(425, '20014586', '633TO1M01', NULL, '20014586', 'MOTOR, MOBILE FLAT BELT CONVEYOR- 6334E1', '7902', 'ASEQ', 'SG-2302-PC-633-TO01', '2302', '425'),
(426, '20014587', '633TO1M02', NULL, '20014587', 'MOTOR, GEAR- F/RUNNING GEAR LORRY STERN', '7902', 'ASEQ', 'SG-2302-PC-633-TO01', '2302', '426'),
(427, '20014588', '633TO2M01', NULL, '20014588', 'MOTOR, MOBILE FLAT BELT CONVEYOR- 6334E2', '7902', 'ASEQ', 'SG-2302-PC-633-TO02', '2302', '427'),
(428, '20014590', '633BC1M01', NULL, '20014590', 'MOTOR, BAG DISCHARGE BELT CONVEYOR- F/PA', '7902', 'ASEQ', 'SG-2302-PC-633-BC01', '2302', '428'),
(429, '20014591', '633BC2M01', NULL, '20014591', 'MOTOR, BAG DIRECTING BELT CONVEYOR- F/PA', '7902', 'ASEQ', 'SG-2302-PC-633-BC02', '2302', '429'),
(430, '20014592', '633BC3M01', NULL, '20014592', 'MOTOR, BELT CONVEYOR- DRIVEN ROLLER WAY', '7902', 'ASEQ', 'SG-2302-PC-633-BC03', '2302', '430'),
(431, '20014594', '633BC5M01', NULL, '20014594', 'MOTOR, FLAT BELT CONVEYOR- HORZ.REVERS F', '7902', 'ASEQ', 'SG-2302-PC-633-BC05', '2302', '431'),
(432, '20014595', '633BN1M01', NULL, '20014595', 'MOTOR, BAG CLEANER- SUPPORTING ROLLER F/', '7902', 'ASEQ', 'SG-2302-PC-633-BN01', '2302', '432'),
(433, '20014596', '633BP1M01', NULL, '20014596', 'MOTOR, BAG APPLICATOR/PLACER RADIMAT - P', '7902', 'ASEQ', 'SG-2302-PC-633-BP01', '2302', '433'),
(434, '20014598', '633BR2M01', NULL, '20014598', 'MOTOR, BAG HOISTING REJECTING DEVICE- F/', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '434'),
(435, '20014599', '633BW1M01', NULL, '20014599', 'MOTOR, WEIGHT COMPENSATION WEIGHER- F/PA', '7902', 'ASEQ', 'SG-2302-PC-633-BW01', '2302', '435'),
(436, '20014600', '633FN1M01', NULL, '20014600', 'MOTOR, ID FAN- DUST COLLECTOR#08 F/PACK.', '7902', 'ASEQ', 'SG-2302-PC-633-BF01', '2302', '436'),
(437, '20014602', '633PM1M01', NULL, '20014602', 'MOTOR, CEMENT BAG PACKER 633PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '437'),
(438, '20014603', '633PM1M02', NULL, '20014603', 'MOTOR, CEMENT BAG PACKER 633PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '438'),
(439, '20014604', '633PM1M03', NULL, '20014604', 'MOTOR, CEMENT BAG PACKER 633PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '439'),
(440, '20014606', '633PM1M05', NULL, '20014606', 'MOTOR, CEMENT BAG PACKER 633PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '440'),
(441, '20014607', '633PM1M06', NULL, '20014607', 'MOTOR, CEMENT BAG PACKER 633PM1 FOR SPOU', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '441'),
(442, '20014610', '633RF2M01', NULL, '20014610', 'MOTOR, DRIVE- ROTARY FEEDER DUST COLL. #', '7902', 'ASEQ', 'SG-2302-PC-633-RF02', '2302', '442'),
(443, '20014611', '633SC1M01', NULL, '20014611', 'MOTOR, SCREW CONVEYOR- SPILLAGE RETURN F', '7902', 'ASEQ', 'SG-2302-PC-633-SC01', '2302', '443'),
(444, '20014612', '633SX1M01', NULL, '20014612', 'MOTOR, BAG DESTROYER- ROTARY CUTTER-I F/', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '444'),
(445, '20014614', '633SX3M01', NULL, '20014614', 'MOTOR, ROTARY SCREEN DRUM- F/PACKING MAC', '7902', 'ASEQ', 'SG-2302-PC-633-PM01', '2302', '445'),
(446, '10002059', 'NN-9', NULL, '10002059', 'RUMAH DINAS BLOK NN-9 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '446'),
(447, '10002060', 'NN-10', NULL, '10002060', 'RUMAH DINAS BLOK NN-10 TYPE 70', '7902', 'INST', 'SG-2307-GN-PRD-NN', '2307', '447'),
(448, '10002061', 'A-10', NULL, '10002061', 'RUMAH DINAS TYPE.A.10~(RUMAH DIREKTUR)', '7901', 'INST', 'SG-2306-GN-PRD-A', '2306', '448'),
(449, '10002062', 'A-07', NULL, '10002062', 'RUMAH DINAS TYPE,A~NO.7~SEGUNTNG', '7901', 'INST', 'SG-2306-GN-PRD-A', '2306', '449'),
(450, '10002063', 'A-08', NULL, '10002063', 'RUMAH DINAS TYPE,A~NO.8~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-A', '2306', '450'),
(451, '10002064', 'A-09', NULL, '10002064', 'RUMAH DINAS TYPE,A~NO.9~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-A', '2306', '451'),
(452, '10002065', 'A-11', NULL, '10002065', 'RUMAH DINAS TYPE,A~NO.11~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-A', '2306', '452'),
(453, '10002066', 'B-01', NULL, '10002066', 'RUMAH DINAS TYPE.B~NO.1~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '453'),
(454, '10002067', 'B-02', NULL, '10002067', 'RUMAH DINAS TYPE.B~NO.2~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '454'),
(455, '10002068', 'B-03', NULL, '10002068', 'RUMAH DINAS TYPE,B~NO.3~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '455'),
(456, '10002069', 'B-04', NULL, '10002069', 'RUMAH DINAS TYPE,B~NO.4~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '456'),
(457, '10002070', 'B-05', NULL, '10002070', 'RUMAH DINAS TYPE,B~NO.5~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '457'),
(458, '10002071', 'B-06', NULL, '10002071', 'RUMAH DINAS TYPE,B~NO.6~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '458'),
(459, '10002072', 'B-07', NULL, '10002072', 'RUMAH DINAS TYPE,B~NO.7~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-B', '2306', '459'),
(460, '10002073', 'C-12', NULL, '10002073', 'RUMAH DINAS TYPE,C~NO.12~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '460'),
(461, '10002074', 'C-13', NULL, '10002074', 'RUMAH DINAS TYPE,C~NO.13~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '461'),
(462, '10002075', 'C-14', NULL, '10002075', 'RUMAH DINAS TYPE,C~NO.14~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '462'),
(463, '10002076', 'C-15', NULL, '10002076', 'RUMAH DINAS TYPE,C~NO.15~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '463'),
(464, '10002077', 'C-16', NULL, '10002077', 'RUMAH DINAS TYPE,C~NO.16~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '464'),
(465, '10002078', 'C-17', NULL, '10002078', 'RUMAH DINAS TYPE,C~NO.17~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '465'),
(466, '10002079', 'C-18', NULL, '10002079', 'RUMAH DINAS TYPE,C~NO.18~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '466'),
(467, '10002080', 'C-19', NULL, '10002080', 'RUMAH DINAS TYPE,C~NO.19~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '467'),
(468, '10002081', 'C-20', NULL, '10002081', 'RUMAH DINAS TYPE,C~NO.20~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '468'),
(469, '10002082', 'C-21', NULL, '10002082', 'RUMAH DINAS TYPE,C~NO.21~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '469'),
(470, '10002083', 'C-22', NULL, '10002083', 'RUMAH DINAS TYPE,C~NO.22~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '470'),
(471, '10002084', 'C-23', NULL, '10002084', 'RUMAH DINAS TYPE,C~NO.23~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '471'),
(472, '10002085', 'C-24', NULL, '10002085', 'RUMAH DINAS TYPE,C~NO.24~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '472'),
(473, '10002086', 'C-25', NULL, '10002086', 'RUMAH DINAS TYPE,C~NO.25~SEGUNTING', '7901', 'INST', 'SG-2306-GN-PRD-C', '2306', '473'),
(474, '10002087', 'D-02', NULL, '10002087', 'RUMAH DINAS TYPE,D~NO.02~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '474'),
(475, '10002088', 'D-04', NULL, '10002088', 'RUMAH DINAS TYPE,D~NO.04~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '475'),
(476, '10002089', 'D-05', NULL, '10002089', 'RUMAH DINAS TYPE,D~NO.05~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '476'),
(477, '10002090', 'D-06', NULL, '10002090', 'RUMAH DINAS TYPE,D~NO.06~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '477'),
(478, '10002091', 'D-007', NULL, '10002091', 'RUMAH DINAS TYPE,D~NO.07~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '478'),
(479, '10002092', 'D-08', NULL, '10002092', 'RUMAH DINAS TYPE,D~NO.08~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '479'),
(480, '10002094', 'D-10', NULL, '10002094', 'RUMAH DINAS TYPE,D~NO.10~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '480'),
(481, '10002095', 'D-11', NULL, '10002095', 'RUMAH DINAS TYPE,D~NO.11~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '481'),
(482, '10002097', 'D-13', NULL, '10002097', 'RUMAH DINAS TYPE,D~NO.13~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '482'),
(483, '10002098', 'D-14', NULL, '10002098', 'RUMAH DINAS TYPE,D~NO.14~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '483'),
(484, '10002099', 'D-15', NULL, '10002099', 'RUMAH DINAS TYPE,D~NO.15~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '484'),
(485, '10002101', 'D-17', NULL, '10002101', 'RUMAH DINAS TYPE,D~NO.17~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '485'),
(486, '10002102', 'D-18', NULL, '10002102', 'RUMAH DINAS TYPE,D~NO.18~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '486'),
(487, '10002103', 'D-19', NULL, '10002103', 'RUMAH DINAS TYPE,D~NO.19~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '487'),
(488, '10002105', 'D-22', NULL, '10002105', 'RUMAH DINAS TYPE,D~NO.22~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '488'),
(489, '10002106', 'D-24', NULL, '10002106', 'RUMAH DINAS TYPE,D~NO.24~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '489'),
(490, '10002108', 'D-26', NULL, '10002108', 'RUMAH DINAS TYPE,D~NO.26~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '490'),
(491, '10002109', 'D-27', NULL, '10002109', 'RUMAH DINAS TYPE,D~NO.27~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '491'),
(492, '10002110', 'D-28', NULL, '10002110', 'RUMAH DINAS TYPE,D~NO.28~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '492'),
(493, '10002112', 'D-30', NULL, '10002112', 'RUMAH DINAS TYPE,D~NO.30~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '493'),
(494, '10002113', 'D-31', NULL, '10002113', 'RUMAH DINAS TYPE,D~NO.31~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '494'),
(495, '10002114', 'D-32', NULL, '10002114', 'RUMAH DINAS TYPE,D~NO.32~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '495'),
(496, '10002116', 'D-34', NULL, '10002116', 'RUMAH DINAS TYPE,D~NO.34~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '496'),
(497, '10002117', 'D-35', NULL, '10002117', 'RUMAH DINAS TYPE,D~NO.35~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '497'),
(498, '10002119', 'D-37', NULL, '10002119', 'RUMAH DINAS TYPE,D~NO.37~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '498'),
(499, '10002120', 'D-38', NULL, '10002120', 'RUMAH DINAS TYPE,D~NO.38~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '499'),
(500, '10002121', 'D-39', NULL, '10002121', 'RUMAH DINAS TYPE,D~NO.39~TUBANAN', '7901', 'INST', 'SG-2306-GN-PRD-D', '2306', '500');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_master_inspection_area`
--

CREATE TABLE `she_master_inspection_area` (
  `id` int(11) NOT NULL,
  `AREA_NAME` varchar(255) NOT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `CREATE_AT` varchar(255) NOT NULL,
  `CREATE_BY` varchar(255) NOT NULL,
  `UPDATE_AT` varchar(255) DEFAULT NULL,
  `UPDATE_BY` varchar(255) DEFAULT NULL,
  `DELETE_AT` varchar(255) DEFAULT NULL,
  `DELETE_BY` varchar(255) DEFAULT NULL,
  `COMPANY` varchar(255) NOT NULL,
  `PLANT` int(255) NOT NULL,
  `RNUM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_master_inspection_area`
--

INSERT INTO `she_master_inspection_area` (`id`, `AREA_NAME`, `NOTE`, `CREATE_AT`, `CREATE_BY`, `UPDATE_AT`, `UPDATE_BY`, `DELETE_AT`, `DELETE_BY`, `COMPANY`, `PLANT`, `RNUM`) VALUES
(1, 'COBA', NULL, '15-JAN-19', 'SYAHRIAL.RAMADANI', '15-JAN-19', 'SYAHRIAL.RAMADANI', NULL, NULL, '5000', 5001, 1),
(2, 'CRUSHER TUBAN 1', NULL, '21-FEB-18', 'INDRA.NOFIANDI', '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, '5000', 5001, 2),
(3, 'OP. CRUSHER', NULL, '21-FEB-18', 'INDRA.NOFIANDI', '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, '5000', 5001, 3),
(4, 'CRUSHER TUBAN 2', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 4),
(5, 'CRUSHER TUBAN 3', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 5),
(6, 'CRUSHER TUBAN 4', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 6),
(7, 'PMCR', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 7),
(8, 'PLI CRUSHER', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 8),
(9, 'UMUM', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 9),
(10, 'BENGKEL LISTRIK', NULL, '21-FEB-18', 'INDRA.NOFIANDI', '24-OCT-18', 'INDRA.NOFIANDI', NULL, NULL, '5000', 5001, 10),
(11, 'BENGKEL MESIN', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 11),
(12, 'CCOLER 2', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 12),
(13, 'DORMITORI', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 13),
(14, 'MAIN OFFICE', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 14),
(15, 'CCR TUBAN 1 & 2', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 15),
(16, 'CCR TUBAN 1 & 2', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 16),
(17, 'CCR TUBAN 3 & 4', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 17),
(18, 'KPSG', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 18),
(19, 'GEDUNG ADUTARIUM', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 19),
(20, 'GEDUNG DIKLAT', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 20),
(21, 'HOUSE PUMP KPSG', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 21),
(22, 'GEDUNG TERAK ( 2 )', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 22),
(23, 'SEKSI SEKRETARIAT', NULL, '21-FEB-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 23),
(24, 'RAW MILL', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 24),
(25, 'COAL MILL', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 25),
(26, 'PREHEATER', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 26),
(27, 'KILN-COOLER', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 27),
(28, 'FINISH MILL', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 28),
(29, 'PACKER', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 29),
(30, 'PELABUHAN', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 30),
(31, 'AAB', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 31),
(32, 'MAINSUB', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 32),
(33, 'CCT', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 33),
(34, 'PERKANTORAN', NULL, '19-DEC-18', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, '5000', 5001, 34);

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_master_pegawai`
--

CREATE TABLE `she_master_pegawai` (
  `id` int(11) NOT NULL,
  `mk_nopeg` varchar(300) NOT NULL,
  `mk_nama` varchar(300) NOT NULL,
  `mk_cttr` varchar(300) NOT NULL,
  `mk_cttr_text` varchar(300) NOT NULL,
  `mk_employee_emp_group` int(11) NOT NULL,
  `mk_employee_emp_group_text` varchar(50) NOT NULL,
  `mk_employee_emp_subgroup` int(11) NOT NULL,
  `mk_employee_emp_subgroup_text` varchar(300) NOT NULL,
  `company` int(11) NOT NULL,
  `company_text` varchar(200) NOT NULL,
  `persarea` int(11) NOT NULL,
  `persarea_text` varchar(100) NOT NULL,
  `cp_kode` varchar(200) NOT NULL,
  `mk_tgl_lahir` date NOT NULL,
  `mk_jenis_kel_code` int(11) NOT NULL,
  `mk_jenis_kel` varchar(50) NOT NULL,
  `mk_perkawinan` varchar(50) NOT NULL,
  `mk_tgl_masuk` date NOT NULL,
  `mk_tgl_pensiun` date NOT NULL,
  `mk_tgl_meninggal` date NOT NULL,
  `mk_kontrak_desc` varchar(50) NOT NULL,
  `mk_py_area` int(11) NOT NULL,
  `mk_py_area_text` varchar(50) NOT NULL,
  `lokasi_code` int(11) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `mk_email` varchar(300) NOT NULL,
  `mk_alamat_rumah` varchar(300) NOT NULL,
  `mk_alamat_kantor` varchar(300) DEFAULT NULL,
  `muk_short` varchar(100) NOT NULL,
  `muk_nama` varchar(200) NOT NULL,
  `muk_level` varchar(200) NOT NULL,
  `muk_parent` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_master_pegawai`
--

INSERT INTO `she_master_pegawai` (`id`, `mk_nopeg`, `mk_nama`, `mk_cttr`, `mk_cttr_text`, `mk_employee_emp_group`, `mk_employee_emp_group_text`, `mk_employee_emp_subgroup`, `mk_employee_emp_subgroup_text`, `company`, `company_text`, `persarea`, `persarea_text`, `cp_kode`, `mk_tgl_lahir`, `mk_jenis_kel_code`, `mk_jenis_kel`, `mk_perkawinan`, `mk_tgl_masuk`, `mk_tgl_pensiun`, `mk_tgl_meninggal`, `mk_kontrak_desc`, `mk_py_area`, `mk_py_area_text`, `lokasi_code`, `lokasi`, `mk_email`, `mk_alamat_rumah`, `mk_alamat_kantor`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`) VALUES
(505, '00000535', 'TEDDY B. SETYADI, SE.', '2005200000', 'GROUP HEAD PENJUALAN', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010892', '1963-11-26', 1, 'Male', 'Kawin', '1987-10-05', '2019-12-01', '2019-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'TEDDY.SETYADI@SEMENINDONESIA.COM', 'PER.DINAS FF-12 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25200000', 'Group Head of Sales', 'KOMP', '50039238'),
(506, '00000536', 'SOESETYOKO S., SE.', '2006000000', 'DIR STRATEGI BISNIS DAN PENGMBN USAHA', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010893', '1963-07-01', 1, 'Male', 'Kawin', '1987-10-05', '2019-08-01', '2019-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SOESETYOKO.S@SEMENINDONESIA.COM', 'PER.DINAS C-20 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22000000', 'Strat Bus & Enterprise Dev Directorate', 'DIR', '50000000'),
(507, '00000548', 'SRI WILUJENG, Ir.', '2007051000', 'UNIT PENGEMBANGAN SISTEM  MANAJEMEN', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010905', '1965-01-07', 2, 'Female', 'Kawin', '1989-12-15', '2021-02-01', '2021-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SRI.WILUJENG@SEMENINDONESIA.COM', 'PERDIN. BLOK F-10 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22021000', 'Unit of Management System Development', 'BIRO', '50050412'),
(508, '00000549', 'LUSIDA AFTIARTI, Dra.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010906', '1966-08-19', 2, 'Female', 'Kawin', '1989-12-18', '2022-09-01', '2022-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'LUSIDA.AFTIARTI@SEMENINDONESIA.COM', 'PERDIN BLOK FF-04 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650A000', 'Dana Pensiun Semen Gresik', 'BIRO', '50032594'),
(509, '00000550', 'SUMARJI', '2004110000', 'DEPARTEMEN DESAIN DAN ENGINEERING', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50000369', '1963-09-29', 1, 'Male', 'Kawin', '1989-12-21', '2019-10-01', '2019-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUMARJI@SEMENINDONESIA.COM', 'JL. GELATIK II/05 GKA RT.3 RW.4 KEMBANGAN KEBOMAS Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24020000', 'Department of Design & Engineering', 'DEPT', '50032268'),
(510, '00000552', 'MUHAMAD SULKAN, ST.', '2004110000', 'DEPARTEMEN DESAIN DAN ENGINEERING', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010909', '1967-06-11', 1, 'Male', 'Kawin', '1989-12-21', '2023-07-01', '2023-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUHAMAD.SULKAN@SEMENINDONESIA.COM', 'PER.DINAS G-141 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24020000', 'Department of Design & Engineering', 'DEPT', '50032268'),
(511, '00000559', 'HERU SETYADI, ST.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010916', '1964-01-26', 1, 'Male', 'Kawin', '1990-11-17', '2020-02-01', '2020-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HERU.SETYADI@SEMENINDONESIA.COM', 'PER.DINAS E-52 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(513, '00000565', 'BUDIYONO, ST.', '2004115000', 'UNIT PENGEMBANGAN TEKNOLOGI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010922', '1966-08-10', 1, 'Male', 'Kawin', '1990-11-17', '2022-09-01', '2022-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'BUDIYONO@SEMENINDONESIA.COM', 'PER.DINAS G-163 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24021000', 'Unit of Technology Development', 'BIRO', '50032326'),
(514, '00000567', 'ABDUL MUSHOLLI', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010924', '1967-01-29', 1, 'Male', 'Kawin', '1990-11-17', '2023-02-01', '2023-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ABDUL.MUSHOLLI@SEMENINDONESIA.COM', 'JL. VETERAN 8/47 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(515, '00000569', 'KUSMAN', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010926', '1967-04-17', 1, 'Male', 'Kawin', '1990-11-17', '2023-05-01', '2023-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'KUSMAN@SEMENINDONESIA.COM', 'DESA SOGO RT-2/RW-3 BABAT Lamongan 62211 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(516, '00000571', 'SUGIARTO', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010928', '1967-07-15', 1, 'Male', 'Kawin', '1990-11-17', '2023-08-01', '2023-08-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'SUGIARTO@SEMENINDONESIA.COM', 'KAWERAN RT3 NO.8 TALUN Blitar (kabupaten) 66183 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(517, '00000578', 'MARJUKI, ST.', '2004170200', 'SEKSI PERENCANAAN PEMEL INFRA SCM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010935', '1968-05-12', 1, 'Male', 'Kawin', '1990-11-17', '2024-06-01', '2024-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MARJUKI.578@SEMENINDONESIA.COM', 'PER.DINAS DD-16 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101200', 'Section of SCM Infrastructure Maint Plan', 'SECT', '50050482'),
(518, '00000587', 'MOCHAMAD GOFUR', '2005221100', 'SEKSI PENJUALAN JATIM I', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010944', '1969-01-04', 1, 'Male', 'Kawin', '1990-11-17', '2025-02-01', '2025-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOCHAMAD.GOFUR@SEMENINDONESIA.COM', 'PERDIN. D-74 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25221100', 'Section of Jatim I Sales', 'SECT', '50050552'),
(519, '00000593', 'SUYONO, SE.', '2001026000', 'UNIT KOMUNIKASI INTERNAL', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010950', '1969-06-21', 1, 'Male', 'Kawin', '1990-11-17', '2025-07-01', '2025-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUYONO@SEMENINDONESIA.COM', 'JETEK RT07 RW03 SUMARI,DUDUK SAMPEY Gresik 61162 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21132000', 'Unit of Internal Communication', 'BIRO', '50045251'),
(520, '00000597', 'ZAINAL ARIFIN, SE.', '2001026000', 'UNIT KOMUNIKASI INTERNAL', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010954', '1969-09-29', 1, 'Male', 'Kawin', '1990-11-17', '2025-10-01', '2025-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'ZAINAL.ARIFIN597@SEMENINDONESIA.COM', 'JL. KUTILANG II / 1 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21132000', 'Unit of Internal Communication', 'BIRO', '50045251'),
(521, '00000598', 'SUBIYANTO, ST.', '2005223200', 'SEKSI PENJUALAN JABODETABEK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010955', '1969-10-18', 1, 'Male', 'Kawin', '1990-11-17', '2025-11-01', '2025-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'SUBIYANTO.598@SEMENINDONESIA.COM', 'PERDIN. BLOK D-63 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25223200', 'Section of Jabodebek Sales', 'SECT', '50050559'),
(522, '00000600', 'MUHAMAD MUJIB, ST.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010957', '1969-11-17', 1, 'Male', 'Kawin', '1990-11-17', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUHAMAD.MUJIB@SEMENINDONESIA.COM', 'PER.DINAS C-7 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(523, '00000603', 'ARIS MUNANDAR', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010960', '1970-01-19', 1, 'Male', 'Kawin', '1990-11-17', '2026-02-01', '2026-02-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'ARIS.MUNANDAR@SEMENINDONESIA.COM', 'JL. KAPTEN DULASIM I - 1 Gresik 61112 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(524, '00000606', 'TEGUH SANTOSO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010963', '1970-03-19', 1, 'Male', 'Kawin', '1990-11-17', '2026-04-01', '2026-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'TEGUH.SANTOSO@SEMENINDONESIA.COM', 'PER.DIN CC-10 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(525, '00000608', 'MATSAHERI', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010965', '1970-04-18', 1, 'Male', 'Kawin', '1990-11-17', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MATSAHERI@SEMENINDONESIA.COM', 'PERDIN BLOK G-151 GRESIK Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(526, '00000612', 'MULYADI', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010969', '1970-06-05', 1, 'Male', 'Kawin', '1990-11-17', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MULYADI.612@SEMENINDONESIA.COM', 'PER.DINAS BLOK D-051 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(527, '00000619', 'AHMAD SHODIQIN', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010976', '1970-09-19', 1, 'Male', 'Kawin', '1990-11-17', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AHMAD.SHODIQIN@SEMENINDONESIA.COM', 'DESA SEMAMPIR CERME Gresik 61171 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(528, '00000620', 'SUPA\'AT, S.Kom.', '2002111000', 'UNIT PENGADAAN OPERASIONAL SI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010977', '1970-09-20', 1, 'Male', 'Kawin', '1990-11-17', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUPAAT@SEMENINDONESIA.COM', 'PER.DINAS G-155 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27211000', 'Unit of SI Operational Procurement', 'BIRO', '50050623'),
(529, '00000621', 'MOCHAMAD NA\'IN, SE.', '2004170200', 'SEKSI PERENCANAAN PEMEL INFRA SCM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010978', '1970-10-22', 1, 'Male', 'Kawin', '1990-11-17', '2026-11-01', '2026-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOCHAMAD.NAIN@SEMENINDONESIA.COM', 'PER.DINAS G-164 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101200', 'Section of SCM Infrastructure Maint Plan', 'SECT', '50050482'),
(530, '00000622', 'M. ALI ROHMAN SAID, ST.', '2002111000', 'UNIT PENGADAAN OPERASIONAL SI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010979', '1970-10-24', 1, 'Male', 'Kawin', '1990-11-17', '2026-11-01', '2026-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ALI.SAID@SEMENINDONESIA.COM', 'JL. DR. WAHIDIN SH. 14-A/32 Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27211000', 'Unit of SI Operational Procurement', 'BIRO', '50050623'),
(531, '00000625', 'HADI SUTIKNO', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010982', '1971-01-14', 1, 'Male', 'Kawin', '1990-11-17', '2027-02-01', '2027-02-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'HADI.SUTIKNO625@SEMENINDONESIA.COM', 'PER.DINAS KK-02 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(532, '00000628', 'AFANDI', '2005224300', 'SEKSI PROMOSI DAN PELAYANAN PELANGGAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010985', '1971-04-04', 1, 'Male', 'Kawin', '1990-11-17', '2027-05-01', '2027-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AFANDI@SEMENINDONESIA.COM', 'PER. DINAS D - 109 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224300', 'Section of Promotion & Customer Serv II', 'SECT', '50050563'),
(533, '00000632', 'SUGENG PRAYITNO', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010989', '1972-10-19', 1, 'Male', 'Kawin', '1990-11-17', '2028-11-01', '2028-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUGENG.PRAYITNO@SEMENINDONESIA.COM', 'JL. KAPT.DARMOSUGONDO 7/1 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(534, '00000633', 'DEWA AYU NYOMAN SUPIATI, SE.', '2002013000', 'UNIT AKUNTANSI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010990', '1969-03-12', 2, 'Female', 'Kawin', '1990-12-01', '2025-04-01', '2025-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DEWA.AYU@SEMENINDONESIA.COM', 'PER. DINAS GG-302 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27122000', 'Unit of Accounting', 'BIRO', '50050425'),
(535, '00000634', 'AHMAD JAKFAR, SE., MM.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010991', '1970-06-10', 1, 'Male', 'Kawin', '1990-12-01', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AHMAD.JAKFAR@SEMENINDONESIA.COM', 'GKA BLOK EA-14 KEDANYANG Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650B000', 'Semen Indonesia Foundation', 'BIRO', '50032594'),
(536, '00000635', 'AINUR ROZI', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010992', '1963-04-21', 1, 'Male', 'Kawin', '1991-01-21', '2019-05-01', '2019-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AINUR.ROZI@SEMENINDONESIA.COM', 'PER.DINAS G-123 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(537, '00000637', 'EDY PURNOMO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010994', '1965-02-21', 1, 'Male', 'Kawin', '1991-02-13', '2021-03-01', '2021-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'EDY.PURNOMO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-59 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(538, '00000640', 'A M N A N', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50010997', '1966-01-03', 1, 'Male', 'Kawin', '1991-02-13', '2022-02-01', '2022-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AMNAN@SEMENINDONESIA.COM', 'PERDIN. G-73 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(539, '00000643', 'SYAIUL QIROM', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011000', '1968-06-02', 1, 'Male', 'Kawin', '1991-02-13', '2024-07-01', '2024-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SYAIUL.QIROM@SEMENINDONESIA.COM', 'JL. WALET RAYA FA-29 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(540, '00000645', 'HENDRA YUNIANTO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011002', '1969-06-09', 1, 'Male', 'Kawin', '1991-02-13', '2025-07-01', '2025-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HENDRA.YUNIANTO@SEMENINDONESIA.COM', 'PERDIN. BLOK G-251 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(541, '00000648', 'AGUNG TRI UTOMO', '2002022000', 'UNIT LAYANAN ICT', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011005', '1969-11-14', 1, 'Male', 'Kawin', '1991-02-13', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'AGUNG.UTOMO@SEMENINDONESIA.COM', 'MARGORUKUN VI/29 Surabaya 60172 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26021000', 'Unit of ICT Service', 'BIRO', '50050598'),
(542, '00000650', 'RUDY SUBIJANTORO, SE., PIA.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011007', '1970-06-30', 1, 'Male', 'Kawin', '1991-02-13', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'RUDY.SUBIJANTORO@SEMENINDONESIA.COM', 'JL. SIWALANKERTO TIMUR I/2 RT.4 RW.5 KEL.SIWALANKERTO Surabaya 60236 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(543, '00000652', 'AGOES SOEHARTONO, SE.', '2001110000', 'DEPARTEMEN ICT', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011009', '1970-07-31', 1, 'Male', 'Kawin', '1991-02-13', '2026-08-01', '2026-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AGOES.SOEHARTONO@SEMENINDONESIA.COM', 'PER.DINAS D-49 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26020000', 'Department of ICT', 'DEPT', '50032577'),
(544, '00000654', 'CHOIRUL ANAFI, S.Kom.', '2007033000', 'UNIT KERUMAHTANGGAAN & KANTOR PERWAKILAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011011', '1970-11-17', 1, 'Male', 'Kawin', '1991-02-13', '2026-12-01', '2026-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'CHOIRUL.ANAFI@SEMENINDONESIA.COM', 'WONOREJO 4/9 RT.011 RW.06 DS. WONOREJO Surabaya 60263 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21142000', 'Unit of Household & Represntative Office', 'BIRO', '50045252'),
(545, '00000655', 'MUSTA\'IN', '2007042000', 'UNIT KNOWLEDGE MANAGEMENT & INOVASI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011012', '1970-11-17', 1, 'Male', 'Kawin', '1991-02-13', '2026-12-01', '2026-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUSTAIN.655@SEMENINDONESIA.COM', 'JL. WALET I/GA-13 RT.04 RW.04, GKA Gresik 61161 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26133000', 'Unit of KM & Innovation', 'BIRO', '50050608'),
(546, '00000657', 'SENTOT PURWANTO', '2001230000', 'DEPARTEMEN CORPORATE OFFICE', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011014', '1970-12-29', 1, 'Male', 'Kawin', '1991-02-13', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SENTOT.POERWANTO@SEMENINDONESIA.COM', 'JL. BOGEN II 37 D Surabaya 60133 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21140000', 'Department of Corporate Office', 'DEPT', '50045147'),
(547, '00000660', 'RAHMAT ARDHIANSJAH', '2007043000', 'UNIT PELATIHAN & PENGEMBANGAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011017', '1971-09-21', 1, 'Male', 'Kawin', '1991-02-13', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'RAHMAT.ARDHIANSJAH@SEMENINDONESIA.COM', 'PER.DINAS BLOK D - 46 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26132000', 'Unit of Training & Development', 'BIRO', '50050608'),
(548, '00000661', 'SANTOSA WIDODO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011018', '1971-12-04', 1, 'Male', 'Kawin', '1991-02-13', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'SANTOSA.WIDODO@SEMENINDONESIA.COM', 'PER.DINAS F-12 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(549, '00000662', 'ERFANTI QODARSIH, SE., Akt., QIA.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011019', '1970-02-02', 2, 'Female', 'Kawin', '1991-03-07', '2026-03-01', '2026-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ERFANTI.QODARSIH@SEMENINDONESIA.COM', 'PER.DINAS D-117 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(550, '00000663', 'HERY WAHYUDI, SE.,M.PSDM.', '2007100000', 'GROUP HEAD SUMBER DAYA MANUSIA', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011020', '1965-05-23', 1, 'Male', 'Kawin', '1991-03-18', '2021-06-01', '2021-06-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'HERY.WAHYUDI@SEMENINDONESIA.COM', 'PER.DINAS D-6 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26100000', 'Group Head of Human Capital', 'KOMP', '50032577'),
(551, '00000665', 'MAT SULKAN, Ir., MM.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011022', '1964-06-09', 1, 'Male', 'Kawin', '1991-04-05', '2020-07-01', '2020-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MAT.SULKAN@SEMENINDONESIA.COM', 'PER.DINAS C-22 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650K000', 'Thang Long Cement Company', 'BIRO', '50032594'),
(552, '00000666', 'HERU SASONO, Ir., MM.', '2003010000', 'DEPARTEMEN PENGELOLAAN PRODUKSI', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011023', '1965-01-27', 1, 'Male', 'Kawin', '1991-04-05', '2021-02-01', '2021-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'HERU.SASONO@SEMENINDONESIA.COM', 'PER.DINAS AA-17 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23010000', 'Department of Production Management', 'DEPT', '50045130'),
(553, '00000668', 'GAGUK YUDIARINTO, Drs., Psi., MM.', '2007050000', 'DEPARTEMEN PENGEMBANGAN PROSES BISNIS', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011025', '1964-04-14', 1, 'Male', 'Kawin', '1991-05-01', '2020-05-01', '2020-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'GAGUK.YUDIARINTO@SEMENINDONESIA.COM', 'PER.DINAS F-31 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22020000', 'Department of Business Process Dev', 'DEPT', '50045131'),
(554, '00000669', 'LILIK BUDIARTA, Ir.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011026', '1965-02-24', 1, 'Male', 'Kawin', '1991-05-01', '2021-03-01', '2021-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'LILIK.BUDIARTA@SEMENINDONESIA.COM', 'PER.DINAS BLOK F-25 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26509000', 'PT Industri Kemasan Semen Gresik', 'BIRO', '50032594'),
(555, '00000672', 'VIDELIS SIA', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50000374', '1966-04-24', 1, 'Male', 'Kawin', '1991-08-31', '2022-05-01', '2022-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'VIDELIS.SIA@SEMENINDONESIA.COM', 'MONDOKAN SANTOSO BLOK S-16 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(556, '00000673', 'STEFANUS HARI', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50000372', '1966-11-01', 1, 'Male', 'Kawin', '1991-08-31', '2022-12-01', '2022-12-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'STEFANUS.HARRY@SEMENINDONESIA.COM', 'JL. PRAMUKA X RT-3/RW-1 SIDOREJO Tuban 62315 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(557, '00000676', 'FILOMENO MONIZ', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011033', '1969-04-05', 1, 'Male', 'Kawin', '1991-08-31', '2025-05-01', '2025-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'FILOMENO.MONIZ@SEMENINDONESIA.COM', 'MONDOKAN SANTOSO BLOK DD-27 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(558, '00000677', 'LAURENSIUS LUKU', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011034', '1969-10-08', 1, 'Male', 'Kawin', '1991-08-31', '2025-11-01', '2025-11-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'LAURENSIUS.LUKU@SEMENINDONESIA.COM', 'PERUM SIWALAN PERMAI III E2-22 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(559, '00000679', 'FERDIANA GAFFAR, Dra., AK.', '2002010000', 'DEPARTEMEN AKUNTANSI', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50000367', '1966-02-14', 2, 'Female', 'Kawin', '1991-09-03', '2022-03-01', '2022-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'FERDIANA.GAFFAR@SEMENINDONESIA.COM', 'PER.DINAS EE-13 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27120000', 'Department of Accounting', 'DEPT', '50048620'),
(560, '00000680', 'TUBAGUS M. DHARURY, Drs., MM.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011037', '1967-03-29', 1, 'Male', 'Kawin', '1991-09-03', '2023-04-01', '2023-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'TUBAGUS.DHARURY@SEMENINDONESIA.COM', 'JL. GAYUNGSARI TIMUR IV MGK 06 Surabaya 61211 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(561, '00000686', 'SLAMET WAHYONO', '2002111000', 'UNIT PENGADAAN OPERASIONAL SI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011043', '1963-09-20', 1, 'Male', 'Kawin', '1991-09-09', '2019-10-01', '2019-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SLAMET.WAHYONO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-95 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27211000', 'Unit of SI Operational Procurement', 'BIRO', '50050623'),
(562, '00000687', 'MUDJI ASTUTIK', '2002014000', 'UNIT OPERASIONAL PERBENDAHARAAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011044', '1964-10-08', 2, 'Female', 'Kawin', '1991-09-09', '2020-11-01', '2020-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUDJI.ASTUTIK@SEMENINDONESIA.COM', 'JL. DR.WAHIDIN S RT.XX/76 Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27132000', 'Unit of Operational Treasury', 'BIRO', '50050429'),
(563, '00000688', 'HADI SUWARNO', '2002201000', 'UNIT LAYANAN PENGADAAN & PERSEDIAAN', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011045', '1965-09-05', 1, 'Male', 'Kawin', '1991-09-09', '2021-10-01', '2021-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HADI.SUWARNO@SEMENINDONESIA.COM', 'PER.DINAS D-41 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201000', 'Unit of Procurement Service & Inventory', 'BIRO', '50050613'),
(564, '00000689', 'MEMED HARNOKO', '2005221100', 'SEKSI PENJUALAN JATIM I', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011046', '1965-12-11', 1, 'Male', 'Kawin', '1991-09-09', '2022-01-01', '2022-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MEMED.HARNOKO@SEMENINDONESIA.COM', 'PER.DINAS GG-308 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25221100', 'Section of Jatim I Sales', 'SECT', '50050552'),
(565, '00000690', 'BAMBANG WAHYOE RAHARDJO', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011047', '1965-12-12', 1, 'Male', 'Kawin', '1991-09-09', '2022-01-01', '2022-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'BAMBANG.RAHARDJO@SEMENINDONESIA.COM', 'JL. AKASIA NO. 1 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(566, '00000692', 'IRWAN HURYANSYAH', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011049', '1966-06-06', 1, 'Male', 'Kawin', '1991-09-09', '2022-07-01', '2022-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'IRWAN.HURYANSYAH@SEMENINDONESIA.COM', 'JL. PAHLAWAN XIV/4 Gresik 61113 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(567, '00000693', 'HARI SUTRISNO', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011050', '1966-11-07', 1, 'Male', 'Kawin', '1991-09-09', '2022-12-01', '2022-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HARI.SUTRISNO@SEMENINDONESIA.COM', 'RANDUAGUNG RT.II RW.01 NO. 52 Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(568, '00000695', 'EKO SETYAWAN, ST.', '2004130000', 'DEPARTEMEN PENGELOLAAN PROYEK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011052', '1968-09-21', 1, 'Male', 'Kawin', '1991-09-09', '2024-10-01', '2024-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'EKO.SETYAWAN@SEMENINDONESIA.COM', 'PER.DINAS CC-18 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24030000', 'Department of Project Management', 'DEPT', '50032268'),
(569, '00000696', 'WAHYU DARMAWAN, ST.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011053', '1969-01-21', 1, 'Male', 'Kawin', '1991-09-09', '2025-02-01', '2025-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'WAHYU.DARMAWAN@SEMENINDONESIA.COM', 'PER.DINAS LL-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(570, '00000699', 'GUNARSO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011056', '1969-06-16', 1, 'Male', 'Kawin', '1991-09-09', '2025-07-01', '2025-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'GUNARSO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-056 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(571, '00000700', 'HORAS SIBURIAN, SE.', '2002022000', 'UNIT LAYANAN ICT', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011057', '1969-08-07', 1, 'Male', 'Kawin', '1991-09-09', '2025-09-01', '2025-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HORAS.SIBURIAN@SEMENINDONESIA.COM', 'PERDIN. BLOK G-05 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26021000', 'Unit of ICT Service', 'BIRO', '50050598'),
(572, '00000701', 'ACHMAD NURIL, S.Pd.', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011058', '1970-02-20', 1, 'Male', 'Kawin', '1991-09-09', '2026-03-01', '2026-03-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'ACHMAD.NURIL@SEMENINDONESIA.COM', 'PER.DINAS G-30 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(573, '00000702', 'BUDI HERMAWAN', '2004170000', 'UNIT PERENCANAAN INFRASTRUKTUR SCM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011059', '1970-04-03', 1, 'Male', 'Kawin', '1991-09-09', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'BUDI.HERMAWAN@SEMENINDONESIA.COM', 'KALI KEPITING 161-H RT-8/RW-5 Surabaya 60132 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101000', 'Unit of SCM Infrastructure Planning', 'BIRO', '50050481'),
(574, '00000703', 'IS SURYADI, ST.', '2002111000', 'UNIT PENGADAAN OPERASIONAL SI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011060', '1970-04-04', 1, 'Male', 'Kawin', '1991-09-09', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'IS.SURYADI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-58 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27211000', 'Unit of SI Operational Procurement', 'BIRO', '50050623'),
(575, '00000704', 'YATIN', '2002201100', 'SEKSI LAYANAN PENGADAAN BU', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011061', '1970-09-07', 1, 'Male', 'Kawin', '1991-09-09', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'YATIN@SEMENINDONESIA.COM', 'PER. DINAS G-13 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201500', 'Section of BU Procurement Service', 'SECT', '50050614'),
(576, '00000705', 'YUDI SUPRIHADI', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011062', '1970-09-10', 1, 'Male', 'Kawin', '1991-09-09', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'YUDI.SUPRIHADI@SEMENINDONESIA.COM', 'PUTAT JAYA C TIMUR 6/45 Surabaya 60189 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(577, '00000706', 'HEROE BOEDI LAKSONO', '2003060000', 'DEPARTEMEN PENGELOLAAN QUALITY ASSURANCE', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011063', '1970-10-21', 1, 'Male', 'Kawin', '1991-09-09', '2026-11-01', '2026-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HEROE.BOEDI@SEMENINDONESIA.COM', 'PER.DINAS DD-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23030000', 'Department of Quality Assurance', 'DEPT', '50045130'),
(578, '00000707', 'HERI SOEDJATMIKO, ST.', '2004110000', 'DEPARTEMEN DESAIN DAN ENGINEERING', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011064', '1971-05-27', 1, 'Male', 'Kawin', '1991-09-09', '2027-06-01', '2027-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HERI.SOEDJATMIKO@SEMENINDONESIA.COM', 'PER.DINAS GG-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24020000', 'Department of Design & Engineering', 'DEPT', '50032268'),
(579, '00000709', 'MOCH. SYAFII', '2003045000', 'UNIT PENGEMBANGAN AFR DAN ENERGI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011066', '1971-07-03', 1, 'Male', 'Kawin', '1991-09-09', '2027-08-01', '2027-08-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOCH.SYAFII@SEMENINDONESIA.COM', 'PER. DINAS E-37 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24011000', 'Unit of AFR & Energy Development', 'BIRO', '50050476'),
(580, '00000711', 'KARNO', '2004110000', 'DEPARTEMEN DESAIN DAN ENGINEERING', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011068', '1971-09-09', 1, 'Male', 'Kawin', '1991-09-09', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'KARNO@SEMENINDONESIA.COM', 'JL. KAPTEN DARMO SUGONDO X RT4/RW2 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24020000', 'Department of Design & Engineering', 'DEPT', '50032268'),
(581, '00000713', 'MOCHAMAD KOHAR', '2001230000', 'DEPARTEMEN CORPORATE OFFICE', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011070', '1971-10-14', 1, 'Male', 'Kawin', '1991-09-09', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'MOCHAMAD.KOHAR@SEMENINDONESIA.COM', 'PER. DINAS D - 37 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21140000', 'Department of Corporate Office', 'DEPT', '50045147'),
(582, '00000720', 'DEDI JUNANTO', '2004162300', 'SEKSI PENGANTONGAN CELUKAN BAWANG', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011077', '1972-06-02', 1, 'Male', 'Kawin', '1991-09-09', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DEDI.JUNANTO@SEMENINDONESIA.COM', 'PER. DINAS D-04 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24122300', 'Section of Celukan Bawang Packing Plant', 'SECT', '50050502'),
(583, '00000721', 'AHMAD PARNO SAVERILLAH, SE., M.I.Kom', '2001221000', 'UNIT KOMUNIKASI EKSTERNAL', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011078', '1972-06-07', 1, 'Male', 'Kawin', '1991-09-09', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'PARNO.721@SEMENINDONESIA.COM', 'JL. CENDRAWASIH IV/FB-16 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21131000', 'Unit of External Communication', 'BIRO', '50045251'),
(584, '00000722', 'I NYOMAN TRI HENDRI A., ST.', '2004162300', 'SEKSI PENGANTONGAN CELUKAN BAWANG', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011079', '1972-06-14', 1, 'Male', 'Kawin', '1991-09-09', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'NYOMAN.HENDRI@SEMENINDONESIA.COM', 'PER.DINAS FF-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24122300', 'Section of Celukan Bawang Packing Plant', 'SECT', '50050502'),
(585, '00000724', 'ANDI SUMARWOTO, SE., QIA.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011081', '1972-07-12', 1, 'Male', 'Kawin', '1991-09-09', '2028-08-01', '2028-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ANDI.SUMARWOTO@SEMENINDONESIA.COM', 'JL. ELANG D-01 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(586, '00000725', 'MOCH. HASAN', '2007042000', 'UNIT KNOWLEDGE MANAGEMENT & INOVASI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011082', '1972-07-12', 1, 'Male', 'Kawin', '1991-09-09', '2028-08-01', '2028-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOCH.HASAN@SEMENINDONESIA.COM', 'PERDIN. BLOK D-127 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26133000', 'Unit of KM & Innovation', 'BIRO', '50050608'),
(587, '00000730', 'YOSA FIKRI, ST.', '2006080000', 'DEPARTEMEN PENGELOLAAN PORTFOLIO', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011087', '1981-09-13', 1, 'Male', 'Kawin', '2010-05-01', '2037-10-01', '2037-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'YOSA.FIKRI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-24 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22130000', 'Department of Portfolio Management', 'DEPT', '50050414'),
(588, '00000732', 'FIRDIANSYAH OKTARIZKY, S.Sos.', '2005022000', 'UNIT KOMUNIKASI PEMASARAN', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011089', '1982-10-21', 1, 'Male', 'Kawin', '2010-05-01', '2038-11-01', '2038-11-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'FIRDIANSYAH.O@SEMENINDONESIA.COM', 'PERDIN. BLOK D-22 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25022000', 'Unit of Marketing Communication', 'BIRO', '50050515'),
(589, '00000736', 'RIDUWAN MALIKI, ST.', '2003020000', 'DEPARTEMEN PENGELOLAAN HSE', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011093', '1986-11-06', 1, 'Male', 'Kawin', '2010-05-01', '2042-12-01', '2042-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'RIDUWAN.MALIKI@SEMENINDONESIA.COM', 'PERDIN. BLOK E-11 Gresik 61211 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23040000', 'Department of SHE', 'DEPT', '50045130'),
(590, '00000737', 'EDY SARAYA, ST., MBA., MM.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011094', '1973-01-31', 1, 'Male', 'Kawin', '1991-09-09', '2029-02-01', '2029-02-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'EDY.SARAYA@SEMENINDONESIA.COM', 'PER.DINAS E-35 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(591, '00000738', 'NADYA KARNINA, S.M.', '2007041000', 'UNIT MANAJEMEN KOMPETENSI & PEMBELAJARAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011095', '1987-04-26', 2, 'Female', 'Kawin', '2010-05-01', '2043-05-01', '2043-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'NADYA.KARNINA@SEMENINDONESIA.COM', 'JL. GRIYA KEBRAON TENGAH Surabaya 60222 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26131000', 'Unit of Competency & Learning Management', 'BIRO', '50050608'),
(592, '00000741', 'FACHRUR RIDLO', '2007044000', 'UNIT SERTIFIKASI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011098', '1973-03-22', 1, 'Male', 'Kawin', '1991-09-09', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'FACHRUR.RIDLO@SEMENINDONESIA.COM', 'JL. PANDEAN II NO. 6 Surabaya 60274 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26134000', 'Unit of Certification', 'BIRO', '50050608'),
(593, '00007583', 'TABATTIA DWI', '2005020000', 'DEPARTEMEN PEMASARAN', 1, 'Active', 70, 'Non-Eselon', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50048200', '1994-05-17', 2, 'Female', 'Kawin', '2018-07-01', '2019-06-30', '2019-06-30', 'Kontrak', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'TABATTIA.DWI@SEMENINDONESIA.COM', 'PERUMAHAN PUNDOK UNGU PERMAI BLOK EE 2/10 RT/RW 011/010 Bekasi (kota) 17145 Jawa Barat', 'Jl. Veteran, Gresik 61122', '25020000', 'Department of Marketing', 'DEPT', '50039238'),
(594, '00000742', 'TETY KUSUMA WARDANI, ST.', '2007050000', 'DEPARTEMEN PENGEMBANGAN PROSES BISNIS', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011099', '1988-08-18', 2, 'Female', 'Lajang', '2010-05-01', '2044-09-01', '2044-09-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'TETY.WARDANI@SEMENINDONESIA.COM', 'JL. CIMANUK 31 RANDU AGUNG Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22020000', 'Department of Business Process Dev', 'DEPT', '50045131'),
(595, '00000743', 'DIDIK SUWARYANTA', '2005222100', 'SEKSI PENJUALAN JATENG & DIY I', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011100', '1973-07-13', 1, 'Male', 'Kawin', '1991-09-09', '2029-08-01', '2029-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DIDIK.SUWARYANTA@SEMENINDONESIA.COM', 'PER.DINAS NN-10 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25222100', 'Section of Jawa Tengah & DIY I', 'SECT', '50050556'),
(596, '00000745', 'WISMADI MARSONGKO', '2002051000', 'UNIT EVALUASI KINERJA KEUANGAN', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011105', '1963-09-26', 1, 'Male', 'Kawin', '1991-11-01', '2019-10-01', '2019-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'WISMADI.MARSONGKO@SEMENINDONESIA.COM', 'DUSUN KARANGMULYO RT-8/RW-2 KEREK Tuban 62356 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27112000', 'Unit of Financial Performance Evaluation', 'BIRO', '50048621'),
(597, '00000746', 'TAKARINI SBU., Dra., AK.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011106', '1966-02-02', 2, 'Female', 'Kawin', '1991-11-01', '2022-03-01', '2022-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'TAKARINI.SBU@SEMENINDONESIA.COM', 'PER.DINAS F-08 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26502000', 'PT Cipta Nirmala', 'BIRO', '50032594'),
(598, '00000748', 'AGUS SUBUR KOMARULLAH, SE.', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011108', '1966-08-07', 1, 'Male', 'Kawin', '1991-11-21', '2022-09-01', '2022-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'AGUS.SUBUR@SEMENINDONESIA.COM', 'PER.DINAS G-106 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(599, '00000750', 'ARIES DWI ADHA', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011110', '1965-04-19', 2, 'Female', 'Kawin', '1991-12-18', '2021-05-01', '2021-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ARIES.ADHA@SEMENINDONESIA.COM', 'JL. BLITAR III-17 GKB Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(600, '00000753', 'TAWANG ARIEF NUGROHO, DR.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011114', '1977-05-10', 1, 'Male', 'Kawin', '2011-02-01', '2033-06-01', '2033-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'TAWANG.NUGROHO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-77 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26502000', 'PT Cipta Nirmala', 'BIRO', '50032594'),
(601, '00000755', 'I NYOMAN SUHENDRA', '2109002000', 'KOMISARIS', 1, 'Active', 12, 'Commissioner/Commite', 2000, 'PT. Semen Indonesia', 2010, 'SMI - Dirkom & Perangkat', '50011116', '1970-04-28', 1, 'Male', 'Kawin', '2007-06-04', '2020-06-03', '2020-06-03', 'Komite', 99, 'Non-payroll-relevant', 2000, 'Holding', 'NYOMAN.SUHENDRA@SEMENINDONESIA.COM', 'Jl. TARUNA JAYA I  No: 16B  10110 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '20100000', 'Secretariat of Commissioners', 'KOMP', '50000000'),
(602, '00000758', 'SALOME JULIANA WIRAWAN', '2109002000', 'KOMISARIS', 1, 'Active', 12, 'Commissioner/Commite', 2000, 'PT. Semen Indonesia', 2010, 'SMI - Dirkom & Perangkat', '50011119', '1977-07-07', 2, 'Female', 'Kawin', '2008-07-07', '2018-07-06', '2018-07-06', 'Komite', 23, 'SI Direksi&Komisaris', 2101, 'Gresik', 'JULIANA.WIRAWAN@SEMENINDONESIA.COM', 'KEMANGGISAN PULO 24 PALMERAH  10110 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '20100000', 'Secretariat of Commissioners', 'KOMP', '50000000'),
(603, '00000773', 'SYAFRIZAL, ST., MT.', '2009100000', 'KOMITE AUDIT', 1, 'Active', 12, 'Commissioner/Commite', 2000, 'PT. Semen Indonesia', 2010, 'SMI - Dirkom & Perangkat', '50011134', '1971-11-25', 1, 'Male', 'Kawin', '2011-04-01', '2019-03-31', '2019-03-31', 'Komite', 23, 'SI Direksi&Komisaris', 2101, 'Gresik', 'SYAFRIZAL@SEMENINDONESIA.COM', 'VILLA BUKIT MAS I BLOK B NO.2 Bandung (kota) 40191 Jawa Barat', 'Jl. Veteran, Gresik 61122', '20202000', 'Committee of SMRI', 'BIRO', '50000025'),
(604, '00000775', 'NINDA ELISA DJOHAERI', '2109002000', 'KOMISARIS', 1, 'Active', 12, 'Commissioner/Commite', 2000, 'PT. Semen Indonesia', 2010, 'SMI - Dirkom & Perangkat', '50011136', '1958-08-30', 2, 'Female', 'Kawin', '2010-11-15', '2019-04-30', '2019-04-30', 'Komite', 23, 'SI Direksi&Komisaris', 2101, 'Gresik', 'NINDA.DJOHAERI@SEMENINDONESIA.COM', 'PULO GADUNG RT 09 RW 01 JAKTIM Jakarta Timur 13930 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '20100000', 'Secretariat of Commissioners', 'KOMP', '50000000');
INSERT INTO `she_master_pegawai` (`id`, `mk_nopeg`, `mk_nama`, `mk_cttr`, `mk_cttr_text`, `mk_employee_emp_group`, `mk_employee_emp_group_text`, `mk_employee_emp_subgroup`, `mk_employee_emp_subgroup_text`, `company`, `company_text`, `persarea`, `persarea_text`, `cp_kode`, `mk_tgl_lahir`, `mk_jenis_kel_code`, `mk_jenis_kel`, `mk_perkawinan`, `mk_tgl_masuk`, `mk_tgl_pensiun`, `mk_tgl_meninggal`, `mk_kontrak_desc`, `mk_py_area`, `mk_py_area_text`, `lokasi_code`, `lokasi`, `mk_email`, `mk_alamat_rumah`, `mk_alamat_kantor`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`) VALUES
(605, '00000778', 'ARIS SUNARSO, Ir., MM.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011139', '1966-11-27', 1, 'Male', 'Kawin', '1992-01-02', '2022-12-01', '2022-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'ARIS.SUNARSO@SEMENINDONESIA.COM', 'JL. PRAMUKA I/15 RT. 03 RW. 01 SIDOREJO TUBAN Tuban 62315 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650G000', 'PT Solusi Bangun Indonesia', 'BIRO', '50032594'),
(606, '00000779', 'DODIK PRASETYO, SE.', '2002041000', 'UNIT REGULASI & COMPLIANCE', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011140', '1967-04-28', 1, 'Male', 'Kawin', '1992-01-06', '2023-05-01', '2023-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'DODIK.PRASETYO@SEMENINDONESIA.COM', 'PER.DINAS G-144 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27121000', 'Unit of Regulatory & Compliance', 'BIRO', '50050425'),
(607, '00000782', 'NANIK INDRIANI', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011143', '1969-04-23', 2, 'Female', 'Kawin', '1992-01-16', '2025-05-01', '2025-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'NANIK.INDRIANI@SEMENINDONESIA.COM', 'PER.DINAS E-53 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26501000', 'Koperasi Warga SG', 'BIRO', '50032594'),
(608, '00000783', 'ZAENAL MUTTAQIN, SE.', '2006070000', 'DEPARTEMEN PENGELOLAAN RISIKO KORPORAT', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011144', '1967-07-31', 1, 'Male', 'Kawin', '1992-02-03', '2023-08-01', '2023-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ZAENAL.MUTTAQIN@SEMENINDONESIA.COM', 'JL. KARAH AGUNG XII / 7 Surabaya 60232 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22010000', 'Department of Enterprise Risk Management', 'DEPT', '50045131'),
(609, '00000784', 'SUDARTINI, SE.', '2002015000', 'UNIT PAJAK DAN ASURANSI', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011145', '1968-06-28', 2, 'Female', 'Kawin', '1992-02-17', '2024-07-01', '2024-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'SUDARTINI@SEMENINDONESIA.COM', 'PER.DINAS G-136 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27123000', 'Unit of Tax & Insurance', 'BIRO', '50050425'),
(610, '00000785', 'MULYONO, Ir.', '2003020000', 'DEPARTEMEN PENGELOLAAN HSE', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011146', '1968-05-14', 1, 'Male', 'Kawin', '1992-03-02', '2024-06-01', '2024-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MULYONO@SEMENINDONESIA.COM', 'PER.DINAS E-56 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23040000', 'Department of SHE', 'DEPT', '50045130'),
(611, '00000786', 'RETNO SULISTIJOWATI, SE., MA.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011147', '1969-05-03', 2, 'Female', 'Jd/Dd', '1992-03-02', '2025-06-01', '2025-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'RETNO.SULISTIJOWATI@SEMENINDONESIA.COM', 'PERUM. PURI INDAH BLOK G NO.10 RT.32 / RW.008 Sidoarjo 61224 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26503000', 'PT Semen Indonesia Logistik', 'BIRO', '50032594'),
(612, '00000788', 'J.B. TRIJONO ARI PURNAWAN, SE.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011149', '1970-04-07', 1, 'Male', 'Kawin', '1992-08-03', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'TRIJONO.PURNAWAN@SEMENINDONESIA.COM', 'PER.DINAS BLOK B-06 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650R000', 'PT Semen Kupang Indonesia', 'BIRO', '50032594'),
(613, '00000789', 'EFFNU SUBIYANTO, DR., ST., MBA.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011150', '1971-04-22', 1, 'Male', 'Kawin', '1992-08-03', '2027-05-01', '2027-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'EFFNU.SUBIYANTO@SEMENINDONESIA.COM', 'PERUM. DARUL AISYAH II/11, RT/RW : 13/ IX , DS. YOSOWILANGON Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26503000', 'PT Semen Indonesia Logistik', 'BIRO', '50032594'),
(614, '00000791', 'WAHYUNINGTIYAS', '2002013000', 'UNIT AKUNTANSI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011152', '1972-12-06', 2, 'Female', 'Kawin', '1992-09-01', '2029-01-01', '2029-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'WAHYUNINGTIYAS@SEMENINDONESIA.COM', 'JL. DR.WAHIDIN SH. 28 BARU BLOK G-4 Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27122000', 'Unit of Accounting', 'BIRO', '50050425'),
(615, '00000792', 'OGGY SEDYOBASUKI, Drs.', '2007033000', 'UNIT KERUMAHTANGGAAN & KANTOR PERWAKILAN', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011153', '1965-06-17', 1, 'Male', 'Kawin', '1992-09-21', '2021-07-01', '2021-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'OGGY.SEDYOBASUKI@SEMENINDONESIA.COM', 'PER.DINAS F-14 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21142000', 'Unit of Household & Represntative Office', 'BIRO', '50045252'),
(616, '00000794', 'ANDOYO PUJI RAHARJO, SE.', '2006090000', 'INTEGRATION MANAGEMENT OFFICE', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011155', '1969-10-12', 1, 'Male', 'Kawin', '1992-10-01', '2025-11-01', '2025-11-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'ANDOYO.RAHARJO@SEMENINDONESIA.COM', 'PER.DINAS F-03 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22030000', 'Integration Management Office', 'DEPT', '50045131'),
(617, '00000797', 'FACHRUR ROJI, ST.', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011158', '1970-06-13', 1, 'Male', 'Kawin', '1992-11-02', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'FACHRUR.ROJI@SEMENINDONESIA.COM', 'PER.DINAS H-09 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(618, '00000798', 'DIDIT DWIWANTONO, ST.', '2004110000', 'DEPARTEMEN DESAIN DAN ENGINEERING', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011159', '1968-03-23', 1, 'Male', 'Kawin', '1992-11-02', '2024-04-01', '2024-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DIDIT.DWIWANTONO@SEMENINDONESIA.COM', 'PER.DINAS B-05 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24020000', 'Department of Design & Engineering', 'DEPT', '50032268'),
(619, '00000800', 'MUCHAMAD SUPRIYADI, SE., Akt., CA.', '2002010000', 'DEPARTEMEN AKUNTANSI', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011161', '1970-07-02', 1, 'Male', 'Kawin', '1992-11-02', '2026-08-01', '2026-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'MUCHAMAD.SUPRIYADI@SEMENINDONESIA.COM', 'PER.DINAS B-07 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27120000', 'Department of Accounting', 'DEPT', '50048620'),
(620, '00000802', 'RIZKI HIDAJAH', '2002014000', 'UNIT OPERASIONAL PERBENDAHARAAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011163', '1971-12-15', 2, 'Female', 'Jd/Dd', '1993-01-18', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'RIZKI.HIDAJAH@SEMENINDONESIA.COM', 'PER. DINAS D-67 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27132000', 'Unit of Operational Treasury', 'BIRO', '50050429'),
(621, '00000803', 'MUKHTAR EFFENDI, SE.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011164', '1970-06-14', 1, 'Male', 'Kawin', '1993-02-08', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUKHTAR.EFFENDI@SEMENINDONESIA.COM', 'PER. DINAS G-162 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(622, '00000805', 'M A F U L A, SE.', '2007025000', 'UNIT HUBUNGAN KEPEGAWAIAN', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011166', '1969-02-03', 2, 'Female', 'Kawin', '1993-04-01', '2025-03-01', '2025-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MAFULA@SEMENINDONESIA.COM', 'PER.DINAS G-12 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26123000', 'Unit of Employee Relation', 'BIRO', '50050604'),
(623, '00000806', 'BUDI WAHYUDARSONO C., SE., CA.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011167', '1970-05-12', 1, 'Male', 'Kawin', '1993-04-01', '2026-06-01', '2026-06-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2305, 'Tuban', 'BUDI.WAHYUDARSONO@SEMENINDONESIA.COM', 'PER.DINAS E-7 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26508000', 'PT United Tractors Semen Gresik', 'BIRO', '50032594'),
(624, '00000807', 'IRMAWATI SRI A., Ir., MT.', '2007050000', 'DEPARTEMEN PENGEMBANGAN PROSES BISNIS', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011168', '1968-08-08', 2, 'Female', 'Kawin', '1993-04-05', '2024-09-01', '2024-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'IRMAWATI.SRI@SEMENINDONESIA.COM', 'PERDIN. BLOK C-25 Gresik 60132 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22020000', 'Department of Business Process Dev', 'DEPT', '50045131'),
(625, '00000808', 'ARDI PRASETIYO, Ir.', '2006100000', 'GROUP HEAD PERENCANAAN KORPORAT', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011169', '1965-05-24', 1, 'Male', 'Kawin', '1993-06-02', '2021-06-01', '2021-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ARDI.PRASETIYO@SEMENINDONESIA.COM', 'PER.DINAS F-17 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22100000', 'Group Head of Corporate Planning', 'KOMP', '50045131'),
(626, '00000809', 'BAGUS DWIWASONO, ST., CPSLog.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011170', '1968-05-03', 1, 'Male', 'Kawin', '1993-06-02', '2024-06-01', '2024-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'BAGUS.DWIWASONO@SEMENINDONESIA.COM', 'PER.DINAS J-9 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650G000', 'PT Solusi Bangun Indonesia', 'BIRO', '50032594'),
(627, '00000811', 'TRI EDDY SUSANTO, ST., MT.', '2003040000', 'DEPARTEMEN LITBANG', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011172', '1969-11-04', 1, 'Male', 'Kawin', '1993-06-02', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'TRI.SUSANTO@SEMENINDONESIA.COM', 'PER.DINAS F-11 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24010000', 'Department of Research & Development', 'DEPT', '50032268'),
(628, '00000812', 'AGUS DJATMIKA DARMONO, ST.', '2004117000', 'UNIT ENJINIRING II', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011173', '1970-08-30', 1, 'Male', 'Kawin', '1993-06-02', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'AGUS.DARMONO@SEMENINDONESIA.COM', 'PERDIN. BLOK G-8 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24023000', 'Unit of Engineering II', 'BIRO', '50032326'),
(629, '00000814', 'RUDI HARTONO, Ir., MM.', '2005050000', 'DEPARTEMEN READY MIX & CONCRETE (RMC)', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011175', '1967-07-17', 1, 'Male', 'Kawin', '1993-06-16', '2023-08-01', '2023-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'RUDI.HARTONO@SEMENINDONESIA.COM', 'PER.DINAS F-30 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25010000', 'Department of Ready Mix & Concrete', 'DEPT', '50039238'),
(630, '00000816', 'HARY SUSANTO, ST.', '2001050000', 'DEPARTEMEN PENGADAAN STRATEGIS', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011177', '1970-05-19', 1, 'Male', 'Kawin', '1993-06-16', '2026-06-01', '2026-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HARY.SUSANTO@SEMENINDONESIA.COM', 'PER.DINAS D-55 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27220000', 'Department of Strategic Procurement', 'DEPT', '50050613'),
(631, '00000817', 'FAJAR SOLEH FAGI EFFENDI, ST.', '2003061000', 'UNIT QUALITY ASSURANCE TUBAN DAN GRESIK', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011178', '1972-06-19', 1, 'Male', 'Kawin', '1993-06-16', '2028-07-01', '2028-07-01', 'Promosi PJ SI', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'FAJAR.SOLEH@SEMENINDONESIA.COM', 'PER.DINAS C-06 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23031000', 'Unit of Tuban & Gresik Quality Assurance', 'BIRO', '50050416'),
(632, '00000819', 'ACHMED VAIVAL ISTIADI, Ir., MBA.', '2004000000', 'DIREKTORAT ENJINIRING & INFRASTRUKTUR', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011180', '1967-03-10', 1, 'Male', 'Kawin', '1993-08-02', '2023-04-01', '2023-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'ACHMED.ISTIADI@SEMENINDONESIA.COM', 'PER.DINAS H-14 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24000000', 'Engineering & Project Directorate', 'DIR', '50000000'),
(633, '00000824', 'HASAN BASRI', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011185', '1969-11-03', 1, 'Male', 'Kawin', '1993-08-20', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HASAN.BASRI824@SEMENINDONESIA.COM', 'PERDIN. BLOK G-248 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(634, '00000833', 'WIDI SUGIANTORO', '2007033000', 'UNIT KERUMAHTANGGAAN & KANTOR PERWAKILAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011194', '1971-11-16', 1, 'Male', 'Kawin', '1993-08-20', '2027-12-01', '2027-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'WIDI.SUGIANTORO@SEMENINDONESIA.COM', 'PERDIN BLOK D-71 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21142000', 'Unit of Household & Represntative Office', 'BIRO', '50045252'),
(635, '00000841', 'EDI SISWANTO, SE.', '2002013000', 'UNIT AKUNTANSI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011202', '1973-03-20', 1, 'Male', 'Kawin', '1993-08-20', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'EDI.SISWANTO@SEMENINDONESIA.COM', 'JL. AWIKOEN I/IIC Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27122000', 'Unit of Accounting', 'BIRO', '50050425'),
(636, '00000844', 'WITONO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011205', '1973-06-07', 1, 'Male', 'Kawin', '1993-08-20', '2029-07-01', '2029-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2500, 'Rembang', 'WITONO@SEMENINDONESIA.COM', 'PER. DINAS G-027 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(637, '00000845', 'DWI SUHARSONO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011206', '1973-06-12', 1, 'Male', 'Kawin', '1993-08-20', '2029-07-01', '2029-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DWI.SUHARSONO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-72 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(638, '00000851', 'AGUNG TJAHYONO', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011212', '1973-12-25', 1, 'Male', 'Kawin', '1993-08-20', '2030-01-01', '2030-01-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'AGUNG.TJAHYONO@SEMENINDONESIA.COM', 'DS. SAMBONG GEDE GG III MERAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(639, '00000852', 'ARIE ARWOTO', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011213', '1974-02-28', 1, 'Male', 'Kawin', '1993-08-20', '2030-03-01', '2030-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ARIE.ARWOTO@SEMENINDONESIA.COM', 'JL. MADURA L-3 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(640, '00000858', 'ACHMAD TOHARI', '2004161200', 'SEKSI PEMELIHARAAN PABRIK CIGADING', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011219', '1974-05-09', 1, 'Male', 'Kawin', '1993-08-20', '2030-06-01', '2030-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ACHMAD.TOHARI@SEMENINDONESIA.COM', 'PER. DINAS BLOK B-6 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24121200', 'Section of Cigading Plant Maintenance', 'SECT', '50050499'),
(641, '00000861', 'SAIFUL BASRI', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011222', '1974-06-24', 1, 'Male', 'Kawin', '1993-08-20', '2030-07-01', '2030-07-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'SAIFUL.BASRI@SEMENINDONESIA.COM', 'JL. SINDUJOYO XA 1/27 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(642, '00000863', 'MUHAMMAD TOHA AFIFI', '2003040000', 'DEPARTEMEN LITBANG', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011224', '1974-09-28', 1, 'Male', 'Kawin', '1993-08-20', '2030-10-01', '2030-10-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUHAMMAD.TOHA@SEMENINDONESIA.COM', 'PER. DINAS G-239 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24010000', 'Department of Research & Development', 'DEPT', '50032268'),
(643, '00000865', 'MUHAMMAD HASAN ZAHIDI, SE.', '2002014000', 'UNIT OPERASIONAL PERBENDAHARAAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011226', '1974-10-29', 1, 'Male', 'Kawin', '1993-08-20', '2030-11-01', '2030-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HASAN.ZAHIDI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-14 GRESIK Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27132000', 'Unit of Operational Treasury', 'BIRO', '50050429'),
(644, '00000866', 'FACHRUDIN, ST.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011227', '1974-11-06', 1, 'Male', 'Kawin', '1993-08-20', '2030-12-01', '2030-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'FACHRUDIN@SEMENINDONESIA.COM', 'PERDIN. BLOK D-93 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(645, '00000867', 'WAKHIDIN, SE., PIA.', '2004130000', 'DEPARTEMEN PENGELOLAAN PROYEK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011228', '1975-05-05', 1, 'Male', 'Kawin', '1993-08-20', '2031-06-01', '2031-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'WAKHIDIN@SEMENINDONESIA.COM', 'PERDIN. BLOK D-53 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24030000', 'Department of Project Management', 'DEPT', '50032268'),
(646, '00000868', 'ABDUL MANAN, ST.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011229', '1975-06-04', 1, 'Male', 'Kawin', '1993-08-20', '2031-07-01', '2031-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'ABDUL.MANAN@SEMENINDONESIA.COM', 'PER. DINAS D-47 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(647, '00000871', 'AGUS SUPRIYANTO', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011232', '1963-04-16', 1, 'Male', 'Kawin', '1993-09-17', '2019-05-01', '2019-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'AGUS.SUPRIYANTO@SEMENINDONESIA.COM', 'GG. KRAMAT RT 11/7 JAGAKARSA  10110 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(648, '00000872', 'HERI PURNOMO', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011233', '1963-07-10', 1, 'Male', 'Kawin', '1993-09-17', '2019-08-01', '2019-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'HERI.PURNOMO872@SEMENINDONESIA.COM', 'KOMP.PRINDUSTRIAN VI  10110 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(649, '00000876', 'DWI CHRISTANTO', '2005040100', 'SEKSI ADMINISTRASI CORPORATE SALES', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011237', '1971-03-07', 1, 'Male', 'Kawin', '1993-09-17', '2027-04-01', '2027-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DWI.CHRISTANTO@SEMENINDONESIA.COM', 'PER. DINAS E-05. Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25240100', 'Section of Corporate Sales Adm', 'SECT', '50050585'),
(650, '00000880', 'NANI KUSTRIANINGSIH, SE.', '2007043000', 'UNIT PELATIHAN & PENGEMBANGAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011241', '1972-07-04', 2, 'Female', 'Jd/Dd', '1993-09-17', '2028-08-01', '2028-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'NANI.KUSTRIANINGSIH@SEMENINDONESIA.COM', 'PER.DINAS G-109 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26132000', 'Unit of Training & Development', 'BIRO', '50050608'),
(651, '00000881', 'EKO AGUS SETYAWAN', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011242', '1972-08-07', 1, 'Male', 'Kawin', '1993-09-17', '2028-09-01', '2028-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'EKO.SETYAWAN881@SEMENINDONESIA.COM', 'JL. VETERAN XIII/15 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(652, '00000882', 'ANANG SULISTYONO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011243', '1972-09-21', 1, 'Male', 'Kawin', '1993-09-17', '2028-10-01', '2028-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'ANANG.SULISTYONO@SEMENINDONESIA.COM', 'JL. PEMBANGUNAN NO.27 Lamongan 62211 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(653, '00000887', 'MARYONO, SE.', '2001050000', 'DEPARTEMEN PENGADAAN STRATEGIS', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011248', '1973-08-11', 1, 'Male', 'Kawin', '1993-09-17', '2029-09-01', '2029-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'MARYONO.887@SEMENINDONESIA.COM', 'PER. DINAS D-18 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27220000', 'Department of Strategic Procurement', 'DEPT', '50050613'),
(654, '00000890', 'M. ALI RIYADI, SE.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011251', '1973-11-12', 1, 'Male', 'Kawin', '1993-09-17', '2029-12-01', '2029-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ALI.RIYADI@SEMENINDONESIA.COM', 'PER.DINAS GG-304 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(655, '00000899', 'IWAN PRASTYO UTOMO', '2002201100', 'SEKSI LAYANAN PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011260', '1974-09-06', 1, 'Male', 'Kawin', '1993-09-17', '2030-10-01', '2030-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'IWAN.PRASTYO@SEMENINDONESIA.COM', 'JL. KUTILANG V/VB12 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201500', 'Section of BU Procurement Service', 'SECT', '50050614'),
(656, '00000902', 'KARSONO, SH.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011263', '1975-01-06', 1, 'Male', 'Kawin', '1993-09-17', '2031-02-01', '2031-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'KARSONO@SEMENINDONESIA.COM', 'PER.DINAS K-05 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(657, '00000907', 'ALI MUHTAR', '2001026000', 'UNIT KOMUNIKASI INTERNAL', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011268', '1967-04-21', 1, 'Male', 'Kawin', '1993-11-01', '2023-05-01', '2023-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ALI.MUHTAR@SEMENINDONESIA.COM', 'JL. LUKMAN HAKIM RT-01/02 NO.211 Tuban 62316 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21132000', 'Unit of Internal Communication', 'BIRO', '50045251'),
(658, '00000908', 'MIFTAHUL KHOIRI', '2002014000', 'UNIT OPERASIONAL PERBENDAHARAAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011269', '1967-11-22', 1, 'Male', 'Kawin', '1993-11-01', '2023-12-01', '2023-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MIFTAHUL.KHOIRI@SEMENINDONESIA.COM', 'JL. MERAK I / E8 GRAHA KEMBANGAN ASRI Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27132000', 'Unit of Operational Treasury', 'BIRO', '50050429'),
(659, '00000909', 'N.K. BUDI PRAMONO', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011270', '1968-04-26', 1, 'Male', 'Kawin', '1993-11-01', '2024-05-01', '2024-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'BUDI.PRAMONO@SEMENINDONESIA.COM', 'JL. VETERAN IX-E 01 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(660, '00000910', 'ARIF PURWANTO', '2005232300', 'SEKSI PENJUALAN KALTENG & KALBAR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011271', '1968-10-22', 1, 'Male', 'Kawin', '1993-11-01', '2024-11-01', '2024-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ARIF.PURWANTO@SEMENINDONESIA.COM', 'PER. DINAS BLOK GG-20 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25232300', 'Section of Kalteng & Kalbar Sales', 'SECT', '50050573'),
(661, '00000911', 'DANANG BAGUS WITJAKSONO, SE.', '2005222100', 'SEKSI PENJUALAN JATENG & DIY I', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011272', '1968-10-27', 1, 'Male', 'Kawin', '1993-11-01', '2024-11-01', '2024-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DANANG.BAGUS@SEMENINDONESIA.COM', 'PER.DINAS G-118 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25222100', 'Section of Jawa Tengah & DIY I', 'SECT', '50050556'),
(662, '00000915', 'ACHMAD SYAIFUL', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011276', '1970-04-20', 1, 'Male', 'Kawin', '1993-11-01', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'ACHMAD.SYAIFUL@SEMENINDONESIA.COM', 'JL. RAYA TEMANDANG Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(663, '00000917', 'MUHAMMAD INDRAYONO, SE., MM.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011278', '1970-05-23', 1, 'Male', 'Kawin', '1993-11-01', '2026-06-01', '2026-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUHAMMAD.INDRAYONO@SEMENINDONESIA.COM', 'GILANG GG.KAMBOJA 31C TAMAN Sidoarjo 61211 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(664, '00000918', 'SUYITNO', '2005221200', 'SEKSI PENJUALAN JATIM II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011279', '1970-06-04', 1, 'Male', 'Kawin', '1993-11-01', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUYITNO.918@SEMENINDONESIA.COM', 'PER.DINAS G-107 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25221200', 'Section of Jatim II Sales', 'SECT', '50050552'),
(665, '00000920', 'ACHMAD FARCHAN', '2003061000', 'UNIT QUALITY ASSURANCE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011281', '1970-08-28', 1, 'Male', 'Kawin', '1993-11-01', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'ACHMAD.FARCHAN@SEMENINDONESIA.COM', 'JL. SUNAN MURIA 36 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23031000', 'Unit of Tuban & Gresik Quality Assurance', 'BIRO', '50050416'),
(666, '00000921', 'HARI YANTORO', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011282', '1970-09-02', 1, 'Male', 'Kawin', '1993-11-01', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'HARI.YANTORO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-120 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(667, '00000923', 'AKHMAD BASUNI, SE.', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011284', '1970-12-19', 1, 'Male', 'Kawin', '1993-11-01', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AKHMAD.BASUNI@SEMENINDONESIA.COM', 'JL. SUNAN MURIA 36 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(668, '00000924', 'UMMI CHOLSUM', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011285', '1971-01-28', 2, 'Female', 'Lajang', '1993-11-01', '2027-02-01', '2027-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'UMMI.CHOLSUM@SEMENINDONESIA.COM', 'PERUM GKGA BLOK CC-27 KEDANYANG Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(669, '00000928', 'M. FATCHUR RACHMAN, S.Kom.', '2007023000', 'UNIT REMUNERASI & PENILAIAN KINERJA', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011289', '1971-10-07', 1, 'Male', 'Kawin', '1993-11-01', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'FATCHUR.RACHMAN@SEMENINDONESIA.COM', 'PER.DINAS G-46 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26122000', 'Unit of Remuneration & Perform Appraisal', 'BIRO', '50050604'),
(670, '00000929', 'ANDHIKA FIRMANSYAH, SE.', '2005040300', 'SEKSI PENJUALAN PROYEK & PRODUK II', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011290', '1971-10-30', 1, 'Male', 'Kawin', '1993-11-01', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ANDHIKA.FIRMANSYAH@SEMENINDONESIA.COM', 'PER.DINAS D-02 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25240300', 'Section of Project & Customized Prod II', 'SECT', '50050585'),
(671, '00000930', 'YUDI KRISTIAWAN', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011291', '1972-01-08', 1, 'Male', 'Kawin', '1993-11-01', '2028-02-01', '2028-02-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'YUDI.KRISTIAWAN@SEMENINDONESIA.COM', 'PERDIN. BLOK G-149 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(672, '00000931', 'BAMBANG ERMAWANTO, SH.', '2004170200', 'SEKSI PERENCANAAN PEMEL INFRA SCM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011292', '1972-03-17', 1, 'Male', 'Kawin', '1993-11-01', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'BAMBANG.ERMAWANTO931@SEMENINDONESIA.COM', 'PERDIN. BLOK D-100 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101200', 'Section of SCM Infrastructure Maint Plan', 'SECT', '50050482'),
(673, '00000932', 'KHUSNUL ZAENI', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011293', '1972-05-23', 1, 'Male', 'Kawin', '1993-11-01', '2028-06-01', '2028-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'KHUSNUL.ZAENI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-85 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(674, '00000934', 'MOCH. SOEBCHAN, SE.', '2007043000', 'UNIT PELATIHAN & PENGEMBANGAN', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011295', '1972-07-24', 1, 'Male', 'Kawin', '1993-11-01', '2028-08-01', '2028-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOCH.SOEBCHAN@SEMENINDONESIA.COM', 'PER.DINAS BLOK EE-21 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26132000', 'Unit of Training & Development', 'BIRO', '50050608'),
(675, '00000935', 'HENY TREFIRAWATI, SE.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011296', '1972-08-23', 2, 'Female', 'Kawin', '1993-11-01', '2028-09-01', '2028-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HENY.TREFIRAWATI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-39 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(676, '00000937', 'MOHAMMAD ARBA\'IN', '2003040000', 'DEPARTEMEN LITBANG', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011298', '1973-05-23', 1, 'Male', 'Kawin', '1993-11-01', '2029-06-01', '2029-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOHAMMAD.ARBAIN@SEMENINDONESIA.COM', 'PER.DINAS G-163 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24010000', 'Department of Research & Development', 'DEPT', '50032268'),
(677, '00000940', 'MUCHAMMAD RIDUWAN, SE.', '2005221200', 'SEKSI PENJUALAN JATIM II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011301', '1974-04-26', 1, 'Male', 'Kawin', '1993-11-01', '2030-05-01', '2030-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUCHAMMAD.RIDUWAN@SEMENINDONESIA.COM', 'PER.DINAS G-98 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25221200', 'Section of Jatim II Sales', 'SECT', '50050552'),
(678, '00000941', 'MUHAMMAD FAISHAL', '2002111000', 'UNIT PENGADAAN OPERASIONAL SI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011302', '1974-06-07', 1, 'Male', 'Kawin', '1993-11-01', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUHAMMAD.FAISHAL@SEMENINDONESIA.COM', 'JL. KH. FAQIH USMAN XV/21 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27211000', 'Unit of SI Operational Procurement', 'BIRO', '50050623'),
(679, '00000942', 'EKO SUTRISNO', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011303', '1974-06-20', 1, 'Male', 'Kawin', '1993-11-01', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'EKO.SUTRISNO@SEMENINDONESIA.COM', 'TEMANDANG MERAK URAK Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(680, '00000944', 'SISWANTO, SE.', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011305', '1974-08-25', 1, 'Male', 'Kawin', '1993-11-01', '2030-09-01', '2030-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'SISWANTO.944@SEMENINDONESIA.COM', 'PER. DINAS E 48 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(681, '00000945', 'INPRES RAKHMAD HIDAYAT, SE.', '2005223000', 'UNIT PENJUALAN AREA 5', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011306', '1974-11-10', 1, 'Male', 'Kawin', '1993-11-01', '2030-12-01', '2030-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'INPRES.HIDAYAT@SEMENINDONESIA.COM', 'PER.DINAS D-99 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25223000', 'Unit of Area Sales 5', 'BIRO', '50050551'),
(682, '00000946', 'MOHAMMAD AINUL YAKIN', '2002201100', 'SEKSI LAYANAN PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011307', '1974-11-23', 1, 'Male', 'Kawin', '1993-11-01', '2030-12-01', '2030-12-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'AINUL.YAKIN@SEMENINDONESIA.COM', 'PER.DINAS G-105 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201500', 'Section of BU Procurement Service', 'SECT', '50050614'),
(683, '00000947', 'IMRON ROSYADI', '2007043000', 'UNIT PELATIHAN & PENGEMBANGAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011308', '1974-12-29', 1, 'Male', 'Kawin', '1993-11-01', '2031-01-01', '2031-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'IMRON.ROSYADI@SEMENINDONESIA.COM', 'JL. BUNGA TERATAI NO. 14 RT. 1 RW. 6 SIDOKUMPUL Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26132000', 'Unit of Training & Development', 'BIRO', '50050608'),
(684, '00000948', 'CHARLES NALLE', '2007011000', 'UNIT LITIGASI', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011309', '1975-04-14', 1, 'Male', 'Kawin', '1993-11-01', '2031-05-01', '2031-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'CHARLES.NALLE@SEMENINDONESIA.COM', 'PERDIN SEMEN GRESIK BLOK D-122 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26012000', 'Unit of Litigation', 'BIRO', '50050594'),
(685, '00000950', 'WARAS TAUFIQ H., SE.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011311', '1971-06-08', 1, 'Male', 'Kawin', '1994-01-03', '2027-07-01', '2027-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'WARAS.TAUFIQ@SEMENINDONESIA.COM', 'PER.DINAS D-106 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650N000', 'PT Sinergi Informatika Semen Indonesia', 'BIRO', '50032594'),
(686, '00000951', 'SUPRIYADI, ST.', '2007013000', 'UNIT HUKUM BISNIS', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011312', '1972-02-14', 1, 'Male', 'Kawin', '1994-01-03', '2028-03-01', '2028-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUPRIYADI.951@SEMENINDONESIA.COM', 'PER.DINAS GG-300 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26013000', 'Unit of Business Legal', 'BIRO', '50050594'),
(687, '00000952', 'HARDIYANTA, SE.', '2007011000', 'UNIT LITIGASI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011313', '1972-12-21', 1, 'Male', 'Kawin', '1994-01-03', '2029-01-01', '2029-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HARDIYANTA@SEMENINDONESIA.COM', 'JL. TANJUNG HARAPAN V NO.18 GKB Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26012000', 'Unit of Litigation', 'BIRO', '50050594'),
(688, '00000953', 'PAIMAN', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011314', '1973-08-20', 1, 'Male', 'Kawin', '1994-01-03', '2029-09-01', '2029-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'PAIMAN@SEMENINDONESIA.COM', 'PER. DINAS G-18 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(689, '00000956', 'IZAAK NOORASMARA', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011317', '1965-06-02', 1, 'Male', 'Kawin', '1994-01-17', '2021-07-01', '2021-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'IZAAK.NOORASMARA@SEMENINDONESIA.COM', 'PERDIN. BLOK P-03 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(690, '00000957', 'MOH THONI', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011318', '1965-09-23', 1, 'Male', 'Kawin', '1994-01-17', '2021-10-01', '2021-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'MOH.THONI@SEMENINDONESIA.COM', 'LATSARI GG V - 2 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(691, '00000960', 'DARYANTO, SE.', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011321', '1968-06-12', 1, 'Male', 'Kawin', '1994-01-17', '2024-07-01', '2024-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DARYANTO.960@SEMENINDONESIA.COM', 'DS.PONGPONGAN KEC. MERAKURAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(692, '00000962', 'BAMBANG SUKISWORO', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011323', '1968-12-03', 1, 'Male', 'Kawin', '1994-01-17', '2025-01-01', '2025-01-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'BAMBANG.SUKISWORO@SEMENINDONESIA.COM', 'TEMANDANG Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(693, '00000964', 'AHMAD NURIL HUDA, SE.', '2007043000', 'UNIT PELATIHAN & PENGEMBANGAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011325', '1969-05-04', 1, 'Male', 'Kawin', '1994-01-17', '2025-06-01', '2025-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AHMAD.HUDA@SEMENINDONESIA.COM', 'DS. SUMENGKO RT-10/RW-2 DUDUK S. Gresik 61162 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26132000', 'Unit of Training & Development', 'BIRO', '50050608'),
(694, '00000965', 'SULISTIYANTO', '2005222200', 'SEKSI PENJUALAN JATENG & DIY II', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011326', '1969-08-29', 1, 'Male', 'Kawin', '1994-01-17', '2025-09-01', '2025-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SULISTIYANTO@SEMENINDONESIA.COM', 'PER.DINAS G-274 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25222200', 'Section of Jawa Tengah & DIY II', 'SECT', '50050556'),
(695, '00000967', 'SLAMET JANURI', '2005224100', 'SEKSI ADMINISTRASI PENJUALAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011328', '1970-01-19', 1, 'Male', 'Kawin', '1994-01-17', '2026-02-01', '2026-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SLAMET.JANURI@SEMENINDONESIA.COM', 'JL. PANGLIMA SUDIRMAN 14 B Gresik 61111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224100', 'Section of Sales Administration II', 'SECT', '50050563'),
(696, '00000968', 'R. DODY SUPRIYODI', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011329', '1970-03-21', 1, 'Male', 'Kawin', '1994-01-17', '2026-04-01', '2026-04-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DODY.SUPRIYODI@SEMENINDONESIA.COM', 'JL. TEUKU UMAR V - 5 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(697, '00000970', 'HADI SUTRISNO', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011331', '1970-12-18', 1, 'Male', 'Kawin', '1994-01-17', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'HADI.SUTRISNO@SEMENINDONESIA.COM', 'JL. AGUS SALIM 102 Tuban 62313 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(698, '00000971', 'SITI ROCHAMA, SE.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011332', '1971-03-03', 2, 'Female', 'Kawin', '1994-01-17', '2027-04-01', '2027-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SITI.ROCHAMA@SEMENINDONESIA.COM', 'PER. DINAS G-17 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(699, '00000972', 'DARFINA, SE.', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011333', '1971-04-16', 1, 'Male', 'Kawin', '1994-01-17', '2027-05-01', '2027-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DARFINA@SEMENINDONESIA.COM', 'MONDOKAN SANTOSO PP 05 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(700, '00000973', 'JOHANA MARGARETHA P.', '2005224100', 'SEKSI ADMINISTRASI PENJUALAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011334', '1971-04-22', 2, 'Female', 'Lajang', '1994-01-17', '2027-05-01', '2027-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'JOHANA.MARGARETHA@SEMENINDONESIA.COM', 'PER.DINAS G-168 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224100', 'Section of Sales Administration II', 'SECT', '50050563'),
(701, '00000975', 'MOHAMAD ATHO\'URROHMAN', '2005224100', 'SEKSI ADMINISTRASI PENJUALAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011336', '1971-10-14', 1, 'Male', 'Kawin', '1994-01-17', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOHAMAD.ATHOURROHMAN@SEMENINDONESIA.COM', 'JL. KUTILANG IX/YB-03 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224100', 'Section of Sales Administration II', 'SECT', '50050563'),
(702, '00000976', 'KUSNO ANDRIYONO', '2002201700', 'SEKSI LAYANAN PENGADAAN & PERSEDIAAN SG', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011337', '1971-12-02', 1, 'Male', 'Kawin', '1994-01-17', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2500, 'Rembang', 'KUSNO.ANDRIYONO@SEMENINDONESIA.COM', 'DESA SUGIHWARAS RT 8 JENU Tuban 62352 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201800', 'Section of SG Proc Service & Inventory', 'SECT', '50050614'),
(703, '00000978', 'RINI MERS NURDIJATI, SH.', '2007043000', 'UNIT PELATIHAN & PENGEMBANGAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011339', '1972-03-23', 2, 'Female', 'Kawin', '1994-01-17', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'RINI.MERS@SEMENINDONESIA.COM', 'PER. DINAS AA - 6 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26132000', 'Unit of Training & Development', 'BIRO', '50050608');
INSERT INTO `she_master_pegawai` (`id`, `mk_nopeg`, `mk_nama`, `mk_cttr`, `mk_cttr_text`, `mk_employee_emp_group`, `mk_employee_emp_group_text`, `mk_employee_emp_subgroup`, `mk_employee_emp_subgroup_text`, `company`, `company_text`, `persarea`, `persarea_text`, `cp_kode`, `mk_tgl_lahir`, `mk_jenis_kel_code`, `mk_jenis_kel`, `mk_perkawinan`, `mk_tgl_masuk`, `mk_tgl_pensiun`, `mk_tgl_meninggal`, `mk_kontrak_desc`, `mk_py_area`, `mk_py_area_text`, `lokasi_code`, `lokasi`, `mk_email`, `mk_alamat_rumah`, `mk_alamat_kantor`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`) VALUES
(704, '00000979', 'SISWOYO', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011340', '1972-03-26', 1, 'Male', 'Kawin', '1994-01-17', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'SISWOYO@SEMENINDONESIA.COM', 'DESA SEMANDING RT.II RW.VI Tuban 62381 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(705, '00000980', 'LUKSONO, ST.', '2004153200', 'SEKSI PENGANTONGAN PONTIANAK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011341', '1972-05-10', 1, 'Male', 'Kawin', '1994-01-17', '2028-06-01', '2028-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'LUKSONO@SEMENINDONESIA.COM', 'PER.DIN N-1 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24113400', 'Section of Pontianak Packing Plant', 'SECT', '50050493'),
(706, '00000982', 'SRI WACHYUNINGSIH, SE.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011343', '1973-02-06', 2, 'Female', 'Kawin', '1994-01-17', '2029-03-01', '2029-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SRI.WACHYUNINGSIH@SEMENINDONESIA.COM', 'PER.DINAS D-73 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(707, '00000984', 'KUSIYADI', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011345', '1973-06-16', 1, 'Male', 'Kawin', '1994-01-17', '2029-07-01', '2029-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'KUSIYADI@SEMENINDONESIA.COM', 'CAMPURREJO RENGEL Tuban 62371 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(708, '00000987', 'MOHAMMAD HASANUDIN', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011348', '1973-12-26', 1, 'Male', 'Kawin', '1994-01-17', '2030-01-01', '2030-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOHAMMAD.HASANUDIN@SEMENINDONESIA.COM', 'PERDIN. BLOK D-69 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(709, '00000988', 'RACHMAD DWI SANTOSO, SE.', '2007044000', 'UNIT SERTIFIKASI', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011349', '1974-03-11', 1, 'Male', 'Kawin', '1994-01-17', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'RACHMAD.SANTOSO@SEMENINDONESIA.COM', 'PER.DINAS D-32 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26134000', 'Unit of Certification', 'BIRO', '50050608'),
(710, '00000989', 'NYIMAS NURHANISAH, S.Psi.', '2005224300', 'SEKSI PROMOSI DAN PELAYANAN PELANGGAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011350', '1974-06-11', 2, 'Female', 'Kawin', '1994-01-17', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'NYIMAS.NURHANISAH@SEMENINDONESIA.COM', 'PER.DINAS D-57 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224300', 'Section of Promotion & Customer Serv II', 'SECT', '50050563'),
(711, '00000991', 'SAMSUL HUDI', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011352', '1974-06-26', 1, 'Male', 'Kawin', '1994-01-17', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'SAMSUL.HUDI@SEMENINDONESIA.COM', 'PER.DINAS G-42 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(712, '00000993', 'CHOIROTUN NIKMAH', '2005224100', 'SEKSI ADMINISTRASI PENJUALAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011354', '1975-03-06', 2, 'Female', 'Kawin', '1994-01-17', '2031-04-01', '2031-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'CHOIROTUN.NIKMAH@SEMENINDONESIA.COM', 'JL. ELANG C-11 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224100', 'Section of Sales Administration II', 'SECT', '50050563'),
(713, '00000995', 'HARIS ZUL CHAIRI', '2007050000', 'DEPARTEMEN PENGEMBANGAN PROSES BISNIS', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011356', '1970-01-28', 1, 'Male', 'Kawin', '1994-02-16', '2026-02-01', '2026-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HARIS.CHAIRI@SEMENINDONESIA.COM', 'PER.DINAS E-15 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22020000', 'Department of Business Process Dev', 'DEPT', '50045131'),
(714, '00000996', 'EVIE AMALIANA, SE.', '2001230000', 'DEPARTEMEN CORPORATE OFFICE', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011357', '1968-05-18', 2, 'Female', 'Kawin', '1994-03-01', '2024-06-01', '2024-06-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'EVIE.AMALIANA@SEMENINDONESIA.COM', 'KAMPUNG BARU RT-3/RW-9 23 CIRACAS Jakarta Timur 10110 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '21140000', 'Department of Corporate Office', 'DEPT', '50045147'),
(715, '00001000', 'ARIEF HARYONO', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50000359', '1970-04-02', 1, 'Male', 'Kawin', '1994-03-16', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ARIEF.HARYONO@SEMENINDONESIA.COM', 'PERDIN. BLOK E-02 GRESIK Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(716, '00001002', 'DANAR TRI HARIYANTO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011363', '1970-09-18', 1, 'Male', 'Kawin', '1994-03-16', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DANAR.TRI@SEMENINDONESIA.COM', 'LATSARI I - 10-A Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(717, '00001003', 'S E N E N', '2002014000', 'UNIT OPERASIONAL PERBENDAHARAAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011364', '1970-10-08', 1, 'Male', 'Kawin', '1994-03-16', '2026-11-01', '2026-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SENEN@SEMENINDONESIA.COM', 'PERDIN. BLOK G-29 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27132000', 'Unit of Operational Treasury', 'BIRO', '50050429'),
(718, '00001004', 'NASIKIN', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 50, 'Associate', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011365', '1971-02-07', 1, 'Male', 'Kawin', '1994-03-16', '2027-03-01', '2027-03-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'NASIKIN@SEMENINDONESIA.COM', 'SIWALAN PERMAI IV/20 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(719, '00001005', 'SETYO KARNO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011366', '1971-06-09', 1, 'Male', 'Kawin', '1994-03-16', '2027-07-01', '2027-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SETYO.KARNO@SEMENINDONESIA.COM', 'JL. KUTILANG V/16 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(720, '00001007', 'SAPUAN', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011368', '1971-11-25', 1, 'Male', 'Kawin', '1994-03-16', '2027-12-01', '2027-12-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'SAPUAN@SEMENINDONESIA.COM', 'JL. GOTONG ROYONG DS. SOGO BABAT Lamongan 62211 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(721, '00001010', 'BASHORI ALWI', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011371', '1972-05-25', 1, 'Male', 'Kawin', '1994-03-16', '2028-06-01', '2028-06-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'BASHORI.ALWI@SEMENINDONESIA.COM', 'LATSARI GG 1 - 10-A Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(722, '00001011', 'ADIE PRAYITNO, SH.', '2003061000', 'UNIT QUALITY ASSURANCE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011372', '1973-01-02', 1, 'Male', 'Kawin', '1994-03-16', '2029-02-01', '2029-02-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'ADIE.PRAYITNO@SEMENINDONESIA.COM', 'PERDIN BLOK G-158 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23031000', 'Unit of Tuban & Gresik Quality Assurance', 'BIRO', '50050416'),
(723, '00001014', 'BOIMAN', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011375', '1973-05-09', 1, 'Male', 'Kawin', '1994-03-16', '2029-06-01', '2029-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2500, 'Rembang', 'BOIMAN@SEMENINDONESIA.COM', 'JL. SUNAN MURIA 39 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(724, '00001015', 'GOEK DWI WHENDRA A.', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011376', '1973-12-14', 1, 'Male', 'Kawin', '1994-03-16', '2030-01-01', '2030-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'GOEK.DWI@SEMENINDONESIA.COM', 'PERDIN.BLOK D-66 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(725, '00001018', 'ARIEF SUKMONO', '2002044000', 'UNIT KEBIJAKAN PERBENDAHARAAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011379', '1974-10-17', 1, 'Male', 'Lajang', '1994-03-16', '2030-11-01', '2030-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ARIEF.SUKMONO@SEMENINDONESIA.COM', 'PER. DINAS D-119 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27131000', 'Unit of Treasury Policy', 'BIRO', '50050429'),
(726, '00001019', 'TEGUH SUNARYO', '2002112000', 'UNIT PENGADAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011380', '1974-12-10', 1, 'Male', 'Kawin', '1994-03-16', '2031-01-01', '2031-01-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'TEGUH.SUNARYO@SEMENINDONESIA.COM', ' Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27212000', 'Unit of BU Procurement', 'BIRO', '50050623'),
(727, '00001020', 'BAMBANG SUDAR SONO', '2005036000', 'UNIT OUTBOND INVENTORY', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011381', '1975-06-25', 1, 'Male', 'Kawin', '1994-03-16', '2031-07-01', '2031-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'BAMBANG.SONO@SEMENINDONESIA.COM', 'JL. SUNAN MURIA 59 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25113000', 'Unit of Outbond Inventory', 'BIRO', '50050523'),
(728, '00001021', 'MUHAMMAD HASAN', '2003064000', 'SEKSI QUALITY ASSURANCE SG', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011382', '1971-12-10', 1, 'Male', 'Kawin', '1994-03-25', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2500, 'Rembang', 'MUHAMMAD.HASAN@SEMENINDONESIA.COM', 'PER.DINAS E-05 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23030100', 'Section of SG Quality Assurance', 'SECT', '50050416'),
(729, '00001022', 'RUDHY RIANTO SETIAWAN, Ir.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011383', '1967-12-04', 1, 'Male', 'Kawin', '1994-05-02', '2024-01-01', '2024-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'RUDHY.SETIAWAN@SEMENINDONESIA.COM', 'PER.DINAS J-5 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26501000', 'Koperasi Warga SG', 'BIRO', '50032594'),
(730, '00001023', 'SHINTA WINDU H.P., SE., PIA.', '2007023000', 'UNIT REMUNERASI & PENILAIAN KINERJA', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011384', '1971-08-15', 2, 'Female', 'Kawin', '1994-05-02', '2027-09-01', '2027-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SHINTA.WINDU@SEMENINDONESIA.COM', 'JL. DARMOKALI 16 Surabaya 60186 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26122000', 'Unit of Remuneration & Perform Appraisal', 'BIRO', '50050604'),
(731, '00001025', 'MUTTAQIEN', '2005223100', 'SEKSI PENJUALAN JABAR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011386', '1965-11-14', 1, 'Male', 'Kawin', '1994-05-16', '2021-12-01', '2021-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUTTAQIEN@SEMENINDONESIA.COM', 'PER. DINAS D-86 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25223100', 'Section of Jawa Barat Sales', 'SECT', '50050559'),
(732, '00001028', 'AGUS WAHYUDI', '2001050000', 'DEPARTEMEN PENGADAAN STRATEGIS', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011389', '1967-08-20', 1, 'Male', 'Kawin', '1994-05-16', '2023-09-01', '2023-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AGUS.WAHYUDI@SEMENINDONESIA.COM', 'JL. RIAU E-16 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27220000', 'Department of Strategic Procurement', 'DEPT', '50050613'),
(733, '00001029', 'SOETJIPTO, ST.', '2003045000', 'UNIT PENGEMBANGAN AFR DAN ENERGI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011390', '1968-11-23', 1, 'Male', 'Kawin', '1994-05-16', '2024-12-01', '2024-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SOETJIPTO@SEMENINDONESIA.COM', 'PERDIN G-270 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24011000', 'Unit of AFR & Energy Development', 'BIRO', '50050476'),
(734, '00001030', 'SUBIYANTO', '2005224300', 'SEKSI PROMOSI DAN PELAYANAN PELANGGAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011391', '1969-07-15', 1, 'Male', 'Kawin', '1994-05-16', '2025-08-01', '2025-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUBIYANTO.1030@SEMENINDONESIA.COM', 'JL. RA KARTINI I-B/08-A Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224300', 'Section of Promotion & Customer Serv II', 'SECT', '50050563'),
(735, '00001031', 'M. TEGUH WIDODO', '2003061000', 'UNIT QUALITY ASSURANCE TUBAN DAN GRESIK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011392', '1969-08-29', 1, 'Male', 'Kawin', '1994-05-16', '2025-09-01', '2025-09-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'TEGUH.WIDODO@SEMENINDONESIA.COM', 'PER.DINAS EE-16 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23031000', 'Unit of Tuban & Gresik Quality Assurance', 'BIRO', '50050416'),
(736, '00001035', 'DWI INTARTI', '2007025000', 'UNIT HUBUNGAN KEPEGAWAIAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011396', '1971-09-10', 2, 'Female', 'Kawin', '1994-05-16', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DWI.INTARTI@SEMENINDONESIA.COM', 'JL. RA. KARTINI 14 A/27 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26123000', 'Unit of Employee Relation', 'BIRO', '50050604'),
(737, '00001036', 'SRI HARTATIK RESMININGSIH, SE.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011397', '1971-12-30', 2, 'Female', 'Cerai', '1994-05-16', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SRI.HARTATIK@SEMENINDONESIA.COM', 'PER.DINAS G-40 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(738, '00001043', 'DIDIK SUBARIYANTO', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011404', '1973-03-15', 1, 'Male', 'Kawin', '1994-05-16', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DIDIK.SUBARIYANTO@SEMENINDONESIA.COM', 'JL. BARATA JAYA IV/92-A Surabaya 60284 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(739, '00001046', 'JOKO SURASA', '2005224300', 'SEKSI PROMOSI DAN PELAYANAN PELANGGAN II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011407', '1974-06-25', 1, 'Male', 'Kawin', '1994-05-16', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'JOKO.SURASA@SEMENINDONESIA.COM', 'PERDIN. BLOK E-21 Gresik 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25224300', 'Section of Promotion & Customer Serv II', 'SECT', '50050563'),
(740, '00001048', 'EKO ELHAM ROHMANTO', '2004110000', 'DEPARTEMEN DESAIN DAN ENGINEERING', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011409', '1975-07-05', 1, 'Male', 'Kawin', '1994-05-16', '2031-08-01', '2031-08-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'EKO.ELHAM@SEMENINDONESIA.COM', 'PER.DINAS A-09 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24020000', 'Department of Design & Engineering', 'DEPT', '50032268'),
(741, '00001049', 'M. LUDFI SETYADI, Ir.', '2006070000', 'DEPARTEMEN PENGELOLAAN RISIKO KORPORAT', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011410', '1968-02-25', 1, 'Male', 'Kawin', '1994-05-17', '2024-03-01', '2024-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'LUDFI.SETYADI@SEMENINDONESIA.COM', 'PER.DINAS A-03 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22010000', 'Department of Enterprise Risk Management', 'DEPT', '50045131'),
(742, '00001050', 'AWAN NUGROHO', '2003020000', 'DEPARTEMEN PENGELOLAAN HSE', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011411', '1969-08-15', 1, 'Male', 'Kawin', '1994-06-09', '2025-09-01', '2025-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AWAN.NUGROHO@SEMENINDONESIA.COM', 'PER.DINAS D-112 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23040000', 'Department of SHE', 'DEPT', '50045130'),
(743, '00001051', 'RUDDY ARIYANTO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011412', '1969-11-23', 1, 'Male', 'Kawin', '1994-06-09', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'RUDDY.ARIYANTO@SEMENINDONESIA.COM', 'JL. SUNAN KALIJOGO I/9 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(744, '00001054', 'TEGUH IMAM SANTOSO, SE.', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011415', '1971-09-26', 1, 'Male', 'Kawin', '1994-06-09', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'TEGUH.IMAM@SEMENINDONESIA.COM', 'PERDIN. BLOK G-241 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(745, '00001055', 'PRAPTI MURTINI, SE.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011416', '1971-12-30', 2, 'Female', 'Kawin', '1994-06-09', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'PRAPTI.MURTINI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-11 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(746, '00001056', 'ZAINAL KHOIRIN', '2005232200', 'SEKSI PENJUALAN KALSEL', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011417', '1972-01-01', 1, 'Male', 'Kawin', '1994-06-09', '2028-02-01', '2028-02-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ZAINAL.KHOIRIN@SEMENINDONESIA.COM', 'PERDIN SG F10 SUMURGUNG TUBAN Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25232200', 'Section of Kalimantan Selatan Sales', 'SECT', '50050573'),
(747, '00001058', 'DWI PURWANTO, SE.', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011419', '1972-11-25', 1, 'Male', 'Kawin', '1994-06-09', '2028-12-01', '2028-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DWI.PURWANTO@SEMENINDONESIA.COM', 'PER.DINAS E-38 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(748, '00001059', 'DWI PURWANTO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011420', '1973-01-24', 1, 'Male', 'Kawin', '1994-06-09', '2029-02-01', '2029-02-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DWI.PURWANTO1059@SEMENINDONESIA.COM', 'PERUM. SIWALAN PERMAI IA/49 TUBAN Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(749, '00001060', 'HERU PUTRA TIMUR, SH.', '2005114000', 'UNIT OPERASI TRANSPORTASI II', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011421', '1973-03-10', 1, 'Male', 'Kawin', '1994-06-09', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'HERU.TIMUR@SEMENINDONESIA.COM', 'JL. TEUKU UMAR V - 5 LATSARI Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25124000', 'Unit of Operational Transportation II', 'BIRO', '50050527'),
(750, '00001061', 'DONNY KUSBIANTORO', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011422', '1973-10-11', 1, 'Male', 'Kawin', '1994-06-09', '2029-11-01', '2029-11-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'DONNY.KUSBIANTORO@SEMENINDONESIA.COM', 'PER.DIN P-12 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(751, '00000433', 'MOCH JA\'FAR', '7203480902', 'UNIT QUALITY CONTROL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010790', '1963-10-10', 1, 'Male', 'Kawin', '1982-08-01', '2019-11-01', '2019-11-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MOCH.JAFAR@SEMENINDONESIA.COM', 'PER. DINAS E-54 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71213000', 'Unit of Quality Control', 'BIRO', '50048700'),
(752, '00000551', 'RACHMAT HARIJOKO', '7203434914', 'SEKSI KEBERSIHAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50000370', '1965-01-03', 1, 'Male', 'Kawin', '1989-12-21', '2021-02-01', '2021-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'RACHMAT.HARIYOKO@SEMENINDONESIA.COM', 'JAGIR SIDORESMO 8/36 RT.03 RW.01 Surabaya 60282 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212300', 'Section of Hygiene', 'SECT', '50048709'),
(753, '00000558', 'SAKDUN', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010915', '1963-11-09', 1, 'Male', 'Kawin', '1990-11-17', '2019-12-01', '2019-12-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SAKDUN@SEMENINDONESIA.COM', 'BUNGA RT.02 RW.IX Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(754, '00000560', 'SUMINTO', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50000373', '1965-04-05', 1, 'Male', 'Kawin', '1990-11-17', '2021-05-01', '2021-05-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SUMINTO@SEMENINDONESIA.COM', 'JL. RA KARTINI XX/63 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(755, '00000564', 'MUSTAKIM', '7203434914', 'SEKSI KEBERSIHAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010921', '1966-06-16', 1, 'Male', 'Kawin', '1990-11-17', '2022-07-01', '2022-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MUSTAKIM.564@SEMENINDONESIA.COM', 'SAMBUNG GEDE MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212300', 'Section of Hygiene', 'SECT', '50048709'),
(756, '00000566', 'AGUS SALIM', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010923', '1966-08-26', 1, 'Male', 'Kawin', '1990-11-17', '2022-09-01', '2022-09-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'AGUS.SALIM566@SEMENINDONESIA.COM', 'JL. VETERAN VII/23 E Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(757, '00000568', 'MOH. NAUFAL', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010925', '1967-04-01', 1, 'Male', 'Kawin', '1990-11-17', '2023-05-01', '2023-05-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MOH.NAUFAL@SEMENINDONESIA.COM', 'PER.DINAS G-315 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(758, '00000572', 'BUDI SULISTIONO', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010929', '1967-10-11', 1, 'Male', 'Kawin', '1990-11-17', '2023-11-01', '2023-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'BUDI.SULISTIONO@SEMENINDONESIA.COM', 'PER.DINAS EE-18 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(759, '00000574', 'JOKO BEKTISUSILO', '7203322906', 'SEKSI PEMELIHARAAN MESIN PACKER & PELAB', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010931', '1967-11-27', 1, 'Male', 'Kawin', '1990-11-17', '2023-12-01', '2023-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'JOKO.BEKTISUSILO@SEMENINDONESIA.COM', 'DESA GAJI 1 RT-1/RW-1 KEREK Tuban 62356 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252400', 'Section of Packer&TBN Port Machine Maint', 'SECT', '50048719'),
(760, '00000575', 'BAMBANG KUSWANTO', '7203522906', 'SIE PEMELIHARAAN MESIN KILN & COAL MILL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010932', '1968-02-26', 1, 'Male', 'Kawin', '1990-11-17', '2024-03-01', '2024-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'BAMBANG.KUSWANTO@SEMENINDONESIA.COM', 'DESA KRADENAN RT1/RW1 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252200', 'Section of KCM 3-4 Machine Maintenance', 'SECT', '50048719'),
(761, '00000576', 'NURSAHADI', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010933', '1968-04-11', 1, 'Male', 'Kawin', '1990-11-17', '2024-05-01', '2024-05-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'NURSAHADI@SEMENINDONESIA.COM', 'JL. DR WAHIDIN S IB/52 Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(762, '00000577', 'SUGIANTO, SE.', '7201000000', 'GROUP HEAD OPERASIONAL PABRIK', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010934', '1968-05-04', 1, 'Male', 'Kawin', '1990-11-17', '2024-06-01', '2024-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUGIANTO@SEMENINDONESIA.COM', 'BONDOWOSO 01/22 RT.3 RW.6 YOSOWILANGON MANYAR Gresik 61151 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71120000', 'Committee of Tbn & Rembang Raw Mat Expan', 'DEPT', '50048671'),
(763, '00000579', 'HADI WINARKO', '7203414901', 'SEKSI KONSTRUKSI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010936', '1968-06-15', 1, 'Male', 'Kawin', '1990-11-17', '2024-07-01', '2024-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HADI.WINARKO@SEMENINDONESIA.COM', 'PERUM MONDOKAN SANTOSO BB/17 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257300', 'Section of Construction', 'SECT', '50048724'),
(764, '00000580', 'MOHAMMAD AFIF', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010937', '1968-06-22', 1, 'Male', 'Kawin', '1990-11-17', '2024-07-01', '2024-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOHAMMAD.AFIF@SEMENINDONESIA.COM', 'JL. MERAK III BLOK G-16 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(765, '00000581', 'SUJIANTO', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010938', '1968-07-04', 1, 'Male', 'Kawin', '1990-11-17', '2024-08-01', '2024-08-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SUJIANTO@SEMENINDONESIA.COM', 'JL. MERAK 3/H-7 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(766, '00000582', 'SUPARMAN, ST.', '7203413906', 'SEKSI BENGKEL MESIN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010939', '1968-07-16', 1, 'Male', 'Kawin', '1990-11-17', '2024-08-01', '2024-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUPARMAN.582@SEMENINDONESIA.COM', 'PER.DINAS Q-05 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257100', 'Section of Machine Workshop', 'SECT', '50048724'),
(767, '00000583', 'MARFAUZI', '7203322906', 'SEKSI PEMELIHARAAN MESIN PACKER & PELAB', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010940', '1968-07-27', 1, 'Male', 'Kawin', '1990-11-17', '2024-08-01', '2024-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MARFAUZI@SEMENINDONESIA.COM', 'PER.DINAS E-06 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252400', 'Section of Packer&TBN Port Machine Maint', 'SECT', '50048719'),
(768, '00000585', 'M.NUR SJA\'BANY, ST., PIA.', '7202310000', 'UNIT SISTEM MANAJEMEN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010942', '1968-11-16', 1, 'Male', 'Kawin', '1990-11-17', '2024-12-01', '2024-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NUR.SJABANY@SEMENINDONESIA.COM', 'PER.DINAS F-16 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71301000', 'Unit of Management System', 'BIRO', '50048673'),
(769, '00000586', 'HARI WAHYUDI', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010943', '1968-12-06', 1, 'Male', 'Kawin', '1990-11-17', '2025-01-01', '2025-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HARI.WAHYUDI@SEMENINDONESIA.COM', 'JL. LUKMAN HAKIM 121/29 Tuban 62316 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(770, '00000588', 'SULARDI', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010945', '1969-01-19', 1, 'Male', 'Kawin', '1990-11-17', '2025-02-01', '2025-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SULARDI@SEMENINDONESIA.COM', 'PER.DINAS D-76 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(771, '00000589', 'HARIADI WURDIANTO, ST.', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010946', '1969-02-27', 1, 'Male', 'Kawin', '1990-11-17', '2025-03-01', '2025-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HARIADI.WURDIANTO@SEMENINDONESIA.COM', 'PER.DINAS KK-06 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(772, '00000590', 'SUNARTO', '7203420901', 'UNIT PERENCANAAN DAN EVALUASI PEMELIHARA', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010947', '1969-03-02', 1, 'Male', 'Kawin', '1990-11-17', '2025-04-01', '2025-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUNARTO@SEMENINDONESIA.COM', 'PER. DINAS F-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255000', 'Unit of Maintenance Planning & Eval', 'BIRO', '50048704'),
(773, '00000591', 'MAT KHOLIL', '7203322906', 'SEKSI PEMELIHARAAN MESIN PACKER & PELAB', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010948', '1969-03-14', 1, 'Male', 'Kawin', '1990-11-17', '2025-04-01', '2025-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MAT.KHOLIL@SEMENINDONESIA.COM', 'PER.DINAS M-03 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252400', 'Section of Packer&TBN Port Machine Maint', 'SECT', '50048719'),
(774, '00000592', 'EDY PURWANTO', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010949', '1969-05-01', 1, 'Male', 'Kawin', '1990-11-17', '2025-06-01', '2025-06-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'EDY.PURWANTO@SEMENINDONESIA.COM', 'PER.DINAS S-03 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(775, '00000595', 'AGOES SOELAIMANTO', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010952', '1969-08-26', 1, 'Male', 'Kawin', '1990-11-17', '2025-09-01', '2025-09-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'AGOES.SOELAIMANTO@SEMENINDONESIA.COM', 'PERDIN. BLOK G-25 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(776, '00000596', 'NUR MUDHOFAR', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010953', '1969-09-08', 1, 'Male', 'Kawin', '1990-11-17', '2025-10-01', '2025-10-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'NUR.MUDHOFAR@SEMENINDONESIA.COM', 'JL. MERAK III H.03 GKA MANYAR Gresik 61151 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(777, '00000599', 'M. HERNA PUDJIANTO', '7203322906', 'SEKSI PEMELIHARAAN MESIN PACKER & PELAB', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010956', '1969-10-21', 1, 'Male', 'Kawin', '1990-11-17', '2025-11-01', '2025-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HERNA.PUDJIANTO@SEMENINDONESIA.COM', 'PER.DIN CC-6 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252400', 'Section of Packer&TBN Port Machine Maint', 'SECT', '50048719'),
(778, '00000601', 'SUGENG SUJARWO', '7203214910', 'ALTERNATIF FUEL TBN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010958', '1969-11-17', 1, 'Male', 'Kawin', '1990-11-17', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUGENG.SUJARWO@SEMENINDONESIA.COM', 'PER.DINAS DD-06 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212200', 'Section of AF & 3rd Material', 'SECT', '50048709'),
(779, '00000602', 'HARI SELO SETIADJI', '7201210000', 'UNIT KEAMANAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010959', '1970-01-13', 1, 'Male', 'Kawin', '1990-11-17', '2026-02-01', '2026-02-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HARI.SETIADJI@SEMENINDONESIA.COM', 'PER.DINAS FF-12 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71112000', 'Unit of Security', 'BIRO', '50048674'),
(780, '00000605', 'IMAM PRIANTO', '7203313055', 'FINISH MILL 1 T3 SEKSI FM TBN34', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010962', '1970-02-21', 1, 'Male', 'Kawin', '1990-11-17', '2026-03-01', '2026-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'IMAM.PRIANTO@SEMENINDONESIA.COM', 'PER.DINAS P-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241200', 'Section of FM 3-4 Operation', 'SECT', '50048716'),
(781, '00000607', 'JUWARI', '7203313055', 'FINISH MILL 1 T3 SEKSI FM TBN34', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010964', '1970-04-12', 1, 'Male', 'Kawin', '1990-11-17', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'JUWARI.607@SEMENINDONESIA.COM', 'PER.DINAS GG-08 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241200', 'Section of FM 3-4 Operation', 'SECT', '50048716'),
(782, '00000609', 'BUSRON WAHJONO', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010966', '1970-05-13', 1, 'Male', 'Kawin', '1990-11-17', '2026-06-01', '2026-06-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'BUSRON.WAHJONO@SEMENINDONESIA.COM', 'KUPANG KRAJAN VIII/12 Surabaya 60185 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(783, '00000610', 'EDI DWIANTORO', '7203122906', 'SEKSI PEMELIHARAAN MESIN CRUSHER', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010967', '1970-05-24', 1, 'Male', 'Kawin', '1990-11-17', '2026-06-01', '2026-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'EDI.DWIANTORO@SEMENINDONESIA.COM', 'JL. PANGLIMA SUDIRMAN XVI/25 Gresik 61111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71222200', 'Section of Crusher Machine Maintenance', 'SECT', '50048712'),
(784, '00000611', 'SYAIFUL ARIF, ST.', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010968', '1970-06-02', 1, 'Male', 'Kawin', '1990-11-17', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SYAIFUL.ARIF@SEMENINDONESIA.COM', 'JL. KH.HASYIM AS\'ARI 12/1 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(785, '00000613', 'ISRAWAN', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010970', '1970-06-21', 1, 'Male', 'Kawin', '1990-11-17', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ISRAWAN@SEMENINDONESIA.COM', 'PER.DINAS NN-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(786, '00000614', 'JUNI WAHJU WIDODO, ST.', '7203413906', 'SEKSI BENGKEL MESIN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010971', '1970-06-29', 1, 'Male', 'Kawin', '1990-11-17', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'JUNI.WIDODO@SEMENINDONESIA.COM', 'PER.DINAS CC-20 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257100', 'Section of Machine Workshop', 'SECT', '50048724'),
(787, '00000615', 'YULIANTO BUDI P.', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010972', '1970-07-05', 1, 'Male', 'Kawin', '1990-11-17', '2026-08-01', '2026-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'YULIANTO.BUDI@SEMENINDONESIA.COM', 'PER.DINAS JJ-03 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(788, '00000616', 'GUGUK WITONO', '7203213033', 'RAW MILL TBN3 - SEKSI RKC3', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010973', '1970-08-02', 1, 'Male', 'Kawin', '1990-11-17', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'GUGUK.WITONO@SEMENINDONESIA.COM', 'PER.DINAS Q-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232100', 'Section of RKC 3 Operation', 'SECT', '50048715'),
(789, '00000617', 'WAKIDUL ANAM', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010974', '1970-08-08', 1, 'Male', 'Kawin', '1990-11-17', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'WAKIDUL.ANAM@SEMENINDONESIA.COM', 'DESA ROOMO MANYAR Gresik 61151 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(790, '00000618', 'AGUS SUBIANTORO', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010975', '1970-08-13', 1, 'Male', 'Kawin', '1990-11-17', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AGUS.SUBIANTORO@SEMENINDONESIA.COM', 'JL. VETERAN VIII/5 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(791, '00000623', 'AROCHMAN', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010980', '1970-12-08', 1, 'Male', 'Kawin', '1990-11-17', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AROCHMAN@SEMENINDONESIA.COM', 'PER.DINAS NN-02 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(792, '00000624', 'ABDUL KHOLIQ', '7203212032', 'RAW MILL TBN2 - SEKSI RKC2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010981', '1970-12-28', 1, 'Male', 'Kawin', '1990-11-17', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ABDUL.KHOLIQ@SEMENINDONESIA.COM', 'JL.MASJID ALFALAH II/07 LATSARI Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231200', 'Section of RKC 2 Operation', 'SECT', '50048714'),
(793, '00000626', 'SUMAN', '7203214910', 'ALTERNATIF FUEL TBN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010983', '1971-02-01', 1, 'Male', 'Kawin', '1990-11-17', '2027-03-01', '2027-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUMAN@SEMENINDONESIA.COM', 'JL. BASUKI RAHMAT 2 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212200', 'Section of AF & 3rd Material', 'SECT', '50048709'),
(794, '00000627', 'AGUS SALAKHUDIN', '7203221906', 'SEKSI PEMELIHARAAN MESIN ROLLER MILL 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010984', '1971-02-15', 1, 'Male', 'Kawin', '1990-11-17', '2027-03-01', '2027-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AGUS.SALAKHUDIN@SEMENINDONESIA.COM', 'PER.DINAS NN-06 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71251100', 'Section of RM 1-2 Machine Maintenance', 'SECT', '50048718'),
(795, '00000629', 'ABDUL CHOLIK', '7203312061', 'PACKER T1 CRH SIE PACKER & PELB TBN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010986', '1971-07-05', 1, 'Male', 'Kawin', '1990-11-17', '2027-08-01', '2027-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ABDUL.CHOLIK@SEMENINDONESIA.COM', 'PER.DINAS T-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241300', 'Section of Packer Operation', 'SECT', '50048716'),
(796, '00000631', 'SURYANTO', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010988', '1971-12-11', 1, 'Male', 'Kawin', '1990-11-17', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SURYANTO.631@SEMENINDONESIA.COM', 'DESA BEJI KEC.JENU Tuban 62352 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(797, '00000636', 'PADANG WIBOWO', '7203412907', 'SEKSI BENGKEL LISTRIK & INSTRUMENTASI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010993', '1965-02-05', 1, 'Male', 'Kawin', '1991-02-13', '2021-03-01', '2021-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'PADANG.WIBOWO@SEMENINDONESIA.COM', 'PER.DINAS EE-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257200', 'Section of Electrical & Instr Workshop', 'SECT', '50048724'),
(798, '00000638', 'MOH. MAS\'UD SIDIK', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010995', '1965-06-01', 1, 'Male', 'Kawin', '1991-02-13', '2021-07-01', '2021-07-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MASUD.SIDIK@SEMENINDONESIA.COM', 'DESA SAMIR Gresik 61111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(799, '00000639', 'MUSLIH', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010996', '1965-07-26', 1, 'Male', 'Kawin', '1991-02-13', '2021-08-01', '2021-08-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MUSLIH@SEMENINDONESIA.COM', 'DESA SAMIR RT.II NO.119 Gresik 61111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(800, '00000641', 'ACHMAD SHOLEH', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010998', '1966-01-27', 1, 'Male', 'Kawin', '1991-02-13', '2022-02-01', '2022-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'ACHMAD.SHOLEH@SEMENINDONESIA.COM', 'PERDIN BLOK D-103 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(801, '00000642', 'TALKAH MARDWI', '7203124907', 'SEKSI  PEMEL LISTRIK & INSTRUMEN CRUSHER', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50010999', '1966-03-02', 1, 'Male', 'Kawin', '1991-02-13', '2022-04-01', '2022-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'TALKAH.MARDWI@SEMENINDONESIA.COM', 'PER.DINAS T-01 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71222300', 'Section of Crusher Elins Maint', 'SECT', '50048712'),
(802, '00000644', 'DWI ARIS GUNAWAN', '7203333907', 'SEKSI PEMEL LIST & INSTR PACKER & PELABH', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011001', '1968-10-13', 1, 'Male', 'Kawin', '1991-02-13', '2024-11-01', '2024-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'DWI.ARIS@SEMENINDONESIA.COM', 'PER.DINAS A-8 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253400', 'Section of Packer & TBN Port Elins Maint', 'SECT', '50048720'),
(803, '00000646', 'R. SATRIJO UTOMO, SE.', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011003', '1969-06-28', 1, 'Male', 'Kawin', '1991-02-13', '2025-07-01', '2025-07-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SATRIJO.UTOMO@SEMENINDONESIA.COM', 'PER. DINAS PTSG BLOK E -09 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717');
INSERT INTO `she_master_pegawai` (`id`, `mk_nopeg`, `mk_nama`, `mk_cttr`, `mk_cttr_text`, `mk_employee_emp_group`, `mk_employee_emp_group_text`, `mk_employee_emp_subgroup`, `mk_employee_emp_subgroup_text`, `company`, `company_text`, `persarea`, `persarea_text`, `cp_kode`, `mk_tgl_lahir`, `mk_jenis_kel_code`, `mk_jenis_kel`, `mk_perkawinan`, `mk_tgl_masuk`, `mk_tgl_pensiun`, `mk_tgl_meninggal`, `mk_kontrak_desc`, `mk_py_area`, `mk_py_area_text`, `lokasi_code`, `lokasi`, `mk_email`, `mk_alamat_rumah`, `mk_alamat_kantor`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`) VALUES
(804, '00000647', 'JUWARI', '7203124907', 'SEKSI  PEMEL LISTRIK & INSTRUMEN CRUSHER', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011004', '1969-09-08', 1, 'Male', 'Kawin', '1991-02-13', '2025-10-01', '2025-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'JUWARI@SEMENINDONESIA.COM', 'PER.DINAS B-16 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71222300', 'Section of Crusher Elins Maint', 'SECT', '50048712'),
(805, '00000649', 'W A S I S', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011006', '1969-12-25', 1, 'Male', 'Kawin', '1991-02-13', '2026-01-01', '2026-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'WASIS@SEMENINDONESIA.COM', 'JL. WELIRANG 64 A KEPANJEN Malang (kabupaten) 65111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(806, '00000651', 'SUDARNO', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011008', '1970-07-27', 1, 'Male', 'Kawin', '1991-02-13', '2026-08-01', '2026-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUDARNO@SEMENINDONESIA.COM', 'JL. KARANG PUCANG 101 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(807, '00000653', 'DJOKO MARSUDIONO, SE.', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011010', '1970-10-22', 1, 'Male', 'Kawin', '1991-02-13', '2026-11-01', '2026-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'DJOKO.MARSUDIONO@SEMENINDONESIA.COM', 'PER.DINAS F-11 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(808, '00000656', 'HARYO SUBAGYO', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011013', '1970-11-26', 1, 'Male', 'Kawin', '1991-02-13', '2026-12-01', '2026-12-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'HARYO.SUBAGYO@SEMENINDONESIA.COM', 'JL. POGOT BARU 36 Surabaya 60129 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(809, '00000658', 'MOHAMAD SOIM', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011015', '1971-01-27', 1, 'Male', 'Kawin', '1991-02-13', '2027-02-01', '2027-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MOHAMAD.SOIM@SEMENINDONESIA.COM', 'JL. VETERAN III-B/41-B Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(810, '00000659', 'YUDIONO', '7203231907', 'SEKSI PEMELIHARAAN LISTRIK RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011016', '1971-04-17', 1, 'Male', 'Kawin', '1991-02-13', '2027-05-01', '2027-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'YUDIONO@SEMENINDONESIA.COM', 'DESA MANDIREJO MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253100', 'Section of RKC 1-2 Electrical Maint', 'SECT', '50048720'),
(811, '00000694', 'MOCH. HARIS JUSRON', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011051', '1968-04-23', 1, 'Male', 'Kawin', '1991-09-09', '2024-05-01', '2024-05-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MOCH.JUSRON@SEMENINDONESIA.COM', 'PERDIN PTSG BLOK G - 04 Surabaya 60224 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(812, '00000697', 'HADI POERWANTORO', '7203213033', 'RAW MILL TBN3 - SEKSI RKC3', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011054', '1969-01-22', 1, 'Male', 'Kawin', '1991-09-09', '2025-02-01', '2025-02-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HADI.POERWANTORO@SEMENINDONESIA.COM', 'MONDOKAN SANTOSO BLOK Y-3 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232100', 'Section of RKC 3 Operation', 'SECT', '50048715'),
(813, '00000698', 'SUGENG MULYONO', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011055', '1969-02-18', 1, 'Male', 'Kawin', '1991-09-09', '2025-03-01', '2025-03-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SUGENG.MULYONO@SEMENINDONESIA.COM', 'PERDIN. BLOK D-118 GRESIK Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(814, '00000714', 'BAMBANG HERMAWAN', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011071', '1971-12-15', 1, 'Male', 'Kawin', '1991-09-09', '2028-01-01', '2028-01-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'BAMBANG.HERMAWAN@SEMENINDONESIA.COM', 'KRUKAH LAMA II/10 Surabaya 60284 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(815, '00000715', 'IMRON KHOLIDI', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011072', '1972-03-02', 1, 'Male', 'Kawin', '1991-09-09', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'IMRON.KHOLIDI@SEMENINDONESIA.COM', 'PER.DINAS C-1 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(816, '00000716', 'GATHOT SUWARNO', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011073', '1972-03-11', 1, 'Male', 'Kawin', '1991-09-09', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'GATHOT.SUWARNO@SEMENINDONESIA.COM', 'PER.DINAS CC-12 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(817, '00000717', 'DARTO', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011074', '1972-03-16', 1, 'Male', 'Kawin', '1991-09-09', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'DARTO@SEMENINDONESIA.COM', 'PERDIN. BLOK G-16 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(818, '00000718', 'PRASETYO DJATI', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011075', '1972-04-28', 1, 'Male', 'Kawin', '1991-09-09', '2028-05-01', '2028-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'PRASETYO.DJATI@SEMENINDONESIA.COM', 'JL. KARANG INDAH BLOK AA-15 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(819, '00000719', 'SUMANTO, ST.', '7203532907', 'SEKSI PEMEL INSTRUMEN RKC 3-4', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011076', '1972-05-10', 1, 'Male', 'Kawin', '1991-09-09', '2028-06-01', '2028-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUMANTO@SEMENINDONESIA.COM', 'PER.DINAS NN-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71254200', 'Section of RKC 3-4 Instrument Maint', 'SECT', '50048721'),
(820, '00000726', 'TARWOKO', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011083', '1972-08-23', 1, 'Male', 'Kawin', '1991-09-09', '2028-09-01', '2028-09-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'TARWOKO@SEMENINDONESIA.COM', 'JL. VETERAN 1 RT/RW 05/02 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(821, '00000727', 'RADIP SURFIANTO', '7203211031', 'RAW MILL TBN1 SEKSI RKC1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011084', '1972-09-12', 1, 'Male', 'Kawin', '1991-09-09', '2028-10-01', '2028-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'RADIP.SURFIANTO@SEMENINDONESIA.COM', 'PER.DINAS K-2 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231100', 'Section of RKC 1 Operation', 'SECT', '50048714'),
(822, '00000728', 'SLAMET WIDODO, ST.', '7203232907', 'SEKSI PEMEL INSTRUMEN RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011085', '1980-07-10', 1, 'Male', 'Kawin', '2010-05-01', '2036-08-01', '2036-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SLAMET.WIDODO728@SEMENINDONESIA.COM', 'DS. KEBONAGUNG RT.22/07 Madiun (kabupaten) 63111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253200', 'Section of RKC 1-2 Instrument Maint', 'SECT', '50048720'),
(823, '00000729', 'TOTOK KARYANTO', '7103343907', 'SEKSI PEMEL. LISTRIK GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011086', '1972-10-27', 1, 'Male', 'Kawin', '1991-09-09', '2028-11-01', '2028-11-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'TOTOK.KARYANTO@SEMENINDONESIA.COM', 'JL. MAGETAN I/13 GKB Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242300', 'Section of Gresik Plant&Port Elect Maint', 'SECT', '50048717'),
(824, '00000731', 'ARIEF MARTOYO', '7203211031', 'RAW MILL TBN1 SEKSI RKC1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011088', '1972-12-04', 1, 'Male', 'Kawin', '1991-09-09', '2029-01-01', '2029-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ARIEF.MARTOYO@SEMENINDONESIA.COM', 'PER.DINAS GG-18 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231100', 'Section of RKC 1 Operation', 'SECT', '50048714'),
(825, '00000733', 'ALMASIUS SUBAGYA', '7203311051', 'FINISH MILL 1 T1 SEKSI FM TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011090', '1972-12-31', 1, 'Male', 'Kawin', '1991-09-09', '2029-01-01', '2029-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ALMASIUS.SUBAGYA@SEMENINDONESIA.COM', 'PER.DINAS GG-5 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241100', 'Section of FM 1-2 Operation', 'SECT', '50048716'),
(826, '00000734', 'FAUZAN RAMADHANI, ST.', '7203232907', 'SEKSI PEMEL INSTRUMEN RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011091', '1984-06-14', 1, 'Male', 'Kawin', '2010-05-01', '2040-07-01', '2040-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'FAUZAN.RAMADHANI@SEMENINDONESIA.COM', 'JL. MANUKAN DONO I/14 SBY Surabaya 60185 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253200', 'Section of RKC 1-2 Instrument Maint', 'SECT', '50048720'),
(827, '00000739', 'GUNUNG BINAGARA', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011096', '1973-03-17', 1, 'Male', 'Kawin', '1991-09-09', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'GUNUNG.BINAGARA@SEMENINDONESIA.COM', 'PER.DINAS Q-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(828, '00000740', 'NALENDRA PERMANA, ST.', '7203231907', 'SEKSI PEMELIHARAAN LISTRIK RKC 1-2', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011097', '1988-04-20', 1, 'Male', 'Kawin', '2010-05-01', '2044-05-01', '2044-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NALENDRA.PERMANA@SEMENINDONESIA.COM', 'GAYUNG KEBONSARI 9/7A SURABAYA Surabaya 60231 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253100', 'Section of RKC 1-2 Electrical Maint', 'SECT', '50048720'),
(829, '00000780', 'SETIAWAN PRASETYO', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 20, 'Senior Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011141', '1968-03-05', 1, 'Male', 'Kawin', '1992-01-06', '2024-04-01', '2024-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SETIAWAN.PRASETYO@SEMENINDONESIA.COM', 'PER.DINAS G-193 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(830, '00000793', 'GINARKO ISNUBROTO, Drs., MBA., Ak.', '7202000000', 'GROUP HEAD SUPPORTING', 1, 'Active', 15, 'General Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50044801', '1968-08-23', 1, 'Male', 'Kawin', '1992-09-21', '2024-09-01', '2024-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'GINARKO.ISNUBROTO@SEMENINDONESIA.COM', 'PERDIN. BLOK FF-03 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71300000', 'Group Head of Supporting', 'KOMP', '00007000'),
(831, '00000795', 'SYAMSUL HUDHA, ST.', '7203400901', 'DEPARTEMEN PEMELIHARAAN', 1, 'Active', 20, 'Senior Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011156', '1967-09-22', 1, 'Male', 'Kawin', '1992-11-02', '2023-10-01', '2023-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SYAMSUL.HUDHA@SEMENINDONESIA.COM', 'PER.DINAS G-09 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71250000', 'Department of Maintenance', 'DEPT', '50048672'),
(832, '00000796', 'ICHWANUL MUSLIMIN', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011157', '1968-07-17', 1, 'Male', 'Kawin', '1992-11-02', '2024-08-01', '2024-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ICHWANUL.MUSLIMIN@SEMENINDONESIA.COM', 'PER.DINAS J-2 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(833, '00000799', 'MOCHAMAD SYAIFUL, ST.', '7203300901', 'DEPARTEMEN PRODUKSI  SEMEN', 1, 'Active', 15, 'General Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011160', '1968-03-24', 1, 'Male', 'Kawin', '1992-11-02', '2024-04-01', '2024-04-01', 'Promosi PJ SI', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCHAMAD.SYAIFUL@SEMENINDONESIA.COM', 'PER.DINAS J-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71240000', 'Department of Cement Production', 'DEPT', '50048672'),
(834, '00000804', 'MOHAMMAD SYUCHRIADI, SE.', '7203312000', 'SEKSI PELABUHAN TUBAN & GRESIK', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011165', '1971-08-30', 1, 'Male', 'Kawin', '1993-02-08', '2027-09-01', '2027-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOHAMMAD.SYUCHRIADI@SEMENINDONESIA.COM', 'JL. JERUK VII NO. 2 RT.4 RW.8 PERUMNAS KAMAL BANYUAJUH Bangkalan 69162 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242400', 'Section of Gresik & Tuban Port Operation', 'SECT', '50048717'),
(835, '00000815', 'NUR WIDJAJANTI, SE.', '7202100000', 'DEPARTEMEN KEUANGAN & SDM', 1, 'Active', 15, 'General Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011176', '1969-09-11', 2, 'Female', 'Kawin', '1993-06-16', '2025-10-01', '2025-10-01', 'Promosi PJ SI', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NUR.WIDJAYANTI@SEMENINDONESIA.COM', 'JL. KEBRAON ASRI L-10 Surabaya 60222 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71310000', 'Department of Finance & HC', 'DEPT', '50048673'),
(836, '00000821', 'WAHJUDY ERNANTO PUTRO, ST.', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011182', '1970-04-18', 1, 'Male', 'Kawin', '1993-08-20', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'WAHJUDY.PUTRO@SEMENINDONESIA.COM', 'PER.DINAS N-8 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(837, '00000822', 'SUHARIYANTO', '7203323906', 'SEKSI PEMELIHARAAN MESIN FM TBN34', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011183', '1966-06-22', 1, 'Male', 'Kawin', '1993-08-20', '2022-07-01', '2022-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUHARIYANTO@SEMENINDONESIA.COM', 'JL. KAPTEN DULASIM 27 Gresik 61112 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252300', 'Section of FM 3-4 Machine Maintenance', 'SECT', '50048719'),
(838, '00000823', 'NGUDI CATUR PRIYANTO', '7203122906', 'SEKSI PEMELIHARAAN MESIN CRUSHER', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011184', '1968-09-26', 1, 'Male', 'Kawin', '1993-08-20', '2024-10-01', '2024-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NGUDI.PRIYANTO@SEMENINDONESIA.COM', 'JL. TANJUNG 38 CILACAP Semarang (kabupaten) 50111 Jawa Tengah', 'Jl. Veteran, Gresik 61122', '71222200', 'Section of Crusher Machine Maintenance', 'SECT', '50048712'),
(839, '00000825', 'AGUS SULISDIONO', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011186', '1969-11-28', 1, 'Male', 'Kawin', '1993-08-20', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AGUS.SULISDIONO@SEMENINDONESIA.COM', 'JL. DIPONEGORO GG. PUSPASARI Tuban 62313 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(840, '00000826', 'IMAM SUBEKI', '7203414901', 'SEKSI KONSTRUKSI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011187', '1970-09-09', 1, 'Male', 'Kawin', '1993-08-20', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'IMAM.SUBEKI@SEMENINDONESIA.COM', 'JL. JENU 288 MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257300', 'Section of Construction', 'SECT', '50048724'),
(841, '00000827', 'SAIFUL ARIFIN', '7203411906', 'SEKSI PEMELIHARAAN UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011188', '1970-11-03', 1, 'Male', 'Kawin', '1993-08-20', '2026-12-01', '2026-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SAIFUL.ARIFIN@SEMENINDONESIA.COM', 'LATSARI V NO.5 RT-4/RW-1 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223400', 'Section of Utility Maintenance', 'SECT', '50048713'),
(842, '00000828', 'MARIONO', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011189', '1971-01-29', 1, 'Male', 'Kawin', '1993-08-20', '2027-02-01', '2027-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MARIONO@SEMENINDONESIA.COM', 'JL. AWIKUN I/10 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(843, '00000830', 'SUNOTO', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011191', '1971-05-01', 1, 'Male', 'Kawin', '1993-08-20', '2027-06-01', '2027-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUNOTO@SEMENINDONESIA.COM', 'PER. DINAS EE - 04, SUMURGUNG Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(844, '00000831', 'HADI WAHYONO', '7203311051', 'FINISH MILL 1 T1 SEKSI FM TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011192', '1971-10-05', 1, 'Male', 'Kawin', '1993-08-20', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HADI.WAHYONO@SEMENINDONESIA.COM', 'PERUM KARANG INDAH BE-9 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241100', 'Section of FM 1-2 Operation', 'SECT', '50048716'),
(845, '00000832', 'EDI SUSMIANTO', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011193', '1971-11-04', 1, 'Male', 'Kawin', '1993-08-20', '2027-12-01', '2027-12-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'EDI.SUSMIANTO@SEMENINDONESIA.COM', 'PERDIN BLOK D-45 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(846, '00000834', 'SUMARIYANTO', '7203522906', 'SIE PEMELIHARAAN MESIN KILN & COAL MILL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011195', '1972-06-08', 1, 'Male', 'Kawin', '1993-08-20', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUMARIYANTO@SEMENINDONESIA.COM', 'JL. KARANG PUCANG 101 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252200', 'Section of KCM 3-4 Machine Maintenance', 'SECT', '50048719'),
(847, '00000835', 'MUCHAMAD MAKSUM', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011196', '1972-08-05', 1, 'Male', 'Kawin', '1993-08-20', '2028-09-01', '2028-09-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MUCHAMAD.MAKSUM@SEMENINDONESIA.COM', 'PERDIN. D-13 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(848, '00000837', 'SUDARMAWAN', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011198', '1972-11-14', 1, 'Male', 'Kawin', '1993-08-20', '2028-12-01', '2028-12-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SUDARMAWAN@SEMENINDONESIA.COM', 'JL. JAGUNG SUPRAPTO VIF/8 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(849, '00000838', 'SLAMET SOEDJATMIKO', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011199', '1972-11-21', 1, 'Male', 'Kawin', '1993-08-20', '2028-12-01', '2028-12-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SLAMET.SOEDJATMIKO@SEMENINDONESIA.COM', 'PER.DINAS D-80 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(850, '00000839', 'NGUDIJONO, SE.', '7203420901', 'UNIT PERENCANAAN DAN EVALUASI PEMELIHARA', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011200', '1973-02-10', 1, 'Male', 'Kawin', '1993-08-20', '2029-03-01', '2029-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NGUDIJONO@SEMENINDONESIA.COM', 'JL. LUKMAN HAKIM 120/64 Tuban 62316 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255000', 'Unit of Maintenance Planning & Eval', 'BIRO', '50048704'),
(851, '00000840', 'ADIB HAMIDI, ST.', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011201', '1973-02-28', 1, 'Male', 'Kawin', '1993-08-20', '2029-03-01', '2029-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ADIB.HAMIDI@SEMENINDONESIA.COM', 'JL. NYAI AGENG AREM IX/26 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(852, '00000842', 'KHOIRON YASIR', '7203221906', 'SEKSI PEMELIHARAAN MESIN ROLLER MILL 1-2', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011203', '1973-04-27', 1, 'Male', 'Kawin', '1993-08-20', '2029-05-01', '2029-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'KHOIRON.YASIR@SEMENINDONESIA.COM', 'JL. MADURA L-08 SIDORUKUN INDA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71251100', 'Section of RM 1-2 Machine Maintenance', 'SECT', '50048718'),
(853, '00000843', 'MOCHAMAD UMAR', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011204', '1973-05-10', 1, 'Male', 'Kawin', '1993-08-20', '2029-06-01', '2029-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCHAMAD.UMAR@SEMENINDONESIA.COM', 'DS. SAMBUNG GEDE GG III MERAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(854, '00000846', 'MUHAMMAD NASIR ROCHMAN', '7203222906', 'SIE PEMELIHARAAN MESIN KILN & COAL MILL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011207', '1973-08-28', 1, 'Male', 'Kawin', '1993-08-20', '2029-09-01', '2029-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MUHAMMAD.ROCHMAN@SEMENINDONESIA.COM', 'JL. JAMBU RAYA III/41 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71251200', 'Section of KCM 1-2 Machine Maintenance', 'SECT', '50048718'),
(855, '00000847', 'MUHAMMAD TAUFIQ', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011208', '1973-09-18', 1, 'Male', 'Kawin', '1993-08-20', '2029-10-01', '2029-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MUHAMAD.TAUFIQ@SEMENINDONESIA.COM', 'RT 03 RW 09 DS PERBON TUBAN Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(856, '00000848', 'HASAN AS\'ARY', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011209', '1973-09-24', 1, 'Male', 'Kawin', '1993-08-20', '2029-10-01', '2029-10-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'HASAN.ASARY@SEMENINDONESIA.COM', 'JL. SUNAN GIRI IV NO.10 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(857, '00000849', 'LESTARIYONO', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011210', '1973-11-18', 1, 'Male', 'Kawin', '1993-08-20', '2029-12-01', '2029-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'LESTARIYONO@SEMENINDONESIA.COM', 'DS. TUWIRI WETAN 53 MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(858, '00000850', 'EFFENDY HARYADI', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011211', '1973-11-23', 1, 'Male', 'Kawin', '1993-08-20', '2029-12-01', '2029-12-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'EFFENDY.HARYADI@SEMENINDONESIA.COM', 'JL. PANDIGILING 272 B Surabaya 60262 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(859, '00000853', 'MALIKAN', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011214', '1974-03-02', 1, 'Male', 'Kawin', '1993-08-20', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MALIKAN@SEMENINDONESIA.COM', 'JL. A. RACMAN HAKIM NO.7 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(860, '00000854', 'WITOYO', '7203611901', 'SEKSI PEMELIHARAAN EPDC', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011215', '1974-03-14', 1, 'Male', 'Kawin', '1993-08-20', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'WITOYO@SEMENINDONESIA.COM', 'JL.LETDA SUCIPTO RT.004/RW.007 KEL.PERBON Tuban 62313 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71254400', 'Section of EPDC Maintenance', 'SECT', '50048721'),
(861, '00000855', 'QOMARUDDIN', '7203222906', 'SIE PEMELIHARAAN MESIN KILN & COAL MILL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011216', '1974-04-03', 1, 'Male', 'Kawin', '1993-08-20', '2030-05-01', '2030-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'QOMARUDDIN.855@SEMENINDONESIA.COM', 'PER. DINAS B - 08, SUMURGUNG Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71251200', 'Section of KCM 1-2 Machine Maintenance', 'SECT', '50048718'),
(862, '00000856', 'SULANTIP WIBAWANTO, ST.', '7203413906', 'SEKSI BENGKEL MESIN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011217', '1974-04-04', 1, 'Male', 'Kawin', '1993-08-20', '2030-05-01', '2030-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SULANTIP.WIBAWANTO@SEMENINDONESIA.COM', 'JL. PASAR IKAN NO.10 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257100', 'Section of Machine Workshop', 'SECT', '50048724'),
(863, '00000857', 'MOH. SAIFUDDIN', '7103342906', 'SEKSI PEMELIHARAAN MESIN  GP GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011218', '1974-05-05', 1, 'Male', 'Kawin', '1993-08-20', '2030-06-01', '2030-06-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MOH.SAIFUDDIN@SEMENINDONESIA.COM', 'PERDIN BLOK D-60 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242200', 'Section of GSK Plant&Port Machine Maint', 'SECT', '50048717'),
(864, '00000859', 'TUTUK SUPARDI', '7203521906', 'SEKSI PEMELIHARAAN MESIN ROLLER MILL 3-4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011220', '1974-05-25', 1, 'Male', 'Kawin', '1993-08-20', '2030-06-01', '2030-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'TUTUK.SUPARDI@SEMENINDONESIA.COM', 'KEBONSARI GG.I NO. 493 Tuban 62317 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252100', 'Section of RM 3-4 Machine Maintenance', 'SECT', '50048719'),
(865, '00000860', 'ARIF ZAINUDDIN', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011221', '1974-06-18', 1, 'Male', 'Kawin', '1993-08-20', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ARIF.ZAINUDDIN@SEMENINDONESIA.COM', 'PER.DINAS BLOK D-50 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(866, '00000862', 'SUPARDI', '7203522906', 'SIE PEMELIHARAAN MESIN KILN & COAL MILL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011223', '1974-08-15', 1, 'Male', 'Kawin', '1993-08-20', '2030-09-01', '2030-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUPARDI.862@SEMENINDONESIA.COM', 'JL. WR.SUPRATMAN NO.53 Tuban 62318 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71252200', 'Section of KCM 3-4 Machine Maintenance', 'SECT', '50048719'),
(867, '00000864', 'SLAMET HARIADI', '7203414901', 'SEKSI KONSTRUKSI', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011225', '1974-10-17', 1, 'Male', 'Kawin', '1993-08-20', '2030-11-01', '2030-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SLAMET.HARIADI@SEMENINDONESIA.COM', 'PER.DINAS GG-06 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257300', 'Section of Construction', 'SECT', '50048724'),
(868, '00000869', 'AGUNG SUJARWO', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011230', '1975-09-12', 1, 'Male', 'Kawin', '1993-08-20', '2031-10-01', '2031-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AGUNG.SUJARWO@SEMENINDONESIA.COM', 'LATSARI GG V - 5 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(869, '00000870', 'GATOT SUPRIYANTO', '7203434914', 'SEKSI KEBERSIHAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011231', '1975-11-26', 1, 'Male', 'Kawin', '1993-08-20', '2031-12-01', '2031-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'GATOT.SUPRIYANTO@SEMENINDONESIA.COM', 'DS. TUWIRI WETAN 53 MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212300', 'Section of Hygiene', 'SECT', '50048709'),
(870, '00000873', 'TRI PRASTYA YUNIANTO', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011234', '1966-06-18', 1, 'Male', 'Kawin', '1993-09-17', '2022-07-01', '2022-07-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'TRI.PRASTYA@SEMENINDONESIA.COM', 'JL. KASUARI JB-11 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(871, '00000874', 'ALI MANSYUR', '7203312000', 'SEKSI PELABUHAN TUBAN & GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011235', '1970-07-20', 1, 'Male', 'Kawin', '1993-09-17', '2026-08-01', '2026-08-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'ALI.MANSYUR@SEMENINDONESIA.COM', 'JL. KARIMUN JAWA M-5 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242400', 'Section of Gresik & Tuban Port Operation', 'SECT', '50048717'),
(872, '00000875', 'PARJITO', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011236', '1970-09-23', 1, 'Male', 'Kawin', '1993-09-17', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'PARJITO@SEMENINDONESIA.COM', 'SEMANDING RT03/RW.II Tuban 62381 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(873, '00000877', 'MUSTIHAM', '7203213033', 'RAW MILL TBN3 - SEKSI RKC3', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011238', '1971-07-01', 1, 'Male', 'Kawin', '1993-09-17', '2027-08-01', '2027-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MUSTIHAM@SEMENINDONESIA.COM', 'PER.DINAS GG-17 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232100', 'Section of RKC 3 Operation', 'SECT', '50048715'),
(874, '00000878', 'AGUS PRIYANTO', '7203212032', 'RAW MILL TBN2 - SEKSI RKC2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011239', '1971-08-22', 1, 'Male', 'Kawin', '1993-09-17', '2027-09-01', '2027-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AGUS.PRIYANTO@SEMENINDONESIA.COM', 'JL. AWIKOEN I/II.C NO.4 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231200', 'Section of RKC 2 Operation', 'SECT', '50048714'),
(875, '00000879', 'KASMARI', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011240', '1972-03-23', 1, 'Male', 'Kawin', '1993-09-17', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'KASMARI@SEMENINDONESIA.COM', 'PERDIN. BLOK G-14 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(876, '00000883', 'MOH. SODIG', '7203434914', 'SEKSI KEBERSIHAN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011244', '1972-09-23', 1, 'Male', 'Kawin', '1993-09-17', '2028-10-01', '2028-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOH.SODIG@SEMENINDONESIA.COM', 'SAMBONGGEDE Gg. SAWO  MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212300', 'Section of Hygiene', 'SECT', '50048709'),
(877, '00000884', 'IMAM MAGHFUR', '7203211031', 'RAW MILL TBN1 SEKSI RKC1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011245', '1973-03-05', 1, 'Male', 'Kawin', '1993-09-17', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'IMAM.MAGHFUR@SEMENINDONESIA.COM', 'RT 002/006 MONDOKAN Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231100', 'Section of RKC 1 Operation', 'SECT', '50048714'),
(878, '00000885', 'EDDIN RUSITO', '7201210000', 'UNIT KEAMANAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011246', '1973-05-23', 1, 'Male', 'Kawin', '1993-09-17', '2029-06-01', '2029-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'EDDIN.RUSITO@SEMENINDONESIA.COM', 'JL. SUNAN KALIJAGA I/2.B Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71112000', 'Unit of Security', 'BIRO', '50048674'),
(879, '00000886', 'RESTONI', '7203313055', 'FINISH MILL 1 T3 SEKSI FM TBN34', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011247', '1973-06-08', 1, 'Male', 'Kawin', '1993-09-17', '2029-07-01', '2029-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'RESTONI@SEMENINDONESIA.COM', 'DESA KARANGAGUNG RT.02 RW.04 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241200', 'Section of FM 3-4 Operation', 'SECT', '50048716'),
(880, '00000888', 'SUSANTO, ST.', '7203311051', 'FINISH MILL 1 T1 SEKSI FM TBN', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011249', '1973-08-25', 1, 'Male', 'Kawin', '1993-09-17', '2029-09-01', '2029-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUSANTO@SEMENINDONESIA.COM', 'PER.DIN FF-18 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241100', 'Section of FM 1-2 Operation', 'SECT', '50048716'),
(881, '00000889', 'CANDRA PRABONDO SARI', '7203123901', 'SEKSI ALAT BERAT & TRANS BATUBARA', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011250', '1973-08-27', 1, 'Male', 'Kawin', '1993-09-17', '2029-09-01', '2029-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'CANDRA.PRABONDO@SEMENINDONESIA.COM', 'PER.DINAS F-09 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212100', 'Section of Heavy Equip & Coal Transport', 'SECT', '50048709'),
(882, '00000891', 'LULUS PRIYONO, ST.', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011252', '1974-01-19', 1, 'Male', 'Kawin', '1993-09-17', '2030-02-01', '2030-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'LULUS.PRIYONO@SEMENINDONESIA.COM', 'TAMAN SIWALAN INDAH J-9 MENGANTI Gresik 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(883, '00000893', 'YATMUJI', '7203211031', 'RAW MILL TBN1 SEKSI RKC1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011254', '1974-03-20', 1, 'Male', 'Kawin', '1993-09-17', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'YATMUJI@SEMENINDONESIA.COM', 'JL. SUNAN KUDUS 7 LATSARI Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231100', 'Section of RKC 1 Operation', 'SECT', '50048714'),
(884, '00000894', 'RULLY IRFAN', '7203312000', 'SEKSI PELABUHAN TUBAN & GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011255', '1974-04-16', 1, 'Male', 'Kawin', '1993-09-17', '2030-05-01', '2030-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'RULLY.ERFAN@SEMENINDONESIA.COM', 'PER.DINAS CC-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242400', 'Section of Gresik & Tuban Port Operation', 'SECT', '50048717'),
(885, '00000896', 'AGUS YULIANTO', '7203311051', 'FINISH MILL 1 T1 SEKSI FM TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011257', '1974-07-02', 1, 'Male', 'Kawin', '1993-09-17', '2030-08-01', '2030-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AGUS.YULIANTO@SEMENINDONESIA.COM', 'DESA SUMURJALAK RT.02/RW.V Tuban 62381 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241100', 'Section of FM 1-2 Operation', 'SECT', '50048716'),
(886, '00000897', 'ABDURROHMAN', '7203311051', 'FINISH MILL 1 T1 SEKSI FM TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50000361', '1974-07-03', 1, 'Male', 'Kawin', '1993-09-17', '2030-08-01', '2030-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ABDURROHMAN@SEMENINDONESIA.COM', 'PER.DINAS KK-04 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241100', 'Section of FM 1-2 Operation', 'SECT', '50048716'),
(887, '00000898', 'ANDHY NUGROHO', '7203311051', 'FINISH MILL 1 T1 SEKSI FM TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011259', '1974-08-29', 1, 'Male', 'Kawin', '1993-09-17', '2030-09-01', '2030-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ANDHY.NUGROHO@SEMENINDONESIA.COM', 'SENDANGHARJO 7/45 Tuban 62319 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241100', 'Section of FM 1-2 Operation', 'SECT', '50048716'),
(888, '00000900', 'AGUS SETIYONO', '7203312061', 'PACKER T1 CRH SIE PACKER & PELB TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011261', '1974-11-01', 1, 'Male', 'Kawin', '1993-09-17', '2030-12-01', '2030-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SETIYONO.AGUS@SEMENINDONESIA.COM', 'JL. PRAMUKA GG VII - 24 Tuban 62315 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241300', 'Section of Packer Operation', 'SECT', '50048716'),
(889, '00000901', 'MUSTAKIM', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011262', '1975-01-06', 1, 'Male', 'Kawin', '1993-09-17', '2031-02-01', '2031-02-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'MUSTAKIM.901@SEMENINDONESIA.COM', 'JL. KAPTEN DULASIM II.E/11 Gresik 61112 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(890, '00000903', 'SHOLMAI DAYYIN', '7103341901', 'SEKSI FINISH MILL & PACKER GRESI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011264', '1975-05-18', 1, 'Male', 'Kawin', '1993-09-17', '2031-06-01', '2031-06-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SHOLMAI.DAYYIN@SEMENINDONESIA.COM', 'ROMOKALISARI III/15 Surabaya 60192 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242100', 'Section of Gresik Plant Operation', 'SECT', '50048717'),
(891, '00000904', 'MAT RIPIN, SE.', '7203312061', 'PACKER T1 CRH SIE PACKER & PELB TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011265', '1975-07-09', 1, 'Male', 'Kawin', '1993-09-17', '2031-08-01', '2031-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MAT.RIPIN@SEMENINDONESIA.COM', 'RONGGOMULYO GG WIJAYA KUSUMA I Tuban 62312 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241300', 'Section of Packer Operation', 'SECT', '50048716'),
(892, '00000906', 'THOYIB ROSYIDI', '7203312000', 'SEKSI PELABUHAN TUBAN & GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011267', '1966-02-01', 1, 'Male', 'Kawin', '1993-11-01', '2022-03-01', '2022-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'THOYIB.ROSYIDI@SEMENINDONESIA.COM', 'JL. SUNAN KALIJAGA 26 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242400', 'Section of Gresik & Tuban Port Operation', 'SECT', '50048717'),
(893, '00000913', 'MOCHAMMAD RAAFI\'UD, S.Sos.', '7203312000', 'SEKSI PELABUHAN TUBAN & GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011274', '1969-10-27', 1, 'Male', 'Kawin', '1993-11-01', '2025-11-01', '2025-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCHAMMAD.RAAFIUD@SEMENINDONESIA.COM', 'PER. DINAS F - 06, SUMURGUNG Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242400', 'Section of Gresik & Tuban Port Operation', 'SECT', '50048717'),
(894, '00000914', 'ABDUL MUNIF EFENDI, SE.', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011275', '1970-04-17', 1, 'Male', 'Kawin', '1993-11-01', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ABDUL.MUNIF@SEMENINDONESIA.COM', 'JL. VETERAN IX D/4 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(895, '00000916', 'MUHAMMAD IRFAN', '7203312061', 'PACKER T1 CRH SIE PACKER & PELB TBN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011277', '1970-05-09', 1, 'Male', 'Kawin', '1993-11-01', '2026-06-01', '2026-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MUHAMMAD.IRFAN@SEMENINDONESIA.COM', 'JL. DIPONEGORO GG KENARI - 11 Tuban 62313 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71241300', 'Section of Packer Operation', 'SECT', '50048716'),
(896, '00000919', 'JUNIARDI KRISTANTO', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011280', '1970-06-06', 1, 'Male', 'Kawin', '1993-11-01', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'JUNIARDI.KRISTANTO@SEMENINDONESIA.COM', 'JL FLORES BLOK BB NO 5 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(897, '00000922', 'ZAENAL CHOIRI', '7203440901', 'UNIT INSPEKSI PEMELIHARAAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011283', '1970-12-06', 1, 'Male', 'Kawin', '1993-11-01', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ZAENAL.CHOIRI@SEMENINDONESIA.COM', 'JL. SUNAN MURIA 36 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71256000', 'Unit of Maintenance Inspection', 'BIRO', '50048704'),
(898, '00000926', 'IVAN SETYABUDIE, SE.', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011287', '1971-09-15', 1, 'Male', 'Kawin', '1993-11-01', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'IVAN.SETYABUDIE@SEMENINDONESIA.COM', 'PER.DINAS FF-21 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(899, '00000927', 'MOCH. FACHRUDIN', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011288', '1971-09-17', 1, 'Male', 'Kawin', '1993-11-01', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCH.FACHRUDIN@SEMENINDONESIA.COM', 'DSN. KRAJAN RT. 1 RW. 2 KEL. KARANG KEC. SEMANDING Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(900, '00000933', 'EDY HERMAWAN', '7201230000', 'UNIT SARANA UMUM', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011294', '1972-05-28', 1, 'Male', 'Kawin', '1993-11-01', '2028-06-01', '2028-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'EDY.HERMAWAN@SEMENINDONESIA.COM', 'JL. DIPONEGORO 11 Tuban 62313 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71313000', 'Unit of General Affair & Asset', 'BIRO', '50048705'),
(901, '00000936', 'KANTI PULUH HANDAYANI', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011297', '1973-05-10', 2, 'Female', 'Kawin', '1993-11-01', '2029-06-01', '2029-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'KANTI.PULUH@SEMENINDONESIA.COM', 'JL.MUTIARA II C-4 BUKIT KARANG Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(902, '00000938', 'MAYA YUSNITA, SE.', '7202110000', 'UNIT AKUNTANSI & KEUANGAN OPERASIONAL', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011299', '1973-06-04', 2, 'Female', 'Kawin', '1993-11-01', '2029-07-01', '2029-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MAYA.YUSNITA@SEMENINDONESIA.COM', 'TEMANDANG 349 RT-5/RW-3 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71311000', 'Unit of Opr Accounting & Finance', 'BIRO', '50048705');
INSERT INTO `she_master_pegawai` (`id`, `mk_nopeg`, `mk_nama`, `mk_cttr`, `mk_cttr_text`, `mk_employee_emp_group`, `mk_employee_emp_group_text`, `mk_employee_emp_subgroup`, `mk_employee_emp_subgroup_text`, `company`, `company_text`, `persarea`, `persarea_text`, `cp_kode`, `mk_tgl_lahir`, `mk_jenis_kel_code`, `mk_jenis_kel`, `mk_perkawinan`, `mk_tgl_masuk`, `mk_tgl_pensiun`, `mk_tgl_meninggal`, `mk_kontrak_desc`, `mk_py_area`, `mk_py_area_text`, `lokasi_code`, `lokasi`, `mk_email`, `mk_alamat_rumah`, `mk_alamat_kantor`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`) VALUES
(903, '00000939', 'NUR HASYIM', '7201230000', 'UNIT SARANA UMUM', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011300', '1973-08-24', 1, 'Male', 'Kawin', '1993-11-01', '2029-09-01', '2029-09-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NUR.HASYIM@SEMENINDONESIA.COM', 'JL. TEUKU UMAR GG V - 2 LATSAR Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71313000', 'Unit of General Affair & Asset', 'BIRO', '50048705'),
(904, '00000943', 'UMU KHOIRIYAH', '7201230000', 'UNIT SARANA UMUM', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011304', '1974-07-12', 2, 'Female', 'Kawin', '1993-11-01', '2030-08-01', '2030-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'UMU.KHOIRIYAH@SEMENINDONESIA.COM', 'TEMANDANG MERAK URAK Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71313000', 'Unit of General Affair & Asset', 'BIRO', '50048705'),
(905, '00000954', 'FATHUR RAHMAN', '7202110000', 'UNIT AKUNTANSI & KEUANGAN OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011315', '1974-05-02', 1, 'Male', 'Kawin', '1994-01-03', '2030-06-01', '2030-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'FATHUR.RAHMAN@SEMENINDONESIA.COM', 'TEMANDANG MERAK URAK Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71311000', 'Unit of Opr Accounting & Finance', 'BIRO', '50048705'),
(906, '00000955', 'LAURENSIUS SLAMET MARTONO', '7203131000', 'SEKSI PENGELOLAAN LAHAN PASCA TAMBANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011316', '1974-06-18', 1, 'Male', 'Kawin', '1994-01-03', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'LAURENSIUS.SLAMET@SEMENINDONESIA.COM', 'LATSARI GG V - 5 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71221300', 'Section of Land Reclamation', 'SECT', '50048711'),
(907, '00000958', 'RAHMAT PATRIA', '7203312000', 'SEKSI PELABUHAN TUBAN & GRESIK', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011319', '1966-07-13', 1, 'Male', 'Kawin', '1994-01-17', '2022-08-01', '2022-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'RAHMAT.PATRIA@SEMENINDONESIA.COM', 'JL. UNTUNG SURAPATI 19 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71242400', 'Section of Gresik & Tuban Port Operation', 'SECT', '50048717'),
(908, '00000961', 'ALI IMRON', '7202210000', 'UNIT SDM OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011322', '1968-07-19', 1, 'Male', 'Kawin', '1994-01-17', '2024-08-01', '2024-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ALI.IMRON@SEMENINDONESIA.COM', 'PER. DINAS GG-322 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71312000', 'Unit of Opr Human Capital', 'BIRO', '50048705'),
(909, '00000969', 'HARI SISWOYO', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011330', '1970-06-01', 1, 'Male', 'Kawin', '1994-01-17', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HARI.SISWOYO@SEMENINDONESIA.COM', 'JL. SIWALAN PERMAI 1-A/31 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(910, '00000974', 'DADANG SUBAGYO', '7203532907', 'SEKSI PEMEL INSTRUMEN RKC 3-4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011335', '1971-10-10', 1, 'Male', 'Kawin', '1994-01-17', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'DADANG.SUBAGYO@SEMENINDONESIA.COM', 'PER.DINAS KK-03 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71254200', 'Section of RKC 3-4 Instrument Maint', 'SECT', '50048721'),
(911, '00000977', 'EKO AGUS MARDIANTO', '7202110000', 'UNIT AKUNTANSI & KEUANGAN OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011338', '1972-03-09', 1, 'Male', 'Kawin', '1994-01-17', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'EKO.MARDIANTO@SEMENINDONESIA.COM', 'JL. TEUKU UMAR V - 2 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71311000', 'Unit of Opr Accounting & Finance', 'BIRO', '50048705'),
(912, '00000981', 'DWI WACANA', '7202110000', 'UNIT AKUNTANSI & KEUANGAN OPERASIONAL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011342', '1972-11-15', 1, 'Male', 'Kawin', '1994-01-17', '2028-12-01', '2028-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'DWI.WACANA@SEMENINDONESIA.COM', 'LATSARI RT-5/RW-2 16-B Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71311000', 'Unit of Opr Accounting & Finance', 'BIRO', '50048705'),
(913, '00000983', 'PARDIONO', '7203121011', 'CRUSHER BT. KAPUR TUBAN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011344', '1973-04-04', 1, 'Male', 'Kawin', '1994-01-17', '2029-05-01', '2029-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'PARDIONO@SEMENINDONESIA.COM', 'DS. BANYU URIP RT-1/RW-3 SENORI Tuban 62365 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71222100', 'Section of Crusher Operation', 'SECT', '50048712'),
(914, '00000985', 'PUJI RAHAYU, SE.', '7202110000', 'UNIT AKUNTANSI & KEUANGAN OPERASIONAL', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011346', '1973-06-30', 2, 'Female', 'Kawin', '1994-01-17', '2029-07-01', '2029-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'PUJI.RAHAYU@SEMENINDONESIA.COM', 'PER.DINAS D-25 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71311000', 'Unit of Opr Accounting & Finance', 'BIRO', '50048705'),
(915, '00000986', 'YULI NAWANINGSIH', '7201110000', 'UNIT HUMAS & CSR', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011347', '1973-07-23', 2, 'Female', 'Kawin', '1994-01-17', '2029-08-01', '2029-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'YULI.NAWANINGSIH@SEMENINDONESIA.COM', 'JL. TEUKU UMAR GG V - 2 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71111000', 'Unit of Public Relation & CSR', 'BIRO', '50048674'),
(916, '00000990', 'WASITO EDI', '7201230000', 'UNIT SARANA UMUM', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011351', '1974-06-15', 1, 'Male', 'Kawin', '1994-01-17', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'WASITO.EDI@SEMENINDONESIA.COM', 'PER.DINAS DD - 8 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71313000', 'Unit of General Affair & Asset', 'BIRO', '50048705'),
(917, '00000997', 'WIRONSI', '7203212032', 'RAW MILL TBN2 - SEKSI RKC2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011358', '1969-11-07', 1, 'Male', 'Kawin', '1994-03-16', '2025-12-01', '2025-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'WIRONSI@SEMENINDONESIA.COM', 'SAMBUNG GEDE MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231200', 'Section of RKC 2 Operation', 'SECT', '50048714'),
(918, '00000998', 'SANTOSO', '7203231907', 'SEKSI PEMELIHARAAN LISTRIK RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011359', '1970-02-01', 1, 'Male', 'Kawin', '1994-03-16', '2026-03-01', '2026-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SANTOSO.998@SEMENINDONESIA.COM', 'DESA SEMANDING KEC. SEMANDING Tuban 62381 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253100', 'Section of RKC 1-2 Electrical Maint', 'SECT', '50048720'),
(919, '00000999', 'SUYANTO', '7203480902', 'UNIT QUALITY CONTROL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011360', '1970-02-01', 1, 'Male', 'Kawin', '1994-03-16', '2026-03-01', '2026-03-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'SUYANTO.999@SEMENINDONESIA.COM', 'KEL. SINGOSARI RT-3/RW-9 KEBOMAS Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71213000', 'Unit of Quality Control', 'BIRO', '50048700'),
(920, '00001001', 'MOCH. SOBIRIN DWI APRIONO', '7203212032', 'RAW MILL TBN2 - SEKSI RKC2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011362', '1970-04-26', 1, 'Male', 'Kawin', '1994-03-16', '2026-05-01', '2026-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCH.SOBIRIN@SEMENINDONESIA.COM', 'SAMBUNG GEDE MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231200', 'Section of RKC 2 Operation', 'SECT', '50048714'),
(921, '00001006', 'DENNY KURNIAWAN', '7203331907', 'SEKSI PEMEL LISTRIK & INSTR FM 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011367', '1971-09-28', 1, 'Male', 'Kawin', '1994-03-16', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'DENNY.KURNIAWAN@SEMENINDONESIA.COM', 'PER.DINAS B-12 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253300', 'Section of FM 1-2 Elect & Instr Maint', 'SECT', '50048720'),
(922, '00001009', 'JONI MUSLIHAN', '7203480902', 'UNIT QUALITY CONTROL', 1, 'Active', 50, 'Associate', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011370', '1972-05-13', 1, 'Male', 'Kawin', '1994-03-16', '2028-06-01', '2028-06-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'JONI.MUSLIHAN@SEMENINDONESIA.COM', 'DS. CERME LOR 68 RT-1/RW-7 CER Gresik 61171 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71213000', 'Unit of Quality Control', 'BIRO', '50048700'),
(923, '00001012', 'NARSAN', '7203480902', 'UNIT QUALITY CONTROL', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011373', '1973-03-07', 1, 'Male', 'Kawin', '1994-03-16', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 70, 'PT SI Krywn - Gresik', 7100, 'Gresik', 'NARSAN@SEMENINDONESIA.COM', 'DSN SINGOREJO RT-4/RW-4 DAHANREJO Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71213000', 'Unit of Quality Control', 'BIRO', '50048700'),
(924, '00001013', 'MOCH. SYAKIR', '7203333907', 'SEKSI PEMEL LIST & INSTR PACKER & PELABH', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011374', '1973-05-01', 1, 'Male', 'Kawin', '1994-03-16', '2029-06-01', '2029-06-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCH.SYAKIR@SEMENINDONESIA.COM', 'DSN.JUWIRI RT.02 RW.02 TUWIRI WTN Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253400', 'Section of Packer & TBN Port Elins Maint', 'SECT', '50048720'),
(925, '00001016', 'AKHMAD GHUFRON', '7203411906', 'SEKSI PEMELIHARAAN UTILITAS', 1, 'Active', 30, 'Manager', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011377', '1974-03-01', 1, 'Male', 'Kawin', '1994-03-16', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'AKHMAD.GHUFRON@SEMENINDONESIA.COM', 'SAMBONG GEDE RT-1/RW-8 MERAK U Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223400', 'Section of Utility Maintenance', 'SECT', '50048713'),
(926, '00001017', 'MOCH. KHAIRUL AMALA', '7203112901', 'UTILITAS OP UTILITAS', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011378', '1974-09-04', 1, 'Male', 'Kawin', '1994-03-16', '2030-10-01', '2030-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MOCH.KHAIRUL@SEMENINDONESIA.COM', 'BUKIT KARANG AJ-5 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71223300', 'Section of Utility Operation', 'SECT', '50048713'),
(927, '00001026', 'SUGIYANTO, ST.', '7203213033', 'RAW MILL TBN3 - SEKSI RKC3', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011387', '1967-03-06', 1, 'Male', 'Kawin', '1994-05-16', '2023-04-01', '2023-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUGIYANTO.1026@SEMENINDONESIA.COM', 'PER.DINAS DD-18 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232100', 'Section of RKC 3 Operation', 'SECT', '50048715'),
(928, '00001027', 'ACHMAD GOFAR', '7203232907', 'SEKSI PEMEL INSTRUMEN RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011388', '1967-03-06', 1, 'Male', 'Kawin', '1994-05-16', '2023-04-01', '2023-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ACHMAD.GOFAR@SEMENINDONESIA.COM', 'JL. SUNAN KUDUS 7 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253200', 'Section of RKC 1-2 Instrument Maint', 'SECT', '50048720'),
(929, '00001032', 'SIGIT SUTARTO', '7203231907', 'SEKSI PEMELIHARAAN LISTRIK RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011393', '1970-09-14', 1, 'Male', 'Kawin', '1994-05-16', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SIGIT.SUTARTO@SEMENINDONESIA.COM', 'JL. DR. SUTOMO IV-A/4 Gresik 61111 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253100', 'Section of RKC 1-2 Electrical Maint', 'SECT', '50048720'),
(930, '00001033', 'BAMBANG NURCAHYO', '7203214910', 'ALTERNATIF FUEL TBN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011394', '1971-07-19', 1, 'Male', 'Kawin', '1994-05-16', '2027-08-01', '2027-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'BAMBANG.NURCAHYO@SEMENINDONESIA.COM', 'JL. BAMBU I/13 TASIK MADU Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212200', 'Section of AF & 3rd Material', 'SECT', '50048709'),
(931, '00001034', 'SUGIYONO', '7203434914', 'SEKSI KEBERSIHAN', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011395', '1971-07-21', 1, 'Male', 'Kawin', '1994-05-16', '2027-08-01', '2027-08-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUGIYONO.1034@SEMENINDONESIA.COM', 'DESA TEMANDANG MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212300', 'Section of Hygiene', 'SECT', '50048709'),
(932, '00001037', 'JAYUS ISWANDI', '7203213033', 'RAW MILL TBN3 - SEKSI RKC3', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011398', '1972-03-09', 1, 'Male', 'Kawin', '1994-05-16', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'JAYUS.ISWANDI@SEMENINDONESIA.COM', 'PER.DINAS P - 05, SUMURGUNG Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232100', 'Section of RKC 3 Operation', 'SECT', '50048715'),
(933, '00001038', 'MULYO EDI', '7203214910', 'ALTERNATIF FUEL TBN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011399', '1972-03-11', 1, 'Male', 'Kawin', '1994-05-16', '2028-04-01', '2028-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'MULYO.EDI@SEMENINDONESIA.COM', 'DESA TEMANDANG 129 MERAK URAK Tuban 62355 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212200', 'Section of AF & 3rd Material', 'SECT', '50048709'),
(934, '00001039', 'IWAN HIDAYAT ARSAD', '7203412907', 'SEKSI BENGKEL LISTRIK & INSTRUMENTASI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011400', '1972-10-09', 1, 'Male', 'Kawin', '1994-05-16', '2028-11-01', '2028-11-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'IWAN.HIDAYAT@SEMENINDONESIA.COM', 'JL. DIPONEGORO GG KENARI - 9 Tuban 62313 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257200', 'Section of Electrical & Instr Workshop', 'SECT', '50048724'),
(935, '00001040', 'GUNTARI', '7203214910', 'ALTERNATIF FUEL TBN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011401', '1972-11-09', 1, 'Male', 'Kawin', '1994-05-16', '2028-12-01', '2028-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'GUNTARI@SEMENINDONESIA.COM', 'DESA DAWUNG RT-2/RW-1 PALANG Tuban 62391 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212200', 'Section of AF & 3rd Material', 'SECT', '50048709'),
(936, '00001041', 'EDY WINARTO', '7203421901', 'SEKSI PERENCANAAN SUKU CADANG', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011402', '1972-12-11', 1, 'Male', 'Kawin', '1994-05-16', '2029-01-01', '2029-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'EDY.WINARTO@SEMENINDONESIA.COM', 'PER.DIN GG-15 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71255200', 'Section of Spareparts Planning', 'SECT', '50048722'),
(937, '00001042', 'HENRI SARJONO', '7203232907', 'SEKSI PEMEL INSTRUMEN RKC 1-2', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011403', '1973-03-04', 1, 'Male', 'Kawin', '1994-05-16', '2029-04-01', '2029-04-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HENRI.SARJONO@SEMENINDONESIA.COM', 'JL. SALAK 3 RT-5/RW-3 MAGETAN Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253200', 'Section of RKC 1-2 Instrument Maint', 'SECT', '50048720'),
(938, '00001044', 'KHOIRUL KHAFANI', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011405', '1974-04-26', 1, 'Male', 'Kawin', '1994-05-16', '2030-05-01', '2030-05-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'KHOIRUL.KHAFANI@SEMENINDONESIA.COM', 'JL. SUNAN KALIJAGA 52 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(939, '00001045', 'PRIYANTO BASUKI', '7203211031', 'RAW MILL TBN1 SEKSI RKC1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011406', '1974-06-21', 1, 'Male', 'Kawin', '1994-05-16', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'PRIYANTO.BASUKI@SEMENINDONESIA.COM', 'JL. PETEMON KUBURAN 40-A Surabaya 60152 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71231100', 'Section of RKC 1 Operation', 'SECT', '50048714'),
(940, '00001047', 'NUGROHO TRIANTORO', '7203511034', 'RAW MILL TBN4 SEKSI RKC4', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011408', '1974-11-30', 1, 'Male', 'Kawin', '1994-05-16', '2030-12-01', '2030-12-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'NUGROHO.TRIANTORO@SEMENINDONESIA.COM', 'PER.DINAS I-05 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71232200', 'Section of RKC 4 Operation', 'SECT', '50048715'),
(941, '00001052', 'HENDRI DJAUHARI', '7203333907', 'SEKSI PEMEL LIST & INSTR PACKER & PELABH', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011413', '1969-12-28', 1, 'Male', 'Kawin', '1994-06-09', '2026-01-01', '2026-01-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HENDRI.DJAUHARI@SEMENINDONESIA.COM', 'PER.DINAS CC-08 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71253400', 'Section of Packer & TBN Port Elins Maint', 'SECT', '50048720'),
(942, '00001053', 'ARIFIN', '7203214910', 'ALTERNATIF FUEL TBN1', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011414', '1971-02-01', 1, 'Male', 'Kawin', '1994-06-09', '2027-03-01', '2027-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'ARIFIN.1053@SEMENINDONESIA.COM', 'JL IRIGASI 44 BAMBE, DRIYOREJO Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71212200', 'Section of AF & 3rd Material', 'SECT', '50048709'),
(943, '00001057', 'SUBHAN', '7203414901', 'SEKSI KONSTRUKSI', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011418', '1972-09-23', 1, 'Male', 'Kawin', '1994-06-09', '2028-10-01', '2028-10-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'SUBHAN.1057@SEMENINDONESIA.COM', 'JL. SUNAN KALIJAGA 52 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71257300', 'Section of Construction', 'SECT', '50048724'),
(944, '00001062', 'HARIS WIBOWO, SE.', '7201230000', 'UNIT SARANA UMUM', 1, 'Active', 40, 'Supervisor', 7000, 'PT. Semen Indonesia', 7022, 'PT SI (SI - BU)', '50011423', '1974-02-09', 1, 'Male', 'Kawin', '1994-06-09', '2030-03-01', '2030-03-01', 'Karyawan Tetap', 71, 'PT SI Krywn - Tuban', 7300, 'Tuban', 'HARIS.WIBOWO@SEMENINDONESIA.COM', 'PER.DINAS F-02 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '71313000', 'Unit of General Affair & Asset', 'BIRO', '50048705'),
(945, '00000562', 'SUGIANTO', '5001220000', 'UNIT KOMUNIKASI & CSR', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50010919', '1965-08-21', 1, 'Male', 'Kawin', '1990-11-17', '2021-09-01', '2021-09-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SUGIANTO.562@SEMENINDONESIA.COM', 'PER.DINAS NN-08 Tuban 62351 Jawa Timur', NULL, '51012000', 'Unit of Communication & CSR', 'BIRO', '50048645'),
(946, '00000570', 'SYAMSUL MA\'ARIF, ST.', '5004524901', 'SEKSI INSPEKSI PEMELIHARAAN', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50010927', '1967-04-18', 1, 'Male', 'Kawin', '1990-11-17', '2023-05-01', '2023-05-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SYAMSUL.MAARIF@SEMENINDONESIA.COM', 'PER.DINAS F-04 Tuban 62351 Jawa Timur', NULL, '52022300', 'Section of Maintenance Inspection', 'SECT', '50050284'),
(947, '00000573', 'SUCIPTO, ST.', '5004121901', 'SEKSI OPERASI & PEMELIHARAAN UTILITAS', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50010930', '1967-11-18', 1, 'Male', 'Kawin', '1990-11-17', '2023-12-01', '2023-12-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SUCIPTO@SEMENINDONESIA.COM', 'PER.DINAS A-07 Tuban 62351 Jawa Timur', NULL, '52011100', 'Section of Utility Operation & Maint', 'SECT', '50048690'),
(948, '00000584', 'SUWOKO', '5001220000', 'UNIT KOMUNIKASI & CSR', 1, 'Active', 40, 'Supervisor', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50010941', '1968-08-23', 1, 'Male', 'Kawin', '1990-11-17', '2024-09-01', '2024-09-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SUWOKO@SEMENINDONESIA.COM', 'JL. ELANG C-11 GKA Gresik 61122 Jawa Timur', NULL, '51012000', 'Unit of Communication & CSR', 'BIRO', '50048645'),
(949, '00000594', 'RACHMAD SOLIKIN, ST.', '5002330000', 'UNIT SMSG', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50010951', '1969-08-05', 1, 'Male', 'Kawin', '1990-11-17', '2025-09-01', '2025-09-01', 'Promosi PJ SI', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'RACHMAD.SOLIKIN@SEMENINDONESIA.COM', 'PER.DINAS KK-01 Tuban 62351 Jawa Timur', NULL, '53001000', 'Unit of SMSG', 'BIRO', '50029623'),
(950, '00000630', 'ARDI SUWOJO', '5004521901', 'SEKSI PERENCANAAN PEMELIHARAAN & CAPEX', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50010987', '1971-08-10', 1, 'Male', 'Kawin', '1990-11-17', '2027-09-01', '2027-09-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'ARDI.SUWOJO@SEMENINDONESIA.COM', 'PER.DINAS FF-03 Tuban 62351 Jawa Timur', NULL, '52022400', 'Section of Maintenance Planning & CAPEX', 'SECT', '50050284'),
(951, '00000708', 'YULI YASTORO, ST.', '5002220000', 'UNIT SDM & SARANA UMUM', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011065', '1971-07-01', 1, 'Male', 'Kawin', '1991-09-09', '2027-08-01', '2027-08-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'YULI.YASTORO@SEMENINDONESIA.COM', 'PER.DINAS EE-20 Tuban 62351 Jawa Timur', NULL, '53012000', 'Unit of Human Capital & General Affairs', 'BIRO', '50048652'),
(952, '00000723', 'ACHMAD SHODIEQ', '5004513901', 'SEKSI PEMELIHARAAN INSTRUMEN & DCS', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011080', '1972-06-30', 1, 'Male', 'Kawin', '1991-09-09', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'ACHMAD.SHODIEQ@SEMENINDONESIA.COM', 'PER.DINAS MM-07 Tuban 62351 Jawa Timur', NULL, '52021300', 'Section of Instrument & DCS Maintenance', 'SECT', '50050283'),
(953, '00000813', 'KUSWANDI, SH.', '5001220000', 'UNIT KOMUNIKASI & CSR', 1, 'Active', 20, 'Senior Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011174', '1970-12-02', 1, 'Male', 'Kawin', '1993-06-02', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'KUSWANDI@SEMENINDONESIA.COM', 'PER.DINAS B-17 Tuban 62351 Jawa Timur', NULL, '51012000', 'Unit of Communication & CSR', 'BIRO', '50048645'),
(954, '00000818', 'SLAMET MURSIDIARSO, ST.', '5001240000', 'UNIT HUKUM & GRC', 1, 'Active', 20, 'Senior Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011179', '1970-09-25', 1, 'Male', 'Kawin', '1993-07-01', '2026-10-01', '2026-10-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SLAMET.MURSIDIARSO@SEMENINDONESIA.COM', 'PER.DINAS G-3 Tuban 62351 Jawa Timur', NULL, '51013000', 'Unit of Legal & GRC', 'BIRO', '50048645'),
(955, '00000820', 'SUNARYO, ST.', '5004160901', 'UNIT PERENCANAAN & PENGENDALIAN PRODUKSI', 1, 'Active', 20, 'Senior Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011181', '1970-08-31', 1, 'Male', 'Kawin', '1993-08-02', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SUNARYO.SG@SEMENINDONESIA.COM', 'PER.DINAS JJ-07 Tuban 62351 Jawa Timur', NULL, '52013000', 'Unit of Production Plan & Control', 'BIRO', '50040478'),
(956, '00000892', 'DHARMA SUNYATA, SE.', '5002330000', 'UNIT SMSG', 1, 'Active', 20, 'Senior Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011253', '1974-02-22', 1, 'Male', 'Kawin', '1993-09-17', '2030-03-01', '2030-03-01', 'Promosi PJ SI', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'DHARMA.SUNYATA@SEMENINDONESIA.COM', 'JL.KEBONSARI TENGAH Gg MURNI 1 Gresik 61122 Jawa Timur', NULL, '53001000', 'Unit of SMSG', 'BIRO', '50029623'),
(957, '00000895', 'HARI SUWARTONO', '5004112901', 'SEKSI OPERASI KILN & COAL MILL', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011256', '1974-04-22', 1, 'Male', 'Kawin', '1993-09-17', '2030-05-01', '2030-05-01', 'Promosi PJ SI', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'HARI.SUWARTONO@SEMENINDONESIA.COM', 'PER.DINAS GG-17 Tuban 62351 Jawa Timur', NULL, '52012200', 'Section of KCM Operation', 'SECT', '50040483'),
(958, '00000925', 'NONO HARIJANTO', '5002220000', 'UNIT SDM & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011286', '1971-02-20', 1, 'Male', 'Kawin', '1993-11-01', '2027-03-01', '2027-03-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'NONO.HARIJANTO@SEMENINDONESIA.COM', 'PER.DINAS EE-01 Tuban 62351 Jawa Timur', NULL, '53012000', 'Unit of Human Capital & General Affairs', 'BIRO', '50048652'),
(959, '00001008', 'SABRANITI', '5004111901', 'SEKSI OPERASI CRUSHER & RAW MILL', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011369', '1972-03-17', 1, 'Male', 'Kawin', '1994-03-16', '2028-04-01', '2028-04-01', 'Promosi PJ SI', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'SABRANITI@SEMENINDONESIA.COM', 'PER.DIN D-8 Tuban 62351 Jawa Timur', NULL, '52012100', 'Section of Crusher & Raw Mill Operation', 'SECT', '50040483'),
(960, '00001024', 'UMMI SHOFIANA, SE., MBA.', '5002300000', 'DEPARTEMEN KEUANGAN & SDM', 1, 'Active', 15, 'General Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011385', '1970-08-05', 2, 'Female', 'Kawin', '1994-05-16', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'UMMI.SHOFIANA@SEMENINDONESIA.COM', 'JL. DUKUH KUPANG XI NO. 1 Surabaya 60225 Jawa Timur', NULL, '53010000', 'Department of Finance & Human Capital', 'DEPT', '50029623'),
(961, '00001063', 'ALI MAHFUD', '5002220000', 'UNIT SDM & SARANA UMUM', 1, 'Active', 30, 'Manager', 5000, 'PT. Semen Gresik', 5021, 'Semen Gresik (SMI - SG)', '50011424', '1974-03-16', 1, 'Male', 'Kawin', '1994-06-09', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 52, 'SG Krywn - Rembang', 5400, 'Rembang', 'ALI.MAHFUD@SEMENINDONESIA.COM', 'PERDIN. BLOK J-10 RT.4 RW.9 SUMURGUNG Tuban 62351 Jawa Timur', NULL, '53012000', 'Unit of Human Capital & General Affairs', 'BIRO', '50048652'),
(962, '00001066', 'GUNTORO, SE.', '2002013000', 'UNIT AKUNTANSI', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011427', '1975-04-05', 1, 'Male', 'Kawin', '1994-06-09', '2031-05-01', '2031-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'GUNTORO.1066@SEMENINDONESIA.COM', 'PER.DINAS G-117 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27122000', 'Unit of Accounting', 'BIRO', '50050425'),
(963, '00001068', 'FEBRIWAN, SE.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011429', '1969-02-01', 1, 'Male', 'Kawin', '1994-07-01', '2025-03-01', '2025-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'FEBRIWAN@SEMENINDONESIA.COM', 'PER.DINAS B-03 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(964, '00001072', 'JATMIKO, SH.', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011433', '1971-04-09', 1, 'Male', 'Kawin', '1994-08-05', '2027-05-01', '2027-05-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'JATMIKO@SEMENINDONESIA.COM', 'DESA SOKOSARI RT-I/RW-I SOKO Tuban 62372 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(965, '00001081', 'CHOIRUL INSAN', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011442', '1974-03-14', 1, 'Male', 'Kawin', '1994-08-05', '2030-04-01', '2030-04-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'CHOIRUL.INSAN@SEMENINDONESIA.COM', 'PER. DINAS D - 10, SUMURGUNG Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(966, '00001082', 'KUSIYANTO', '2001050000', 'DEPARTEMEN PENGADAAN STRATEGIS', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011443', '1974-04-08', 1, 'Male', 'Kawin', '1994-08-05', '2030-05-01', '2030-05-01', 'Promosi PJ SI', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'KUSIYANTO@SEMENINDONESIA.COM', 'PER. DINAS D-100 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27220000', 'Department of Strategic Procurement', 'DEPT', '50050613'),
(967, '00001084', 'HERY KURNIAWAN, SE., MM.', '2007051000', 'UNIT PENGEMBANGAN SISTEM  MANAJEMEN', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011445', '1974-05-30', 1, 'Male', 'Kawin', '1994-08-05', '2030-06-01', '2030-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HERY.KURNIAWAN@SEMENINDONESIA.COM', 'PERDIN. BLOK D-83 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22021000', 'Unit of Management System Development', 'BIRO', '50050412'),
(968, '00001086', 'JUNAIDI TRI UTOMO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011447', '1974-06-14', 1, 'Male', 'Kawin', '1994-08-05', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'JUNAIDI.UTOMO@SEMENINDONESIA.COM', 'LATSARI I/29-C Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(969, '00001087', 'M. IMRON', '2003024000', 'SEKSI HSE SG', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011448', '1974-06-22', 1, 'Male', 'Kawin', '1994-08-05', '2030-07-01', '2030-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2500, 'Rembang', 'M.IMRON@SEMENINDONESIA.COM', 'DESA KESAMBEN RT-3/RW-1 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23040100', 'Section of SG SHE', 'SECT', '50050421'),
(970, '00001088', 'UNTUNG SUGIHARTONO', '2003021000', 'UNIT HSE TUBAN DAN GRESIK', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011449', '1974-07-31', 1, 'Male', 'Kawin', '1994-08-05', '2030-08-01', '2030-08-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'UNTUNG.SUGIHARTONO@SEMENINDONESIA.COM', 'JL. MARGO DADI IV/20 Surabaya 60172 Jawa Timur', 'Jl. Veteran, Gresik 61122', '23041000', 'Unit of Tuban & Gresik SHE', 'BIRO', '50050421'),
(971, '00001092', 'HER ARSA PAMBUDI, SE., M.MT.', '2007000000', 'DIREKTUR SUMBER DAYA MANUSIA DAN HUKUM', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011453', '1970-06-03', 1, 'Male', 'Kawin', '1994-08-16', '2026-07-01', '2026-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'HER.PAMBUDI@SEMENINDONESIA.COM', 'PER.DINAS F-26 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26000000', 'Human Capital & Legal Directorate', 'DIR', '50000000'),
(972, '00001093', 'AGUSWINARTO PUTRADJAKA, PIA.', '2002014000', 'UNIT OPERASIONAL PERBENDAHARAAN', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011454', '1970-08-09', 1, 'Male', 'Kawin', '1994-08-16', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'AGUSWINARTO.P@SEMENINDONESIA.COM', 'PERDIN. BLOK B-2 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27132000', 'Unit of Operational Treasury', 'BIRO', '50050429'),
(973, '00001095', 'LILIES AMBAR RUKMI, SE.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011456', '1969-08-30', 2, 'Female', 'Kawin', '1994-09-01', '2025-09-01', '2025-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'LILIES.RUKMI@SEMENINDONESIA.COM', 'WISMA TROPODO B V/1 WARU Surabaya 60119 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650A000', 'Dana Pensiun Semen Gresik', 'BIRO', '50032594'),
(974, '00001096', 'ACHMAD FAUZAN, SE., MM.', '2006080000', 'DEPARTEMEN PENGELOLAAN PORTFOLIO', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011457', '1970-08-28', 1, 'Male', 'Kawin', '1994-09-01', '2026-09-01', '2026-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ACHMAD.FAUZAN@SEMENINDONESIA.COM', 'PER.DINAS G-145 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22130000', 'Department of Portfolio Management', 'DEPT', '50050414'),
(975, '00001097', 'ANGGRAINI NURULLIA, SE.', '2001026000', 'UNIT KOMUNIKASI INTERNAL', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011458', '1971-10-11', 2, 'Female', 'Kawin', '1994-09-01', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ANGGRAINI.NURULLIA@SEMENINDONESIA.COM', 'PER.DINAS F-7 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21132000', 'Unit of Internal Communication', 'BIRO', '50045251'),
(976, '00001098', 'HASAN ARIFIN, SE.', '2002013000', 'UNIT AKUNTANSI', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011459', '1974-10-23', 1, 'Male', 'Kawin', '1994-09-01', '2030-11-01', '2030-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'HASAN.ARIFIN@SEMENINDONESIA.COM', 'PER.DINAS G-37 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27122000', 'Unit of Accounting', 'BIRO', '50050425'),
(977, '00001099', 'EKO WIRANTONO, ST., MM.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011460', '1968-03-23', 1, 'Male', 'Kawin', '1994-10-17', '2024-04-01', '2024-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'EKO.WIRANTONO@SEMENINDONESIA.COM', 'PER.DINAS FF-07 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26505000', 'PT Swadaya Graha', 'BIRO', '50032594'),
(978, '00001101', 'RUDI HERMAWAN, ST.', '2006090000', 'INTEGRATION MANAGEMENT OFFICE', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011462', '1970-05-30', 1, 'Male', 'Kawin', '1994-10-17', '2026-06-01', '2026-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'RUDI.HERMAWAN@SEMENINDONESIA.COM', 'PERDIN BLOK F-15 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22030000', 'Integration Management Office', 'DEPT', '50045131'),
(979, '00001102', 'AGUNG WIHARTO, S.IP.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011463', '1967-03-05', 1, 'Male', 'Kawin', '1994-11-01', '2023-04-01', '2023-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2205, 'Jakarta', 'AGUNG.WIHARTO@SEMENINDONESIA.COM', 'JL. TEBET DALAM II A 12 Jakarta Selatan 10110 DKI Jakarta', 'Jl. Veteran, Gresik 61122', '2650G000', 'PT Solusi Bangun Indonesia', 'BIRO', '50032594'),
(980, '00001103', 'ADITYO SUGENG P., SE.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011464', '1969-04-28', 1, 'Male', 'Kawin', '1994-11-17', '2025-05-01', '2025-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'ADITYO.SUGENG@SEMENINDONESIA.COM', 'PER.DINAS E-03 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26506000', 'PT Swabina Gatra', 'BIRO', '50032594'),
(981, '00001106', 'AFANDI', '2001030000', 'DEPARTEMEN CSR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011467', '1965-02-07', 1, 'Male', 'Kawin', '1995-01-02', '2021-03-01', '2021-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AFANDI.1106@SEMENINDONESIA.COM', 'JL. WAHIDIN SH XXXVI / 3 R. AGUNG Gresik 61121 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21110000', 'Department of CSR', 'DEPT', '50045147'),
(982, '00001107', 'MOKHAMAD MAS\'UD', '2007033000', 'UNIT KERUMAHTANGGAAN & KANTOR PERWAKILAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011468', '1965-05-12', 1, 'Male', 'Kawin', '1995-01-02', '2021-06-01', '2021-06-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOKHAMAD.MASUD@SEMENINDONESIA.COM', 'PERDIN. D-61 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21142000', 'Unit of Household & Represntative Office', 'BIRO', '50045252'),
(983, '00001109', 'MULYONO, SE.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011470', '1970-12-17', 1, 'Male', 'Kawin', '1995-01-02', '2027-01-01', '2027-01-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MULYONO.1109@SEMENINDONESIA.COM', 'JL. VETERAN V A/02A RT.01 RW.02 SINGOSARI KEBOMAS Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(984, '00001110', 'ASRI WAHJUSUKRISNO, SE.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011471', '1971-02-08', 1, 'Male', 'Kawin', '1995-01-02', '2027-03-01', '2027-03-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2205, 'Jakarta', 'ASRI.WAHJUSUKRISNO@SEMENINDONESIA.COM', 'PER.DINAS F-32 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650N000', 'PT Sinergi Informatika Semen Indonesia', 'BIRO', '50032594'),
(985, '00001111', 'HENDRO SOELISTIJONO', '2004170200', 'SEKSI PERENCANAAN PEMEL INFRA SCM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011472', '1971-03-12', 1, 'Male', 'Kawin', '1995-01-02', '2027-04-01', '2027-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'HENDRO.SOELISTIJONO@SEMENINDONESIA.COM', 'BATURETNO 4/90 Tuban 62318 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101200', 'Section of SCM Infrastructure Maint Plan', 'SECT', '50050482'),
(986, '00001114', 'SETYO BUDI PRAYITNO, SE.', '2001010000', 'INTERNAL AUDIT SI GROUP', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011475', '1972-06-04', 1, 'Male', 'Kawin', '1995-01-02', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SETYO.PRAYITNO@SEMENINDONESIA.COM', 'PERDIN. BLOK E-29 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21010000', 'Group Internal Audit', 'DEPT', '50000026'),
(987, '00001115', 'DADAN DUPARMAN, ST., M.MT.', '2006100000', 'GROUP HEAD PERENCANAAN KORPORAT', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011476', '1968-04-12', 1, 'Male', 'Kawin', '1995-04-03', '2024-05-01', '2024-05-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'DADAN.DUPARMAN@SEMENINDONESIA.COM', 'PER.DINAS C-21 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22100000', 'Group Head of Corporate Planning', 'KOMP', '50045131'),
(988, '00001117', 'ROOSANTOSO WASKITO N., ST., MM.', '2006020000', 'DEPARTEMEN PERENCANAAN STRATEGIS', 1, 'Active', 20, 'Senior Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011478', '1969-06-23', 1, 'Male', 'Kawin', '1995-04-03', '2025-07-01', '2025-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'ROOSANTOSO.WASKITO@SEMENINDONESIA.COM', 'PER.DINAS D-84 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '22110000', 'Department of Strategic Planning', 'DEPT', '50050414'),
(989, '00001118', 'MUFTI ARIMURTI, ST.', '2005200000', 'GROUP HEAD PENJUALAN', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011479', '1971-09-04', 1, 'Male', 'Kawin', '1995-04-03', '2027-10-01', '2027-10-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2200, 'Jakarta', 'MUFTI.ARIMURTI@SEMENINDONESIA.COM', 'PER.DINAS EE-16 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25200000', 'Group Head of Sales', 'KOMP', '50039238'),
(990, '00001119', 'OTTO ANDRI PRIYONO, ST.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011480', '1971-10-05', 1, 'Male', 'Kawin', '1995-04-03', '2027-11-01', '2027-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'OTTO.PRIYONO@SEMENINDONESIA.COM', 'PERDIN. BLOK FF-10 Tuban 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26506000', 'PT Swabina Gatra', 'BIRO', '50032594'),
(991, '00001120', 'BAMBANG TRIDOSO O., Ir.', '2007020000', 'DEPARTEMEN SDM OPERASIONAL', 1, 'Active', 15, 'General Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011481', '1965-10-30', 1, 'Male', 'Kawin', '1995-04-17', '2021-11-01', '2021-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2105, 'Gresik', 'BAMBANG.TRIDOSO@SEMENINDONESIA.COM', 'PER.DINAS B-07 Tuban 62351 Jawa Timur', 'Jl. Veteran, Gresik 61122', '2650L000', 'PT Krakatau Semen Indonesia', 'BIRO', '50032594'),
(992, '00001126', 'MUHAIMIN', '2007033000', 'UNIT KERUMAHTANGGAAN & KANTOR PERWAKILAN', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011487', '1969-03-20', 1, 'Male', 'Kawin', '1995-04-21', '2025-04-01', '2025-04-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUHAIMIN@SEMENINDONESIA.COM', 'JL. MERAK III/ G-09 GKA Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21142000', 'Unit of Household & Represntative Office', 'BIRO', '50045252'),
(993, '00001127', 'INDARTO YUDO', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011488', '1969-06-01', 1, 'Male', 'Kawin', '1995-04-21', '2025-07-01', '2025-07-01', 'Karyawan Tetap', 21, 'SI Karyawan - Tuban', 2300, 'Tuban', 'INDARTO.YUDO@SEMENINDONESIA.COM', 'JL. SUNAN KALIJOGO 36 LATSARI Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(994, '00001130', 'MUSTOFA HADI, S.Kom.', '2007101000', 'UNIT MANAJEMEN ASET & SARANA UMUM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011491', '1971-06-26', 1, 'Male', 'Kawin', '1995-04-21', '2027-07-01', '2027-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUSTOFA.HADI@SEMENINDONESIA.COM', 'JL.KH.SYAFI\'I RT-3/3 DAHANREJO Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26101000', 'Unit of General Affair & Asset', 'BIRO', '50050600'),
(995, '00001131', 'MOCH. IMAM MUTTAQIN', '2005223100', 'SEKSI PENJUALAN JABAR', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011492', '1971-08-08', 1, 'Male', 'Kawin', '1995-04-21', '2027-09-01', '2027-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MOCH.IMAM@SEMENINDONESIA.COM', 'PERDIN. BLOK G-16 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '25223100', 'Section of Jawa Barat Sales', 'SECT', '50050559'),
(996, '00001134', 'MUCHAMAD RIZAL', '2002201400', 'SEKSI PERSEDIAAN BU', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011495', '1972-06-22', 1, 'Male', 'Kawin', '1995-04-21', '2028-07-01', '2028-07-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'MUCHAMAD.RIZAL@SEMENINDONESIA.COM', 'PERDIN. BLOK E-36 Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '27201100', 'Section of BU Inventory', 'SECT', '50050614'),
(997, '00001143', 'AGUS ADI SUBEKTI', '2007023000', 'UNIT REMUNERASI & PENILAIAN KINERJA', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011504', '1975-08-18', 1, 'Male', 'Lajang', '1995-04-21', '2031-09-01', '2031-09-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'AGUS.SUBEKTI@SEMENINDONESIA.COM', 'PERDIN. BLOK D-39 GRESIK Gresik 61122 Jawa Timur', 'Jl. Veteran, Gresik 61122', '26122000', 'Unit of Remuneration & Perform Appraisal', 'BIRO', '50050604'),
(998, '00001145', 'SUKAMTO, ST.', '2004170200', 'SEKSI PERENCANAAN PEMEL INFRA SCM', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011506', '1975-10-10', 1, 'Male', 'Kawin', '1995-04-21', '2031-11-01', '2031-11-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SUKAMTO@SEMENINDONESIA.COM', 'LATSARI I/7 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101200', 'Section of SCM Infrastructure Maint Plan', 'SECT', '50050482'),
(999, '00001146', 'MOCHAMMAD SANI YUWONO, SE.', '2001230000', 'DEPARTEMEN CORPORATE OFFICE', 1, 'Active', 30, 'Manager', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011507', '1975-11-23', 1, 'Male', 'Kawin', '1995-04-21', '2031-12-01', '2031-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'SANI.YUWONO@SEMENINDONESIA.COM', 'JL. SUNAN MURIA 23 Tuban 62311 Jawa Timur', 'Jl. Veteran, Gresik 61122', '21140000', 'Department of Corporate Office', 'DEPT', '50045147'),
(1000, '00001152', 'BUDI SETYO NUGROHO., S.Kom.', '2004170100', 'SEKSI PERENCANAAN OPERASI INFRA SCM', 1, 'Active', 40, 'Supervisor', 2000, 'PT. Semen Indonesia', 2000, 'Semen Indonesia', '50011513', '1971-11-25', 1, 'Male', 'Kawin', '1995-06-16', '2027-12-01', '2027-12-01', 'Karyawan Tetap', 20, 'SI Karyawan - Gresik', 2100, 'Gresik', 'BUDI.SETYO@SEMENINDONESIA.COM', 'KARANG INDAH BH-22 Tuban 62314 Jawa Timur', 'Jl. Veteran, Gresik 61122', '24101100', 'Section of SCM Infrastructure Opr Plan', 'SECT', '50050482');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_master_unit_kerja`
--

CREATE TABLE `she_master_unit_kerja` (
  `id` int(11) NOT NULL,
  `muk_kode` varchar(200) NOT NULL,
  `muk_short` varchar(200) NOT NULL,
  `muk_nama` varchar(200) NOT NULL,
  `muk_level` varchar(200) NOT NULL,
  `muk_parent` varchar(200) NOT NULL,
  `company` varchar(100) NOT NULL,
  `muk_begda` varchar(100) NOT NULL,
  `muk_endda` varchar(100) NOT NULL,
  `muk_cctr` varchar(100) NOT NULL,
  `muk_changed_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_master_unit_kerja`
--

INSERT INTO `she_master_unit_kerja` (`id`, `muk_kode`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`, `company`, `muk_begda`, `muk_endda`, `muk_cctr`, `muk_changed_on`) VALUES
(1, '50000000', '20000000', 'Board of Commissioners', 'COMP', '00002000', '2000', '2011-01-01', '9999-12-31', '2009000000', '2013-12-30'),
(2, '50000001', '20100000', 'Secretariat of Commissioners', 'KOMP', '50000000', '2000', '2011-01-01', '9999-12-31', '2109002000', '2017-05-02'),
(3, '50000025', '20200000', 'Committees', 'KOMP', '50000000', '2000', '2011-01-01', '9999-12-31', '2009100000', '2017-05-02'),
(4, '50000026', '21000000', 'President Directorate', 'DIR', '50000000', '2000', '2011-01-01', '9999-12-31', '2001000000', '2019-01-10'),
(5, '50000027', '21100000', 'Internal Audit', 'DEPT', '50000026', '2000', '2011-01-01', '2013-12-31', '2101100000', '2014-01-28'),
(6, '50000028', '21101000', 'Bureau of Accounting & Finance Audit', 'BIRO', '50000027', '2000', '2011-01-01', '2013-12-31', '2101110000', '2014-01-28'),
(7, '50000029', '21102000', 'Bureau of Commercial & Mgmt System Audit', 'BIRO', '50000027', '2000', '2011-01-01', '2013-12-31', '2101120000', '2014-01-28'),
(8, '50000030', '21103000', 'Bureau of Technical Audit', 'BIRO', '50000027', '2000', '2011-01-01', '2013-12-31', '2101130000', '2014-01-28'),
(9, '50000031', '21200000', 'Corporate Secretary', 'DEPT', '50000026', '2000', '2011-01-01', '2017-12-31', '2001020000', '2018-03-07'),
(10, '50000032', '21201000', 'Bureau of Public Relation', 'BIRO', '50000031', '2000', '2013-05-01', '2013-12-31', '2101420000', '2014-02-03'),
(11, '50000034', '21201200', 'Section of Internal Rlt & Media Non Jkt', 'SECT', '50000032', '2000', '2011-01-01', '2013-12-31', '2101423000', '2014-02-03'),
(12, '50000040', '21201300', 'Section of External Relation', 'SECT', '50000032', '2000', '2011-01-01', '2013-12-31', '2101422000', '2014-02-03'),
(13, '50000043', '21204000', 'Bureau of Investor Relation', 'BIRO', '50000031', '2000', '2012-09-01', '2016-05-31', '2001021000', '2016-06-21'),
(14, '50000044', '21203000', 'Bureau of Secretariat & Protocol', 'BIRO', '50000031', '2000', '2012-08-01', '2017-12-31', '2001023000', '2018-03-07'),
(15, '50000045', '21203100', 'Section of Gresik Secretariat', 'SECT', '50000044', '2000', '2011-01-01', '2013-12-31', '2101431000', '2014-02-03'),
(16, '50000049', '21203200', 'Section of Gresik Protocol', 'SECT', '50000044', '2000', '2011-01-01', '2013-12-31', '2101432000', '2014-02-03'),
(17, '50000053', '21203300', 'Section of Tuban Secretariat & Protocol', 'SECT', '50000044', '2000', '2011-01-01', '2013-12-31', '2201433000', '2014-02-03'),
(18, '50000058', '27100000', 'Department of Corporate Development', 'DEPT', '50016550', '2000', '2011-03-11', '2016-05-31', '2006010000', '2016-07-21'),
(19, '50000059', '21400000', 'Department of Corp Comm & Social Env Mgt', 'DEPT', '50000026', '2000', '2011-01-01', '2013-12-31', '2101500000', '2014-01-28'),
(20, '50000060', '21400100', 'Section of CSR Adm & Evaluation', 'SECT', '50000059', '2000', '2011-01-01', '2013-12-31', '2101501000', '2014-01-28'),
(21, '50000065', '21401000', 'Bureau of Partnership Programmes', 'BIRO', '50000059', '2000', '2012-07-01', '2013-12-31', '2101510000', '2014-01-28'),
(22, '50000066', '21402000', 'Bureau of Community Development', 'BIRO', '50000059', '2000', '2011-01-01', '2013-12-31', '2101520000', '2014-01-28'),
(23, '50000067', '21402100', 'Section of Gresik Community Development', 'SECT', '50000066', '2000', '2011-01-01', '2013-12-31', '2101521000', '2014-01-28'),
(24, '50000074', '21402200', 'Section of Tuban Community Development', 'SECT', '50000066', '2000', '2011-01-01', '2013-12-31', '2201522000', '2014-01-28'),
(25, '50000080', '26100000', 'Department of Legal & Risk Management', 'DEPT', '50016423', '2000', '2011-03-11', '2013-12-31', '2107100000', '2014-02-03'),
(26, '50000081', '26101000', 'Bureau of Advisory & Adm Legal', 'BIRO', '50000080', '2000', '2012-03-01', '2013-12-31', '2107110000', '2014-02-03'),
(27, '50000082', '26102000', 'Bureau of Corporate Legal', 'BIRO', '50000080', '2000', '2011-11-01', '2013-12-31', '2107120000', '2014-02-03'),
(28, '50000083', '26103000', 'Bureau of Risk Management', 'BIRO', '50000080', '2000', '2011-11-01', '2013-12-31', '2107130000', '2014-02-03'),
(29, '50000084', '26200000', 'Department of Human Capital', 'DEPT', '50016423', '2000', '2011-10-01', '2013-12-31', '2107200000', '2014-02-03'),
(30, '50000085', '26201000', 'Bureau of Personnel', 'BIRO', '50000084', '2000', '2012-07-01', '2013-12-31', '2107210000', '2014-02-03'),
(31, '50000086', '26201100', 'Section of Personnel Administration', 'SECT', '50000085', '2000', '2011-01-01', '2013-12-31', '2107211000', '2014-02-03'),
(32, '50000091', '26201200', 'Section of Personnel Relation', 'SECT', '50000085', '2000', '2011-01-01', '2013-12-31', '2107212000', '2014-02-03'),
(33, '50000096', '26201300', 'Section of Tuban Personnel', 'SECT', '50000085', '2000', '2012-02-01', '2013-12-31', '2207213000', '2014-02-03'),
(34, '50000101', '26201400', 'Section of Occupational Health Services', 'SECT', '50000085', '2000', '2011-01-01', '2013-12-31', '2107214000', '2014-02-03'),
(35, '50000105', '26202000', 'Bureau of Training', 'BIRO', '50000084', '2000', '2012-07-01', '2013-12-31', '2107220000', '2014-02-03'),
(36, '50000106', '26202100', 'Section of Training Planning', 'SECT', '50000105', '2000', '2011-04-01', '2013-12-31', '2107221000', '2014-02-03'),
(37, '50000110', '26202200', 'Section of Training Organizing', 'SECT', '50000105', '2000', '2012-06-01', '2013-12-31', '2107222000', '2014-02-03'),
(38, '50000116', '26203000', 'Bureau of Human Capital & OD', 'BIRO', '50000084', '2000', '2011-10-01', '2013-12-31', '2107230000', '2014-02-03'),
(39, '50000117', '27200000', 'Department of Group Capex Management', 'DEPT', '50016550', '2000', '2011-03-11', '2013-12-31', '2106200000', '2014-02-03'),
(40, '50000118', '26300000', 'Team of Group Human Capital Development', 'DEPT', '50016423', '2000', '2011-03-11', '2013-12-31', '2109101000', '2014-02-03'),
(41, '50000120', '21500000', 'Team of Office of The CEO', 'DEPT', '50000026', '2000', '2014-01-01', '2016-05-31', '2009100100', '2016-06-29'),
(42, '50000121', '22000000', 'Marketing Directorate', 'DIR', '50000000', '2000', '2011-01-01', '2013-12-31', '2105000000', '2014-01-29'),
(43, '50000122', '22100000', 'Department of Marketing Development', 'DEPT', '50000121', '2000', '2011-01-01', '2013-12-31', '2105300000', '2014-01-29'),
(44, '50000123', '22101000', 'Bureau of Customer Services', 'BIRO', '50000122', '2000', '2011-01-01', '2013-12-31', '2105320000', '2014-01-29'),
(45, '50000124', '22102000', 'Bureau of Marketing Planning', 'BIRO', '50000122', '2000', '2011-01-01', '2013-12-31', '2105310000', '2014-01-29'),
(46, '50000125', '22103000', 'Bureau of Marketing Comunication', 'BIRO', '50000122', '2000', '2011-01-01', '2013-12-31', '2105330000', '2014-01-29'),
(47, '50000126', '22200000', 'Department of Sales', 'DEPT', '50000121', '2000', '2011-01-01', '2013-12-31', '2105100000', '2014-01-29'),
(48, '50000127', '22200100', 'Section of Sales Administration', 'SECT', '50000126', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(49, '50000140', '22201000', 'Bureau of Sales Region I', 'BIRO', '50000126', '2000', '2011-01-01', '2013-12-31', '2105110000', '2014-01-29'),
(50, '50000141', '22201100', 'Section of Sales Jatim I', 'SECT', '50000140', '2000', '2011-01-01', '2013-12-31', '2105111000', '2014-01-20'),
(51, '50000142', '22201200', 'Section of Sales Jatim II', 'SECT', '50000140', '2000', '2011-01-01', '2013-12-31', '2105112000', '2014-01-20'),
(52, '50000143', '22201300', 'Section of Sales Bali', 'SECT', '50000140', '2000', '2011-01-01', '2013-09-30', '2105113000', '2013-10-03'),
(53, '50000144', '22202000', 'Bureau of Sales Region II', 'BIRO', '50000126', '2000', '2011-01-01', '2013-12-31', '2105120000', '2014-01-20'),
(54, '50000145', '22202100', 'Section of Sales Jabar, Banten & DKI', 'SECT', '50000144', '2000', '2011-01-01', '2013-12-31', '2105121000', '2014-01-20'),
(55, '50000146', '22202200', 'Section of Sales Jateng & DIY', 'SECT', '50000144', '2000', '2011-01-01', '2013-12-31', '2105122000', '2014-01-20'),
(56, '50000147', '22203000', 'Bureau of Sales Region III', 'BIRO', '50000126', '2000', '2011-01-01', '2013-12-31', '2105130000', '2014-01-20'),
(57, '50000148', '22203100', 'Section of Sales Bulk Cement', 'SECT', '50000147', '2000', '2011-01-01', '2013-09-30', '2105132000', '2013-10-02'),
(58, '50000149', '22203200', 'Section of Sales External Java', 'SECT', '50000147', '2000', '2011-01-01', '2013-12-31', '2105131000', '2014-01-20'),
(59, '50000150', '22300000', 'Department of Distribution & Trnsp', 'DEPT', '50000121', '2000', '2011-01-01', '2013-12-31', '2105200000', '2014-01-29'),
(60, '50000151', '22300100', 'Section of Distribution & Trnsp Adm', 'SECT', '50000150', '2000', '2011-01-01', '2013-12-31', '2105201000', '2014-01-29'),
(61, '50000157', '22301000', 'Bureau of Distribution', 'BIRO', '50000150', '2000', '2011-01-01', '2013-12-31', '2105210000', '2014-01-29'),
(62, '50000158', '22301100', 'Section of Gresik Shipment', 'SECT', '50000157', '2000', '2011-01-01', '2013-12-31', '2105211000', '2014-01-29'),
(63, '50000164', '22301200', 'Section of Tuban Shipment', 'SECT', '50000157', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(64, '50000171', '22301300', 'Section of Warehouse Operation', 'SECT', '50000157', '2000', '2011-01-01', '2013-12-31', '2105213000', '2014-01-29'),
(65, '50000184', '22302000', 'Bureau of Transportation', 'BIRO', '50000150', '2000', '2011-01-01', '2013-12-31', '2105220000', '2014-01-29'),
(66, '50000192', '22302200', 'Section of Sea Transportation', 'SECT', '50000184', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(67, '50000200', '22302300', 'Section of Port Administration', 'SECT', '50000184', '2000', '2011-01-01', '2013-12-31', '2105221000', '2014-01-20'),
(68, '50000207', '23000000', 'Production Directorate', 'DIR', '50000000', '2000', '2011-01-01', '2013-12-31', '2203000901', '2014-02-03'),
(69, '50000208', '23100000', 'Department of Raw Material Production', 'DEPT', '50000207', '2000', '2011-01-01', '2013-12-31', '2203100901', '2014-01-30'),
(70, '50000209', '23101000', 'Bureau of Mining Planning & Monitoring', 'BIRO', '50000208', '2000', '2011-01-01', '9999-12-31', '2203130901', '2012-11-14'),
(71, '50000210', '23101300', 'Section of Reclamation Land Management', 'SECT', '50020328', '2000', '2012-07-01', '2013-12-31', '2203131000', '2014-01-30'),
(72, '50000214', '23102000', 'Bureau of Raw Material Production', 'BIRO', '50000208', '2000', '2011-01-01', '2013-12-31', '2203120901', '2014-01-30'),
(73, '50000215', '23102100', 'Section of Crusher Operation', 'SECT', '50000214', '2000', '2011-01-01', '2013-12-31', '2203121901', '2014-01-30'),
(74, '50000228', '23102200', 'Section of Crusher Machine Maintenance', 'SECT', '50000214', '2000', '2011-01-01', '2013-12-31', '2203122906', '2014-01-30'),
(75, '50000241', '23102300', 'Section of Crusher Electl & Instr Maint', 'SECT', '50000214', '2000', '2011-01-01', '2013-12-31', '2203124907', '2014-01-30'),
(76, '50000250', '23102400', 'Section of Heavy Equipment', 'SECT', '50000214', '2000', '2011-01-01', '2013-12-31', '2203123901', '2014-01-30'),
(77, '50000259', '23103000', 'Bureau of Process Control', 'BIRO', '50000208', '2000', '2011-01-01', '2013-12-31', '2203110902', '2014-01-30'),
(78, '50000260', '23103100', 'Section of Utility Operation', 'SECT', '50000259', '2000', '2011-01-01', '2013-12-31', '2203112901', '2014-01-30'),
(79, '50000271', '23103100', 'Section of Process Control', 'SECT', '50000259', '2000', '2012-07-01', '2013-12-31', '2203111902', '2014-01-30'),
(80, '50000283', '23200000', 'Department of Clinker Production', 'DEPT', '50000207', '2000', '2011-01-01', '2012-06-30', '2203200901', '2013-01-14'),
(81, '50000284', '23201000', 'Bureau of Clinker Production', 'BIRO', '50000283', '2000', '2011-01-01', '2012-06-30', '2203210901', '2013-01-15'),
(82, '50000285', '23201100', 'Section of RKC I', 'SECT', '50000284', '2000', '2011-01-01', '2012-06-30', '2203211901', '2013-02-05'),
(83, '50000307', '23201200', 'Section of RKC II', 'SECT', '50000284', '2000', '2011-01-01', '2012-06-30', '2203212901', '2013-02-05'),
(84, '50000329', '23201300', 'Section of RKC III', 'SECT', '50000284', '2000', '2011-01-01', '2012-06-30', '2203213901', '2013-02-05'),
(85, '50000351', '23201400', 'Section of Alternative Fuel Management', 'SECT', '50000284', '2000', '2011-01-01', '2012-06-30', '2203214912', '2013-02-05'),
(86, '50000354', '23202000', 'Bureau of Machine I Maintenance', 'BIRO', '50000283', '2000', '2011-01-01', '2012-06-30', '2203120901', '2013-01-15'),
(87, '50000355', '23202100', 'Section of Roller Mill Maintenance', 'SECT', '50000354', '2000', '2011-01-01', '2012-06-30', '2203221906', '2013-02-05'),
(88, '50000368', '23202200', 'Section of KCM Machine Maintenance', 'SECT', '50000354', '2000', '2011-01-01', '2012-06-30', '2203222906', '2013-02-05'),
(89, '50000381', '23203000', 'Bureau of Elctl & Instr I Maintenance', 'BIRO', '50000283', '2000', '2011-01-01', '2012-06-30', '2203230901', '2013-01-15'),
(90, '50000382', '23203100', 'Section of Electrical I Maintenance', 'SECT', '50000381', '2000', '2011-01-01', '2012-06-30', '2203231907', '2013-02-05'),
(91, '50000394', '23203200', 'Section of Instrument I Maintenance', 'SECT', '50000381', '2000', '2011-01-01', '2012-06-30', '2203232907', '2013-02-05'),
(92, '50000406', '23203300', 'Section of Control System', 'SECT', '50000381', '2000', '2011-02-01', '2012-06-30', '2203233901', '2013-02-05'),
(93, '50000414', '23400000', 'Department of Cement Production', 'DEPT', '50000207', '2000', '2012-05-01', '2013-12-31', '2203300901', '2014-02-03'),
(94, '50000415', '23301000', 'Bureau of Cement Production', 'BIRO', '50000414', '2000', '2011-01-01', '2013-12-31', '2203310901', '2014-02-03'),
(95, '50000416', '23301100', 'Section of Tuban Finish Mill', 'SECT', '50000415', '2000', '2011-01-01', '2012-06-30', '2203311901', '2013-01-08'),
(96, '50000445', '23301200', 'Section of Tuban Packer & Port', 'SECT', '50000415', '2000', '2011-01-01', '2012-06-30', '2203312901', '2013-01-08'),
(97, '50000466', '23302000', 'Bureau of Machine II Maintenance', 'BIRO', '50000414', '2000', '2011-01-01', '2012-07-31', '2203320901', '2013-08-21'),
(98, '50000467', '23302100', 'Section of Tuban FM Machine Maintenance', 'SECT', '50000466', '2000', '2011-01-01', '2012-06-30', '2203321906', '2013-08-21'),
(99, '50000478', '23302200', 'Section of Packer & Port Machine Maint', 'SECT', '50000466', '2000', '2011-01-01', '2012-07-31', '2203322906', '2013-08-21'),
(100, '50000488', '23303000', 'Bureau of Elctl & Instr I', 'BIRO', '50000414', '2000', '2011-01-01', '2012-06-30', '2203322906', '2013-01-08'),
(101, '50000489', '23303100', 'Section of Electrical II Maintenance', 'SECT', '50000488', '2000', '2011-01-01', '2012-06-30', '2203331907', '2013-01-07'),
(102, '50000498', '23303200', 'Section of Instrument II Maintenance', 'SECT', '50000488', '2000', '2011-01-01', '2012-06-30', '2203332907', '2013-01-07'),
(103, '50000508', '23304000', 'Bureau of Gresik Plant', 'BIRO', '50000414', '2000', '2011-01-01', '2013-12-31', '2103340901', '2014-02-03'),
(104, '50000509', '23304100', 'Section of Gresik FM & Packer Operation', 'SECT', '50000508', '2000', '2011-01-01', '2013-12-31', '2103341901', '2014-02-03'),
(105, '50000526', '23304200', 'Section of Gresik Machine Maintenance', 'SECT', '50000508', '2000', '2011-01-01', '2013-12-31', '2103342906', '2014-02-03'),
(106, '50000539', '23304300', 'Section of Gresik Electrical Maintenance', 'SECT', '50000508', '2000', '2011-01-01', '2013-12-31', '2103343907', '2014-02-03'),
(107, '50000557', '23500000', 'Department of Technical', 'DEPT', '50000207', '2000', '2012-05-01', '2013-12-31', '2203400901', '2014-02-03'),
(108, '50000558', '23401000', 'Bureau of Workshop & Construction', 'BIRO', '50000557', '2000', '2011-01-01', '2013-12-31', '2203410901', '2014-02-03'),
(109, '50000559', '23401100', 'Section of Utility Maintenance', 'SECT', '50000558', '2000', '2011-01-01', '2013-12-31', '2203411906', '2014-02-03'),
(110, '50000572', '23401200', 'Section of Electrical & Instr Workshop', 'SECT', '50000558', '2000', '2011-01-01', '2013-12-31', '2203412907', '2014-02-03'),
(111, '50000582', '23401300', 'Section of Machine Workshop', 'SECT', '50000558', '2000', '2011-01-01', '2013-12-31', '2203413906', '2014-02-03'),
(112, '50000597', '23401400', 'Section of Construction', 'SECT', '50000558', '2000', '2011-01-01', '2013-12-31', '2203414901', '2014-02-03'),
(113, '50000615', '23402000', 'Bureau of Technical Planning', 'BIRO', '50000557', '2000', '2011-01-01', '2013-12-31', '2203420901', '2014-02-03'),
(114, '50000616', '23402100', 'Section of Maintenance Inspection', 'SECT', '50000615', '2000', '2011-01-01', '2013-12-31', '2203422901', '2014-02-03'),
(115, '50000633', '23402200', 'Section of Gresik Technical Planning', 'SECT', '50000615', '2000', '2011-01-01', '2013-12-31', '2103423901', '2014-02-03'),
(116, '50000642', '23402300', 'Section of Maintenance Planning', 'SECT', '50000615', '2000', '2011-01-01', '2012-07-01', '2203424901', '2012-12-17'),
(117, '50000649', '23402400', 'Section of Spareparts Planning', 'SECT', '50000615', '2000', '2011-01-01', '2013-12-31', '2203421901', '2014-02-03'),
(118, '50000665', '23403000', 'Bureau of Health Safety Environment', 'BIRO', '50000557', '2000', '2011-01-01', '2013-12-31', '2203430914', '2014-02-03'),
(119, '50000666', '23403100', 'Section of Gresik HSE', 'SECT', '50000665', '2000', '2011-01-01', '2013-12-31', '2103431914', '2014-02-03'),
(120, '50000674', '23403200', 'Section of Tuban HSE', 'SECT', '50000665', '2000', '2011-01-01', '2012-06-30', '2203432914', '2013-01-08'),
(121, '50000687', '23403300', 'Section of Pollution Pvnt Maintenance', 'SECT', '50000665', '2000', '2011-01-01', '2012-06-30', '2203433914', '2013-01-14'),
(122, '50000697', '23600000', 'Team of Group Productivity Improvement', 'DEPT', '50000207', '2000', '2012-05-01', '2013-12-31', '2209105901', '2014-02-03'),
(123, '50000698', '24000000', 'Operational & RD Directorate', 'DIR', '50000000', '2000', '2011-01-01', '2013-12-31', '2104000000', '2014-02-03'),
(124, '50000699', '24100000', 'Department of Research Development & QA', 'DEPT', '50000698', '2000', '2011-01-01', '2013-12-31', '2204400902', '2014-02-03'),
(125, '50000700', '24101000', 'Bureau of Quality Assurance', 'BIRO', '50000699', '2000', '2011-01-01', '2013-12-31', '2204410902', '2014-02-03'),
(126, '50000701', '24101100', 'Section of Quality Assurance', 'SECT', '50000700', '2000', '2011-01-01', '2013-12-31', '2204411902', '2014-02-03'),
(127, '50000708', '24101200', 'Section of Material Test', 'SECT', '50000700', '2000', '2011-01-01', '2013-12-31', '2204412902', '2014-02-03'),
(128, '50000713', '24101300', 'Section of Material & Prod Planning', 'SECT', '50000700', '2000', '2011-01-01', '2013-12-31', '2204413902', '2014-02-03'),
(129, '50000717', '24102000', 'Bureau of SMSG', 'BIRO', '50000699', '2000', '2011-01-01', '2013-12-31', '2104440000', '2014-02-03'),
(130, '50000718', '24103000', 'Bureau of Process Energy & Envr Dev', 'BIRO', '50000699', '2000', '2011-01-01', '2013-12-31', '2204420902', '2014-02-03'),
(131, '50000719', '24103100', 'Section of Enviromental Monitoring', 'SECT', '50000718', '2000', '2011-01-01', '2013-12-31', '2204421902', '2014-02-03'),
(132, '50000724', '24104000', 'Bureau of Product & Aplication Dev', 'BIRO', '50000699', '2000', '2011-01-01', '2013-12-31', '2204430902', '2014-02-03'),
(133, '50000725', '24200000', 'Department of Design & Engineering', 'DEPT', '50000698', '2000', '2011-01-01', '2013-12-31', '2204300915', '2014-02-03'),
(134, '50000726', '24200100', 'Section of Technical Adm of DE', 'SECT', '50000725', '2000', '2011-01-01', '2013-12-31', '2204301915', '2014-02-03'),
(135, '50000729', '24201000', 'Bureau of Civil Design', 'BIRO', '50000725', '2000', '2011-01-01', '2013-12-31', '2204330915', '2014-02-03'),
(136, '50000730', '24202000', 'Bureau of Electrical & Instrument Design', 'BIRO', '50000725', '2000', '2011-01-01', '2013-12-31', '2204320915', '2014-02-03'),
(137, '50000731', '24203000', 'Bureau of Process & Mechanical Design', 'BIRO', '50000725', '2000', '2011-01-01', '2013-12-31', '2204310915', '2014-02-03'),
(138, '50000732', '24300000', 'Department of Proc & Invent Management', 'DEPT', '50000698', '2000', '2011-01-01', '2013-12-31', '2104200000', '2014-02-03'),
(139, '50000733', '24301000', 'Bureau of Procurement Planning', 'BIRO', '50000732', '2000', '2011-01-01', '2013-12-31', '2104210000', '2014-02-03'),
(140, '50000734', '24302000', 'Bureau of Goods Procurement', 'BIRO', '50000732', '2000', '2011-01-01', '2013-12-31', '2104220000', '2014-02-03'),
(141, '50000735', '24302100', 'Section of Spareparts Procurement', 'SECT', '50000734', '2000', '2011-01-01', '2013-12-31', '2104221000', '2014-02-03'),
(142, '50000744', '24302200', 'Section of Material Procurement', 'SECT', '50000734', '2000', '2011-01-01', '2013-12-31', '2104222000', '2014-02-03'),
(143, '50000751', '24303000', 'Bureau of Services Procurement', 'BIRO', '50000732', '2000', '2011-01-01', '2013-12-31', '2104230000', '2014-02-03'),
(144, '50000752', '24303100', 'Section of Routine Services Procurement', 'SECT', '50000751', '2000', '2011-01-01', '2013-12-31', '2104231000', '2014-02-03'),
(145, '50000758', '24303200', 'Section of Non Routine Services Pcmt', 'SECT', '50000751', '2000', '2011-01-01', '2013-12-31', '2104231000', '2014-02-03'),
(146, '50000764', '24304000', 'Bureau of Inventory Management', 'BIRO', '50000732', '2000', '2011-01-01', '2013-12-31', '2204240901', '2014-02-03'),
(147, '50000765', '24304100', 'Section of Inventory Planning', 'SECT', '50000764', '2000', '2011-01-01', '2013-12-31', '2204241901', '2014-02-03'),
(148, '50000770', '24304200', 'Section of Gresik Inventory Management', 'SECT', '50000764', '2000', '2011-01-01', '2013-12-31', '2104242901', '2014-02-03'),
(149, '50000774', '24304300', 'Section of Goods Receiptance', 'SECT', '50000764', '2000', '2011-01-01', '2013-12-31', '2204243901', '2014-02-03'),
(150, '50000779', '24304400', 'Section of Tuban Inventory Management', 'SECT', '50000764', '2000', '2011-01-01', '2013-12-31', '2204244901', '2014-02-03'),
(151, '50000786', '27300000', 'Team of Group Raw Material Expansion', 'DEPT', '50000698', '2000', '2011-03-11', '2013-09-30', '2109106000', '2013-10-03'),
(152, '50000787', '27500000', 'Team of Group Energy Development', 'DEPT', '50016550', '2000', '2011-03-11', '2013-12-31', '2109107000', '2013-12-26'),
(153, '50000788', '24400000', 'Team of PPKPT Improvement Project', 'DEPT', '50000698', '2000', '2012-07-01', '2013-12-31', '2204300915', '2014-02-03'),
(154, '50000789', '24550000', 'Team of Group Packing Plant Project', 'DEPT', '50000698', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(155, '50000790', '24600000', 'Department of Group Strat Pcmt Policy', 'DEPT', '50000698', '2000', '2011-01-01', '2013-12-31', '2104100000', '2014-02-03'),
(156, '50000791', '24700000', 'Team of Group New Plant & Pwr Plant Prj', 'DEPT', '50000698', '2000', '2011-01-01', '2013-03-01', '2109109000', '2013-06-20'),
(157, '50000792', '23700000', 'Team of Quarry Expansion Project', 'KOMP', '50000207', '2000', '2012-07-01', '9999-12-31', '2109112000', '2014-02-03'),
(158, '50000793', '27000000', 'Finance Directorate', 'DIR', '50000000', '2000', '2018-01-01', '9999-12-31', '2002000000', '2019-01-05'),
(159, '50000794', '25100000', 'Department of Accounting & Finance', 'DEPT', '50000793', '2000', '2011-01-01', '2013-12-31', '2102200000', '2014-02-03'),
(160, '50000795', '25101000', 'Bureau of Treasury', 'BIRO', '50000794', '2000', '2011-01-01', '2013-12-31', '2102210000', '2014-02-03'),
(161, '50000796', '25101100', 'Section of Receipt & Payment', 'SECT', '50000795', '2000', '2011-01-01', '2013-12-31', '2102211000', '2014-02-03'),
(162, '50000803', '25101200', 'Section of Liquidity Pln & Management', 'SECT', '50000795', '2000', '2011-01-01', '2013-12-31', '2102211000', '2014-02-03'),
(163, '50000806', '25102000', 'Bureau of Receivable & Payable Mgt', 'BIRO', '50000794', '2000', '2011-01-01', '2013-12-31', '2102220000', '2014-02-03'),
(164, '50000807', '25102100', 'Section of Receivable Management', 'SECT', '50000806', '2000', '2011-01-01', '2013-12-31', '2102221000', '2014-02-03'),
(165, '50000814', '25102300', 'Section of Collection', 'SECT', '50000806', '2000', '2011-01-01', '2013-12-31', '2102223000', '2014-02-03'),
(166, '50000817', '25103000', 'Bureau of Taxation & Insurance', 'BIRO', '50000794', '2000', '2011-01-01', '2013-12-31', '2102230000', '2014-02-03'),
(167, '50000818', '25103100', 'Section of Taxation', 'SECT', '50000817', '2000', '2011-01-01', '2013-12-31', '2102231000', '2014-02-03'),
(168, '50000822', '25103200', 'Section of Insurance', 'SECT', '50000817', '2000', '2011-01-01', '2013-12-31', '2102232000', '2014-02-03'),
(169, '50000825', '25104000', 'Bureau of Accounting & Finance Reporting', 'BIRO', '50000794', '2000', '2011-01-01', '2013-12-31', '2102240000', '2014-02-03'),
(170, '50000826', '25104100', 'Section of General Accounting', 'SECT', '50000825', '2000', '2011-01-01', '2013-12-31', '2102242000', '2014-02-03'),
(171, '50000832', '25104200', 'Section of Cost Accounting', 'SECT', '50000825', '2000', '2011-01-01', '2013-12-31', '2102243000', '2014-02-03'),
(172, '50000837', '25104300', 'Section of Verification', 'SECT', '50000825', '2000', '2011-01-01', '2013-12-31', '2102244000', '2014-02-03'),
(173, '50000845', '25104400', 'Section of Tuban Accounting & Finance', 'SECT', '50000825', '2000', '2011-01-01', '2013-12-31', '2202241000', '2014-02-03'),
(174, '50000850', '25200000', 'Department of Group/SG Tecominfo Mgmt', 'DEPT', '50000793', '2000', '2011-01-01', '2013-09-30', '2102300000', '2013-10-03'),
(175, '50000851', '25200100', 'Section of SG Info Sys QA & Governation', 'SECT', '50000850', '2000', '2011-01-01', '2013-09-30', '2102301000', '2013-10-03'),
(176, '50000854', '25201200', 'Section of Aplication Maintenance', 'SECT', '50000852', '2000', '2011-01-01', '2013-09-30', '2102332000', '2013-10-03'),
(177, '50000855', '25202000', 'Bureau of Tecominfo Infrastructure Mgmt', 'BIRO', '50000850', '2000', '2011-01-01', '2013-09-30', '2102340000', '2013-10-03'),
(178, '50000856', '25202100', 'Section of Network  Maintenance', 'SECT', '50000855', '2000', '2011-01-01', '2013-09-30', '2102341000', '2013-10-03'),
(179, '50000857', '25202200', 'Section of Server & Database Maintenance', 'SECT', '50000855', '2000', '2011-01-01', '2013-09-30', '2102342000', '2013-10-03'),
(180, '50000863', '25204000', 'Bureau of SG Information Sys Management', 'BIRO', '50000850', '2000', '2011-01-01', '2013-09-30', '2102310000', '2013-10-03'),
(181, '50000864', '25204100', 'Section of Information System Services', 'SECT', '50000863', '2000', '2011-01-01', '2013-09-30', '2102311000', '2013-10-03'),
(182, '50000870', '25204200', 'Section of Information System Network', 'SECT', '50000863', '2000', '2011-01-01', '2013-09-30', '2102312000', '2013-10-03'),
(183, '50000875', '25204300', 'Section of Server Management', 'SECT', '50000863', '2000', '2011-01-01', '2013-09-30', '2102313000', '2013-10-03'),
(184, '50000880', '25204400', 'Section of Tuban Information System', 'SECT', '50000863', '2000', '2011-01-01', '2013-09-30', '2202314000', '2013-10-03'),
(185, '50000886', '25205000', 'Bureau of SG Aplication Maint & Dev', 'BIRO', '50000850', '2000', '2011-01-01', '2013-09-30', '2102320000', '2013-10-03'),
(186, '50000887', '25300000', 'Department of Group Finance Management', 'DEPT', '50000793', '2000', '2011-01-01', '2013-12-31', '2102100000', '2014-02-03'),
(187, '50000888', '25301000', 'Bureau of Performance Mgmt & Budgeting', 'BIRO', '50000887', '2000', '2011-01-01', '2013-12-31', '2102110000', '2014-02-03'),
(188, '50000889', '25400000', 'Team of Group Tecominfo Development', 'DEPT', '50000793', '2000', '2011-01-01', '2013-09-30', '2109104000', '2013-10-23'),
(189, '50000890', '26400000', 'Department of General Facilities', 'DEPT', '50016423', '2000', '2011-03-11', '2013-12-31', '2107300000', '2014-02-03'),
(190, '50000891', '26401000', 'Bureau of Corporate Asset Management', 'BIRO', '50000890', '2000', '2011-01-01', '2013-12-31', '2107340000', '2014-02-03'),
(191, '50000892', '25501100', 'Section of Asset Mgmt Administration', 'SECT', '50000891', '2000', '2011-01-01', '2013-12-31', '2107341000', '2014-02-03'),
(192, '50000895', '25501200', 'Section of Asset Mgmt Optimalization', 'SECT', '50000891', '2000', '2011-01-01', '2013-12-31', '2107342000', '2014-02-03'),
(193, '50000899', '26402000', 'Bureau of Gresik Asset Maintenance', 'BIRO', '50000890', '2000', '2012-07-01', '2013-12-31', '2107320000', '2014-02-03'),
(194, '50000900', '26402100', 'Section of Gresik Household', 'SECT', '50000899', '2000', '2011-08-01', '2013-12-31', '2107321000', '2014-02-03'),
(195, '50000911', '26402200', 'Section of Gresik Gen Facilities Maint', 'SECT', '50000899', '2000', '2011-08-01', '2013-12-31', '2107322000', '2014-02-03'),
(196, '50000924', '26403000', 'Bureau of Tuban Asset Management', 'BIRO', '50000890', '2000', '2011-10-01', '2013-12-31', '2207310000', '2014-02-03'),
(197, '50000925', '26403100', 'Section of Tuban Household', 'SECT', '50000924', '2000', '2012-06-01', '2013-12-31', '2207311000', '2014-02-03'),
(198, '50000933', '26403200', 'Section of Tuban General Affair Maint', 'SECT', '50000924', '2000', '2012-06-01', '2013-12-31', '2207312000', '2014-02-03'),
(199, '50000942', '26404000', 'Bureau of Jakarta Gen Facilities & Adm', 'BIRO', '50000890', '2000', '2011-01-01', '2013-12-31', '2107350000', '2014-02-03'),
(200, '50000943', '26404100', 'Section of Jakarta Office & Adm', 'SECT', '50000942', '2000', '2011-01-01', '2013-09-30', '2107351000', '2013-10-03'),
(201, '50000947', '26404200', 'Section of Jakarta Prot & Gen Facilities', 'SECT', '50000942', '2000', '2011-03-11', '2013-12-31', '2107352000', '2013-12-26'),
(202, '50000950', '26405000', 'Bureau of Security', 'BIRO', '50000890', '2000', '2011-07-01', '2013-12-31', '2207330914', '2014-02-03'),
(203, '50000951', '26405100', 'Section of Gresik Security', 'SECT', '50000950', '2000', '2012-07-01', '2013-12-31', '2107331914', '2014-02-03'),
(204, '50000965', '26405200', 'Section of Tuban Security', 'SECT', '50000950', '2000', '2011-12-01', '2013-12-31', '2207332914', '2014-02-03'),
(205, '50016094', '26501000', 'Koperasi Warga SG', 'BIRO', '50000975', '2000', '2012-02-01', '2013-12-31', '', '2014-02-03'),
(206, '50016095', '26502000', 'PT Cipta Nirmala', 'BIRO', '50000975', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(207, '50016096', '26503000', 'PT Varia Usaha', 'BIRO', '50000975', '2000', '2012-07-01', '2013-12-31', '', '2014-02-03'),
(208, '50016097', '26504000', 'PT Varia Usaha Beton', 'BIRO', '50000975', '2000', '2012-05-01', '2013-12-31', '', '2014-02-03'),
(209, '50016098', '26505000', 'PT Swadaya Graha', 'BIRO', '50000975', '2000', '2012-05-01', '2013-12-31', '', '2014-02-03'),
(210, '50016099', '26506000', 'PT Swabina Gatra', 'BIRO', '50000975', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(211, '50016400', '26507000', 'PT Kawasan Industri Gresik', 'BIRO', '50000975', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(212, '50016402', '26509000', 'PT Industri Kemasan Semen Gresik', 'BIRO', '50000975', '2000', '2012-02-01', '2013-12-31', '', '2014-02-03'),
(213, '50016403', '2650A000', 'Dana Pensiun Semen Gresik', 'BIRO', '50000975', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(214, '50016404', '2650B000', 'Semen Gresik Foundation', 'BIRO', '50000975', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(215, '50016423', '26000000', 'Human Capital & G A Directorate', 'DIR', '50000000', '2000', '2011-03-11', '2013-12-31', '2107000000', '2014-02-03'),
(216, '50016550', '27000000', 'Strat Business & Enter Dev Directorate', 'DIR', '50000000', '2000', '2011-03-11', '2017-07-31', '2006000000', '2017-09-05'),
(217, '50017627', '22302400', 'Section of Land Transportation', 'SECT', '50000184', '2000', '2011-06-01', '2013-12-31', '2105223000', '2014-01-29'),
(218, '50017644', '20201000', 'Committee of GCG', 'BIRO', '50000025', '2000', '2011-01-01', '9999-12-31', '', '2011-06-22'),
(219, '50017645', '20202000', 'Committee of SMRI', 'BIRO', '50000025', '2000', '2011-01-01', '9999-12-31', '', '2011-06-22'),
(220, '50000852', '25201000', 'Bureau of Aplication & Operation', 'BIRO', '50000850', '2000', '2011-01-01', '2013-09-30', '2102330000', '2013-10-03'),
(221, '50016401', '26508000', 'PT United Tractors Semen Gresik', 'BIRO', '50000975', '2000', '2011-08-01', '2013-12-31', '', '2014-02-03'),
(222, '50000119', '21800000', 'Team of Group Internal Audit', 'DEPT', '50000026', '2000', '2011-01-01', '2012-07-31', '2109102000', '2015-01-29'),
(223, '50000185', '22302100', 'Section of Land Transportation', 'SECT', '50000184', '2000', '2011-01-01', '2011-05-31', '2105223000', '2013-01-07'),
(224, '50000811', '25102200', 'Section of Payable Management', 'SECT', '50000806', '2000', '2011-01-01', '2013-12-31', '2102222000', '2014-02-03'),
(225, '50000206', '22400000', 'Team of Group Marketing Strat & Policy', 'DEPT', '50000121', '2000', '2011-01-01', '2013-12-31', '2109110000', '2013-12-26'),
(226, '50018405', '2650C000', 'PT SGG Energi Prima', 'BIRO', '50000975', '2000', '2012-02-01', '2013-12-31', '', '2014-02-03'),
(227, '50000035', '21201210', 'Group of Pers Relation', 'GRP', '50000034', '2000', '2011-01-01', '2013-12-31', '2101423000', '2014-02-03'),
(228, '50000036', '21201220', 'Group of Advertisement Management', 'GRP', '50000034', '2000', '2011-01-01', '2013-12-31', '2101423000', '2014-02-03'),
(229, '50000037', '21201230', 'Group of Gresik Reporting & Publishment', 'GRP', '50000034', '2000', '2011-01-01', '2013-12-31', '2101423000', '2014-02-03'),
(230, '50000038', '21201240', 'Group of Internal Media Gapura', 'GRP', '50000034', '2000', '2011-01-01', '2013-12-31', '2101423000', '2014-02-03'),
(231, '50000039', '21201250', 'Group of Tuban Reporting & Publishment', 'GRP', '50000034', '2000', '2011-01-01', '2013-12-31', '2101423000', '2014-02-03'),
(232, '50000041', '21201310', 'Group of Website & Sponsorship', 'GRP', '50000040', '2000', '2011-01-01', '2013-12-31', '2101422000', '2014-02-03'),
(233, '50000042', '21201320', 'Group of Visit & Exhibition', 'GRP', '50000040', '2000', '2011-01-01', '2013-12-31', '2101422000', '2014-02-03'),
(234, '50000046', '21203110', 'Group of Secretariat Administration', 'GRP', '50000045', '2000', '2011-01-01', '2013-12-31', '2101431000', '2014-02-03'),
(235, '50000047', '21203120', 'Group of Gresik Telephone Operation', 'GRP', '50000045', '2000', '2011-01-01', '2013-12-31', '2101431000', '2014-02-03'),
(236, '50000048', '21203130', 'Group of In Active Archives', 'GRP', '50000045', '2000', '2011-01-01', '2013-12-31', '2101431000', '2014-02-03'),
(237, '50000050', '21203210', 'Group of Finance & Administration', 'GRP', '50000049', '2000', '2011-01-01', '2013-12-31', '2101432000', '2014-02-03'),
(238, '50000051', '21203220', 'Group of Guest Services', 'GRP', '50000049', '2000', '2011-01-01', '2013-12-31', '2101432000', '2014-02-03'),
(239, '50000052', '21203230', 'Group of Accomodation & Ticketing', 'GRP', '50000049', '2000', '2011-01-01', '2013-12-31', '2101432000', '2014-02-03'),
(240, '50000054', '21203310', 'Group of Tuban Telephone Operation', 'GRP', '50000053', '2000', '2011-01-01', '2013-12-31', '2201433000', '2014-02-03'),
(241, '50000055', '21203320', 'Group of Tuban Protocol', 'GRP', '50000053', '2000', '2011-01-01', '2013-12-31', '2201433000', '2014-02-03'),
(242, '50000056', '21203330', 'Group of Filling Management', 'GRP', '50000053', '2000', '2011-01-01', '2013-12-31', '2201433000', '2014-02-03'),
(243, '50000057', '21203340', 'Group of Office Stationery & Secretariat', 'GRP', '50000053', '2000', '2011-01-01', '2013-12-31', '2201433000', '2014-02-03'),
(244, '50000061', '21400110', 'Group of Verification', 'GRP', '50000060', '2000', '2011-01-01', '2013-12-31', '2101501000', '2014-01-28'),
(245, '50000064', '21400140', 'Group of Reporting', 'GRP', '50000060', '2000', '2011-01-01', '2013-12-31', '2101501000', '2014-01-28'),
(246, '50000068', '21402110', 'Group of Sport Development', 'GRP', '50000067', '2000', '2011-01-01', '2013-12-31', '2101521000', '2014-01-28'),
(247, '50000069', '21402115', 'Group of Art Development', 'GRP', '50000067', '2000', '2011-01-01', '2013-12-31', '2101521000', '2014-01-28'),
(248, '50000070', '21402120', 'Group of Envr & Spiritual Education Dev', 'GRP', '50000067', '2000', '2011-01-01', '2013-12-31', '2101521000', '2014-01-28'),
(249, '50000071', '21402125', 'Group of Training Education Development', 'GRP', '50000067', '2000', '2012-03-01', '2013-12-31', '2101521000', '2014-01-28'),
(250, '50000072', '21402130', 'Group of Public Facilities Services', 'GRP', '50000067', '2000', '2011-01-01', '2013-12-31', '2101521000', '2014-01-28'),
(251, '50000073', '21402135', 'Group of Finance & Adm Officer', 'GRP', '50000067', '2000', '2011-01-01', '2013-12-31', '2101521000', '2014-01-28'),
(252, '50000075', '21402210', 'Group of Spiritual Development', 'GRP', '50000074', '2000', '2011-01-01', '2013-12-31', '2201522000', '2014-01-28'),
(253, '50000076', '21402220', 'Group of Social & Culture Development', 'GRP', '50000074', '2000', '2011-01-01', '2013-12-31', '2201522000', '2014-01-28'),
(254, '50000077', '21402230', 'Group of Sport Development', 'GRP', '50000074', '2000', '2011-01-01', '2013-12-31', '2201522000', '2014-01-28'),
(255, '50000078', '21402240', 'Group of Health & General Development', 'GRP', '50000074', '2000', '2011-01-01', '2013-12-31', '2201522000', '2014-01-28'),
(256, '50000079', '21402250', 'Group of Area  Eval Development 1 & 2', 'GRP', '50000074', '2000', '2011-01-01', '2013-12-31', '2201522000', '2014-01-28'),
(257, '50000087', '26201110', 'Group of General Administration', 'GRP', '50000086', '2000', '2011-03-01', '2013-12-31', '', '2014-02-03'),
(258, '50000088', '26201120', 'Group of Filling Management', 'GRP', '50000086', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(259, '50000089', '26201130', 'Group of Personnel Data Maintenance', 'GRP', '50000086', '2000', '2012-03-01', '2013-12-31', '', '2014-02-03'),
(260, '50000090', '26201140', 'Group of Leave, Overtime & Data Presence', 'GRP', '50000086', '2000', '2012-03-01', '2013-12-31', '', '2014-02-03'),
(261, '50000092', '26201210', 'Group of Personnel Relation Adm', 'GRP', '50000091', '2000', '2011-08-01', '2013-12-31', '', '2014-02-03'),
(262, '50000093', '26201220', 'Group of Industrial Relation', 'GRP', '50000091', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(263, '50000094', '26201230', 'Group of Internal Relation', 'GRP', '50000091', '2000', '2011-08-01', '2013-12-31', '', '2014-02-03'),
(264, '50000095', '26201240', 'Group of Employee Welfare', 'GRP', '50000091', '2000', '2011-08-01', '2013-12-31', '', '2014-02-03'),
(265, '50000097', '26201310', 'Group of Tuban Personnel Administration', 'GRP', '50000096', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(266, '50000098', '26201320', 'Group of Personnel Relation', 'GRP', '50000096', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(267, '50000099', '26201330', 'Group of Employee Welfare', 'GRP', '50000096', '2000', '2012-03-01', '2013-12-31', '', '2014-02-03'),
(268, '50000100', '26201350', 'Group of Counseling', 'GRP', '50000096', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(269, '50000102', '26201410', 'Group of Occupational Health & Check Up', 'GRP', '50000101', '2000', '2011-05-01', '2013-12-31', '', '2014-02-03'),
(270, '50000103', '26201420', 'Group of Reporting & Evaluation', 'GRP', '50000101', '2000', '2011-07-01', '2013-12-31', '', '2014-02-03'),
(271, '50000104', '26201430', 'Group of Health Services Administration', 'GRP', '50000101', '2000', '2011-01-01', '2013-12-31', '', '2014-02-03'),
(272, '50000107', '26202110', 'Group of Training Evaluation', 'GRP', '50000106', '2000', '2011-10-01', '2013-12-31', '', '2014-02-03'),
(273, '50000108', '26202120', 'Group of Management Training Planning', 'GRP', '50000106', '2000', '2012-07-01', '2013-12-31', '', '2014-02-03'),
(274, '50000109', '26202130', 'Group of Technical Training Planning', 'GRP', '50000106', '2000', '2011-10-01', '2013-12-31', '', '2014-02-03'),
(275, '50000111', '26202210', 'Group of Training Organizing', 'GRP', '50000110', '2000', '2012-07-01', '2013-12-31', '', '2014-02-03'),
(276, '50000112', '26202220', 'Group of Training Organizing Adm', 'GRP', '50000110', '2000', '2012-06-01', '2013-12-31', '', '2014-02-03'),
(277, '50000113', '26202230', 'Group of Student Services', 'GRP', '50000110', '2000', '2011-10-01', '2013-12-31', '', '2014-02-03'),
(278, '50000114', '26202240', 'Group of Library', 'GRP', '50000110', '2000', '2011-12-01', '2013-12-31', '', '2014-02-03'),
(279, '50000115', '26202250', 'Group of Tuban Training', 'GRP', '50000110', '2000', '2012-07-01', '2013-12-31', '', '2014-02-03'),
(280, '50000128', '22200115', 'Group of Operational Services', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(281, '50000130', '22200125', 'Group of Sales Reporting', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(282, '50000132', '22200135', 'Group of Project & Bulk DO', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(283, '50000133', '22200140', 'Group of Jateng & DIY DO', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(284, '50000135', '22200150', 'Group of Outside Java DO', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(285, '50000136', '22200155', 'Group of Jatim I DO', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(286, '50000137', '22200160', 'Group of Jatim II DO', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(287, '50000138', '22200165', 'Group of Rekon I', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(288, '50000139', '22200170', 'Group of Rekon II', 'GRP', '50000127', '2000', '2011-01-01', '2013-12-31', '2105101000', '2014-01-20'),
(289, '50000152', '22300180', 'Group of Reporting & Verification Adm', 'GRP', '50000151', '2000', '2011-01-01', '2013-12-31', '2105201000', '2014-01-29'),
(290, '50000153', '22300170', 'Group of Clinker, Bulk Freight Payment', 'GRP', '50000151', '2000', '2011-01-01', '2013-07-01', '2105201000', '2013-07-16'),
(291, '50000154', '22300110', 'Group of Land Freight Payment I', 'GRP', '50000151', '2000', '2012-05-01', '2013-12-31', '2105201000', '2014-01-29'),
(292, '50000155', '22300120', 'Group of Land Freight Payment II', 'GRP', '50000151', '2000', '2012-05-01', '2013-12-31', '2105201000', '2014-01-29'),
(293, '50000156', '22300190', 'Group of Sea Freight & Whse Mgmt Payment', 'GRP', '50000151', '2000', '2011-01-01', '2013-07-01', '2105201000', '2013-07-16'),
(294, '50000159', '22301110', 'Group of Gresik Cargo & Administration', 'GRP', '50000158', '2000', '2011-01-01', '2013-12-31', '2105211000', '2014-01-29'),
(295, '50000160', '22301120', 'Group of Gresik Shipment', 'GRP', '50000158', '2000', '2011-01-01', '2013-12-31', '2105211000', '2014-01-29'),
(296, '50000161', '22301130', 'Group of Gresik Shipment', 'GRP', '50000158', '2000', '2011-01-01', '2013-12-31', '2105211000', '2014-01-29'),
(297, '50000162', '22301140', 'Group of Gresik Shipment', 'GRP', '50000158', '2000', '2011-01-01', '2013-12-31', '2105211000', '2014-01-29'),
(298, '50000163', '22301150', 'Group of Gresik Shipment', 'GRP', '50000158', '2000', '2011-01-01', '2013-12-31', '2105211000', '2014-01-29'),
(299, '50000165', '22301210', 'Group of Tuban Shipment Administration', 'GRP', '50000164', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(300, '50000166', '22301220', 'Group of Tuban Shipment', 'GRP', '50000164', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(301, '50000167', '22301230', 'Group of Tuban Shipment', 'GRP', '50000164', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(302, '50000168', '22301240', 'Group of Tuban Shipment', 'GRP', '50000164', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(303, '50000169', '22301250', 'Group of Tuban Shipment', 'GRP', '50000164', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(304, '50000170', '22301260', 'Group of Tuban Cargo', 'GRP', '50000164', '2000', '2011-01-01', '2013-12-31', '2205212000', '2014-01-29'),
(305, '50000172', '22301310', 'Group of Operation Adminstration', 'GRP', '50000171', '2000', '2011-01-01', '2013-12-31', '2105213000', '2014-01-20'),
(306, '50000173', '22301315', 'Group of DO Adminstration', 'GRP', '50000171', '2000', '2011-01-01', '2013-12-31', '2105213000', '2014-01-20'),
(307, '50000180', '22301350', 'Group of Ciwandan Warehousing', 'GRP', '50000171', '2000', '2011-01-01', '9999-12-31', '2405213001', '2012-07-30'),
(308, '50000183', '22301365', 'Group of Gresik Port Warehouse Operation', 'GRP', '50000171', '2000', '2011-01-01', '2013-12-31', '2105213000', '2014-01-20'),
(309, '50000193', '22302210', 'Group of Sea Transportation Evaluation', 'GRP', '50000192', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(310, '50000194', '22302220', 'Group of Bag Cement Ship Operation', 'GRP', '50000192', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(311, '50000195', '22302230', 'Group of Bulk Cement Ship Operation', 'GRP', '50000192', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(312, '50000196', '22302240', 'Group of Gresik Port Operation', 'GRP', '50000192', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(313, '50000197', '22302250', 'Group of Gresik Port Operation', 'GRP', '50000192', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(314, '50000201', '22302310', 'Group of Port Evaluation & Adm', 'GRP', '50000200', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(315, '50000202', '22302320', 'Group of Port Operation', 'GRP', '50000200', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(316, '50000203', '22302330', 'Group of Port Operation', 'GRP', '50000200', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(317, '50000204', '22302340', 'Group of Port Operation', 'GRP', '50000200', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(318, '50000205', '22302350', 'Group of Port Operation', 'GRP', '50000200', '2000', '2011-01-01', '2013-12-31', '2105222000', '2014-01-20'),
(319, '50000211', '23101110', 'Group of Reclamation Land Planning', 'GRP', '50000210', '2000', '2012-07-01', '2013-12-31', '2203131000', '2014-01-30'),
(320, '50000212', '23101112', 'Group of Reclamation Land Operation', 'GRP', '50000210', '2000', '2012-06-01', '2013-12-31', '2203131000', '2014-01-30'),
(321, '50000213', '23101113', 'Group of Reclamation Land Operation', 'GRP', '50000210', '2000', '2012-06-01', '2013-12-31', '2203131000', '2014-01-30'),
(322, '50000216', '23102110', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-06-01', '2013-12-31', '2203121901', '2014-01-30'),
(323, '50000217', '23102111', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(324, '50000218', '23102112', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(325, '50000219', '23102113', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(326, '50000220', '23102114', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(327, '50000221', '23102115', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(328, '50000222', '23102116', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(329, '50000223', '23102117', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(330, '50000224', '23102118', 'Group of Crusher Operation', 'GRP', '50000215', '2000', '2012-07-01', '2013-12-31', '2203121901', '2014-01-30'),
(331, '50000225', '23102170', 'Group of Crusher Preventive', 'GRP', '50000215', '2000', '2012-06-01', '2013-04-01', '2203121901', '2013-04-08'),
(332, '50000226', '23102175', 'Group of Crusher Preventive', 'GRP', '50000215', '2000', '2012-06-01', '2013-04-01', '2203121901', '2013-04-08'),
(333, '50000227', '23102180', 'Group of Crusher Preventive', 'GRP', '50000215', '2000', '2012-06-01', '2013-04-01', '2203121901', '2013-04-08'),
(334, '50000229', '23102201', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(335, '50000230', '23102202', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(336, '50000231', '23102203', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(337, '50000232', '23102204', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(338, '50000233', '23102205', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(339, '50000234', '23102206', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(340, '50000235', '23102207', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(341, '50000236', '23102208', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(342, '50000237', '23102209', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(343, '50000238', '23102110', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(344, '50000239', '23102111', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28');
INSERT INTO `she_master_unit_kerja` (`id`, `muk_kode`, `muk_short`, `muk_nama`, `muk_level`, `muk_parent`, `company`, `muk_begda`, `muk_endda`, `muk_cctr`, `muk_changed_on`) VALUES
(345, '50000240', '23102112', 'Group of Crusher Machine', 'GRP', '50000228', '2000', '2011-01-01', '9999-12-31', '2203122906', '2012-07-28'),
(346, '50000242', '23102310', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(347, '50000243', '23102311', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(348, '50000244', '23102312', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(349, '50000245', '23102313', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(350, '50000246', '23102314', 'Group of Crusher Elect &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(351, '50000247', '23102315', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(352, '50000248', '23102316', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(353, '50000249', '23102317', 'Group of Crusher Electl &', 'GRP', '50000241', '2000', '2011-01-01', '9999-12-31', '2203124907', '2012-07-28'),
(354, '50000251', '23102416', 'Group of Coal Operation', 'GRP', '50000250', '2000', '2011-01-01', '2013-12-31', '2203123901', '2014-01-30'),
(355, '50000252', '23102417', 'Group of Coal Operation', 'GRP', '50000250', '2000', '2011-01-01', '2013-12-31', '2203123901', '2014-01-30'),
(356, '50000253', '23102412', 'Group of Heavy Equip Opr Monitoring', 'GRP', '50000250', '2000', '2012-07-01', '2013-12-31', '2203123901', '2014-01-30'),
(357, '50000254', '23102413', 'Group of Heavy Equip Opr Monitoring', 'GRP', '50000250', '2000', '2012-06-01', '2013-12-31', '2203123901', '2014-01-30'),
(358, '50000255', '23102414', 'Group of Heavy Equip Opr Monitoring', 'GRP', '50000250', '2000', '2012-06-01', '2013-12-31', '2203123901', '2014-01-30'),
(359, '50000256', '23102415', 'Group of Heavy Equip Opr Monitoring', 'GRP', '50000250', '2000', '2012-07-01', '2013-12-31', '2203123901', '2014-01-30'),
(360, '50000257', '23102420', 'Group of Heavy Equip Maint Monitoring', 'GRP', '50000250', '2000', '2012-06-01', '2013-04-01', '2203123901', '2013-04-08'),
(361, '50000258', '23102419', 'Group of Spare Part & Fuel Control', 'GRP', '50000250', '2000', '2012-06-01', '2013-12-31', '2203123901', '2014-01-30'),
(362, '50000261', '23103110', 'Group of IDO, Genset, WP Maint Operation', 'GRP', '50000260', '2000', '2012-07-01', '2013-12-31', '2203112901', '2014-01-30'),
(363, '50000262', '23103111', 'Group of IDO, Genset, WP Maint Operation', 'GRP', '50000260', '2000', '2012-07-01', '2013-12-31', '2203112901', '2014-01-30'),
(364, '50000263', '23103112', 'Group of IDO, Genset, WP Maint Operation', 'GRP', '50000260', '2000', '2012-07-01', '2013-12-31', '2203112901', '2014-01-30'),
(365, '50000264', '23103113', 'Group of IDO, Genset, WP Maint Operation', 'GRP', '50000260', '2000', '2011-01-18', '2013-12-31', '2203112901', '2014-01-30'),
(366, '50000265', '23103120', 'Group of Water Treatment Operation', 'GRP', '50000260', '2000', '2012-07-01', '2013-12-31', '2203112901', '2014-01-30'),
(367, '50000266', '23103121', 'Group of Water Treatment Operation', 'GRP', '50000260', '2000', '2011-01-01', '2013-12-31', '2203112901', '2014-01-30'),
(368, '50000267', '23103122', 'Group of Water Treatment Operation', 'GRP', '50000260', '2000', '2011-01-01', '2013-12-31', '2203112901', '2014-01-30'),
(369, '50000268', '23103123', 'Group of Water Treatment Operation', 'GRP', '50000260', '2000', '2012-07-01', '2013-12-31', '2203112901', '2014-01-30'),
(370, '50000269', '23103160', 'Group of Intern Utility I', 'GRP', '50000260', '2000', '2012-07-01', '9999-12-31', '2203112901', '2012-08-03'),
(371, '50000270', '23103165', 'Group of Intern Utility I', 'GRP', '50000260', '2000', '2012-07-01', '9999-12-31', '2203112901', '2012-08-03'),
(372, '50000272', '23103210', 'Group of Gresik Process Control', 'GRP', '50000271', '2000', '2011-01-01', '2013-12-31', '2203111902', '2014-01-30'),
(373, '50000273', '23103211', 'Group of Gresik Process Control', 'GRP', '50000271', '2000', '2011-01-01', '2013-12-31', '2203111902', '2014-01-30'),
(374, '50000274', '23103212', 'Group of Gresik Process Control', 'GRP', '50000271', '2000', '2011-01-01', '2013-12-31', '2203111902', '2014-01-30'),
(375, '50000275', '23103213', 'Group of Gresik Process Control', 'GRP', '50000271', '2000', '2011-01-01', '2013-12-31', '2203111902', '2014-01-30'),
(376, '50000276', '23103220', 'Group of Tuban Process Control', 'GRP', '50000271', '2000', '2011-12-01', '2013-12-31', '2203111902', '2014-01-30'),
(377, '50000277', '23103221', 'Group of Tuban Process Control', 'GRP', '50000271', '2000', '2012-07-01', '2013-12-31', '2203111902', '2014-01-30'),
(378, '50000278', '23103222', 'Group of Tuban Process Control', 'GRP', '50000271', '2000', '2011-01-18', '2013-12-31', '2203111902', '2014-01-30'),
(379, '50000279', '23103223', 'Group of Tuban Process Control', 'GRP', '50000271', '2000', '2012-07-01', '2013-12-31', '2203111902', '2014-01-30'),
(380, '50000280', '23103230', 'Group of Mix Pile Control Evaluation', 'GRP', '50000271', '2000', '2012-07-01', '2013-12-31', '2203111902', '2014-01-30'),
(381, '50000281', 'O 50000281', 'Group of Control Process', '', '50000271', '2000', '2011-01-01', '9999-12-31', '2203111902', '2012-07-28'),
(382, '50000282', 'O 50000282', 'Group of Process Control', '', '50000271', '2000', '2011-01-01', '9999-12-31', '2203111902', '2012-07-28'),
(383, '50000286', '23201120', 'Group of CCR Kiln & Coal Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(384, '50000288', '23201126', 'Group of CCR Kiln & Coal Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(385, '50000289', '23201129', 'Group of CCR Kiln & Coal Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(386, '50000290', '23201132', 'Group of CCR Kiln & Coal Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(387, '50000291', '23201135', 'Group of CCR Roller Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(388, '50000292', '23201138', 'Group of CCR Roller Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(389, '50000293', '23201131', 'Group of CCR Roller Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(390, '50000294', '23201134', 'Group of CCR Roller Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(391, '50000295', '23201137', 'Group of CCR Roller Mill I', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(392, '50000296', '23201140', 'Group of Kiln & Coal Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(393, '50000297', '23201143', 'Group of Kiln & Coal Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(394, '50000298', '23201146', 'Group of Kiln & Coal Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(395, '50000299', '23201149', 'Group of Kiln & Coal Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(396, '50000300', '23201152', 'Group of Roller Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(397, '50000301', '23201155', 'Group of Roller Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(398, '50000302', '23201158', 'Group of Roller Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(399, '50000303', '23201161', 'Group of Roller Mill I Operation', 'GRP', '50000285', '2000', '2012-07-01', '9999-12-31', '2203211901', '2013-02-05'),
(400, '50000304', '23201164', 'Group of RKC I Preventive', 'GRP', '50000285', '2000', '2011-01-01', '9999-12-31', '2203211901', '2013-02-05'),
(401, '50000305', '23201167', 'Group of RKC I Preventive', 'GRP', '50000285', '2000', '2011-01-01', '9999-12-31', '2203211901', '2013-02-05'),
(402, '50000306', '23201170', 'Group of RKC I Preventive', 'GRP', '50000285', '2000', '2011-01-01', '9999-12-31', '2203211901', '2013-02-05'),
(403, '50000310', '23201212', 'Group of CCR Kiln & Coal Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(404, '50000311', '23201213', 'Group of CCR Kiln & Coal Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(405, '50000312', '23201214', 'Group of CCR Kiln & Coal Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(406, '50000313', '23201220', 'Group of CCR Roller Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(407, '50000314', '23201221', 'Group of CCR Roller Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(408, '50000316', '23201223', 'Group of CCR Roller Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(409, '50000317', '23201224', 'Group of CCR Roller Mill II', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(410, '50000318', '23201230', 'Group of Kiln & Coal Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(411, '50000319', '23201231', 'Group of Kiln & Coal Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(412, '50000320', '23201232', 'Group of Kiln & Coal Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(413, '50000321', '23201233', 'Group of Kiln & Coal Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(414, '50000322', '23201240', 'Group of Roller Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(415, '50000323', '23201241', 'Group of Roller Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(416, '50000324', '23201242', 'Group of Roller Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(417, '50000325', '23201243', 'Group of Roller Mill II Operation', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(418, '50000326', '23201250', 'Group of RKC II Preventive', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(419, '50000327', '23201251', 'Group of RKC II Preventive', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(420, '50000328', '23201252', 'Group of RKC II Preventive', 'GRP', '50000307', '2000', '2012-07-01', '9999-12-31', '2203212901', '2013-02-05'),
(421, '50000330', '23201310', 'Group of CCR Kiln & Coal Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(422, '50000331', '23201311', 'Group of CCR Kiln & Coal Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(423, '50000332', '23201312', 'Group of CCR Kiln & Coal Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(424, '50000333', '23201313', 'Group of CCR Kiln & Coal Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(425, '50000334', '23201314', 'Group of CCR Kiln & Coal Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(426, '50000335', '23201320', 'Group of CCR Roller Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(427, '50000336', '23201321', 'Group of CCR Roller Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(428, '50000338', '23201323', 'Group of CCR Roller Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(429, '50000339', '23201324', 'Group of CCR Roller Mill III', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(430, '50000340', '23201330', 'Group of Kiln & Coal Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(431, '50000341', '23201331', 'Group of Kiln & Coal Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(432, '50000342', '23201332', 'Group of Kiln & Coal Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(433, '50000343', '23201333', 'Group of Kiln & Coal Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(434, '50000344', '23201340', 'Group of Roller Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(435, '50000345', '23201341', 'Group of Roller Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(436, '50000346', '23201342', 'Group of Roller Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(437, '50000347', '23201343', 'Group of Roller Mill III Operation', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(438, '50000348', '23201350', 'Group of RKC III Preventive', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(439, '50000349', '23201351', 'Group of RKC III Preventive', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(440, '50000350', '23201352', 'Group of RKC III Preventive', 'GRP', '50000329', '2000', '2012-07-01', '9999-12-31', '2203213901', '2013-02-05'),
(441, '50000352', 'O 50000352', 'Group of Alternative Fuel', '', '50000351', '2000', '2011-01-01', '9999-12-31', '2203214912', '2013-02-05'),
(442, '50000353', 'O 50000353', 'Group of Alternative Fuel', '', '50000351', '2000', '2011-01-01', '9999-12-31', '2203214912', '2013-02-05'),
(443, '50000356', 'O 50000356', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(444, '50000357', 'O 50000357', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(445, '50000358', 'O 50000358', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(446, '50000359', 'O 50000359', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(447, '50000360', 'O 50000360', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(448, '50000361', 'O 50000361', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(449, '50000362', 'O 50000362', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(450, '50000363', 'O 50000363', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(451, '50000364', 'O 50000364', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(452, '50000365', 'O 50000365', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(453, '50000366', 'O 50000366', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(454, '50000367', 'O 50000367', 'Group of RM Machine Maint', '', '50000355', '2000', '2011-01-01', '9999-12-31', '2203221906', '2013-02-05'),
(455, '50000369', 'O 50000369', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(456, '50000370', 'O 50000370', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(457, '50000371', 'O 50000371', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(458, '50000372', 'O 50000372', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(459, '50000373', 'O 50000373', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(460, '50000374', 'O 50000374', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(461, '50000375', 'O 50000375', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(462, '50000376', 'O 50000376', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(463, '50000377', 'O 50000377', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(464, '50000378', 'O 50000378', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(465, '50000379', 'O 50000379', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(466, '50000380', 'O 50000380', 'Group of KCM Machine Main', '', '50000368', '2000', '2011-01-01', '9999-12-31', '2203222906', '2013-02-05'),
(467, '50000383', 'O 50000383', 'Group of Lift & OHC Elect', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(468, '50000384', 'O 50000384', 'Group of Power Distribution Preventive', '', '50000382', '2000', '2012-07-01', '9999-12-31', '2203231907', '2013-02-05'),
(469, '50000385', 'O 50000385', 'Group of Power Distributi', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(470, '50000386', 'O 50000386', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(471, '50000387', 'O 50000387', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(472, '50000388', 'O 50000388', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(473, '50000389', 'O 50000389', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(474, '50000390', 'O 50000390', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(475, '50000391', 'O 50000391', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(476, '50000392', 'O 50000392', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(477, '50000393', 'O 50000393', 'Group of Electrical I Mai', '', '50000382', '2000', '2011-01-01', '9999-12-31', '2203231907', '2013-02-05'),
(478, '50000395', 'O 50000395', 'Group of Instrument I Mai', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(479, '50000396', 'O 50000396', 'Group of Instrument I Mai', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(480, '50000397', 'O 50000397', 'Group of Instrument I Mai', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(481, '50000398', 'O 50000398', 'Group of Instrument I Mai', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(482, '50000399', 'O 50000399', 'Group of Reclaimer Limest', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(483, '50000400', 'O 50000400', 'Group of Reclaimer Limest', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(484, '50000401', 'O 50000401', 'Group of Reclaimer Limest', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(485, '50000402', 'O 50000402', 'Group of Preheater, Kiln', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(486, '50000403', 'O 50000403', 'Group of Preheater, Kiln', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(487, '50000404', 'O 50000404', 'Group of Preheater, Kiln', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(488, '50000405', 'O 50000405', 'Group of CM, System 1,2,3', '', '50000394', '2000', '2011-01-01', '9999-12-31', '2203232907', '2013-02-05'),
(489, '50000407', 'O 50000407', 'Group of CSM Troubleshoot', '', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(490, '50000408', 'O 50000408', 'Group of CSM Troubleshoot', '', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(491, '50000409', 'O 50000409', 'Group of CSM Troubleshoot', '', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(492, '50000410', 'O 50000410', 'Group of CSM Troubleshoot', '', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(493, '50000411', '23203330', 'Group of CSM Preventive', 'GRP', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(494, '50000412', '23203335', 'Group of CSM Preventive', 'GRP', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(495, '50000413', '23203340', 'Group of CSM Preventive', 'GRP', '50000406', '2000', '2011-01-01', '9999-12-31', '2203233901', '2013-02-05'),
(496, '50000417', '23301101', 'Group of CCR Tuban Finish Mill', 'GRP', '50000416', '2000', '2011-01-01', '9999-12-31', '2203311901', '2013-01-08'),
(497, '50000418', '23301102', 'Group of CCR Tuban Finish Mill', 'GRP', '50000416', '2000', '2011-01-01', '9999-12-31', '2203311901', '2013-01-08'),
(498, '50000419', '23301103', 'Group of CCR Tuban Finish Mill', 'GRP', '50000416', '2000', '2011-01-01', '9999-12-31', '2203311901', '2013-01-08'),
(499, '50000420', '23301104', 'Group of CCR Tuban Finish Mill', 'GRP', '50000416', '2000', '2011-01-01', '9999-12-31', '2203311901', '2013-01-08'),
(500, '50000421', '23301105', 'Group of CCR Tuban Finish Mill', 'GRP', '50000416', '2000', '2011-01-01', '9999-12-31', '2203311901', '2013-01-08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_master_vendor`
--

CREATE TABLE `she_master_vendor` (
  `LIFNR` varchar(100) NOT NULL,
  `NAME1` varchar(100) NOT NULL,
  `KATEGORI` varchar(100) DEFAULT NULL,
  `ALAMAT` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PASS` varchar(100) DEFAULT NULL,
  `TYV` varchar(100) DEFAULT NULL,
  `FLAG` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `KETERANGAN` varchar(100) DEFAULT NULL,
  `KTOKK` varchar(100) DEFAULT NULL,
  `RNUM` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_master_vendor`
--

INSERT INTO `she_master_vendor` (`LIFNR`, `NAME1`, `KATEGORI`, `ALAMAT`, `EMAIL`, `PASS`, `TYV`, `FLAG`, `id`, `KETERANGAN`, `KTOKK`, `RNUM`) VALUES
('2', 'PT. SWABINA GATRA LAMA', '1', NULL, NULL, NULL, '1', '1', 4129, NULL, NULL, '1'),
('5', 'PT. IKSG GUDANG PACKER', '1', NULL, NULL, NULL, '1', '1', 4130, NULL, NULL, '2'),
('6', 'PT. PUTRA KARYA AGUNG', '1', NULL, NULL, NULL, '1', '1', 4131, NULL, NULL, '3'),
('7', 'PT. SWADAYA GRAHA A.U LAMA', '1', NULL, NULL, NULL, '1', '1', 4132, NULL, NULL, '4'),
('10', 'BINA LINGKUNGAN', '2', NULL, NULL, NULL, '1', '1', 4133, NULL, NULL, '5'),
('11', 'RS SEMEN GRESIK TUBAN', '1', NULL, NULL, NULL, '1', '1', 4134, NULL, NULL, '6'),
('12', 'KWSG (A.U)', '1', NULL, NULL, NULL, '1', '1', 4135, NULL, NULL, '7'),
('13', 'PT. GRAHASINDO CIPTA PRATAMA', '2', NULL, NULL, NULL, '1', '1', 4136, NULL, NULL, '8'),
('14', 'CV. YUWANA KARYA', '2', NULL, NULL, NULL, '1', '1', 4137, NULL, NULL, '9'),
('17', 'CV. BISAN MAKARYA', '2', NULL, NULL, NULL, '1', '1', 4138, NULL, NULL, '10'),
('20', 'SEMEN GRESIK', '0', NULL, NULL, NULL, '1', '1', 4139, NULL, NULL, '11'),
('21', 'CV. DAYA WISHESA MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4140, NULL, NULL, '12'),
('25', 'UD. MULTITEC', '2', NULL, NULL, NULL, '1', '1', 4141, NULL, NULL, '13'),
('26', 'PT. BINTANG CEMENT MANDIRI', '1', NULL, NULL, NULL, '1', '1', 4142, NULL, NULL, '14'),
('27', 'CV. KAESAR MAHAKARYA', '2', NULL, NULL, NULL, '1', '1', 4143, NULL, NULL, '15'),
('32', 'CV. MAHAKARYA', '2', NULL, NULL, NULL, '1', '1', 4144, NULL, NULL, '16'),
('36', 'CV. SARI BUMI KARYA', '2', NULL, NULL, NULL, '1', '1', 4145, NULL, NULL, '17'),
('37', 'KANTIN PABRIK SI TUBAN', '2', NULL, NULL, NULL, '1', '1', 4146, NULL, NULL, '18'),
('38', 'CV. MULTI USAHA', '2', NULL, NULL, NULL, '1', '1', 4147, NULL, NULL, '19'),
('42', 'PT. GLOBAL SOLUSINDO KOMPUDATA', '2', NULL, NULL, NULL, '1', '1', 4148, NULL, NULL, '20'),
('43', 'CV. LANGGENG JAYA BAROKAH.999.', '2', NULL, NULL, NULL, '1', '1', 4149, NULL, NULL, '21'),
('45', 'CV. DUA PUTRA', '2', NULL, NULL, NULL, '1', '1', 4150, NULL, NULL, '22'),
('46', 'PT. SUCOFINDO', '2', NULL, NULL, NULL, '1', '1', 4151, NULL, NULL, '23'),
('47', 'CV. TEHNIK UMUM', '2', NULL, NULL, NULL, '1', '1', 4152, NULL, NULL, '24'),
('48', 'CV. SARI KARYA MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4153, NULL, NULL, '25'),
('52', 'DRIVER SI', '2', NULL, NULL, NULL, '1', '1', 4156, NULL, NULL, '26'),
('58', 'FLS MIDTH', '1', NULL, NULL, NULL, '1', '1', 4157, NULL, NULL, '27'),
('60', 'CV. SRIKANDI', '2', NULL, NULL, NULL, '1', '1', 4158, NULL, NULL, '28'),
('61', 'CV. STAR OMEGA SAKTI', '2', NULL, NULL, NULL, '1', '1', 4159, NULL, NULL, '29'),
('67', 'PT. VARIA USAHA A.U', '1', NULL, NULL, NULL, '1', '1', 4160, NULL, NULL, '30'),
('68', 'PT. MULTIJAYA TEKNIK INDONESIA', '1', NULL, 'j_multi@ymail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4161, NULL, NULL, '31'),
('69', 'CV. NUR ABADI', '2', NULL, NULL, NULL, '1', '1', 4162, NULL, NULL, '32'),
('70', 'CV. MENARA MAS', '2', NULL, NULL, NULL, '1', '1', 4163, NULL, NULL, '33'),
('72', 'CV. FARCHIKA ENGINEERING', '2', NULL, NULL, NULL, '1', '1', 4164, NULL, NULL, '34'),
('73', 'CV. CIPTA MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4165, NULL, NULL, '35'),
('74', 'PT. WIDYADHANA UTAMA', '2', NULL, NULL, NULL, '1', '1', 4166, NULL, NULL, '36'),
('78', 'CV. BAHTERA INDAH', '2', NULL, NULL, NULL, '1', '1', 4167, NULL, NULL, '37'),
('79', 'PT. SWADAYA CIPTA', '2', NULL, NULL, NULL, '1', '1', 4168, NULL, NULL, '38'),
('80', 'CV. BUMI ATAS ANGIN', '2', NULL, NULL, NULL, '1', '1', 4169, NULL, NULL, '39'),
('81', 'CV. KARYA PUTRI ABADI', '2', NULL, NULL, NULL, '1', '1', 4170, NULL, NULL, '40'),
('83', 'CV. ANDIKA', '2', NULL, NULL, NULL, '1', '1', 4171, NULL, NULL, '41'),
('84', 'CV. CAKRA BUANA', '2', NULL, NULL, NULL, '1', '1', 4172, NULL, NULL, '42'),
('85', 'PT. ARDI TEKINDO PERKASA', '2', NULL, NULL, NULL, '1', '1', 4173, NULL, NULL, '43'),
('86', 'PT. SARANA KARYA BANGUN PERSADA', '2', NULL, NULL, NULL, '1', '1', 4174, NULL, NULL, '44'),
('87', 'CV. HASTA KARYA MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4175, NULL, NULL, '45'),
('89', 'PT. SAMATOR', '2', NULL, NULL, NULL, '1', '1', 4176, NULL, NULL, '46'),
('92', 'CV. QUARTA MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4177, NULL, NULL, '47'),
('94', 'PT. PUJI LESTARI PURNAMA', '2', NULL, NULL, NULL, '1', '1', 4178, NULL, NULL, '48'),
('95', 'PT. KARMAND MITRA  ANDALAN', '2', NULL, NULL, NULL, '1', '1', 4179, NULL, NULL, '49'),
('96', 'CV. ALTON KARUNIA', '2', NULL, NULL, NULL, '1', '1', 4180, NULL, NULL, '50'),
('97', 'PT. ZI-TECHASIA', '2', NULL, NULL, NULL, '1', '1', 4181, NULL, NULL, '51'),
('98', 'PT. KONSULTA (SI)', '1', NULL, NULL, NULL, '1', '1', 4182, NULL, NULL, '52'),
('101', 'PT. MANUNGGAL JAYA SAKTI', '2', NULL, NULL, NULL, '1', '1', 4183, NULL, NULL, '53'),
('106', 'WAHYU PANGESTU, PT', '1', NULL, 'pt_wahyu@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4185, NULL, NULL, '54'),
('110', 'CV. DWI MULYA', '2', NULL, NULL, NULL, '1', '1', 4186, NULL, NULL, '55'),
('112', 'CV. MARGOMULYO', '2', NULL, NULL, NULL, '1', '1', 4187, NULL, NULL, '56'),
('113', 'POLRES TUBAN', '1', NULL, NULL, NULL, '1', '1', 4188, NULL, NULL, '57'),
('115', 'PT. HAS PUTRA JAYA ABADI', '2', NULL, NULL, NULL, '1', '1', 4189, NULL, NULL, '58'),
('118', 'CV. MAHABAH JAYA', '2', NULL, NULL, NULL, '1', '1', 4190, NULL, NULL, '59'),
('119', 'PT. BANK MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4191, NULL, NULL, '60'),
('122', 'CV. INKAMINDO', '2', NULL, NULL, NULL, '1', '1', 4192, NULL, NULL, '61'),
('125', 'PT. BRAVO SECURITY INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4193, NULL, NULL, '62'),
('127', 'PT. SHELTER NUSANTARA', '2', NULL, NULL, NULL, '1', '1', 4194, NULL, NULL, '63'),
('129', 'PT. IKSG', '1', NULL, NULL, NULL, '1', '1', 4195, NULL, NULL, '64'),
('132', 'CV. BRATAJAYA', '2', NULL, NULL, NULL, '1', '1', 4196, NULL, NULL, '65'),
('133', 'CV. ARIHA', '2', NULL, NULL, NULL, '1', '1', 4197, NULL, NULL, '66'),
('136', 'CV. PUTRA JAYA', '2', NULL, NULL, NULL, '1', '1', 4198, NULL, NULL, '67'),
('137', 'PT. ARDHI KARYA', '2', NULL, NULL, NULL, '1', '1', 4199, NULL, NULL, '68'),
('138', 'PT. SURVEYOR INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4200, NULL, NULL, '69'),
('139', 'CV. DWI PUTRA', '2', NULL, NULL, NULL, '1', '1', 4201, NULL, NULL, '70'),
('140', 'CV. RELASI', '2', NULL, NULL, NULL, '1', '1', 4202, NULL, NULL, '71'),
('142', 'PT. INDOBARA BAHANA', '2', NULL, NULL, NULL, '1', '1', 4203, NULL, NULL, '72'),
('145', 'PT. SARANA DWI MAKMUR', '2', NULL, NULL, NULL, '1', '1', 4204, NULL, NULL, '73'),
('150', 'PT. SURYA MAS PERKASA', '2', NULL, NULL, NULL, '1', '1', 4206, NULL, NULL, '74'),
('160', 'PT. HARY SEKAWAN ABADI', '2', NULL, NULL, NULL, '1', '1', 4208, NULL, NULL, '75'),
('161', 'PT. JAYA TEHNIKA UTAMA', '1', NULL, 'jtu_ptku@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4209, NULL, NULL, '76'),
('162', 'ATMA JAYA ABADI, PT', '1', NULL, 'ATMAJAYAABADI@YAHOO.COM', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4210, NULL, NULL, '77'),
('163', 'PT. LIMA DAYA AGUNG SENTOSA', '2', NULL, NULL, NULL, '1', '1', 4211, NULL, NULL, '78'),
('166', 'PT. BAHTERA BERSAUDARA', '2', NULL, NULL, NULL, '1', '1', 4212, NULL, NULL, '79'),
('171', 'PT. BINA KARSA KONSULTAN', '2', NULL, NULL, NULL, '1', '1', 4213, NULL, NULL, '80'),
('174', 'PT. TIMBUL PERSADA', '2', NULL, NULL, NULL, '1', '1', 4214, NULL, NULL, '81'),
('176', 'PT. SAMUDRA LUAS PARAMACITRA', '2', NULL, NULL, NULL, '1', '1', 4215, NULL, NULL, '82'),
('179', 'CV. NOVIANA', '2', NULL, NULL, NULL, '1', '1', 4216, NULL, NULL, '83'),
('180', 'SEMEN INDONESIA FOUNDATION', '1', NULL, NULL, NULL, '1', '1', 4217, NULL, NULL, '84'),
('181', 'PT. GADING MAS SEJATI', '1', NULL, 'pt.gadingmassejati@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4218, NULL, NULL, '240'),
('184', 'BUKIT TUBAN PERSADA, PT', '1', NULL, 'BUKITTUBANPERSADA.PT80@GMAIL.COM', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4220, NULL, NULL, '85'),
('185', 'PT. MULIA DEWI SEJATI', '2', NULL, NULL, NULL, '1', '1', 4221, NULL, NULL, '86'),
('186', 'MENARA MAS PERSADA, PT', '1', NULL, 'menaramas.persada@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4222, NULL, NULL, '87'),
('226', 'PT. ENERGY STUDIO', '2', NULL, NULL, NULL, '1', '1', 4223, NULL, NULL, '88'),
('255', 'AZIZ JAYA ABADI, PT', '1', NULL, 'gunungmaslombok@ymail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4224, NULL, NULL, '89'),
('257', 'PT. SUPRABAKTI MANDIRI', '1', NULL, 'inggus@beltcare.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4226, NULL, NULL, '90'),
('258', 'PT. SWADAYA GRAHA FAB-SUB', '2', NULL, NULL, NULL, '1', '1', 4227, NULL, NULL, '91'),
('268', 'PT. AHMAD PUTRA INDO KARYA', '2', NULL, NULL, NULL, '1', '1', 4228, NULL, NULL, '92'),
('270', 'PERWIRA JAYA SAKTI, PT', '1', NULL, 'pt.perwira.js@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4230, NULL, NULL, '93'),
('271', 'PT. PANCASAKA NATHA PERSADA', '1', NULL, 'AIR_GIRI@YAHOO.COM', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4231, NULL, NULL, '94'),
('273', 'KWSG (SERV & POOL)', '1', NULL, NULL, NULL, '1', '1', 4233, NULL, NULL, '95'),
('274', 'PT. THE SERVICE LINE (SOS)', '2', NULL, NULL, NULL, '1', '1', 4234, NULL, NULL, '96'),
('275', 'PT. MULTI NUR ALAM', '2', NULL, NULL, NULL, '1', '1', 4235, NULL, NULL, '97'),
('276', 'PT. BAHTERA CIPTA ANUGRAH', '1', NULL, 'slametbahtera@yahoo.com', 'b53ef8583c54f7fd6b8634a85d0b1a8a', '1', '1', 4236, NULL, NULL, '98'),
('277', 'PT. SUBUR BUANA SENTOSA', '1', NULL, 'SUBURBUANASENTOSA@YAHOO.CO.ID', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4237, NULL, NULL, '99'),
('302', 'PT. PACKET SYSTEMS INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4238, NULL, NULL, '100'),
('307', 'CV. BIMA SAKTI', '2', NULL, NULL, NULL, '1', '1', 4239, NULL, NULL, '101'),
('312', 'PT. BERKAH INKAM MULIA', '2', NULL, NULL, NULL, '1', '1', 4240, NULL, NULL, '102'),
('316', 'PT. ASKO KARYA RAYA', '1', NULL, 'pt.akr09@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4241, NULL, NULL, '103'),
('320', 'PT. WIRA KARYA INDAH MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4242, NULL, NULL, '104'),
('338', 'PT. DELTA INDRATAMA ORION', '1', NULL, 'pt.dio69@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4244, NULL, NULL, '105'),
('346', 'PT. SAHABAT DAYA TEKNIK', '2', NULL, NULL, NULL, '1', '1', 4245, NULL, NULL, '106'),
('350', 'PT. KARYA DATA KOMUNIKA', '2', NULL, NULL, NULL, '1', '1', 4246, NULL, NULL, '107'),
('351', 'PT. GRAHA CITRA INDOTEKNIK', '2', NULL, NULL, NULL, '1', '1', 4247, NULL, NULL, '108'),
('355', 'PT. KARYA ENERGI INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4249, NULL, NULL, '109'),
('358', 'PT. KHARISMA PUTRI SAKTI', '2', NULL, NULL, NULL, '1', '1', 4250, NULL, NULL, '110'),
('364', 'PT. MAM ENERGINDO', '2', NULL, NULL, NULL, '1', '1', 4251, NULL, NULL, '111'),
('370', 'PT. TUBAN GLOBAL NUSANTARA', '2', NULL, NULL, NULL, '1', '1', 4252, NULL, NULL, '112'),
('374', 'PT. GUNA DHARMA PERKASA', '2', NULL, NULL, NULL, '1', '1', 4253, NULL, NULL, '113'),
('380', 'PT. FARCHIKA BINTANG REKADIMENSI', '2', NULL, NULL, NULL, '1', '1', 4254, NULL, NULL, '114'),
('381', 'LNS INDONESIA, PT', '1', NULL, 'rosanjaya99@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4255, NULL, NULL, '115'),
('384', 'PT. SARANA PEMBANGUNAN PANTURA', '2', NULL, NULL, NULL, '1', '1', 4256, NULL, NULL, '116'),
('386', 'PT. SURYA INTAN PUTRA', '2', NULL, NULL, NULL, '1', '1', 4257, NULL, NULL, '117'),
('390', 'PT. JAGARAGA ADIKA', '2', NULL, NULL, NULL, '1', '1', 4258, NULL, NULL, '118'),
('392', 'PT. PENTA CHEMICALS INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4259, NULL, NULL, '119'),
('395', 'PT. SPEKTRIS METALAB', '2', NULL, NULL, NULL, '1', '1', 4260, NULL, NULL, '120'),
('409', 'SEMEN GRESIK (PKWT)', '0', NULL, NULL, NULL, '1', '1', 4261, NULL, NULL, '121'),
('411', 'AZAM JAYA SEJAHTERA, PT', '1', NULL, 'pt.ajs2016@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4262, NULL, NULL, '122'),
('415', 'TRUBA JAYA ENGINEERING, PT', '1', NULL, 'dyanti.sdian@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4263, NULL, NULL, '123'),
('419', 'PT. SWADAYA GRAHA A.U SIPIL', '1', NULL, NULL, NULL, '1', '1', 4264, NULL, NULL, '124'),
('425', 'KARYA NUGRAHA ABADI SEJAHTERA, PT', '1', NULL, 'PT.KARYANUGRAHA@YAHOO.CO.ID', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4265, NULL, NULL, '125'),
('436', 'PT. MULTI TEKNIK JAYA GUNA', '2', NULL, NULL, NULL, '1', '1', 4266, NULL, NULL, '126'),
('440', 'PT. DWI MULYA MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4268, NULL, NULL, '127'),
('446', 'PT. SINAR TUBAN MANDIRI', '2', NULL, NULL, NULL, '1', '1', 4269, NULL, NULL, '128'),
('449', 'PT. TIANG AWAN L.I-Pallet', '2', NULL, NULL, NULL, '1', '1', 4270, NULL, NULL, '129'),
('450', 'PT. YUWANA KARYA CATUR MANUNGGAL', '2', NULL, NULL, NULL, '1', '1', 4271, NULL, NULL, '130'),
('451', 'PT. VARUNA TIRTA PRAKSYA', '2', NULL, NULL, NULL, '1', '1', 4272, NULL, NULL, '131'),
('457', 'PT. BISAN SARANA TEKNIK', '2', NULL, NULL, NULL, '1', '1', 4274, NULL, NULL, '132'),
('460', 'OUTPASS PABRIK TUBAN', '0', NULL, NULL, NULL, '1', '1', 4275, NULL, NULL, '133'),
('465', 'PT. VARIA USAHA A.U MEKANIKAL', '1', NULL, NULL, NULL, '1', '1', 4276, NULL, NULL, '134'),
('466', 'PT. BERKAH DWI ARUM', '2', NULL, NULL, NULL, '1', '1', 4277, NULL, NULL, '135'),
('476', 'PT. UTSG', '1', NULL, NULL, NULL, '1', '1', 4278, NULL, NULL, '136'),
('483', 'PT. SUNGAI MUARA SAMUDRA', '2', NULL, NULL, NULL, '1', '1', 4279, NULL, NULL, '137'),
('487', 'PT. SARI BUMI TUBAN', '1', NULL, 'pt.sbt7@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4280, NULL, NULL, '138'),
('490', 'PT. VARIA USAHA BETON', '2', NULL, NULL, NULL, '1', '1', 4281, NULL, NULL, '139'),
('491', 'PT. PLN ( PERSERO )', '2', NULL, NULL, NULL, '1', '1', 4282, NULL, NULL, '140'),
('493', 'PT. SURYA TEKNIK JAYA', '1', NULL, 'pt.suryateknikjaya@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4283, NULL, NULL, '209'),
('494', 'PT. GLOBAL CIPTA NIAGA', '1', NULL, 'moh.qosim@glocini.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4284, NULL, NULL, '141'),
('498', 'PT. ULAN BUANA ENGINEERING', '2', NULL, NULL, NULL, '1', '1', 4285, NULL, NULL, '142'),
('500', 'PT. PION MAS SERBA GUNA', '2', NULL, NULL, NULL, '1', '1', 4286, NULL, NULL, '143'),
('1601', 'Seksi PSC', NULL, NULL, NULL, 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4410, NULL, NULL, '144'),
('1602', 'Seksi Rumah Tangga', NULL, NULL, NULL, 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4411, NULL, NULL, '145'),
('1603', 'Seksi Sarana Umum', NULL, NULL, NULL, 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4412, NULL, NULL, '146'),
('1606', 'Seksi Distran', NULL, NULL, NULL, 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4415, NULL, NULL, '147'),
('1608', 'Seksi Keamanan Tuban', NULL, NULL, NULL, 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4417, NULL, NULL, '148'),
('1609', 'Seksi Pengadaan Jasa rutin', NULL, NULL, NULL, 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4418, NULL, NULL, '149'),
('1089', 'INTIDAYA DINAMIKA SEJATI', '1', NULL, 'j_kartawijaya@intidayads.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4421, NULL, NULL, '150'),
('938197', 'SUPRIHATININGSIH', NULL, NULL, 'suprihati.ningsih96@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4424, NULL, NULL, '239'),
('1032', 'PDKB ELECTRICAL', '2', NULL, NULL, NULL, '1', '1', 4441, NULL, NULL, '151'),
('1046', 'TIM ADVERTISING SI', '1', NULL, NULL, NULL, '1', '1', 4442, NULL, NULL, '152'),
('1055', 'PT. KONSULTA (SI) 050117', '1', NULL, NULL, NULL, '1', '1', 4443, NULL, NULL, '153'),
('1061', 'PT. UNITECH MEGA INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4444, NULL, NULL, '154'),
('1065', 'PT. SAKA DUA MANDIRI', '1', NULL, 'SAKA2MANDIRI@GMAIL.COM', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4445, NULL, NULL, '155'),
('1067', 'PT. SBG (SUB POOL)', '1', NULL, NULL, NULL, '1', '1', 4446, NULL, NULL, '156'),
('1068', '# P.JOB TBN 2 (180117)', '2', NULL, NULL, NULL, '1', '1', 4447, NULL, NULL, '157'),
('1071', '# DATA ID OVH', '2', NULL, NULL, NULL, '1', '1', 4448, NULL, NULL, '158'),
('1072', '# OVH MILL 4 TBN 2 (200117)', '2', NULL, NULL, NULL, '1', '1', 4449, NULL, NULL, '159'),
('1077', 'PT. BUKIT TBN PERSADA (UTILITAS)', '2', NULL, NULL, NULL, '1', '1', 4450, NULL, NULL, '160'),
('1078', 'PT. BUKIT TBN PERSADA (EP 1,2)', '2', NULL, NULL, NULL, '1', '1', 4451, NULL, NULL, '161'),
('1080', 'PT. CITRA DUTA MUDA (SEMEN)', '2', NULL, NULL, NULL, '1', '1', 4452, NULL, NULL, '162'),
('1081', 'PT. CITRA DUTA MUDA (TERAK)', '2', NULL, NULL, NULL, '1', '1', 4453, NULL, NULL, '163'),
('1082', 'PT. WIDYADHANA UTAMA NW', '2', NULL, NULL, NULL, '1', '1', 4454, NULL, NULL, '164'),
('1083', 'PT. MENARA MAS P (PMFM1-4)', '2', NULL, NULL, NULL, '1', '1', 4455, NULL, NULL, '165'),
('1084', 'PT. AZIZ JAYA ABADI (PMPP)', '2', NULL, NULL, NULL, '1', '1', 4456, NULL, NULL, '166'),
('1085', 'PT. BUKIT TBN PERSADA (PMKC1-4)', '2', NULL, NULL, NULL, '1', '1', 4457, NULL, NULL, '167'),
('1086', 'PT. MULIA DEWI SEJATI (OP-2)', '2', NULL, NULL, NULL, '1', '1', 4458, NULL, NULL, '168'),
('1087', 'PT. MULIA DEWI SEJATI (OP-1)', '2', NULL, NULL, NULL, '1', '1', 4459, NULL, NULL, '169'),
('1088', 'PT. SWG (PMCR)', '1', NULL, NULL, NULL, '1', '1', 4460, NULL, NULL, '170'),
('1090', 'PT. DELTA INDRATAMA O (OP3)', '2', NULL, NULL, NULL, '1', '1', 4462, NULL, NULL, '171'),
('1091', '# OVH TBN 2 (020517)', '2', NULL, NULL, NULL, '1', '1', 4463, NULL, NULL, '172'),
('1092', 'PT. RENTOKIL INDONESIA', '2', NULL, NULL, NULL, '1', '1', 4464, NULL, NULL, '173'),
('1093', 'PT. EXIS JAYA ABADI - MEK', '1', NULL, 'exisjayaabadi@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4465, NULL, NULL, '174'),
('1094', 'PT. SBG A.U (VARIABLE)', '1', NULL, NULL, NULL, '1', '1', 4466, NULL, NULL, '175'),
('1101', 'PT. BIMA BERKAH MANDIRI-BC', '2', NULL, NULL, NULL, '1', '1', 4467, NULL, NULL, '176'),
('1108', 'PT. DUTA SARANA ENGINEERING', '1', NULL, 'admin@dutasaranaeng.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4468, NULL, NULL, '177'),
('1110', 'PT. MENARA MAS P (B.ELEV)', '2', NULL, NULL, NULL, '1', '1', 4469, NULL, NULL, '178'),
('1113', 'PT. WIRYA KRENINDO P-TAHUNAN', '2', NULL, NULL, NULL, '1', '1', 4470, NULL, NULL, '179'),
('1115', 'PT. PANCASAKA NATHA P 080217', '2', NULL, NULL, NULL, '1', '1', 4471, NULL, NULL, '180'),
('1123', 'PT. SURYA WIJAYA MEGAH-FABA', '2', NULL, NULL, NULL, '1', '1', 4472, NULL, NULL, '181'),
('1127', 'PT. FAJAR BENUA INDOPACK', '2', NULL, NULL, NULL, '1', '1', 4473, NULL, NULL, '182'),
('1128', 'PT. TRIPUTRA JAYA SENTOSA', '2', NULL, NULL, NULL, '1', '1', 4474, NULL, NULL, '183'),
('1129', 'PT. SURYA BUANA SENTOSA', '2', NULL, NULL, NULL, '1', '1', 4475, NULL, NULL, '184'),
('1131', 'PT. WIDYADHANA UTAMA POJECT 2', '2', NULL, NULL, NULL, '1', '1', 4476, NULL, NULL, '185'),
('1132', 'PT. VARIA USAHA B-CLEANING TERSUS', '2', NULL, NULL, NULL, '1', '1', 4477, NULL, NULL, '186'),
('1135', 'PT. PROGANIC -', '2', NULL, NULL, NULL, '1', '1', 4478, NULL, NULL, '187'),
('1138', 'PT. GIAN PERMATA GUNA', '1', NULL, 'gianpermataguna@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4479, NULL, NULL, '188'),
('1141', 'PT. LNS INDONESIA (PMRM)', '2', NULL, NULL, NULL, '1', '1', 4480, NULL, NULL, '189'),
('1142', 'PT. KNAS-CARGO', '2', NULL, NULL, NULL, '1', '1', 4481, NULL, NULL, '190'),
('1145', 'PT. KITA MANDIRI ABADI', '2', NULL, NULL, NULL, '1', '1', 4482, NULL, NULL, '191'),
('1146', 'PT. SWG-FAB 160317 cancel', '1', NULL, NULL, NULL, '1', '1', 4483, NULL, NULL, '192'),
('1156', 'PT. HANSA PRATAMA', '2', NULL, NULL, NULL, '1', '1', 4484, NULL, NULL, '193'),
('1162', 'PT. SBG-PKWT 060417', '1', NULL, NULL, NULL, '1', '1', 4485, NULL, NULL, '194'),
('1166', '# OVH MILL 5 TBN 3 (120417)', '2', NULL, NULL, NULL, '1', '1', 4486, NULL, NULL, '195'),
('1167', 'PT. BERDIKARI PONDASI PERKASA', '2', NULL, NULL, NULL, '1', '1', 4487, NULL, NULL, '196'),
('1168', 'PT. SWG-MEK (BM)', '1', NULL, NULL, NULL, '1', '1', 4488, NULL, NULL, '197'),
('1171', 'PT. BINA JAYANTI KARYA', '2', NULL, NULL, NULL, '1', '1', 4489, NULL, NULL, '198'),
('1172', 'PT. KNAS-UTILITAS', '2', NULL, NULL, NULL, '1', '1', 4490, NULL, NULL, '199'),
('1179', '# OVH MILL 1 TBN 1 (220417)', '2', NULL, NULL, NULL, '1', '1', 4491, NULL, NULL, '200'),
('1181', '# OVH TBN 4 (110217)', '2', NULL, NULL, NULL, '1', '1', 4492, NULL, NULL, '201'),
('1182', 'PT. ALTEK PRIMINDO', '2', NULL, NULL, NULL, '1', '1', 4493, NULL, NULL, '202'),
('1183', 'PT. SWG-MEK (BM) 260417', '1', NULL, NULL, NULL, '1', '1', 4494, NULL, NULL, '203'),
('1184', 'PT. SWG-FAB 260417', '1', NULL, NULL, NULL, '1', '1', 4495, NULL, NULL, '204'),
('1185', 'PT. KHARISMA PUTRI SAKTI 280417', '2', NULL, NULL, NULL, '1', '1', 4496, NULL, NULL, '205'),
('898042', 'MOH. DIYANTO', NULL, NULL, 'todian310@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4500, NULL, NULL, '206'),
('150296', 'Tifa Nur Fitriani', NULL, NULL, 'fitrianitifa@yahoo.com', 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4501, NULL, NULL, '207'),
('281196', 'Novi Dwi Friantini', NULL, NULL, 'novidwifriantini@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '2', '1', 4502, NULL, NULL, '208'),
('1100', 'HARAPAN JAYA TUBAN, PT', '1', NULL, 'jayapurnama0706@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4503, NULL, NULL, '241'),
('114235', 'MITRA NUSANTARA TEKNIK INDONESIA, P', '1', 'RUKO MEGAH GROSIR CEMPAKA MAS BLOK, JAKARTA PUSAT', 'DEDENSE@GMAIL.COM', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4525, NULL, '1100', '242'),
('110038', 'BUANA CAHAYATAMA, PT', '1', 'JL A.W SIAMIN RUKO VILLA BUKIT MAS, SURABAYA', 'buanact@gmail.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4526, NULL, '1100', '243'),
('113752', 'KAWAN TEKNIK SEJATI, PT', '1', 'JL. RAYA NUSANTARA NO. 111, SIDOARJO', 'marketing@ptkawantekniksejati.com', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4543, NULL, '1100', '210'),
('110056', 'FAJAR BENUA INDOPACK, PT', '1', 'JL. DARMO PERMAI TIMUR 19F, JAKARTA UTARA', 'fungky@fajarbenua.co.id', NULL, '1', '1', 4546, NULL, '1100', '211'),
('110058', 'FAKULTAS TEKNIK INDUSTRI ITS', '1', 'KAMPUS ITS - SUKOLILO, SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4547, NULL, '1100', '212'),
('110059', 'F.L. SMIDTH (D/H FULLER INC)', '1', 'D/AGed.BRI II Suite 2101 Jl.Sudirma, JAKARTA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4548, NULL, '1100', '213'),
('110061', 'GADING MURNI, PT.', '1', 'JL. TUNJUNGAN NO. 27, SURABAYA', 'slamet@gadingmurni.co.id', NULL, '1', '1', 4549, NULL, '1100', '214'),
('110063', 'GAYA LISTRIK, CV', '1', 'JL. KEMBANG JEPUN NO. 19, SURABAYA', 'gaya.listrik@gmail.com', NULL, '1', '1', 4550, NULL, '1100', '215'),
('110065', 'GERINDO PUTRA TUNGGAL, PT.', '1', 'JL RAYA KALIRUNGKUT NO 5 BLOK I/11,, SURABAYA', 'andi@gerindo.co.id', NULL, '1', '1', 4551, NULL, '1100', '216'),
('110067', 'GUNA ELEKTRO, PT.', '1', 'JL. RUNGKUT INDUSTRI I/29, JAKARTA BARAT', 'gunasby@rad.net.id', NULL, '1', '1', 4552, NULL, '1100', '217'),
('110069', 'GUNUNG SEWU, UD.', '1', 'JL BUBUTAN No 41, SURABAYA', 'gs1000@sby.dnet.net.id', NULL, '1', '1', 4553, NULL, '1100', '218'),
('110072', 'HARVESTER SANJAYA SEJAHTERA, PT.', '1', 'JL. WASPADA NO. 25, SURABAYA', 'hrvstrsanjaya@gmail.com', NULL, '1', '1', 4554, NULL, '1100', '219'),
('110074', 'HASTA MARGA, FA.', '1', 'DS SENDANG HARJO GANG I/02, TUBAN', 'engineering.mjsgroup@yahoo.com', NULL, '1', '1', 4555, NULL, '1100', '220'),
('110076', 'HILAB SCIENCETAMA, PT.', '1', 'JL TANAH ABANG I/12 D CXXNB-025, JAKARTA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4556, NULL, '1100', '221'),
('110078', 'HIMALAYA EVEREST JAYA, PT', '1', 'JL EMBONG WUNGU No. 15, SURABAYA', 'hejsby@indosat.net.id', NULL, '1', '1', 4557, NULL, '1100', '222'),
('110080', 'INDAH JAYA, CV', '1', 'JL. SUNAN MURIA NO. 40, TUBAN', 'cv.indahjaya@yahoo.com', NULL, '1', '1', 4558, NULL, '1100', '223'),
('110081', 'INDAL STEEL PIPE', '1', 'JL. KEMBANG JEPUN No.38-40, SURABAYA', 'mktisp@maspion.co.id', NULL, '1', '1', 4559, NULL, '1100', '224'),
('110083', 'ECOS BUDIMAN SEJATI, PT', '1', 'JL. CIPAGERAN NO. 33A RT5 RW4, JAWA BARAT', 'eb_sejaty@yahoo.com', 'b15fbfaac3776e5a2ad330fbf7976da7', '1', '1', 4560, NULL, '1100', '225'),
('110085', 'INDOFA UTAMA MULTI CORE, PT', '1', 'JL. MAWAR NO. 27 - 29, SURABAYA', 'juliawati@indofa.com', NULL, '1', '1', 4561, NULL, '1100', '226'),
('110087', 'INDOSCALE INSTRUMENT, CV', '1', 'JL. DAAN MOGOT RAYA NO. 119 RUKO AL, JAKARTA', 'indoscale@cbn.net.id', NULL, '1', '1', 4562, NULL, '1100', '227'),
('110089', 'INDERA CIPTA PRATAMA PERKASA, PT.', '1', 'JL. RAYA KALI RUNGKUT 5, SURABAYA', 'icipta@inderacipta.com', NULL, '1', '1', 4563, NULL, '1100', '228'),
('110091', 'INOSCCO SURYA PRATAMA, PT', '1', 'JL. BIBIS NO. 17, SURABAYA', 'inoscco@yahoo.co.id', NULL, '1', '1', 4564, NULL, '1100', '229'),
('110092', 'INTERDUTA WIRAKARYA SEJATI, PT', '1', 'JL. RAYA I GUSTI NGURAH RAI A2/18, SURABAYA', 'interduta@rad.net.id', NULL, '1', '1', 4565, NULL, '1100', '230'),
('110094', 'INTRA CIPTA RAYA, PT.', '1', 'JL. SEMARANG 94-124 Blk B-20, SURABAYA', 'intraya@intracipta.co.id', NULL, '1', '1', 4566, NULL, '1100', '231'),
('110096', 'INTRACO PENTA', '1', 'JL. DUMAR INDUSTRI Blk. A No. 7-8 M, MULYO SBY', 'renaldy@intracopenta.com', NULL, '1', '1', 4567, NULL, '1100', '232'),
('110098', 'JASA INDONESIA, ASS', '1', 'JL RAYA DARMO 24, SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4568, NULL, '1100', '233'),
('110100', 'JLITHENG WIJAYA', '1', 'JL LANGSEP No 8, TUBAN', 'cv.jlitheng_wijaya@yahoo.co.id', NULL, '1', '1', 4569, NULL, '1100', '234'),
('110103', 'KARYA MANDIRI, CV', '1', 'JL. JEMUR SARI II/16, SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4570, NULL, '1100', '235'),
('110105', 'MHE DEMATIC INDONESIA, PT.', '1', 'JL. RUNGKUT INDUSTRI I/3, SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4571, NULL, '1100', '236'),
('110107', 'KAWI ARIA PUTRA, PT', '1', 'JL. DUKUH KUPANG XIV/26, SURABAYA', 'ekosuprantoir@gmail.com', NULL, '1', '1', 4572, NULL, '1100', '237'),
('110184', 'TANJUNG ALAM, CV', '1', 'JL. RE. MARTADINATA (LAUT) III NO., PASURUAN', 'cv.tanjung.alam@gmail.com', NULL, '1', '1', 4573, NULL, '1100', '238'),
('110185', 'TIRA AUSTENITE Tbk, PT', '1', 'JL. PULO AYANG KAV. R1 KIP JAKARTA, JAKARTA TIMUR', 'sby@tiraaustenite.com', NULL, '1', '1', 4574, NULL, '1100', '244'),
('110186', 'TJAHJA, CV.', '1', 'JL. BASUKI RAHMAD NO. 218, TUBAN', 'hastomi.r@gmail.com', NULL, '1', '1', 4575, NULL, '1100', '245'),
('110188', 'TRAKINDO UTAMA, PT', '1', 'D/A JL. MT HARYONO No. 44, BOJONEGORO', 'kp_bjngoro@yahoo.com', NULL, '1', '1', 4576, NULL, '1100', '246'),
('110190', 'TRI SAKTI (TUBAN)', '1', 'DS.MONTONG SEKAR RT.16 RW.02 MONTON, TUBAN', 'trisakti_daph@yahoo.co.id', NULL, '1', '1', 4577, NULL, '1100', '247'),
('110191', 'TRI SARI KUMPUL, CV', '1', 'MARGOMULYO 66 F KAVLING 4, SURABAYA', 'trisarikumpul@gmail.com', NULL, '1', '1', 4578, NULL, '1100', '248'),
('110192', 'TRI ANUGERAH SEJATI, PT', '1', 'JL. RAYA DIPONEGORO NO. 67, SURABAYA', 'trias@sby.dnet.net.id', NULL, '1', '1', 4579, NULL, '1100', '249'),
('110193', 'TUFFIADI SEMESTA, PT', '1', 'JL. DEMAK MADYA NO. 1 - 3, SURABAYA', 'surabaya@tuffiadisemesta.co.id', NULL, '1', '1', 4580, NULL, '1100', '250'),
('110194', 'TUGU PRATAMA ASS, PT', '1', 'JL. GENTENGKALI 19-19.I., SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4581, NULL, '1100', '251'),
('110195', 'ULTRA DELTA MAJU, PT.', '1', 'GROGOL PERMAI BLOK E 7-8, JAKARTA BARAT', 'purnomo@udm.co.id', NULL, '1', '1', 4582, NULL, '1100', '252'),
('110197', 'UNITED INDO SURABAYA', '1', 'JL. BASUKI RAHMAT 14, SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4583, NULL, '1100', '253'),
('110198', 'UNITED TRACTORS, PT', '1', 'JL. RUNGKUT INDUSTRI III/46, SURABAYA', 'vendor@sg2.sggrp.com', NULL, '1', '1', 4584, NULL, '1100', '254'),
('110200', 'WAHYU MULYO SUKSES, PT', '1', 'JL. R.A. KARTINI BLD. NO. 236 BLK., GRESIK', 'wms_c9a@yahoo.co.id', 'd1e185c1f046029d77eefd4628899f8f', '1', '1', 4585, NULL, '1100', '255');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_safety_dasar_hukum`
--

CREATE TABLE `she_safety_dasar_hukum` (
  `ID` double NOT NULL,
  `AYAT` varchar(5) DEFAULT NULL,
  `PASAL` varchar(5) DEFAULT NULL,
  `DESKRIPSI` varchar(255) DEFAULT NULL,
  `CREATE_AT` datetime DEFAULT NULL,
  `CREATE_BY` varchar(20) DEFAULT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(20) DEFAULT NULL,
  `DELETE_AT` datetime DEFAULT NULL,
  `DELETE_BY` varchar(20) DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  `COMPANY` varchar(4) DEFAULT NULL,
  `PLANT` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_safety_dasar_hukum`
--

INSERT INTO `she_safety_dasar_hukum` (`ID`, `AYAT`, `PASAL`, `DESKRIPSI`, `CREATE_AT`, `CREATE_BY`, `UPDATE_AT`, `UPDATE_BY`, `DELETE_AT`, `DELETE_BY`, `NOTE`, `COMPANY`, `PLANT`) VALUES
(6, '1', '2', 'Pekerja harus menggunakan apd lengkap ketika di dalam pabrik\r\n', '2018-05-08 01:23:19', 'SYAHRIAL.RAMADANI', NULL, NULL, NULL, NULL, NULL, '5000', '5001'),
(4, '1', '1', 'Penyalahgunaan waktu istirahat di lapangan kerja', '2017-12-29 16:35:00', 'INDRA.NOFIANDI', '2018-02-28 05:12:45', 'INDRA.NOFIANDI', NULL, NULL, NULL, '5000', '5002'),
(5, '2', '2', 'ngopi ', '2018-02-28 05:13:06', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, NULL, '5000', '5002'),
(7, '1', '8', 'Pengaman area ketinggian\r\n', '2019-04-11 01:23:19', 'INDRA.NOFIANDI', NULL, NULL, NULL, NULL, NULL, '5000', '5001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_safety_report_category`
--

CREATE TABLE `she_safety_report_category` (
  `ID` bigint(20) NOT NULL,
  `KODE` varchar(5) DEFAULT NULL,
  `DESKRIPSI` varchar(200) NOT NULL,
  `CREATE_AT` datetime DEFAULT NULL,
  `CREATE_BY` varchar(20) DEFAULT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(20) DEFAULT NULL,
  `DELETE_AT` datetime DEFAULT NULL,
  `DELETE_BY` varchar(20) DEFAULT NULL,
  `COMPANY` varchar(4) DEFAULT NULL,
  `PLANT` varchar(4) DEFAULT NULL,
  `KODE_EN` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_safety_report_category`
--

INSERT INTO `she_safety_report_category` (`ID`, `KODE`, `DESKRIPSI`, `CREATE_AT`, `CREATE_BY`, `UPDATE_AT`, `UPDATE_BY`, `DELETE_AT`, `DELETE_BY`, `COMPANY`, `PLANT`, `KODE_EN`) VALUES
(1, 'TTA', 'Beroperasi/Bekerja tanpa otoritas', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(2, 'TTA', 'Memperbaiki peralatan dlm keadaan beroperasi', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(3, 'TTA', 'Mabuk,Bercanda,main-main dan sembrono', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(4, 'TTA', 'Tidak memakai APD', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(5, 'TTA', 'Memakai peralatan rusak', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(6, 'TTA', 'Terlalu memaksakan diri', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(7, 'TTA', 'Bekerja tidak sesuai kompetensi', '2017-07-24 10:54:01', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(8, 'TTA', 'Posisi mengangkat yang salah', '2017-07-24 10:54:02', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(9, 'TTA', 'Penggunaan peralatan yang tidak sesuai', '2017-07-24 10:54:02', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(10, 'TTA', 'Bekerja tidak sesuai prosedur', '2017-07-24 10:54:02', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(11, 'TTA', 'Bekerja diluar jam kerja', '2017-07-24 10:54:02', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(12, 'TTA', 'Bekerja tanpa ijin kerja', '2017-07-24 10:54:02', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UA'),
(13, 'KTA', 'Area kerja yang kotor', '2017-07-24 10:54:02', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(14, 'KTA', 'Pelindung/Pembatas tidak ada/layak', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(15, 'KTA', 'Mesin / peralatan yang rusak', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(16, 'KTA', 'Alat pelindung mesin yang terbuka', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(17, 'KTA', 'Ruang kerja sempit/terbatas', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(18, 'KTA', 'Sistem peringatan kurang/Indikator rusak', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(19, 'KTA', 'Paparan kebisingan', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(20, 'KTA', 'Paparan radiasi', '2017-07-24 10:54:03', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(21, 'KTA', 'Temperatur extrim', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(22, 'KTA', 'Penerangan kurang / tidak layak', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(23, 'KTA', 'Ventilasi tidak layak', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'UC'),
(24, 'FK', 'Kurangnya pengawasan/kepemimpinan', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(25, 'FK', 'Kurangnya suku cadang', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(26, 'FK', 'Kekurangan peralatan', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(27, 'FK', 'Kelengkapan kotak P3K/tandu dsb', '2017-07-24 10:54:04', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(28, 'FK', 'Salah pakai/Salah penggunaan', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(29, 'FK', 'Kelengkapan sarana kebakaran', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(30, 'FK', 'Bocor,longgar,hilang,retak atau pecah', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(31, 'FK', 'Panas, getaran yang berlebihan', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(32, 'FK', 'Kebersihan peralatan', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(33, 'FK', 'Kelayakan Crane, Forklift dll/sertifikat', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(34, 'FK', 'Tersedia MSDS Bahan B3', '2017-07-24 10:54:05', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(35, 'FK', 'Kondisi rambu norma K3', '2017-07-24 10:54:06', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF'),
(36, 'FK', 'Tersedia rambu norma K3', '2017-07-24 10:54:06', 'INIT', NULL, NULL, NULL, NULL, '5000', '5001', 'JF');

-- --------------------------------------------------------

--
-- Struktur dari tabel `she_safety_sio_group`
--

CREATE TABLE `she_safety_sio_group` (
  `id` int(11) NOT NULL,
  `NICKNAME` varchar(100) NOT NULL,
  `NAME` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `she_safety_sio_group`
--

INSERT INTO `she_safety_sio_group` (`id`, `NICKNAME`, `NAME`) VALUES
(1, 'OHC', 'OVER HEAD CRANE'),
(2, 'FK', 'FORKLIFT'),
(3, 'OTC', 'OVERHEAD TRAVELLING CRANE'),
(4, 'LK', 'LOKOMOTIF'),
(5, 'WL', 'WHEEL LOADER'),
(6, 'CC', 'CROWLER CRANE'),
(7, 'BR', 'BASIC RIGGER'),
(8, 'KTL', 'K3 TEKNISI LISTRIK'),
(9, 'SCF', 'TEKNISI K3 PERANCAH ( SCAFFOLDER )'),
(10, 'PKT-B', 'PENANGGULANGAN KEBAKARAN TINGKAT B'),
(11, 'PKT-C', 'PENANGGULANGAN KEBAKARAN TINGKAT C '),
(12, 'PKT-A', 'AHLI K3 PENANGGULANGAN KEBAKARAN TINGKAT A'),
(13, 'PKK', 'PETUGAS K3 KIMIA'),
(14, 'PP', 'PETUGAS P3K DI TEMPAT KERJA'),
(15, 'AKL', 'AHLI K3 LISTRIK'),
(16, 'AKU', 'AHLI K3 UMUM'),
(17, 'PTR', 'PENYALUR PETIR'),
(18, 'GND', 'GONDOLA'),
(19, 'GNST', 'GENSET'),
(20, 'INS-LTR', 'INSTALASI LISTRIK'),
(21, 'HDR', 'HYDRANT'),
(22, 'SL', 'SKID LOADER'),
(23, 'CCR', 'CROWLER CRANE'),
(24, 'ALRM', 'ALARM'),
(25, 'BC', 'BELT CONVEYOR'),
(26, 'BJ', 'BEJANA'),
(31, 'TEST', 'TESTING LISTRIK'),
(33, 'TEST', 'TESTING');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sios`
--

CREATE TABLE `sios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `badge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_lisensi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `masa_berlaku` date NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_kerja_txt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grup` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_certification` int(11) NOT NULL,
  `masa_awal` date NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sios`
--

INSERT INTO `sios` (`id`, `badge`, `unit_kerja`, `no_lisensi`, `kelas`, `masa_berlaku`, `foto`, `user_id`, `user_name`, `unit_kerja_txt`, `grup`, `company`, `plant`, `nama`, `time_certification`, `masa_awal`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, '00000754', '50000000', '83292398212', '02', '2021-12-31', 'IMG_9290.JPG', 435, 'Rizki Alfi Ramdhani', 'Secretariat of Commissioners', 'KTL', '5000', '5002', '00000517', 2, '2019-01-01', '2019-01-01', '2021-12-31', '2019-04-13 01:04:22', '2019-04-13 01:04:22'),
(3, '00000754', '50000000', '83292398212', '02', '2020-12-31', '213189_d1a180d6-43bb-4ac2-a78c-e14af7a885f0.jpg', 435, 'Rizki Alfi Ramdhani', 'President Directorate', 'KTL', '5000', '5002', 'SETIA PURWAKA, S.IP., MM.', 1, '2019-01-01', '2019-01-01', '2020-12-31', '2019-04-22 22:51:19', '2019-04-22 22:51:19'),
(4, '00000754', '50000000', '83292398212', '02', '2020-12-31', 'sio-conbloc.jpg', 435, 'Rizki Alfi Ramdhani', 'President Directorate', 'KTL', NULL, NULL, 'SETIA PURWAKA, S.IP., MM.', 1, '2019-01-01', '2019-01-01', '2020-12-31', '2019-04-23 00:41:49', '2019-04-23 00:41:49'),
(5, '00000518123', '123', '135261', '1', '2020-02-23', 'sertifikasi.jpg', 435, 'JULAIKAH, SE.', 'Unit of Training & Development', 'PKT-A', '2000', '5002', 'JULAIKAH, SE.', 1, '2019-02-23', '2019-02-23', '2020-02-23', '2019-04-23 00:48:29', '2019-04-23 21:02:44'),
(9, 'DWI SUKESTI, SE.', 'President Directorate', '14523236342625341', '2', '2019-05-27', 'SERTIFIKASI_KEMENAKER_RI.jpg', 436, 'Administrator', 'President Directorate', 'CC', '5000', '5002', 'DWI SUKESTI, SE.', 3, '2019-05-27', '2019-05-27', '2019-05-27', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tools`
--

CREATE TABLE `tools` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_buku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `equipment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_teknis` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_pengesahan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uji_ulang` date DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uk_kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uk_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_buku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_aktual` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uji_awal` date DEFAULT NULL,
  `time_certification` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tools`
--

INSERT INTO `tools` (`id`, `no_buku`, `equipment`, `kode`, `data_teknis`, `nomor_pengesahan`, `lokasi`, `uji_ulang`, `user_id`, `user_name`, `note`, `uk_kode`, `uk_text`, `company`, `plant`, `file_buku`, `file_aktual`, `uji_awal`, `time_certification`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(9, '13445', 'OVERHEAD TRAVELLING CRANE', 'OHC', 'Hanya test..', '987666', 'Gresik', '2019-01-21', 435, 'INDRA.NOFIANDI', 'null', '50000001', 'Internal Audit', '5000', '5002', 'download.jpeg', 'Logo_PENS.png', '2018-11-21', 1, '2019-01-01', '2019-12-31', '2019-04-13 01:02:42', '2019-04-24 02:07:14'),
(10, '13445', 'OVERHEAD TRAVELLING CRANE', 'OHC', 'Hanya test..', '987666', 'Gresik', '2019-01-21', 435, 'INDRA.NOFIANDI', 'null', '50000001', 'Internal Audit', '5000', '5002', 'Logo_PENS.png', 'IMG_9306.JPG', '2018-11-21', 1, '2019-01-01', '2019-12-31', '2019-04-22 22:49:02', '2019-04-22 22:49:02'),
(11, '2342546', 'CROWLER CRANE', 'CCR', 'Testing data tennis', '245623', 'Jalan Mawar melati', '2021-04-24', 435, 'Rizki Alfi RAMDHANI', 'null', '21102000', 'Bureau of Commercial & Mgmt System Audit', '2000', '5002', 'sertifikasi.jpg', 'ilustrasi-kecelakaan-kerja-ok_20150615_202217.jpg', '2019-04-24', 2, '2019-04-24', '2021-04-24', '2019-04-24 01:20:05', '2019-05-15 20:02:10'),
(12, '14045555555', 'AHLI K3 PENANGGULANGAN KEBAKARAN TINGKAT A', 'PKT-A', 'Data data data', '15331634', 'Jalan Lokasi', '2023-01-20', 435, 'null', 'null', 'undefined', 'Department of Research Development & QA', '2000', '5002', NULL, NULL, '2022-01-20', 1, '2022-01-20', '2023-01-20', '2019-04-24 01:22:02', '2019-04-24 02:10:40'),
(13, '45352', 'LOKOMOTIF', 'null', 'Tennis testtestes', '2325152', 'Gresik', '2029-01-20', 435, 'null', 'null', 'undefined', 'Bureau of Accounting & Finance Audit', '2000', '5002', NULL, NULL, '2026-01-20', 3, '2026-01-20', '2029-01-20', '2019-04-25 20:27:06', '2019-05-02 23:53:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `unsafe_details`
--

CREATE TABLE `unsafe_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_dokumen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_report` bigint(20) UNSIGNED NOT NULL,
  `temuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penyebab` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_real` date DEFAULT NULL,
  `foto_bef` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_aft` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'OPEN',
  `rekomendasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_temuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '5000',
  `plant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '5002',
  `tindak_lanjut` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `potensi_bahaya` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `uk_kode` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uk_text` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `unsafe_details`
--

INSERT INTO `unsafe_details` (`id`, `no_dokumen`, `id_report`, `temuan`, `lokasi_kode`, `lokasi_text`, `penyebab`, `tanggal_real`, `foto_bef`, `foto_aft`, `status`, `rekomendasi`, `user_id`, `user_name`, `jenis_temuan`, `company`, `plant`, `tindak_lanjut`, `note`, `potensi_bahaya`, `priority`, `uk_kode`, `uk_text`, `created_at`, `updated_at`) VALUES
(4, 'R/538/002/4', 5, 'testing', '1234', 'Jalanjalan', 'sebab', NULL, 'spider-man-wallpaper-miles-morales-inspirational-wallpaper-iron-man-avengers-infinity-war-marvel-ics-of-spider-man-wallpaper-miles-morales.jpg', NULL, 'CLOSE', 'rekoemagfna', 435, NULL, 'UA', NULL, NULL, 'sgag', NULL, 'asg', 2, '3414', 'UKUK', '2019-05-05 21:56:37', '2019-05-05 21:56:37'),
(5, 'R/538/002/5', 7, 'testing', '1234', 'Jalanjalan', 'sebab', NULL, 'spider-man-wallpaper-miles-morales-inspirational-wallpaper-iron-man-avengers-infinity-war-marvel-ics-of-spider-man-wallpaper-miles-morales.jpg', NULL, 'CLOSE', 'rekoemagfna', 435, NULL, 'UA', NULL, NULL, 'sgag', NULL, 'asg', 2, '3414', 'UKUK', '2019-05-05 21:57:26', '2019-05-05 21:57:26'),
(6, 'R/538/002/6', 7, 'testing', '1234', 'Jalanjalan', 'sebab', NULL, 'spider-man-wallpaper-miles-morales-inspirational-wallpaper-iron-man-avengers-infinity-war-marvel-ics-of-spider-man-wallpaper-miles-morales.jpg', NULL, 'OPEN', 'rekoemagfna', 435, NULL, 'UC', '5000', '5002', 'sgag', NULL, 'asg', 2, '3414', 'UKUK', '2019-05-05 22:52:26', '2019-05-05 22:52:26'),
(20, 'R/538/002/20', 22, 'testing', '1234', 'Jalanjalan', 'sebab', NULL, '27-Zulkarnaen-jetty5.jpg', NULL, 'OPEN', 'rekoemagfna', 435, NULL, 'UA', '5000', '5002', 'sgag', NULL, 'asg', 2, '3414', 'UKUK', '2019-05-06 08:06:36', '2019-05-06 08:06:36'),
(21, 'R/538/002/21', 22, 'testing', '1234', 'Jalanjalan', 'sebab', NULL, '27-Zulkarnaen-jetty5.jpg', NULL, 'OPEN', 'rekoemagfna', 435, NULL, 'UA', '5000', '5002', 'sgag', NULL, 'asg', 2, '3414', 'UKUK', '2019-05-06 08:07:54', '2019-05-06 08:07:54'),
(22, 'R/538/002/22', 24, 'testing', '1234', 'Jalanjalan', 'sebab', NULL, 'dasar-dasar-mengenai-kecelakaan-kerja.jpg', NULL, 'CLOSE', 'rekoemagfna', 435, NULL, 'UA', '5000', '5002', 'sgag', NULL, 'asg', 2, '3414', 'UKUK', '2019-05-06 08:25:30', '2019-05-06 08:39:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uk_kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_badge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost_center` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cc_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_k3` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `email`, `email_verified_at`, `username`, `password`, `name`, `company`, `plant`, `ad`, `unit_kerja`, `uk_kode`, `no_badge`, `cost_center`, `cc_text`, `position`, `pos_text`, `is_k3`, `remember_token`, `created_at`, `updated_at`) VALUES
(430, 18, 'IVONDRA.CAHYO@SEMENINDONESIA.COM', NULL, 'IVONDRA.CAHYO', '$P$BQpyXVOTwVPYrd/PkRKx.nBkwYfIuE0', 'IVONDRA CAHYO FEBRIANTO', '7000', '5001', '1', 'Group of FM 3-4 Preventive', '50031969', '00001969', '7203323906', 'SEKSI PEMELIHARAAN MESIN FM TBN34', '50', 'Associate', 0, NULL, '2018-06-09 01:58:00', '2018-06-09 01:58:00'),
(431, 18, 'FAHRUDIN@SEMENINDONESIA.COM', NULL, 'FAHRUDIN', '$P$B5niO66DF7vifEwkNiCy6ZagE5XaqE0', 'FAHRUDIN', '7000', '5001', '1', 'Group of Elect & Instr Workshop Planner', '50046630', '00001183', '7203412907', 'SEKSI BENGKEL LISTRIK & INSTRUMENTASI', '40', 'Supervisor', 0, NULL, '2018-06-09 01:58:00', '2018-06-09 01:58:00'),
(432, 18, 'SAIFUL.ARIFIN@SEMENINDONESIA.COM', NULL, 'SAIFUL.ARIFIN', '$P$BWCfeUce0eoFhm7XozNOnLlZIlUFPC.', 'SAIFUL ARIFIN', '7000', '5001', '1', 'Group of Workshop Actuator', '50046548', '00000827', '7203411906', 'SEKSI PEMELIHARAAN UTILITAS', '40', 'Supervisor', 0, NULL, '2018-06-09 01:58:00', '2018-06-09 01:58:00'),
(433, 20, 'RACHMAT.HARIYOKO@SEMENINDONESIA.COM', NULL, 'RACHMAT.HARIYOKO', 'semenindonesia', 'RACHMAT HARIJOKO', '7000', '5002', '1', 'Group of Plant Cleaning', '50046501', '00000551', '7103431914', 'SEKSI KESELAMATAN KERJA & KEBERSIHAN GSK', '40', 'Supervisor', 0, NULL, '2018-06-09 01:58:00', '2018-06-09 01:58:00'),
(434, 18, 'SAIFUL.FATHA@SEMENINDONESIA.COM', NULL, 'SAIFUL.FATHA', 'semenindonesia', 'SAIFUL FATHA', '7000', '5001', '1', 'Group of Mechanical Inspection', '50046572', '00001215', '7203422901', 'SEKSI INSPEKSI PEMELIHARAAN', '40', 'Supervisor', 0, NULL, '2018-06-09 01:58:00', '2018-06-09 01:58:00'),
(435, 1, 'rizkialfi@semenindonesia.com', NULL, 'RIZKI.ALFI', '$2y$10$X6I1/G9NmBanFiNqbx6S9.1pMWgShM2HT0QOb/AGuuiE4Ss0XLvle', 'Rizki Alfi Ramdhani', 'PT. Semen Indonesia', '5002', '1', 'Department of Strategic ICT', '50045222', '2103161039', '2001110000', 'DEPARTEMEN ICT STRATEGIS', '20', 'Senior Programmer', 0, NULL, '2019-04-13 00:31:43', '2019-04-13 00:31:43'),
(436, 1, 'zen@semenindonesia.com', NULL, 'zen', '$2y$10$BeOoZIdgl.n9MJ5DIm244.bpawor4ytRxwG6jdafZDeIaQqmYAQHS', 'Administrator', 'PT. Semen Indonesia', '5000', '1', 'Administrator', '99999999', '99999999', '99999999', 'Adminstrator', '99', 'Administrator', 1, NULL, '2019-04-13 00:32:51', '2019-04-13 00:32:51');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `accident_reports`
--
ALTER TABLE `accident_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accident_reports_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `apd_master_category`
--
ALTER TABLE `apd_master_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `daily_reports`
--
ALTER TABLE `daily_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daily_reports_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `daily_report_k3_details`
--
ALTER TABLE `daily_report_k3_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daily_report_k3_details_id_unsafe_report_foreign` (`id_unsafe_report`),
  ADD KEY `daily_report_k3_details_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `daily_report_k3_s`
--
ALTER TABLE `daily_report_k3_s`
  ADD PRIMARY KEY (`id`),
  ADD KEY `daily_report_k3_s_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `daily_section_activity`
--
ALTER TABLE `daily_section_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fire_report`
--
ALTER TABLE `fire_report`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fire_report_image`
--
ALTER TABLE `fire_report_image`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_alarm`
--
ALTER TABLE `inspection_alarm`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_apar`
--
ALTER TABLE `inspection_apar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_area`
--
ALTER TABLE `inspection_area`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_gas`
--
ALTER TABLE `inspection_gas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_hydrant`
--
ALTER TABLE `inspection_hydrant`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_pickup`
--
ALTER TABLE `inspection_pickup`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `inspection_pmk`
--
ALTER TABLE `inspection_pmk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `list_file_uploads`
--
ALTER TABLE `list_file_uploads`
  ADD PRIMARY KEY (`id_upload_unsafe`),
  ADD KEY `list_file_uploads_id_unsafe_report_detail_foreign` (`id_unsafe_report_detail`),
  ADD KEY `list_file_uploads_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `list_gas`
--
ALTER TABLE `list_gas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `list_vehicle`
--
ALTER TABLE `list_vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lokasi_alarm`
--
ALTER TABLE `lokasi_alarm`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lokasi_apar`
--
ALTER TABLE `lokasi_apar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lokasi_hydrant`
--
ALTER TABLE `lokasi_hydrant`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `lotto`
--
ALTER TABLE `lotto`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_apd`
--
ALTER TABLE `master_apd`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `order_apd`
--
ALTER TABLE `order_apd`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `parameter_pengecekan_alarm`
--
ALTER TABLE `parameter_pengecekan_alarm`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pinjam_apd`
--
ALTER TABLE `pinjam_apd`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `planning_plants`
--
ALTER TABLE `planning_plants`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `plant`
--
ALTER TABLE `plant`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pplant`
--
ALTER TABLE `pplant`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `release`
--
ALTER TABLE `release`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `she_apd_master_apd`
--
ALTER TABLE `she_apd_master_apd`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `she_master_funcloc`
--
ALTER TABLE `she_master_funcloc`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `she_master_inspection_area`
--
ALTER TABLE `she_master_inspection_area`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `she_master_pegawai`
--
ALTER TABLE `she_master_pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `she_master_unit_kerja`
--
ALTER TABLE `she_master_unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `she_master_vendor`
--
ALTER TABLE `she_master_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `she_safety_sio_group`
--
ALTER TABLE `she_safety_sio_group`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sios`
--
ALTER TABLE `sios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sios_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `tools`
--
ALTER TABLE `tools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tools_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `unsafe_details`
--
ALTER TABLE `unsafe_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unsafe_details_id_report_foreign` (`id_report`),
  ADD KEY `unsafe_details_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `accident_reports`
--
ALTER TABLE `accident_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `apd_master_category`
--
ALTER TABLE `apd_master_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=911;

--
-- AUTO_INCREMENT untuk tabel `daily_reports`
--
ALTER TABLE `daily_reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `daily_report_k3_details`
--
ALTER TABLE `daily_report_k3_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `daily_report_k3_s`
--
ALTER TABLE `daily_report_k3_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `daily_section_activity`
--
ALTER TABLE `daily_section_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `fire_report`
--
ALTER TABLE `fire_report`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `fire_report_image`
--
ALTER TABLE `fire_report_image`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `inspection_alarm`
--
ALTER TABLE `inspection_alarm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `inspection_apar`
--
ALTER TABLE `inspection_apar`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `inspection_area`
--
ALTER TABLE `inspection_area`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `inspection_gas`
--
ALTER TABLE `inspection_gas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `inspection_hydrant`
--
ALTER TABLE `inspection_hydrant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `inspection_pickup`
--
ALTER TABLE `inspection_pickup`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `inspection_pmk`
--
ALTER TABLE `inspection_pmk`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `list_file_uploads`
--
ALTER TABLE `list_file_uploads`
  MODIFY `id_upload_unsafe` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `list_gas`
--
ALTER TABLE `list_gas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `list_vehicle`
--
ALTER TABLE `list_vehicle`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `lokasi_alarm`
--
ALTER TABLE `lokasi_alarm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `lokasi_apar`
--
ALTER TABLE `lokasi_apar`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `lokasi_hydrant`
--
ALTER TABLE `lokasi_hydrant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `lotto`
--
ALTER TABLE `lotto`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `master_apd`
--
ALTER TABLE `master_apd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3332;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `order_apd`
--
ALTER TABLE `order_apd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT untuk tabel `parameter_pengecekan_alarm`
--
ALTER TABLE `parameter_pengecekan_alarm`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `pinjam_apd`
--
ALTER TABLE `pinjam_apd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `planning_plants`
--
ALTER TABLE `planning_plants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `plant`
--
ALTER TABLE `plant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pplant`
--
ALTER TABLE `pplant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `release`
--
ALTER TABLE `release`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `she_apd_master_apd`
--
ALTER TABLE `she_apd_master_apd`
  MODIFY `ID` double NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2883;

--
-- AUTO_INCREMENT untuk tabel `she_master_funcloc`
--
ALTER TABLE `she_master_funcloc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=501;

--
-- AUTO_INCREMENT untuk tabel `she_master_inspection_area`
--
ALTER TABLE `she_master_inspection_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `she_master_pegawai`
--
ALTER TABLE `she_master_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1001;

--
-- AUTO_INCREMENT untuk tabel `she_master_unit_kerja`
--
ALTER TABLE `she_master_unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=501;

--
-- AUTO_INCREMENT untuk tabel `she_master_vendor`
--
ALTER TABLE `she_master_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4586;

--
-- AUTO_INCREMENT untuk tabel `she_safety_sio_group`
--
ALTER TABLE `she_safety_sio_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `sios`
--
ALTER TABLE `sios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tools`
--
ALTER TABLE `tools`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `unsafe_details`
--
ALTER TABLE `unsafe_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=437;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `accident_reports`
--
ALTER TABLE `accident_reports`
  ADD CONSTRAINT `accident_reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `daily_reports`
--
ALTER TABLE `daily_reports`
  ADD CONSTRAINT `daily_reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `daily_report_k3_details`
--
ALTER TABLE `daily_report_k3_details`
  ADD CONSTRAINT `daily_report_k3_details_id_unsafe_report_foreign` FOREIGN KEY (`id_unsafe_report`) REFERENCES `daily_report_k3_s` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `daily_report_k3_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `daily_report_k3_s`
--
ALTER TABLE `daily_report_k3_s`
  ADD CONSTRAINT `daily_report_k3_s_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `list_file_uploads`
--
ALTER TABLE `list_file_uploads`
  ADD CONSTRAINT `list_file_uploads_id_unsafe_report_detail_foreign` FOREIGN KEY (`id_unsafe_report_detail`) REFERENCES `daily_report_k3_details` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `list_file_uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sios`
--
ALTER TABLE `sios`
  ADD CONSTRAINT `sios_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tools`
--
ALTER TABLE `tools`
  ADD CONSTRAINT `tools_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `unsafe_details`
--
ALTER TABLE `unsafe_details`
  ADD CONSTRAINT `unsafe_details_id_report_foreign` FOREIGN KEY (`id_report`) REFERENCES `daily_reports` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `unsafe_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
