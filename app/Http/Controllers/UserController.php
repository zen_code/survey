<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function index()
     {
        $roleModerator=1; $roleSurveyor=2;;
        $jumlahModerator = User::where('role_id', 'LIKE', '%' .$roleModerator. '%')->count();
        $jumlahSurveyor = User::where('role_id', 'LIKE', '%' .$roleSurveyor. '%')->count();
        $users = User::all();

        $params=[
            'users'=>$users,
            'jumlahModerator'=>$jumlahModerator,
            'jumlahSurveyor'=>$jumlahSurveyor,
        ];

        return view('user/index', $params);
    }

    public function form(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = User::find($id);
        }else{
            $data = new User();
        }
        $params =[
            'title' =>'Manajemen User',
            'data' =>$data,
        ];
        return view('user/form', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = User::find($id);
            $data->password = $data->password;
        }else{
            $data = new User();
            $data->password = bcrypt($request->password);
        }

        $data->name = $request->name;
        $data->role_id = $request->role_id;
        $data->email = $request->email;


        try{
            $data->save();
            return "
            <div class='alert alert-success'>User Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>User Failed! User not saved!</div>";
        }
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            User::find($id)->delete();
            return "
            <div class='alert alert-success'>User Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! User not removed!</div>";
        }
    }
}
