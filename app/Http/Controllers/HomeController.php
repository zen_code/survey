<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Survey\Survey;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $jumlahUser = User::count();
        $params = [
            'jumlahUser' => $jumlahUser
        ];

        return view('home', $params);
    }

    public function jenjang()
    {
        $SD='SD';
        $SMP='SMP';
        $SMA='SMA';
        $Diploma='Diploma';
        $Sarjana='Sarjana';
        $jenjangSD = Survey::where('pendidikan', 'LIKE', '%' .$SD. '%')->count();
        $jenjangSMP = Survey::where('pendidikan', 'LIKE', '%' .$SMP. '%')->count();
        $jenjangSMA = Survey::where('pendidikan', 'LIKE', '%' .$SMA. '%')->count();
        $jenjangDiploma = Survey::where('pendidikan', 'LIKE', '%' .$Diploma. '%')->count();
        $jenjangSarjana = Survey::where('pendidikan', 'LIKE', '%' .$Sarjana. '%')->count();

        $jenjang=[$jenjangSD,$jenjangSMP,$jenjangSMA,$jenjangDiploma,$jenjangSarjana];

        $params = [
            'jenjang' => $jenjang,
        ];
        return response()->json($params);
    }

}
