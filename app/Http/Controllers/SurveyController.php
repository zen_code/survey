<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey\Survey;

class SurveyController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hasilSurvey = Survey::all();

        $params=[
             'hasilSurvey'=>$hasilSurvey,
             'title'=>'Survey'
        ];

        return view('survey/index', $params);
    }

    public function form(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = Survey::find($id);
        }else{
            $data = new Survey();
        }
        $params =[
            'title' =>'Survey',
            'data' =>$data,
        ];
        return view('survey/form', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = Survey::find($id);
            $data->status = $data->status;
        }else{
            $data = new Survey();
            $data->status = '0';
        }

        $data->nama = $request->nama;
        $data->nik = $request->nik;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->pendidikan = $request->pendidikan;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>User Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>User Failed! User not saved!</div>";
        }
    }

    public function verifikasi(Request $request)
    {
      $id = intval($request->input('id',0));

      try{
          Survey::where('id', $id)->update(['status' => '1']);
          return "
          <div class='alert alert-success'>Verifikasi Success!</div>
          <script> scrollToTop(); reload(1500); </script>";
      }catch(\Exception $ex){
          return "<div class='alert alert-danger'>Remove Failed! User not removed!</div>";
      }
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            Survey::find($id)->delete();
            return "
            <div class='alert alert-success'>User Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! User not removed!</div>";
        }
    }


}
