<?php

namespace App\Models\Survey;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'data_survey';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
