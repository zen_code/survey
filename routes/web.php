<?php

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/jenjang', 'HomeController@jenjang')->name('jenjang');

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'UserController@index');
    Route::post('/add', 'UserController@form');
    Route::post('/save', 'UserController@save');
    Route::post('/delete', 'UserController@delete');
});

Route::group(['prefix' => 'dataSurvey'], function () {
    Route::get('/', 'SurveyController@index');
    Route::post('/add', 'SurveyController@form');
    Route::post('/save', 'SurveyController@save');
    Route::post('/delete', 'SurveyController@delete');
    Route::post('/verifikasi', 'SurveyController@verifikasi');
});
