<?php

use Illuminate\Http\Request;

Route::post('login', 'API\ApiController@login');
Route::post('register', 'API\ApiController@register');
