// Ajax
$(document).ready(function () {
    $(document).on('click', '.add-modal', function () {
        $('#footer_action_button').text("Add");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('add');
        $('.modal-title').text('Add New Data');
        $('.deleteContent').hide();
        $('.form-horizontal').show();
        // $('#fid').val($(this).data('id'));
        // $('#nn').val($(this).data('nickname'));
        // $('#n').val($(this).data('name'));
        $('#myModal').modal('show');
    });
    $(document).on('click', '.edit-modal', function () {
        $('#footer_action_button').text("Update");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Edit');
        $('.deleteContent').hide();
        $('.form-horizontal').show();
        $('#fid').val($(this).data('id'));
        $('#nn').val($(this).data('nickname'));
        $('#n').val($(this).data('name'));
        $('#myModal').modal('show');
    });
    $(document).on('click', '.delete-modal', function () {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete');
        $('.did').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.dname').html($(this).data('name'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.edit', function () {
        $.ajax({
            type: 'post',
            url: 'url("siogroup/editItem")',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#fid").val(),
                'nickname': $('#nn').val(),
                'name': $('#n').val()
            },
            success: function (data) {
                $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.nickname + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-nickname='" + data.nickname + "'data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-nickname='" + data.nickname + "'data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
            }
        });
    });
    $('.modal-footer').on('click', '.add', function () {
        $.ajax({
            type: 'post',
            url: '/addItem',
            data: {
                '_token': $('input[name=_token]').val(),
                'nickname': $('input[nickname=nickname]').val(),
                'name': $('input[name=name]').val()
            },
            success: function (data) {
                if ((data.errors)) {
                    $('.error').removeClass('hidden');
                    $('.error').text(data.errors.nickname);
                    $('.error').text(data.errors.name);
                } else {
                    $('.error').remove();
                    $('#table').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.nickname + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-nickname='" + data.nickname + "'data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-nickname='" + data.nickname + "'data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                }
            },
        });
        $('#nickname').val('');
        $('#name').val('');
    });
    // $("#add").click(function () {
    //     $.ajax({
    //         type: 'post',
    //         url: '/addItem',
    //         data: {
    //             '_token': $('input[name=_token]').val(),
    //             'nickname': $('input[nickname=nickname]').val(),
    //             'name': $('input[name=name]').val()
    //         },
    //         success: function (data) {
    //             if ((data.errors)) {
    //                 $('.error').removeClass('hidden');
    //                 $('.error').text(data.errors.nickname);
    //                 $('.error').text(data.errors.name);
    //             } else {
    //                 $('.error').remove();
    //                 $('#table').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.nickname + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-nickname='" + data.nickname + "'data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-nickname='" + data.nickname + "'data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
    //             }
    //         },
    //     });
    //     $('#nickname').val('');
    //     $('#name').val('');
    // });
    $('.modal-footer').on('click', '.delete', function () {
        $.ajax({
            type: 'post',
            url: 'url("/siogroup/deleteItem")',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $('.fid').text()
            },
            success: function (data) {
                $('.item' + $('.fid').text()).remove();
            }
        });
    });
});