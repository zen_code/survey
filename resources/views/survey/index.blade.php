@extends('layouts.app')

@section('content')
<title>Hasil Survey</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="">
                                    <h5>List Hasil Survey</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/dataSurvey/add" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%"  id="table-dataSurvey">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Nama</th>
                                                        <th>NIK</th>
                                                        <th>Tanggal Lahir</th>
                                                        <th>Jenis Kelamin</th>
                                                        <th>Pendidikan</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($hasilSurvey as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td class="text-center">{{ $data->nama }}</td>
                                                        <td class="text-center">{{ $data->nik }}</td>
                                                        <td class="text-center">{{ $data->tanggal_lahir }}</td>
                                                        <td class="text-center">{{ $data->jenis_kelamin }}</td>
                                                        <td class="text-center">{{ $data->pendidikan }}</td>
                                                          <td>
                                                            @if($data->status == "0")
                                                              <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                  Belum di Verifikasi
                                                              </button>
                                                            @else
                                                              <button type="button" class="btn btn-icon icon-left btn-success">
                                                                  Verifikasi
                                                              </button>
                                                            @endif
                                                          </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                    onclick="loadModal(this)" title="" target="/dataSurvey/add" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                    onclick="deleteSurvey({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                                @if(Auth::user()->role_id==1)
                                                                  @if($data->status == "0")
                                                                  <button type="button" class="btn btn-icon icon-left btn-success"
                                                                      onclick="verifikasiSurvey({{$data->id}})">
                                                                      Verifikasi
                                                                  </button>
                                                                  @endif
                                                                @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteSurvey(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/dataSurvey/delete", data, "#modal-output");
            })
        }

        function verifikasiSurvey(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to verifikasi this data ?", function () {
               ajaxTransfer("/dataSurvey/verifikasi", data, "#modal-output");
            })
        }

        var table = $('#table-dataSurvey');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
