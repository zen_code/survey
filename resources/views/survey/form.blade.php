<link rel="stylesheet" href="{{asset('assets/modules/bootstrap-toggle/css/bootstrap4-toggle.min.css')}}">
<script src="{{asset('assets/')}}/modules/bootstrap-toggle/js/bootstrap4-toggle.min.js"></script>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Hasil Survey</h6>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
            <hr>
            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-inbox"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control daterange-cus" name="nama" value="{{$data->nama}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>NIK</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-inbox"></i>
                                </div>
                            </div>
                            <input type="number" min="0" class="form-control daterange-cus" name="nik" value="{{$data->nik}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="tanggal_lahir" value="{{ $data->tanggal_lahir}}" required="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="radio radio-info" style="margin:6px 0">
                              <input type="radio" name="jenis_kelamin" <?php if($data->jenis_kelamin =='Laki-Laki') {echo "checked";}?> value="Laki-Laki">
                              <label for="casing1">Laki-Laki</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="radio radio-info" style="margin:6px 0">
                              <input type="radio" name="jenis_kelamin" <?php if($data->jenis_kelamin =='Perempuan') {echo "checked";}?> value="Perempuan">
                              <label for="casing2">Perempuan</label>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label>Pendidikan</label>
                      <select name="pendidikan" class="form-control">
                          <option <?php if($data->pendidikan =='SD') {echo "selected";}?> value="SD">SD</option>
                          <option <?php if($data->pendidikan =='SMP') {echo "selected";}?> value="SMP">SMP</option>
                          <option <?php if($data->pendidikan =='SMA') {echo "selected";}?> value="SMA">SMA</option>
                          <option <?php if($data->pendidikan =='Diploma') {echo "selected";}?> value="Diploma">Diploma</option>
                          <option <?php if($data->pendidikan =='Sarjana') {echo "selected";}?> value="Sarjana">Sarjana</option>
                      </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dataSurvey/save', data, '#result-form-konten');
        })
    })
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
