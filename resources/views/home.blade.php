@extends('layouts.app')

@section('content')
<title>Data Survey</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Grafik Berdasarkan Tahun</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Grafik Berdasarkan Pendidikan</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                  <div class="" style="margin-top:50px">
                                      <div class="table-responsive">
                                          <div id="" style="min-width: 310px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                  <div class="" style="margin-top:50px">
                                      <div class="table-responsive">
                                          <div id="jenjangPendidikan" style="min-width: 310px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('http://localhost/survey/jenjang')}}",
                dataType: 'json',
                success: function (data) {
                    getJenjang(data);
                }
            });
        });

        function getJenjang(data) {
            Highcharts.chart('jenjangPendidikan', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['SD', 'SMP', 'SMA', 'Diploma', 'Sarjana'],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b> {point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Grafik Berdasarkan Jenjang Pendidikan',
                    data: data.jenjang,
                    color: 'green'
                }]
            });
        }


    </script>
@endsection

@endsection
