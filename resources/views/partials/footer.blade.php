<!-- General JS Scripts -->
<!-- <script src="{{asset('assets/')}}/modules/jquery.min.js"></script>
<script src="{{asset('assets/')}}/modules/popper.js"></script>
<script src="{{asset('assets/')}}/modules/tooltip.js"></script>
<script src="{{asset('assets/')}}/modules/bootstrap/js/bootstrap.min.js"></script>
<script src="{{asset('assets/')}}/modules/nicescroll/jquery.nicescroll.min.js"></script>
<script src="{{asset('assets/')}}/modules/moment.min.js"></script>
<script src="{{asset('assets/')}}/js/stisla.js"></script> -->

<!-- JS Libraies -->
<script src="{{asset('assets/')}}/modules/jquery.sparkline.min.js"></script>
<script src="{{asset('assets/')}}/modules/chart.min.js"></script>
<script src="{{asset('assets/')}}/modules/owlcarousel2/dist/owl.carousel.min.js"></script>
<script src="{{asset('assets/')}}/modules/summernote/summernote-bs4.js"></script>
<script src="{{asset('assets/')}}/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

<!-- <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script> -->
<script src="{{asset('assets/')}}/modules/select2/dist/js/select2.min.js"></script>


<script src="{{asset('assets/')}}/modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('assets/')}}/modules/datatables.net-select/js/dataTables.select.min.js"></script>





<!-- Page Specific JS File -->
<script src="{{asset('assets/')}}/js/page/index.js"></script>
<!-- <script src="{{asset('assets/')}}/js/page/components-statistic.js"></script> -->

<!-- Template JS File -->
<script src="{{asset('assets/')}}/js/scripts.js"></script>
<script src="{{asset('assets/')}}/js/custom.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<!-- <script src="{{asset('assets/')}}/js/ajax.js"></script> -->
@yield('scripts')
