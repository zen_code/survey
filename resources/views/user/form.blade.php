<link rel="stylesheet" href="{{asset('assets/modules/bootstrap-toggle/css/bootstrap4-toggle.min.css')}}">
<script src="{{asset('assets/')}}/modules/bootstrap-toggle/js/bootstrap4-toggle.min.js"></script>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Users</h6>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
            <hr>
            <div class="row go_form">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role_id" class="form-control">
                            <option <?php if($data->role_id =='1') {echo "selected";}?> value="1">Moderator</option>
                            <option <?php if($data->role_id =='2') {echo "selected";}?> value="2">Surveyor</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Name</label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <div class="input-group-text">
                                  <i class="fas fa-user"></i>
                              </div>
                          </div>
                          <input type="text" class="form-control daterange-cus" name="name" value="{{$data->name}}">
                      </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-inbox"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control daterange-cus" name="email" value="{{$data->email}}">
                        </div>
                    </div>
                </div>
                @if($data->password == null)
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-lock"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control daterange-cus" name="password" value="{{$data->password}}">
                        </div>
                    </div>
                </div>
                @endif
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/users/save', data, '#result-form-konten');
        })
    })
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
