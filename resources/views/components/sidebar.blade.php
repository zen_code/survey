<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}">DATA SURVEY</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-th-large"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">Master Data</li>
            <li class="dropdown">
                <a href="" class="nav-link has-dropdown"><i class="fas fa-inbox"></i> <span>Master Data</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ request()->is('users') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/users') }}">User</a>
                    </li>
                </ul>
            </li>

            <li class="menu-header">Data Survey</li>
            <li class="dropdown">
                <li class="{{ request()->is('dataSurvey') ? 'active' : '' }}">
                    <a href="{{ url('/dataSurvey') }}" class="nav-link"><i class="fas fa-medkit"></i><span>Data Survey</span></a>
                </li>
            </li>
        </ul>
    </aside>
</div>
